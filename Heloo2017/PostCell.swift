//
//  PostCell.swift
//  Heloo2017
//
//  Created by Константин on 25.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Canvas

fileprivate var isMenuOpened = false

class PostCell: UITableViewCell {
    
    private var _normalShadowedFrame: CGRect!
    fileprivate var normalShadowedFrame: CGRect {
        get {
            if _normalShadowedFrame == nil {
                _normalShadowedFrame = mainShadowedView?.frame ?? .zero
            }
            return _normalShadowedFrame
        }
    }
    
    @IBOutlet weak var mainShadowedView: ShadowedView? {
        didSet {
            _ = normalShadowedFrame
        }
    }
    
    @IBOutlet weak var mainShadowedLeading: NSLayoutConstraint!
    @IBOutlet weak var mainShadowedTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var imageBottomOffset: NSLayoutConstraint?
    @IBOutlet weak var openUserButton: UserButton?
    @IBOutlet weak var action_label: UILabel?
    @IBOutlet weak var post_text: UILabel?
    @IBOutlet var post_text_height_max: NSLayoutConstraint?
    @IBOutlet weak var post_title: UILabel?
    @IBOutlet weak var post_image: UIImageView?
    @IBOutlet weak var post_image_height: NSLayoutConstraint?
    @IBOutlet weak var post_image_button: PostImageButton?
    
    @IBOutlet weak var date_label_view: UIView?
    @IBOutlet weak var date_label: UILabel?
    @IBOutlet weak var noImg_date_label: UILabel?
    
    
    @IBOutlet weak var small_top_icon: UIImageView?
    @IBOutlet weak var big_top_icon: UIImageView?
    @IBOutlet weak var big_top_label: UILabel?
    @IBOutlet weak var small_top_label: UILabel?
    
    @IBOutlet weak var accept_button: AcceptButton?
    @IBOutlet weak var offer_favorite_button: OfferFavoriteButton?
    @IBOutlet weak var comments_button: ShowCommentsButton?
    @IBOutlet weak var likes_button: UIButton?
    
    @IBOutlet weak var favorites_button: FavoriteButton?
    
    @IBOutlet weak var animationView: CSAnimationView?
    
    @IBOutlet weak var gradient_shadow: UIImageView?
    
    @IBOutlet weak var read_more_button: ExpandingButton?
    @IBOutlet weak var read_more_button_height: NSLayoutConstraint?
    
    @IBOutlet weak var placeHolderLabel: UILabel!
    fileprivate var menuView: MenuView?
    
    @IBOutlet weak var loadingLabel: UILabel?
    
    @IBOutlet weak var weekdaysStack: WeekdaysStack?
    
    func setCanDelete(_ value: Bool, block: (()->Void)?=nil) {
        canDelete = value
        deleteTappedBlock = block
    }
    
    func setCanComplain(_ value: Bool, block: (()->Void)?=nil) {
        canComplain = value
        complainTappedBlock = block
    }
    
    func setCanEdit(_ value: Bool, block: (()->Void)?=nil) {
        canEdit = false
        editTappedBlock = block
    }
    
    fileprivate var deleteTappedBlock: (()->Void)?=nil
    fileprivate var complainTappedBlock: (()->Void)?=nil
    fileprivate var editTappedBlock: (()->Void)?=nil
    
    fileprivate var canDelete = false {
        didSet {
            async {
                self.addPanGestureIfNeeded()
            }
            
        }
    }
    fileprivate var canEdit = false {
        didSet {
            async {
                self.addPanGestureIfNeeded()
            }
        }
    }
    fileprivate var canComplain = false {
        didSet {
            async {
                self.addPanGestureIfNeeded()
            }
        }
    }
    
    
    override var autoresizingMask: UIViewAutoresizing {
        get {
            return super.autoresizingMask
        } set {
            super.autoresizingMask = newValue
            updateMenu()
        }
    }
    
    override func prepareForReuse() {
        hideMenu(forced: true)
        super.prepareForReuse()
    }
    
    override func awakeFromNib() {
        self.selectionStyle = .none
    }
    
    private var pan: UIPanGestureRecognizer!
    
    private func addPanGestureIfNeeded() {
        
        menuView?.removeFromSuperview()

        //MARK: edit action ignored
            guard canDelete || canComplain else {
            menuView = nil
            return
        }
        
        guard mainShadowedView != nil else {  return }
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        var targetFrame = self.mainShadowedView!.frame.offsetBy(dx: self.contentWidth, dy: 0)
        targetFrame.size.width = 123
        
        self.menuView = MenuView.init(frame: targetFrame)
        self.menuView?.cornerRadius = self.mainShadowedView!.cornerRadius
        self.menuView?.canComplain = self.canComplain
        self.menuView?.canEdit = self.canEdit
        self.menuView?.canDelete = self.canDelete
        
        self.addSubview(self.menuView!)
        
        if pan == nil {
            pan = UIPanGestureRecognizer(target: self, action: #selector(self.panned(_:)))
            pan.delegate = self
            self.addGestureRecognizer(pan)
        }
    }
    
    private var isHidingNow = false
    
    func hideMenu(forced: Bool = false, completion: ((Bool)->Void)?=nil) {
        guard !self.isHidingNow else { completion?(false); return }

        self.menuView?.stopLoading()
        self.isHidingNow = true
        self.pan?.setTranslation(.zero, in: self)
        if forced {
            self.mainShadowedView?.frame.origin.x = self.normalShadowedFrame.origin.x
            self.mainShadowedLeading?.constant = self.normalShadowedFrame.origin.x
            self.mainShadowedTrailing?.constant = 20 - self.normalShadowedFrame.origin.x
            self.menuView?.frame.origin.x = self.contentWidth
            isMenuOpened = false
            completion?(true)
            self.isHidingNow = false
            return;
        }
        
        self.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
            self.mainShadowedView?.frame.origin.x = self.normalShadowedFrame.origin.x
            self.mainShadowedLeading?.constant = self.normalShadowedFrame.origin.x
            self.mainShadowedTrailing?.constant = 20 - self.normalShadowedFrame.origin.x
            self.menuView?.frame.origin.x = self.contentWidth
        }, completion: { bool in
            self.isUserInteractionEnabled = true
            isMenuOpened = false
            completion?(bool);
            self.isHidingNow = false
            return
        });
    }
    
    private var initialVerticalOffset: CGFloat = 0
    
    func hideMenuWithScrollIntent(forced: Bool = false, completion: ((Bool)->Void)?=nil) {
        guard abs((tableView?.contentOffset.y ?? 0) - initialVerticalOffset) > 20 else { return }
        guard pan == nil || pan.state == .cancelled || pan.state == .ended || pan.state == .failed || pan.state == .possible else { return }
        hideMenu(forced: forced, completion: completion)
    }
    
    private func updateMenu() {
        guard mainShadowedView != nil, menuView != nil else { return }
        
        if isMenuOpened {
            self.mainShadowedView?.frame.origin.x = -self.menuView!.bounds.width - 10
            self.mainShadowedLeading?.constant = -self.menuView!.bounds.width - 10
            self.mainShadowedTrailing?.constant = 20 - (-self.menuView!.bounds.width - 10)
            self.menuView?.frame.origin.x = self.contentWidth - self.menuView!.bounds.width - 10
        } else {
            self.mainShadowedView?.frame.origin.x = self.normalShadowedFrame.origin.x
            self.mainShadowedLeading?.constant = self.normalShadowedFrame.origin.x
            self.mainShadowedTrailing?.constant = 20 - (self.normalShadowedFrame.origin.x)
            self.menuView?.frame.origin.x = self.contentWidth
        }
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
 
    
    private var contentWidth: CGFloat {
        get {
            return UIScreen.main.bounds.width
        }
    }
    
    
    @objc private func panned(_ sender: UIPanGestureRecognizer) {
        guard !isHidingNow else { return }
        
        //MARK: - ignoring canEdit
        guard canDelete || canComplain else {
            hideMenu()
            return
        }
        
        guard abs(sender.translation(in: self).x) > abs(sender.translation(in: self).y) else {
            self.hideMenu()
            return
        }
        
        guard menuView != nil else {
            hideMenu()
            return
        }

        self.tableView?.visibleCells.compactMap({$0 as? PostCell}).filter({$0 != self}).forEach({$0.hideMenu()})
        
        if sender.state == .began {
            initialVerticalOffset = tableView?.contentOffset.y ?? 0
        }
        
        switch sender.state {
        case .ended, .cancelled:

            isMenuOpened = (sender.translation(in: self).x + (isMenuOpened ? (-self.menuView!.bounds.width - 10) : 0)) <= (-self.menuView!.bounds.width - 10)/2
            
            if isMenuOpened {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                    self.mainShadowedView?.frame.origin.x = min(-self.menuView!.bounds.width - 10, 10)
                    self.mainShadowedLeading?.constant = min(-self.menuView!.bounds.width - 10, 10)
                    self.mainShadowedTrailing?.constant = 20 - min((-self.menuView!.bounds.width - 10), 10)
                    self.menuView?.frame.origin.x = self.contentWidth + min( -self.menuView!.bounds.width - 10, 10)
                }, completion: { _ in
                    isMenuOpened = true
                })
            } else {
                hideMenu()
            }
        case .failed:
            isMenuOpened = false
            hideMenu()
            
        default:
            let correctedTranslation = sender.translation(in: self).x + 20
            self.mainShadowedView?.frame.origin.x = min(correctedTranslation + (isMenuOpened ? (-self.menuView!.bounds.width - 10) : 0), 10)
            self.mainShadowedLeading?.constant = min(correctedTranslation + (isMenuOpened ? (-self.menuView!.bounds.width - 10) : 0), 10)
            self.mainShadowedTrailing?.constant = 20 - min((correctedTranslation + (isMenuOpened ? (-self.menuView!.bounds.width - 10) : 0)), 10)
            self.menuView?.frame.origin.x = self.contentWidth + min(correctedTranslation + (isMenuOpened ? (-self.menuView!.bounds.width - 10) : 0), 10)
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isMenuOpened {
            super.touchesBegan(touches, with: event)
        }
        return;
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard !isMenuOpened else {
            
            if let touch = touches.first, self.mainShadowedView?.frame.contains(touch.location(in: self)) ?? false {
                //touch occured in contentView
                self.hideMenu()
                
            }  else {
                if menuView != nil, let touch = touches.first, menuView?.deleteView?.frame.contains(touch.location(in: menuView!)) ?? false {
                    //MARK: - delete view tapped!
                    self.menuView?.startLoading()
                    self.deleteTappedBlock?();
                } else if menuView != nil, let touch = touches.first, menuView?.complainView?.frame.contains(touch.location(in: menuView!)) ?? false {
                    self.menuView?.startLoading()
                    self.complainTappedBlock?()
                } else {
                    print("unkwnown area tapped!")
                }
                
            }
            
            return;
        }
        super.touchesEnded(touches, with: event)
    }
    
}


extension PostImageButton {
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isMenuOpened {
            super.touchesEnded(touches, with: event)
        } else {
            self.postCellView()?.hideMenu()
        }
    }
}


extension UserButton {
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isMenuOpened {
            super.touchesEnded(touches, with: event)
        } else {
            self.postCellView()?.hideMenu()
        }
    }
}

extension ShowCommentsButton {
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isMenuOpened {
            super.touchesEnded(touches, with: event)
        } else {
            self.postCellView()?.hideMenu()
        }
    }
}

class MenuView: ShadowedView {
    var canEdit = false {
        didSet {
            addViews()
        }
    }
    
    var canDelete = false {
        didSet {
            addViews()
        }
    }
    
    var canComplain = false {
        didSet {
            addViews()
        }
    }
    
    fileprivate var deleteView: MenuActionView!
    fileprivate var complainView: MenuActionView!
    private var loadingView: UIActivityIndicatorView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.addViews()
        self.shadowAlpha = 0.2
    }
    
    private func addViews() {
        loadingView = UIActivityIndicatorView(frame: self.bounds)
        loadingView?.hidesWhenStopped = true
        loadingView?.backgroundColor = .white
        loadingView?.activityIndicatorViewStyle = .gray
        
        if canDelete {
            if deleteView == nil {
                deleteView = MenuActionView(type: .delete, frame: self.bounds)
                self.addSubview(deleteView)
            } else {
                deleteView.frame = self.bounds
                deleteView.setNeedsLayout()
                deleteView.layoutIfNeeded()
            }
            loadingView?.color = MenuActionViewType.delete.color
        } else {
            deleteView?.removeFromSuperview()
            deleteView = nil
        }
        
        if canComplain {
            if complainView == nil {
                complainView = MenuActionView(type: .complain, frame: self.bounds)
                self.addSubview(complainView)
            } else {
                complainView.frame = self.bounds
                complainView.setNeedsLayout()
                complainView.layoutIfNeeded()
            }
            
            loadingView?.color = MenuActionViewType.complain.color
            
        } else {
            
            complainView?.removeFromSuperview()
            complainView = nil
        }
        
        if canEdit {
            // MARK: - ignoring canEdit
        }
        
        
        self.addSubview(loadingView!)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
    fileprivate func startLoading() {
        self.loadingView?.startAnimating()
    }
    
    fileprivate func stopLoading() {
        self.loadingView?.stopAnimating()
    }
}

class MenuActionView: UIView {
    
    fileprivate convenience init(type: MenuActionViewType, frame _frame: CGRect) {
        self.init(frame: _frame)
        
        let elementsHeight: CGFloat = 32
        
        let imgView = UIImageView(image: type.image)
        imgView.frame = CGRect(x: 0, y: self.bounds.height/2 - elementsHeight/2, width: self.bounds.width, height: elementsHeight)
        imgView.contentMode = .scaleAspectFit
        self.addSubview(imgView)
        
        let label = UILabel(frame: CGRect(x: 0, y: self.bounds.height/2 + elementsHeight/2, width: self.bounds.width, height: elementsHeight))
        label.text = type.text
        label.textColor = type.color
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightSemibold)
        self.addSubview(label)
        
    }
    
    
}

enum MenuActionViewType {
    case delete
    case edit
    case complain
    
    
    var image: UIImage {
        get {
            switch self {
            case .delete: return #imageLiteral(resourceName: "delete_cell_icon")
            case .complain: return #imageLiteral(resourceName: "error_yellow_icon")
            case .edit: return #imageLiteral(resourceName: "edit_cell_icon")
            }
        }
    }
    
    var text: String {
        get {
            switch self {
            case .delete:  return "Удалить"
            case .complain: return "Пожаловаться"
            case .edit: return "Редактировать"
            }
        }
    }
    
    var color: UIColor {
        get {
            switch self {
            case .delete: return redColor
            case .complain: return yellowColor
            case .edit: return blueColor
            }
        }
    }
}

extension UIView {
    fileprivate func postCellView() -> PostCell? {
        if self is PostCell {
            return self as? PostCell
        } else {
            return self.superview?.postCellView()
        }
    }
}


extension UIView {
    func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = self.superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}

extension UITableViewCell {
    var tableView: UITableView? {
        return self.parentView(of: UITableView.self)
    }
}
