//
//  Beacon.swift
//  Heloo2017
//
//  Created by Константин on 09.05.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import CoreData

let beacon_timeout_seconds: Double = 900

@objc(Beacon)
class Beacon: NSManagedObject {
    
    convenience init(_ insert: Bool) {
        self.init(entity: Beacon.entitys, insertInto: insert ? CoreDataService.instance.privateContext : nil)
    }
    
    class var entitys: NSEntityDescription {
        var entity = NSEntityDescription()
        entity = NSEntityDescription.entity(forEntityName: "Beacon", in: CoreDataService.instance.privateContext)!
        return entity
    }
    
    
    
    enum beaconKeys {
        static let uuid = "uuid"
        static let descript = "description"
        static let proximityUUID = "proximity_uuid"
        static let major = "major"
        static let minor = "minor"
        static let lastSeenDate = "lastSeenDate"
    }
    
    
    @NSManaged var uuid: String
    @NSManaged var descript: String
    @NSManaged var proximityUUID: String
    @NSManaged var cd_major: Int32
    @NSManaged var cd_minor: Int32
    @NSManaged var lastSeenDate: Date
    
    var major: Int {
        get {
            return Int(cd_major)
        } set {
            cd_major = Int32(newValue)
        }
    }
    
    var minor: Int {
        get {
            return Int(cd_minor)
        } set {
            cd_minor = Int32(newValue)
        }
    }
    
    var isActual: Bool {
        get {
            return ((Date().timeIntervalSince1970 - self.lastSeenDate.timeIntervalSince1970) < beacon_timeout_seconds)
        }
    }
    
    func loaded(with json: NSDictionary) -> Beacon {
        
        if let someValue = json[beaconKeys.uuid] as? String {
            self.uuid = someValue
        }
        if let someValue = json[beaconKeys.descript] as? String {
            self.descript = someValue
        }
        if let someValue = json[beaconKeys.proximityUUID] as? String {
            self.proximityUUID = someValue
        }
        if let someValue = json[beaconKeys.major] as? Int {
            self.major = someValue
        } else if let someValue = json[beaconKeys.major] as? String {
            self.major = Int(someValue) ?? 0
        }
        
        if let someValue = json[beaconKeys.minor] as? Int {
            self.minor = someValue
        } else if let someValue = json[beaconKeys.minor] as? String {
            self.minor = Int(someValue) ?? 0
        }
        return self
    }
    
    
    func updateWithOther(_ beacon: Beacon) {
        self.uuid = beacon.uuid
        self.descript = beacon.descript
        self.proximityUUID = beacon.proximityUUID
        self.cd_major = beacon.cd_major
        self.cd_minor = beacon.cd_minor
        self.lastSeenDate = beacon.lastSeenDate
    }
    
//    func encode(with aCoder: NSCoder) {
//        aCoder.encode(self.uuid, forKey: beaconKeys.uuid)
//        aCoder.encode(self.descript, forKey: beaconKeys.descript)
//        aCoder.encode(self.proximityUUID, forKey: beaconKeys.proximityUUID)
//        aCoder.encode(self.major, forKey: beaconKeys.major)
//        aCoder.encode(self.minor, forKey: beaconKeys.minor)
//        aCoder.encode(self.lastSeenDate, forKey: beaconKeys.lastSeenDate)
//
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        self.uuid = aDecoder.decodeObject(forKey: beaconKeys.uuid) as? String ?? ""
//        self.descript = aDecoder.decodeObject(forKey: beaconKeys.descript) as? String ?? ""
//        self.proximityUUID = aDecoder.decodeObject(forKey: beaconKeys.proximityUUID) as? String ?? ""
//        self.major = aDecoder.decodeObject(forKey: beaconKeys.major) as? Int ?? 0
//        self.minor = aDecoder.decodeObject(forKey: beaconKeys.minor) as? Int ?? 0
//        self.lastSeenDate = aDecoder.decodeObject(forKey: beaconKeys.lastSeenDate) as? Date ?? Date()
//    }
//
}

