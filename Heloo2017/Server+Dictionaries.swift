//
//  Server+Dictionaries.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire


extension Server {
    
    func getAvailableInterests(completion: ((Bool)->Void)?=nil) {
        Alamofire.request(url_paths.base + url_paths.interests.dictionary, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            if let items = response.result.value as? [NSDictionary] {
                UserDataManager.instance.availableInterests = []
                for item in items {
                    UserDataManager.instance.availableInterests.append(TreeItem(with: item))
                }
                completion?(true)
            } else {
                completion?(false)
            }
        }
    }
    
    
    
    
    func getAvailablePlaceTypes(completion: (([TreeItem])->Void)?=nil) {
        Alamofire.request(url_paths.base + url_paths.places.types, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let items = response.result.value as? [NSDictionary] {
                UserDataManager.instance.availablePlaceTypes = items.compactMap({TreeItem(with: $0)})
                completion?(UserDataManager.instance.availablePlaceTypes)
            }
        }
    }
    
    
    func getAvailableSubways(by cityTag: String?=nil, completion: (([String: String])->Void)?=nil) {
        var params: [String: Any] = [:]
        if let tag = cityTag {
            params["city"] = tag
        }
        Alamofire.request(url_paths.base + url_paths.subways, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let items = response.result.value as? [NSDictionary] {
                UserDataManager.instance.availableSubways = [:]
                items.forEach({UserDataManager.instance.availableSubways[$0["subway_tag"] as! String] = $0["value"] as? String})
                completion?(UserDataManager.instance.availableSubways)
            }
        }
    }
    
    
    
}
