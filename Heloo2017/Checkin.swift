//
//  Checkin.swift
//  Heloo2017
//
//  Created by Константин on 29.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import CoreGraphics


class ImageObject {
    var url: String!
    var width: CGFloat = 320
    var height: CGFloat = 180
    
    init?(with json: NSDictionary) {
        url = json["url"] as? String
        if url == nil || url.isEmpty {
            return nil
        }
        width = CGFloat(json["width"] as? Double ?? Double(json["width"] as? String ?? "320") ?? 0)
        height = CGFloat(json["height"] as? Double ?? Double(json["height"] as? String ?? "180") ?? 0)
    }
}

class Checkin: NSObject {
    
    enum fields {
        static let id = "uuid"
        static let date  = "created_time"
        static let likes_count = "likes_count"
        static let comments_count  = "comments_count"
        static let isLiked  = "is_liked"
        static let image  = "image"
        static let descript  = "description"
        static let user  = "from"
        static let place  = "place"


    }
    
    var id = ""
    var date: Date = Date()
    var likes_count = 0
    var comments_count = 0
    var isLiked = false
    var artwork: ImageObject?
    var descript: String = ""
    var user: User?
    var place: Place?
    
    var isMine: Bool {
        get {
            return user?.uuid == UserDataManager.instance.currentUser.uuid
        }
    }
    
    init(with json: NSDictionary, place _place: Place? = nil) {
        if let someValue = json[fields.id] as? String {
            self.id = someValue
        }
        if let someValue = json[fields.date] as? String {
            self.date = someValue.stringToDate()
        }
        if let someValue = json[fields.likes_count] as? Int {
            self.likes_count = someValue
        }
        if let someValue = json[fields.comments_count] as? Int {
            self.comments_count = someValue
        }
        if let someValue = json[fields.isLiked] as? Bool {
            self.isLiked = someValue
        }
        if let someValue = json[fields.image] as? NSDictionary {
            self.artwork = ImageObject(with: someValue)
        }
        if let someValue = json[fields.descript] as? String {
            self.descript = someValue
        }
        if let someValue = json[fields.user] as? NSDictionary {
            self.user = User(with: someValue)
        }
        if let someValue = json[fields.place] as? NSDictionary {
            self.place = Place(with: someValue)
        } else {
            self.place = _place
        }
    
    }
}
