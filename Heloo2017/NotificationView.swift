//
//  NotificationView.swift
//  Heloo2017
//
//  Created by Константин on 21.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Foundation

class NotificationView: ShadowedView {
    
    enum notificationTypes {
        case place
        case subscriber
    }


    var place: Place?
    var user: User?
    var offer: NotificationOffer?
    var checkinComment: NotificationCheckinComment?
    var acceptComment: NotificationAcceptComment?
    var message: Message?
    var isOverMPC = false
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var title: UILabel!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var actionLabel: UILabel?
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        update()
    }
    
    func update() {
        if let p = self.place {
            title.text = p.name
            icon.sd_setImage(with: URL(string: p.image_small), placeholderImage: #imageLiteral(resourceName: "place_feed_placeholder"))
        } else if let u = self.user {
            title.text = [u.firstName, u.lastName].filter({!$0.isEmpty}).joined(separator: " ")
            icon.sd_setImage(with: URL(string: u.image_small), placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
            if isOverMPC {
                 actionLabel?.text = "Совсем рядом с Вами"
            } else {
                actionLabel?.text = (u.gender == .male ? "Подписался" : "Подписалась") + " на Вас"
            }
        } else if let o = self.offer {
            title.text = o.place_name
            icon.sd_setImage(with: URL(string: o.place_avatar), placeholderImage: #imageLiteral(resourceName: "place_feed_placeholder"))
            actionLabel?.text = "Акция: \(o.offer_name ?? "Неизвестная акция")"
        } else if let cc = self.checkinComment {
            title.text = [cc.person.firstName, cc.person.lastName].filter({!$0.isEmpty}).joined(separator: " ")
            icon.sd_setImage(with: URL(string: cc.person.image_small), placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
            actionLabel?.text = "Прокомментировал" + (cc.person.gender == .male ? " " : "а ") + "Ваш чекин"
        } else if let ac = self.acceptComment {
            title.text = [ac.person.firstName, ac.person.lastName].filter({!$0.isEmpty}).joined(separator: " ")
            icon.sd_setImage(with: URL(string: ac.person.image_small), placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
            actionLabel?.text = "Прокомментировал" + (ac.person.gender == .male ? " " : "а ") + "Ваш пост"
        } else if let msg = self.message {
            guard let sender = msg.sender else { return }
            title.text = [sender.firstName, sender.lastName].filter({!$0.isEmpty}).joined(separator: " ")
            icon.sd_setImage(with: URL(string: sender.image_small), placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
            let mutable = NSMutableAttributedString(string: "Написал" + (sender.gender == .male ? ": " : "а: "))
            mutable.append(NSAttributedString(string: msg.text, attributes: [NSForegroundColorAttributeName: blueColor]))
            actionLabel?.attributedText = mutable
        }
    }

}
