//
//  MoreViewController.swift
//  Heloo2017
//
//  Created by Константин on 28.07.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class MoreViewController: UITableViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userSegue: UIStoryboardSegue!
    
    @IBOutlet weak var userCell: UITableViewCell!
    @IBOutlet weak var eventsCell: UITableViewCell!
    @IBOutlet weak var offersCell: UITableViewCell!
    @IBOutlet weak var favoritesCell: UITableViewCell!
    @IBOutlet weak var settingsCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fill()
        self.navigationController?.navigationBar.makeClear()
        subscribeForProfileUpdates()
    }
    
     func subscribeForProfileUpdates() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.update), name: .userInfoChanged, object: nil)
    }
    
    @objc func update() {
        async {
            self.fill()
        }
    }
    
    func fill() {
        if let user = UserDataManager.instance.currentUser {
            if let url = URL(string: user.image_small) {
                self.userIcon.sd_setImage(with: url)
            } else if let url = URL(string: user.image) {
                self.userIcon.sd_setImage(with: url)
            }
            self.nameLabel.text = user.firstName.trimmingCharacters(in: .whitespaces).isEmpty ? user.displayName.isEmpty ? "Имя не указано" : user.displayName : user.firstName//[user.firstName, user.lastName].joined(separator: " ")
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !UserDataManager.isAuthorized {
            return 3
        }
        return super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !UserDataManager.isAuthorized {
            return [eventsCell, favoritesCell, settingsCell][indexPath.row]
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UserDataManager.isAuthorized && indexPath.row == 0 {
            return UITableViewAutomaticDimension //136
        }
        return 81
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        
        switch cell {
        case userCell:
            guard UserDataManager.isAuthorized else {
                ErrorPopupManager.showError(type: .access)
                return
            }
            self.performSegue(withIdentifier: "user", sender: cell)
        case eventsCell:
            self.performSegue(withIdentifier: "events", sender: cell)

        case favoritesCell:
            guard UserDataManager.isAuthorized else {
                ErrorPopupManager.showError(type: .access)
                return
            }
            self.performSegue(withIdentifier: "favorites", sender: cell)
        case settingsCell:
            guard UserDataManager.isAuthorized else {
                ErrorPopupManager.showError(type: .access)
                return
            }
            self.performSegue(withIdentifier: "settings", sender: cell)

        default: return;
            
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dist = segue.destination as? EventsViewController {
            dist.isEvents = sender as? UITableViewCell == eventsCell
        }
    }
}
