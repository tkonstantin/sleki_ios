//
//  KolodaCard.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class KolodaCard: UIView {
    
    @IBOutlet weak var no_network_view: UIView!
    @IBOutlet weak var no_results_view: UIView!
    @IBOutlet weak var interests_sublabel: UILabel!
    @IBOutlet weak var placeholder_image: UIImageView!
    @IBOutlet weak var compareButton: UIButton?
    @IBOutlet weak var addToFavorites_button: OfferFavoriteButton?
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var interests_icon: UIImageView?
    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var place_label: UILabel!
    @IBOutlet weak var ages_label: UILabel?
    @IBOutlet weak var user_icon: UIImageView!
    
    weak var searchRef: PeoplePlacesViewController!
    
    var interests: [TreeItem] = []
    
    var uuid: String = ""
    var isFavorite = false
    
    lazy var progress: CGFloat = {
        var intersect: CGFloat = 0
        let userInterests = UserDataManager.instance.currentUser?.interests ?? []
        guard userInterests.count != 0 && self.interests.count != 0 else { return 0 }

        for interest in self.interests {
            if userInterests.contains(where: {$0.key == interest.key}) {
                intersect += 1
            }
        }
        return intersect*(1/(2*CGFloat(userInterests.count)) + 1/(2*CGFloat(self.interests.count)))
    }()

    
    var currentColor: UIColor {
        get {
            if self.progress < 0.33 {
                return redColor
            } else if self.progress < 0.66 {
                return yellowColor
            } else {
                return greenColor
            }
        }
    }
    var bigSL: CAShapeLayer = CAShapeLayer()
    var smallSL: CAShapeLayer = CAShapeLayer()
    
    override func awakeFromNib() {
        
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        guard UserDataManager.isAuthorized else {
            async {
                self.interests_icon?.image = self.interests_icon!.image!.imageWithColor(UIColor.lightGray)
            }
            return
        }

        
        async(after: 300) {
//            self.interests_icon?.image = self.interests_icon!.image!.imageWithColor(self.currentColor)
            if self.interests_icon != nil {
                self.bigSL = CAShapeLayer()
                self.smallSL = CAShapeLayer()
                
                let center = CGPoint(x: self.interests_icon!.layer.frame.midX, y: self.interests_icon!.layer.frame.midY)
                let bigP = UIBezierPath(arcCenter: center, radius: 28, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi, clockwise: false)
                let smallP = UIBezierPath(arcCenter: center, radius: 20, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi, clockwise: false)
                
                self.bigSL.path = bigP.cgPath
                self.bigSL.fillColor = UIColor.clear.cgColor
                self.bigSL.strokeColor = UIColor.white.cgColor//self.currentColor.cgColor
                self.bigSL.lineWidth = 3
                self.bigSL.lineCap = kCALineCapRound
                self.bigSL.strokeStart = 0
                self.bigSL.strokeEnd = 0.0
                
                self.smallSL.path = smallP.cgPath
                self.smallSL.fillColor = UIColor.clear.cgColor
                self.smallSL.strokeColor = UIColor.white.withAlphaComponent(1/2).cgColor//self.currentColor.withAlphaComponent(0.33).cgColor
                self.smallSL.lineWidth = 2
                self.smallSL.lineCap = kCALineCapRound
                self.smallSL.strokeStart = 0
                self.smallSL.strokeEnd = 0.0
                
                self.cardView.layer.addSublayer(self.bigSL)
                self.cardView.layer.addSublayer(self.smallSL)
                
            }
        }
    }
    
    @IBAction func showComparison(_ sender: UIButton) {
        searchRef.showComparison()
    }
    

    
    func animateRings() {
        if searchRef.items.count != 0 {
            async(after: 300) {
                let updatedProgress = self.progress == 0 ? 0.1 : self.progress
                if self.interests_icon != nil {
                    
                    let center = CGPoint(x: self.interests_icon!.layer.frame.midX, y: self.interests_icon!.layer.frame.midY)
                    let bigP = UIBezierPath(arcCenter: center, radius: 28, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi, clockwise: false)
                    let smallP = UIBezierPath(arcCenter: center, radius: 20, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi, clockwise: false)
                    
                    self.bigSL.path = bigP.cgPath
                    self.smallSL.path = smallP.cgPath
                    
                    
//                    self.bigSL.strokeColor = updatedProgress == 0 ? grayColor.withAlphaComponent(0.5).cgColor : self.currentColor.cgColor
                    CATransaction.begin()
                    CATransaction.setAnimationDuration(2.0)
                    self.bigSL.strokeEnd = updatedProgress
                    CATransaction.commit()
                    
                    CATransaction.begin()
                    CATransaction.setAnimationDuration(1.5)
                    self.smallSL.strokeEnd = updatedProgress
                    CATransaction.commit()
                }
            }
        }
    }
    
    
    
    
}
