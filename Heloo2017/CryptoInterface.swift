//
//  CryptoInterface.swift
//  Heloo2017
//
//  Created by Константин on 04/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

class CryptoInterface {
    private static let instance = CryptoInterface()
    
    private let key = CryptoUtils.byteArray(fromHex: "2b7e151628aed2a6abf7158809cf4f3c")
    
   class func encode(_ string: String) -> (String) {
        let randomIVString = String.random
        let iv = CryptoUtils.byteArray(from: randomIVString)
        var bytesArr = CryptoUtils.byteArray(from: string)
    
        if bytesArr.count % Cryptor.Algorithm.aes.blockSize != 0 {
            bytesArr = CryptoUtils.zeroPad(byteArray: bytesArr, blockSize: Cryptor.Algorithm.aes.blockSize)
        }
    
        guard let cryptor = try? Cryptor(operation: .encrypt, algorithm: .aes256, options: .none, key: instance.key, iv: iv) else { return "Ошибка" }
        guard let cipherText = cryptor.update(byteArray: bytesArr)?.final() else { return "Ошибка" }
        return CryptoUtils.hexString(from: cipherText) + randomIVString
    }
    
    class func decode(_ _string: String) -> String {
        let ivString = _string.substring(from: String.Index(_string.count - 16))
        let string = _string.substring(to: String.Index(_string.count - 16))
        
        let iv = CryptoUtils.byteArray(from: ivString)

        var bytesArr = CryptoUtils.byteArray(fromHex: string)
        
        if bytesArr.count % Cryptor.Algorithm.aes.blockSize != 0 {
            bytesArr = CryptoUtils.zeroPad(byteArray: bytesArr, blockSize: Cryptor.Algorithm.aes.blockSize)
        }
        
        guard let cryptor = try? Cryptor(operation: .decrypt, algorithm: .aes256, options: .none, key: instance.key, iv: iv) else { return "Ошибка" }
        guard let decryptedText = cryptor.update(byteArray: bytesArr)?.final() else { return "Ошибка" }
        return String(data: Data(bytes: decryptedText), encoding: .utf8)?.replacingOccurrences(of: "\0", with: "") ?? "Ошибка"
    }
}


extension String {
    static var random: String {
        get {
            let allowed = "0123456789abcdef"
            var result: String = ""
            for _ in 0..<16 {
                let randomIndex = Int(arc4random_uniform(15))
                let char = Array(allowed.characters)[randomIndex]
                result.append(char)
            }
            return result
        }
    }
}

