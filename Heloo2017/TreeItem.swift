//
//  ModernInterest.swift
//  Heloo2017
//
//  Created by Константин on 27.08.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

class TreeItem: NSObject, NSCoding {
    
    override func copy() -> Any {
        let new = TreeItem(with: [:])
        new.title = self.title
        new.key = self.key
        new.isSelected = self.isSelected
        new.associatedItems = self.associatedItems.compactMap({$0.copy() as? TreeItem})
        new.parent = self.parent
        return new
    }
    
    var title = ""
    var key: String = ""
    var isSelected = false
    var associatedItems: [TreeItem] = []
    var parent: TreeItem?
    
    init(title t: String, key k: String) {
        title = t
        key = k
    }
    
    init(with json: NSDictionary) {
        super.init()
        title = (json["value"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines).uppercasedByFirstLetter()
        key = json["type"] as? String ?? ""
        isSelected = json["isSelected"] as? Bool ?? false
        if let assosiated = json["sub_types"] as? [NSDictionary] {
            for interest in assosiated {
                let newModern = TreeItem(with: interest)
                newModern.parent = self
                associatedItems.append(newModern)
            }
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(key, forKey: "key")
        aCoder.encode(isSelected, forKey: "isSelected")
        aCoder.encode(associatedItems, forKey: "assosiatedInterests")
        aCoder.encode(parent, forKey: "parent")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        title = (aDecoder.decodeObject(forKey: "title") as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        key = aDecoder.decodeObject(forKey: "key") as? String ?? ""
        isSelected = aDecoder.decodeObject(forKey: "isSelected") as? Bool ?? false
        associatedItems = aDecoder.decodeObject(forKey: "assosiatedInterests") as? [TreeItem] ?? []
        parent = aDecoder.decodeObject(forKey: "parent") as? TreeItem
    }
    
    
    var apiDict: [String: Any] {
        get {
            return [
                "type": key,
                "value": title
            ]
        }
    }
}

extension Array where Element: TreeItem {
    func makeFlat() -> [TreeItem] {
        var result: [TreeItem] = []
        for item in self {
            result.append(item)
            for subitem in item.associatedItems {
                result.append(subitem)
            }
        }
        return result
    }
}
