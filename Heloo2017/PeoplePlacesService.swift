//
//  PeoplePlacesService.swift
//  Heloo2017
//
//  Created by Константин on 08/12/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import CoreLocation


protocol PPServiceDelegate: class {
    func pp_onContentChanged(appending: Bool)
    func pp_getPlaceFilter() -> placeFilterObject?
    func pp_getPersonFilter() -> personFilterObject?
    func pp_getCurrentLocation() -> CLLocationCoordinate2D?
}

class PeoplePlacesService {
    
    enum types {
        case people
        case places
    }
    
    enum modes {
        case normal
        case nearby
    }
    
    init(type t: types, mode m: modes = .normal, delegate d: PPServiceDelegate)  {
        type = t
        mode = m
        delegate = d
        makeRequest()
    }
    
    fileprivate weak var delegate: PPServiceDelegate?

    
    fileprivate(set) var items: [Searchable] = []
    
    fileprivate var pageToLoad: Int = 0
    fileprivate var canLoadMore: Bool = true
    fileprivate var requestIsRunning = false
    
    fileprivate(set) var type: types = .places
    fileprivate(set) var mode: modes = .normal {
        didSet {
            guard mode != oldValue else { return }
            async { [weak self] in
                self?.clearAndRequest()
            }
        }
    }
    
    
    func changeModeIfPossible(to newMode: modes) -> Bool {
        guard !requestIsRunning else { return false }
        let willBeChanged = mode != newMode
        mode = newMode
        return willBeChanged
    }
    
    func shouldLoadNextPage() {
        async { [weak self] in
            self?.makeRequest()
        }
    }
    
    func reportFilterChanged() {
        async { [weak self] in
            self?.clearAndRequest()
        }
    }
    
    func reportLocationChanged() {
        async { [weak self] in
            self?.clearAndRequest()
        }
    }
    
}

extension PeoplePlacesService {
    
    fileprivate func clearAndRequest() {
        items = []
        pageToLoad = 0
        canLoadMore = true
        makeRequest()
    }
    
    fileprivate func makeRequest() {
        guard !requestIsRunning, canLoadMore else { return }
        requestIsRunning = true
        
        if self.items.count == 0 {
            showHUD()
        }
        
        retrieveItems { [weak self] (loadedItems, hasNext) in
            dismissHUD()
            self?.mergeNewItems(loadedItems)
            self?.canLoadMore = hasNext
            self?.markItemsShown()
            self?.delegate?.pp_onContentChanged(appending: self?.pageToLoad != 0)

            if self?.canLoadMore == true {
                self?.pageToLoad += 1
            }
            async(after: 500, {
                self?.requestIsRunning = false
            })
        }
        
    }
    
    private func retrieveItems(completion: @escaping ([Searchable], Bool)->Void) {
        switch mode {
        case .normal:
            if type == .places {
                Server.makeRequest.getPlaces(page: pageToLoad, filter: delegate?.pp_getPlaceFilter()) { (loadedPlaces, hasNext) in
                    completion(loadedPlaces.compactMap({Searchable(place: $0)}), hasNext)
                }
            } else {
                Server.makeRequest.getPersons(page: pageToLoad, filter: delegate?.pp_getPersonFilter()) { (loadedPersons, hasNext) in
                    completion(loadedPersons.compactMap({Searchable(user: $0)}), hasNext)
                }
            }
            
        case .nearby:
            if type == .places  {
                Server.makeRequest.searchPlaces(coordinates: delegate?.pp_getCurrentLocation()) { (loadedPlaces) in
                    var result: [Place] = loadedPlaces
                    for place in Server.makeRequest.activePlaces {
                        if !result.contains(where: {$0.uuid == place.uuid}) {
                            result.append(place)
                        }
                    }
                    completion(result.compactMap({Searchable(place: $0)}), false)
                }
                
            } else {
                
                Server.makeRequest.searchPersons(coordinates: delegate?.pp_getCurrentLocation()) { (loadedPersons) in
                    var result: [User] = loadedPersons
                    for person in MPCManager.shared.locatedUsers.keys {
                        if !result.contains(where: {$0.uuid == person.uuid}) {
                            result.append(person)
                        }
                    }
                    completion(result.compactMap({Searchable(user: $0)}), false)
                }
                
            }
        }
    }
    
    
    private func markItemsShown() {
        if mode == .nearby {
            if type == .places {
                BeaconManager.instance.nearPlacesHadBeenShown()
                WAPManager.instance.nearPlacesHadBeenShown()
            } else {
                MPCManager.shared.shownUsers = Array(MPCManager.shared.locatedUsers.keys)
                NotificationCenter.default.post(name: .viewed_people_update_notification, object: nil)
            }
        }
    }

    
    private func mergeNewItems(_ newItems: [Searchable]) {
        for newItem in newItems {
            if !items.contains(where: { $0.uuid == newItem.uuid })  {
                items.append(newItem)
            }
        }
    }
    
}

