//
//  Place.swift
//  Heloo2017
//
//  Created by Константин on 08.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class PlacePhotoObject {
    
    var uuid: String!
    var image_url: String?
    var avatar_url: String?
    
    init?(with json: NSDictionary) {
        guard let someValue = json["uuid"] as? String else { return nil }
        uuid = someValue
        image_url = json["image_url"] as? String
        avatar_url = json["avatar_url"] as? String
        
        if (image_url == nil || image_url!.isEmpty) && (avatar_url == nil || avatar_url!.isEmpty) {
            return nil
        }
    }
}

class Place: NSObject {
    
    enum fields {
        static let address = "address"
        static let descript = "description"
        static let lon = "lon"
        static let lat = "lan"
        static let image_large = "image_url"
        static let image_small = "avatar_url"
        static let name = "name"
        static let types = "types"
        static let uuid = "uuid"
        static let isFavorite = "is_favorit"
        static let openingHoursItems = "openingHoursItems"
        static let phone = "phone_number"
        static let checkins_count = "checkins_count"
        static let offers_count = "offers_count"
        static let events_count = "events_count"
        static let email = "email"
        static let web_page = "web_page"
        static let score = "score"
        static let subwayDict = "subway"
        static let subwayString = "value"
        static let subwayLine = "subway_line"
        static let subwayColor = "color"
        static let photos = "photos"
        static let blocked = "blocked"
    }
    
    var address: Address?
    var descript = ""
    var lon: Double = 0.0
    var lat: Double = 0.0
    var image_large = ""
    var image_small = ""
    var photos: [PlacePhotoObject] = []
    var name = ""
    var geo_distance: Double = -1
    var types: [TreeItem] = []
    var uuid = ""
    var isFavorite = false
    var openingHoursItems: [HourItems] = []
    var phone = ""
    var events_count: Int = 0
    var offers_count: Int = 0
    var checkins_count: Int = 0
    var email = ""
    var web_page = ""
    var score: Double = 0
    var subwayString = ""
    var subwayColor: UIColor = .red
    var isOpen: Bool?
    var blocked = false
    
    private func weekDayLabel(for index: Int) -> String {
        switch index {
        case 0: return "Понедельник"
        case 1: return "Вторник"
        case 2: return "Среда"
        case 3: return "Четверг"
        case 4: return "Пятница"
        case 5: return "Суббота"
        case 6: return "Воскресенье"
        case 7: return "Будние дни"
        default: return "Выходные дни"
        }
    }
    
    private func weekDay(for label: String) -> Int {
        for index in 0..<9 {
            let weekDay = weekDayLabel(for: index)
            if  weekDay == label {
                return index
            } else if weekDay == label.components(separatedBy: ":").first?.components(separatedBy: " - ").first {
                return index
            }
        }
        return 0
    }
    
    private func opens(for day: Int) -> Double {
        return self.openingHoursItems.filter({$0.dayOfWeek == day}).first!.opens
    }
    
    private func closes(for day: Int) -> Double {
        return self.openingHoursItems.filter({$0.dayOfWeek == day}).first!.closes
    }
    
    var openingHoursString : [Int: [String: String]] {
        get {
            var resultString: [Int: [String: String]] = [:]
            var currentDaysSequence: Set<Int> = []
            
            let appedningBlock = {
                var sequence_string: [Int: [String: String]] = [:]
                if currentDaysSequence.count == 1 {
                    let currentDay = currentDaysSequence.max()!
                    let weekdays = self.weekDayLabel(for: currentDay) + ": "
                    var hours = self.opens(for: currentDay).hoursString() + " - " + self.closes(for: currentDay).hoursString()
                    if hours == "00:00 - 00:00" {
                        hours = "24 ч."
                    }
                    sequence_string = [currentDay: [weekdays: hours]]
                } else {
                    let max = currentDaysSequence.max()!
                    let min = currentDaysSequence.min()!
                    var weekdays = self.weekDayLabel(for: min) +  " - " + self.weekDayLabel(for: max).lowercased() + ": "
                    if min == 0 && max == 4 {
                        weekdays = "Будние дни: "
                    } else if min == 5 && max == 6 {
                        weekdays = "Выходные дни: "
                    }
                    var hours = self.opens(for: max).hoursString() + " - " + self.closes(for: max).hoursString()
                    if hours == "00:00 - 00:00" {
                        hours = "24 ч."
                    }
                    sequence_string = [min: [weekdays: hours]]
                }
                currentDaysSequence = []
                let value = sequence_string.values.first
                guard !resultString.values.contains(where: { $0.keys.contains(value?.keys.first ?? "") }) else { return }
                resultString[Array(sequence_string.keys).first!] = value
            }
            
            for item in openingHoursItems.sorted(by: {$0.dayOfWeek < $1.dayOfWeek}) {

                let maxInSequence: Int! = currentDaysSequence.max()
                if (maxInSequence == nil) || (maxInSequence == item.dayOfWeek - 1 && (opens(for: maxInSequence) == item.opens && closes(for: maxInSequence) == item.closes)) {
                    //это первый элемент в текущей последовательности, либо он совпадает по времени с текущей последовательностью
                    currentDaysSequence.insert(item.dayOfWeek)
                    if item == openingHoursItems.max(by: {$0.dayOfWeek < $1.dayOfWeek}) {
                        appedningBlock()
                    }
                } else {
                    //это элемент, на котором последовательность обрывается.
                    appedningBlock()
                    currentDaysSequence.insert(item.dayOfWeek)
                    if item == openingHoursItems.max(by: {$0.dayOfWeek < $1.dayOfWeek}) {
                        appedningBlock()
                    }
                }
            }
            
            
            return resultString
        }
    }
    
    var address_string: String {
        get {
            
            return self.address == nil ? "Адрес не указан" : [self.address!.street, self.address!.number, self.address!.city ?? "", self.address!.country ?? ""].filter({!$0.isEmpty}).joined(separator: ", ")
        }
    }
    
    var short_address_string: String {
        get {
            return (self.address?.city ?? "Город не указан")
        }
    }
    
    var contacts: [String: String] {
        get  {
            var result: [String: String] = [:]
            if !phone.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                result["Телефон"] = phone.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            if !email.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                result["E-mail"] = email.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            if !web_page.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                result["Сайт"] = web_page.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            return result
        }
    }
    
    init(with json: NSDictionary) {
        if let someValue = json[fields.score] as? Double {
            self.score = someValue
        }
        if let someValue = json[fields.address] as? NSDictionary {
            self.address = Address(with: someValue)
        }
        if let someValue = json[fields.descript] as? String {
            self.descript = someValue
        }
        if let someValue = json[fields.lon] as? Double {
            self.lon = someValue
        }
        if let someValue = json[fields.lat] as? Double {
            self.lat = someValue
        }
        if let someValue = json[fields.image_large] as? String {
            self.image_large = someValue
        }
        if let someValue = json[fields.image_small] as? String {
            self.image_small = someValue
        }
        if let someValue = json[fields.name] as? String {
            self.name = someValue.uppercasedByFirstLetter()
        }
        if let typesDicts = json[fields.types] as? [NSDictionary] {
            self.types = typesDicts.compactMap({ TreeItem(with: $0) })
        }
        if let someValue = json[fields.uuid] as? String {
            self.uuid = someValue
        }
        if let someValue = json[fields.isFavorite] as? Bool {
            self.isFavorite = someValue
        }
        if let someDicts = json[fields.openingHoursItems] as? [NSDictionary] {
            self.openingHoursItems = []
            for dict in someDicts {
                self.openingHoursItems.append(HourItems(with: dict))
            }
        }
        if let someValue = json[fields.phone] as? String {
            self.phone = someValue
        }
        self.checkins_count = json[fields.checkins_count] as? Int ?? 0
        
        self.offers_count = json[fields.offers_count] as? Int ?? 0
        
        self.events_count = json[fields.events_count] as? Int ?? 0
        
        self.email = json[fields.email] as? String ?? ""
        
        self.web_page = json[fields.web_page] as? String ?? ""
        
        self.blocked = json[fields.blocked] as? Bool ?? false
        
        if let dict = json[fields.subwayDict] as? NSDictionary {
            self.subwayString = dict[fields.subwayString] as? String ?? "станция метро не указана"
            if let line = dict[fields.subwayLine] as? NSDictionary {
                self.subwayColor = UIColor(hex: line[fields.subwayColor] as? String ?? "FFFFFF")
            } else {
                self.subwayColor = .clear
            }
            if let hex = dict[fields.subwayColor] as? String {
                self.subwayColor = UIColor(hex: hex)
            }
        }
        
        if let photoDicts = json[fields.photos] as? [NSDictionary] {
            var photoObjects: [PlacePhotoObject?] = []
            for photoDict in photoDicts {
                photoObjects.append(PlacePhotoObject(with: photoDict))
            }
            self.photos = photoObjects.flatMap({$0})
        }
        
    }
    
}


class HourItems: NSObject {
    
    enum hourKeys {
        static let dayOfWeek = "day_of_week"
        static let opens = "opens"
        static let closes = "closes"
    }
    var dayOfWeek = 0
    var opens = 0.0
    var closes = 0.0
    

    
    
    
    
    init(with json: NSDictionary) {
        if let someValue = json[hourKeys.dayOfWeek] as? Int {
            self.dayOfWeek = someValue
        }
        if let someValue = json[hourKeys.opens] as? Double {
            self.opens = someValue
        }
        if let someValue = json[hourKeys.closes] as? Double {
            self.closes = someValue
        }
    }
    
}


extension Double {
    fileprivate func hoursString() -> String {
        var firstPart = String(Int(self))
        if firstPart.count == 1 {
            firstPart = "0" + firstPart
        }
        var secondPart = String(Int((self - Double(Int(self)))*60))
        if secondPart.count == 1 {
            secondPart = "0" +  secondPart
        }
        return  firstPart + ":" + secondPart
    }
}


extension String {
    func uppercasedByFirstLetter() -> String {
        guard let first = self.first else { return self}
        return self.replacingCharacters(in: self.startIndex..<self.index(after: self.startIndex), with: String(first).uppercased())
    }
}


