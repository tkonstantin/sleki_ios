//
//  ProfileSubwayTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 21.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ProfileSubwayTableViewCell: UITableViewCell {

    @IBOutlet weak var subwayColorView: UIView!
    @IBOutlet weak var subwayTitle: UILabel!
    
    func update(with place: Place) {
        subwayTitle.text = place.subwayString
        subwayColorView.backgroundColor = place.subwayColor
    }
    
}
