//
//  AcceptButton.swift
//  Heloo2017
//
//  Created by Константин on 28.08.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class AcceptButton: ResizableButton {

    var uuid: String = ""
    
    var isAccepted: Bool = false {
        didSet {
            self.setTitle(self.isAccepted ? "Не пойду" : "Пойду", for: .normal)
            self.setBackgroundImage(self.isAccepted ? nil : #imageLiteral(resourceName: "gradient_blue"), for: .normal)
            self.backgroundColor = UIColor(hex: "B2B1C2")

//            UIView.animate(withDuration: 0.2, animations: {
//                self.imageView?.transform =  CGAffineTransform(rotationAngle: .pi/4)
//            }) { (_) in
//                UIView.animate(withDuration: 0.2, animations: {
//                    self.setImage(self.isAccepted ? #imageLiteral(resourceName: "not_to_go_button") : #imageLiteral(resourceName: "to_go_button"), for: .normal)
//                    self.imageView?.transform = CGAffineTransform(rotationAngle: 0)
//                })
//            }

        }
    }

}
