//
//  PhotoViewerViewController.swift
//  Heloo2017
//
//  Created by Константин on 14.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class PhotoViewerViewController: UIViewController {
    
    class func present(with images: [String], from: UIViewController?=UIApplication.shared.keyWindow?.rootViewController, shouldPushIfPossible: Bool = true) {
        let viewer = UIStoryboard(name: "PhotoViewer", bundle: nil).instantiateViewController(withIdentifier: "PhotoViewerViewController") as! PhotoViewerViewController
        viewer.modalPresentationStyle = .overCurrentContext
        viewer.imageURLs = images
        if shouldPushIfPossible, let nav = from as? UINavigationController ?? from?.navigationController {
            nav.pushViewController(viewer, animated: true)
        } else {
            from?.present(viewer, animated: true, completion: nil)
        }
    }
    
    
    fileprivate var previousDeviceOrientation: UIDeviceOrientation = .portrait
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.unlockPortrait()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: .UIDeviceOrientationDidChange, object: nil)
    }
    
    @objc private func rotated() {
        
        if previousDeviceOrientation != validated(from: UIDevice.current.orientation) {
            previousDeviceOrientation = validated(from: UIDevice.current.orientation)
            self.collectionItems.reloadData()
            
        }
        
    }
    
    private func validated(from: UIDeviceOrientation) -> UIDeviceOrientation {
        if from.isFlat || from == .portraitUpsideDown {
            return previousDeviceOrientation
        } else {
            return from
        }
    }
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionItems: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    fileprivate var imageURLs: [String] = []
    
    @IBAction func backTapped(_ sender: UIButton) {
        let isPortrait = validated(from: UIDevice.current.orientation) == .portrait
        
        UIApplication.lockPortrait()
        
        async(after: isPortrait ? 0 : 400) {
            if let nav = self.navigationController {
                nav.popViewController(animated: true)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    var offsetPercentage: CGFloat {
        get {
            return abs(currentOffset*5/self.view.bounds.height)
        }
    }
    
    var currentOffset: CGFloat = 0 {
        didSet {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(1 - offsetPercentage/2)
            self.collectionItems.frame.origin.y = currentOffset
        }
    }
    
    @IBAction func panned(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .cancelled, .ended, .failed:
            if offsetPercentage >= 1 {
                self.dismissByPanning(sender)
            } else {
                UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.1, options: [], animations: {
                    self.currentOffset = 0
                }, completion: nil)
            }
        default:
            self.currentOffset = sender.translation(in: self.view).y
            if offsetPercentage > 2 {
                self.dismissByPanning(sender)
            }
        }
    }
    
    private func dismissByPanning(_ sender: UIPanGestureRecognizer) {
        sender.reset()
        sender.isEnabled = false
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: flag ? 0.15 : 0) {
            self.backButton?.alpha = 0
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
            self.collectionItems?.frame.origin.y = 0
        }
        super.dismiss(animated: flag, completion: completion)
    }
}

extension PhotoViewerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageURLs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MediaViewerPhotoCell
        cell.update(with: imageURLs[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
        pageControl.currentPage = currentPage
    }
}
