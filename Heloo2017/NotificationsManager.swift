//
//  NotificationsManager.swift
//  Heloo2017
//
//  Created by Константин on 19.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


var areNotificationsAllowed = true

var rootNav: UINavigationController? {
    get {
        return UIApplication.shared.keyWindow?.rootViewController?.navigationController
            ?? UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
            ?? rootTab?.selectedViewController as? UINavigationController
            ?? rootTab?.selectedViewController?.navigationController
    }
}


var rootTab: UITabBarController? {
    get {
        return UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
    }
}


extension UIViewController {
    func topController(from some: UIViewController? = rootTab) -> UIViewController? {
        if let presented = some?.presentedViewController {
            return topController(from: presented)
        } else {
            return some
        }
    }
}

extension Notification.Name {
    static let viewed_places_update_notification = NSNotification.Name("viewed_places_update_notification")
    static let viewed_people_update_notification = NSNotification.Name(rawValue: "viewed_people_update_notification")
    static let places_update_notification = NSNotification.Name("places_update_notification")
    static let people_update_notification = NSNotification.Name(rawValue: "people_update_notification")
    static let chatsChanged = NSNotification.Name("chatsChangedNotification")
    static let userInfoChanged = NSNotification.Name("userInfoChangedNotification")
    static let mainTabDidAppear = NSNotification.Name("mainTabDidAppearNotification")
}

class NotificationsManager {
    
    
    static let shared = NotificationsManager()
    
    private var _currentWindow: NotificationsWindow?
    
    private var currentWindow: NotificationsWindow! {
        get {
            if _currentWindow == nil {
                let window = NotificationsWindow(frame: UIScreen.main.bounds)
                window.windowLevel = UIWindowLevelAlert + 1
                window.isHidden = false
                _currentWindow = window
            }
            return _currentWindow!
        }
    }

    
    func showNotification(with item: Any, isOverMPC: Bool = false) {
        
        if let message = item as? Message {
            
            if let chat = CoreDataManager.shared.chat(with: message.chat_uuid) {
                ChattingService.shared.getNewMessages(for: chat, completion: {})
                if let currentChat = (rootTab?.viewControllers?[3] as? UINavigationController)?.topViewController as? ChatViewController {
                    if currentChat.chat?.uuid == chat.uuid {
                        return;
                    }
                }
                
                CoreDataService.instance.save()
                NotificationsUICoordinator.show(item: item, in: currentWindow, isOverMPC: isOverMPC)
            } else {
                NotificationsUICoordinator.show(item: item, in: currentWindow, isOverMPC: isOverMPC)
            }
        } else {
            NotificationsUICoordinator.show(item: item, in: currentWindow, isOverMPC: isOverMPC)
        }
    }
    
    private var pendingOnTapAction: (()->Void)? = nil
    
    func executePending() {
        pendingOnTapAction?()
        pendingOnTapAction = nil
    }
    
    func onTapped(place: Place?=nil, user: User?=nil, offer: NotificationOffer?=nil, checkin_comment: NotificationCheckinComment?=nil, accept_comment: NotificationAcceptComment?=nil, message: Message?=nil) {
        
        //TODO: - адаптировать к замене бокового меню на таббар
        
        guard let menuContent = rootTab as? MainTabsController else {
            pendingOnTapAction = {
                self.onTapped(place: place, user: user, offer: offer, checkin_comment: checkin_comment, accept_comment: accept_comment, message: message)
            }
            
            return
        }
        
        if place != nil { // pushing place's profile in beacon local notification tapped
                menuContent.openNearbyPlaces()
            NotificationsManager.shared.removeNotification()
        } else if user != nil { // pushing subscriber's profile on new subscriber tapped
            let profileVC = MyProfileViewController.makeOne(with: user!, mode: .other)
            
            if let nav = rootNav {
                nav.pushViewController(profileVC, animated: true)
            } else {
                UIApplication.shared.keyWindow?.rootViewController?.present(profileVC, animated: true, completion: nil)
            }
            NotificationsManager.shared.removeNotification()
        } else if offer != nil { // pushing place's profile with opened offer's list on offer tapped
           showHUD()
            Server.makeRequest.getPlace(uuid: offer!.place_uuid, completion: { (place) in
                if place != nil {
                    async {
                       dismissHUD()
                        let placeProfile = PlaceProfileViewController.makeOne(with: place!, shouldOpenOffers: true)
                        if let nav = rootNav {
                            nav.pushViewController(placeProfile, animated: true)
                        } else {
                            UIApplication.shared.keyWindow?.rootViewController?.present(placeProfile, animated: true, completion: nil)
                        }
                    }
                }
            })
            NotificationsManager.shared.removeNotification()
        } else if checkin_comment != nil { // opening my profile on checkin comment tapped
           showHUD()
            
            Server.makeRequest.getCheckin(by: checkin_comment!.checkin_uuid, completion: { (checkin) in
                async {
                   dismissHUD()
                    let virtualButton = ShowCommentsButton()
                    virtualButton.post = Post.init(with: Searchable(checkin: checkin)!)
                    (UIApplication.shared.keyWindow?.rootViewController?.presentedViewController ?? UIApplication.shared.keyWindow?.rootViewController)?.showComments(virtualButton)
                }
            })
            NotificationsManager.shared.removeNotification()
        } else if accept_comment != nil { // opening my profile on accept comment tapped
            
           showHUD()
            
            Server.makeRequest.getAcceptedEvent(by: accept_comment!.accept_uuid, completion: { (accept) in
                
                async {
                   dismissHUD()
                    if accept != nil {
                        let virtualButton = ShowCommentsButton()
                        virtualButton.post = Post.init(with: Searchable(accept: accept!)!)
                        (UIApplication.shared.keyWindow?.rootViewController?.presentedViewController ?? UIApplication.shared.keyWindow?.rootViewController)?.showComments(virtualButton)
                    }
                }
            })
                menuContent.openMyProfile()
            NotificationsManager.shared.removeNotification()
        } else if message != nil { //open chat with user and scroll to selected message (scroll is optional)
            
            menuContent.openChats()
            NotificationsManager.shared.removeNotification()

            if let localChat = CoreDataManager.shared.chat(with: message!.chat_uuid) {
                let chatProfile = ChatViewController.makeOne(with: localChat)
                async(after: 50) {
                    (menuContent.selectedViewController as? UINavigationController)?.pushViewController(chatProfile, animated: true)
                }
            }
        }
    }
    
    
    func onTapped(with userInfo: [AnyHashable: Any], type: AppDelegate.notificationTypes) {
        switch type {
        case .acceptComment:
            let comment = NotificationAcceptComment(userInfo: userInfo)
            self.onTapped(accept_comment: comment)
        case .checkinComment:
            let comment = NotificationCheckinComment(userInfo: userInfo)
            self.onTapped(checkin_comment: comment)
        case .newOffer:
            let offer = NotificationOffer(userInfo: userInfo)
            self.onTapped(offer: offer)
        case .newSubscriber:
            let subscriber = User(userInfo: userInfo)
            self.onTapped(user: subscriber)
        case .chatMessage:
            let message = Message.makeMessage(userInfo: userInfo)
            self.onTapped(message: message)
        }
    }
    
    
    func removeNotification() {
        NotificationsUICoordinator.hideForced()
    }
    
    func userInteractionOccured() {
       NotificationsUICoordinator.onInteracted()
    }
    
    func postPlaceUpdatedNotifications() {
        NotificationCenter.default.post(name: .places_update_notification, object: nil)
    }


    
}



class NotificationsUICoordinator {
    
    private static let shared = NotificationsUICoordinator()
    private var window: NotificationsWindow!
    
    private var pendingItems: [(Any, Bool)] = [] {
        didSet {
            if pendingItems.count != 0 {
                startUpdatingUI()
            }
        }
    }
    
    class func isSupported(_ item: Any) -> Bool {
        return item is Message || item is NotificationOffer || item is NotificationCheckinComment || item is NotificationAcceptComment || item is Place ||  item is User
    }
    
    class func show(item: Any, in window: NotificationsWindow, isOverMPC: Bool = false) {
        guard isSupported(item) else { return }
        shared.window = window
        shared.pendingItems.append(item, isOverMPC)
    }
    
    private var isUpdatingUI: Bool {
        get {
            return updatingUITimer != nil
        }
    }
    private var updatingUITimer: Timer!
    private var currentItemShowningDate: Double? = nil
    private(set) var currentNotificationController: NotificationsViewController?
    
    static var currentNotificationController: NotificationsViewController? {
        get {
            return shared.currentNotificationController
        }
    }
    
    private func startUpdatingUI() {
        guard !isUpdatingUI else { return }
        updatingUITimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateUI), userInfo: nil, repeats: true)
    }
    
    private func stopUpdatingUI() {
        guard isUpdatingUI else { return }
        updatingUITimer?.invalidate()
        updatingUITimer = nil
    }
    
    @objc private func updateUI() {
        
        if let current = currentItemShowningDate {
            if Date().timeIntervalSinceReferenceDate - current >= 3 {

                self.hideNotification()
            }
        } else if pendingItems.count != 0 {
            self.showNotification(for: pendingItems.first?.0, isOverMPC: pendingItems.first?.1 ?? false)
        }
    }
    
    class func hideForced() {
        shared.pendingItems = []
        shared.currentItemShowningDate = nil
        shared.currentNotificationController?.dismissNotificationAnimated {
            shared.currentItemShowningDate = nil
            shared.currentNotificationController = nil
        }
    }
    
    class func onInteracted() {
        shared.currentItemShowningDate = Date().timeIntervalSinceReferenceDate
    }
    
    @objc private func hideNotification(completion: (()->Void)?=nil) {
        
        currentNotificationController?.dismissNotificationAnimated {
            self.currentItemShowningDate = nil
            completion?()
            self.currentNotificationController = nil
        }

    }
    
    @objc func showNotification(for item: Any?, isOverMPC: Bool = false, completion: (()->Void)?=nil) {
        guard areNotificationsAllowed else { completion?(); return }
        
        if pendingItems.count != 0 {
            _ = pendingItems.removeFirst()
        }

        if let controller = notificationController(for: item, isOverMPC: isOverMPC) {
                self.currentItemShowningDate = Date().timeIntervalSinceReferenceDate
                FeebackService.designate.place_located()
                window.rootViewController = controller
                currentNotificationController = controller
                controller.presentNotificationAnimated {
                    completion?()
                }
        } else {
            completion?()
        }
    }
    
    
    private func notificationController(for item: Any?, isOverMPC: Bool = false) -> NotificationsViewController? {
        if let message = item as? Message {
            return NotificationsViewController.makeOne(message: message)
        } else if let offer = item as? NotificationOffer {
            return NotificationsViewController.makeOne(offer: offer)
        } else if let checkinComment = item as? NotificationCheckinComment {
            return NotificationsViewController.makeOne(checkinComment: checkinComment)
        } else if let acceptComment = item as? NotificationAcceptComment {
            return NotificationsViewController.makeOne(acceptComment: acceptComment)
        } else if let place = item as? Place {
            return NotificationsViewController.makeOne(place: place)
        } else if let user = item as? User {
            return NotificationsViewController.makeOne(user: user, isOverMPC: isOverMPC)
        } else {
            return nil
        }
        
    }
    

    
    
}
