//
//  MainTabsController.swift
//  Heloo2017
//
//  Created by Константин on 29.07.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import CoreData

class MainTabsController: UITabBarController, UITabBarControllerDelegate {
    
    @IBOutlet weak var placesTab: UITabBarItem!
    @IBOutlet weak var peopleTab: UITabBarItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDataManager.instance.isTutorialCompletedOnPlaces {
            AppDelegate.configureNotifications()
        }
        
        self.delegate = self
        Presenter.instance.configureNavbarAppearance()
        configurePeoplePlaces()
        subscribeToUpdates()
        
        if !UserDataManager.isAuthorized {
            self.selectedIndex = 1
        }
        
        self.tabBar.items?.forEach({ item in
            item.setTitleTextAttributes([
                NSForegroundColorAttributeName: UIColor(hex: "D1D2D8"),
                NSFontAttributeName: UIFont(name: "AvenirNext-Medium", size: 10)!
                ], for: .normal)
            
            item.setTitleTextAttributes([
                NSForegroundColorAttributeName: UIColor.black,
                NSFontAttributeName: UIFont(name: "AvenirNext-Medium", size: 10)!
                ], for: .selected)
        })
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !UserDataManager.instance.isTutorialCompletedOnNews {
            let tutorial = TutorialViewController.make(with: .news)
            async {
                self.present(tutorial, animated: true, completion: nil)
            }
        }
        
        NotificationsManager.shared.executePending()
        MPCManager.shared.startSearchingIfNeeded()
    }
    
    
    
    func subscribeToUpdates() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatePlaces), name: .viewed_places_update_notification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatePlaces), name: .places_update_notification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUsers), name: .viewed_people_update_notification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUsers), name: .people_update_notification, object: nil)
        _ = fetchController
        
        updatePlaces()
    }
    
    fileprivate lazy var fetchController: NSFetchedResultsController<Chat>! = {
        let fetch = CoreDataManager.shared.chatsListController()
        fetch.delegate = self
        try? fetch.performFetch()
        return fetch
    }()
    
    @objc func updatePlaces() {
        let freshPlacesCount = Server.makeRequest.freshPlaces.count
        self.tabBar.items?[1].badgeValue = freshPlacesCount == 0 ? nil : String(freshPlacesCount)
        if #available(iOS 10.0, *) {
            self.tabBar.items?[1].badgeColor = UIColor(hex: "3AAAFE")
        }
    }
    
    @objc func updateChats() {
        let unreadedMessagesCount = fetchController.fetchedObjects?.reduce(0, { (res, chat) -> Int in
            return res + chat.unreaded_count
        }) ?? 0
        UIApplication.shared.applicationIconBadgeNumber = unreadedMessagesCount
        self.tabBar.items?[3].badgeValue = unreadedMessagesCount == 0 ? nil : String(unreadedMessagesCount)
        if #available(iOS 10.0, *) {
            self.tabBar.items?[3].badgeColor = UIColor(hex: "3AAAFE")
            
        }
    }
    
    @objc func updateUsers() {
        let freshUsers = MPCManager.shared.freshUsers.count
        self.tabBar.items?[2].badgeValue = freshUsers == 0 ? nil : String(freshUsers)
        if #available(iOS 10.0, *) {
            self.tabBar.items?[2].badgeColor = UIColor(hex: "3AAAFE")
        }
    }
    
    func configurePeoplePlaces() {
        self.viewControllers?[1].tabBarItem = placesTab
        self.viewControllers?[2].tabBarItem = peopleTab
        
        let placesVC = (self.viewControllers?[1] as? UINavigationController)?.viewControllers.first as? PeoplePlacesViewController
        let peopleVC = (self.viewControllers?[2] as? UINavigationController)?.viewControllers.first as? PeoplePlacesViewController
        
        placesVC?.type = .places
        peopleVC?.type = .people
        
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        dismissHUD()

        guard !UserDataManager.isAuthorized else { return true }
        
        let restID = viewController.restorationIdentifier
        if restID == "news_nav" || /*restID == "more_nav" ||*/ restID == "chats_nav" {
            ErrorPopupManager.showError(type: .access)
            return false
        } else {
            return true
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TabBarTransitioningAnimated(delegate: self)
    }
    
    

    func openNearbyPlaces() {
        async {
            self.selectedIndex = 1
            async(after: 50, {
                if let placeList = (self.selectedViewController as? UINavigationController)?.viewControllers.first as? PeoplePlacesViewController {
                    placeList.changeMode(placeList.around_mode_button)
                }
            })

        }

    }
    
    
    func openMyProfile() {
        async {
            self.selectedIndex = 4
            async(after: 50, {
                if let more = (self.selectedViewController as? UINavigationController)?.viewControllers.first as? MoreViewController {
                    more.userSegue?.perform()
                }
            })
        }
    }
    
    func openChats() {
        async {
            self.selectedIndex = 3
        }

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return self.selectedViewController?.preferredStatusBarStyle ?? .default
        }
    }
    
}

extension MainTabsController: TabBarTransitioningDelegate {
    func directionIsRight(from: UIViewController, to: UIViewController) -> Bool {
        guard let fromIndex = self.viewControllers?.index(of: from), let toIndex = self.viewControllers?.index(of: to) else { return true }
        return toIndex > fromIndex
    }
}

extension MainTabsController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        updateChats()
    }
}


func defaultSpringAnimation(duration: Double = 0.3, usingSpringWithDamping: CGFloat=0.7, isInteractive: Bool = false, animations: @escaping ()->Void, completion: (()->Void)?=nil) {
    
    UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: usingSpringWithDamping, initialSpringVelocity: 0.2, options: isInteractive ? [.allowUserInteraction, .preferredFramesPerSecond60, .curveEaseIn] : [.preferredFramesPerSecond60, .curveEaseIn], animations: {
        animations()
    }) { (success) in
        completion?()
    }
}
