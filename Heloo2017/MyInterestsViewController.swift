//
//  MyInterestsViewController.swift
//  Heloo2017
//
//  Created by Константин on 02.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class MyInterestsViewController: CardViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    class func createOne() -> MyInterestsViewController {
        let new = UIStoryboard(name: "InterestsBase", bundle: nil).instantiateViewController(withIdentifier: "MyInterestsViewController") as! MyInterestsViewController
        new.modalTransitionStyle = .coverVertical
        new.modalPresentationStyle = .overCurrentContext
        return new
    }

    @IBOutlet weak var scrollHiddenConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollVisibleConstraint: NSLayoutConstraint!
    @IBOutlet weak var blue_blur: UIImageView!
    @IBOutlet weak var interests_icon_big: UIImageView!
    @IBOutlet weak var description_label: UILabel!
    @IBOutlet weak var cardViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var collectionItems: UICollectionView!
    @IBOutlet weak var interests_icon: UIImageView!
    @IBOutlet weak var background_card: UIView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var comments_label: UILabel!
    @IBOutlet weak var user_icon: UIImageView!
    @IBOutlet weak var card_main_top_Space: NSLayoutConstraint!
    
    
    let card_main_top_Space_normal: CGFloat = 50
    var bigSL: CAShapeLayer = CAShapeLayer()
    var smallSL: CAShapeLayer = CAShapeLayer()
    
    
    var viewer: InterestsViewer!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionItems.reloadData()
        self.animateAppearance()
    }

    
    func animateAppearance() {
        user_icon.sd_setImage(with: URL(string: UserDataManager.instance.currentUser.image), placeholderImage: UIImage(named: "blue_blur_rect"))
        
        async {
            
            UIView.animate(withDuration: 0.3) {
                self.user_icon.alpha = 1
                self.blue_blur.alpha = 0.8
            }
            
            self.scrollHiddenConstraint.isActive = false
            self.scrollVisibleConstraint.isActive = true
            
            UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
                self.background_card.alpha = 0.75
            }, completion: nil)
            self.card_main_top_Space.constant = self.card_main_top_Space_normal
            self.animateChanges()
        }
    }
    
    private func animateChanges() {
        UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    
    @IBAction func edit(_ sender: UIButton) {
        let edit = InterestsEditorViewController.makeOne()
        edit.previousStatusBarStyle = UIApplication.shared.statusBarStyle
        edit.completion_block = {
            self.viewer = InterestsViewer(mainSource: UserDataManager.instance.availableInterests)
            self.collectionItems.reloadData()
        }
        self.present(edit, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewer = InterestsViewer(mainSource: UserDataManager.instance.availableInterests)
        
        self.scroll = self.scrollView
        self.scrollView.delegate = self
        self.view.layoutIfNeeded()
        self.collectionItems.contentInset.bottom = 88

        self.animatableViews = [comments_label: 1.0, close_button: 1.0]
        collectionItems.delegate = self
        collectionItems.dataSource = self
        
        if viewer.currentInterests().count == 0 {
            self.interests_icon_big?.image = #imageLiteral(resourceName: "interests_placeholder")
            self.description_label?.text = "Вы пока что не добавили ни одного интереса"
        }
    }
    

    
    @IBAction func close(_ sender: UIButton) {
        self.makeCoverVerticalDismiss()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewer.currentInterests().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let currentItem = viewer.item(at: indexPath.item) else { return collectionItems.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) }
        let cell = collectionItems.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! InterestCollectionViewCell
        cell.update(with: currentItem, isSelected: viewer.isActive(currentItem))
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let current = viewer.item(at: indexPath.item) else { return .zero }
        let hasBackground =  current.parent == nil
        return CGSize(width: current.title.requiredWidth(14, font: UIFont.systemFont(ofSize: 14, weight: hasBackground ? UIFontWeightMedium : UIFontWeightRegular), height: 24 + 10) + 40, height: 36 + 10)
        
    }
    
    
    
    
}
