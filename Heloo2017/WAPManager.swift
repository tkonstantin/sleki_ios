//
//  WAPManager.swift
//  Heloo2017
//
//  Created by Константин on 16.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import UIKit
import Reachability


class WAPManager {
    
    static let instance = WAPManager()
    fileprivate let reachability = Reachability()
    
    var _allWaps: [WAP] = []
    var allWAPs: [WAP] {
        get {
            if _allWaps.count == 0 {
                _allWaps = CoreDataService.instance.allWaps()
            }

            return _allWaps
        } set {
            _allWaps = newValue
        }
    }
    var activeWAPs: [WAP] {
        get {
            return allWAPs.filter({$0.isActual})
        }
    }
    
    
    func startScanning() {
        downloadAllWaps()
    }
    
    private func downloadAllWaps() {
        WapDowloader().downloadAllWaps { (waps) in
            self.allWAPs = waps
            NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(_:)), name: .reachabilityChanged, object: self.reachability)
            try? self.reachability?.startNotifier()
        }
    }
    
    @objc private func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
            startRetrievingBSSID()
        case .cellular:
            print("Reachable via Cellular")
            pauseRetrievingBSSID()
        case .none:
            print("Network not reachable")
            pauseRetrievingBSSID()
        }
    }
    
    private var scanningTimer: Timer?
    
    private func startRetrievingBSSID() {
        scanningTimer?.invalidate()
        scanningTimer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(self.retrieveBSSID), userInfo: nil, repeats: true)
    }
    
    @objc private func retrieveBSSID() {
        if let bssid = Server.makeRequest.getСurrentBSSID() {
            if let index = allWAPs.index(where: {$0.bssid.uppercased() == bssid.uppercased()}) {
                self.notifyAboutFoundedWAP(with: allWAPs[index])
            }
        }
    }
    
    private func notifyAboutFoundedWAP(with wap: WAP) {
        if Date().timeIntervalSince1970 - wap.lastSeenDate.timeIntervalSince1970 > beacon_timeout_seconds {
            getPlaceToShowNotification(by: wap, completion: { (places) in
                for place in places {
                    self.showNotification(with: place, wapUUID: wap.uuid)
                }
            })
        }
    }
    
    private func getPlaceToShowNotification(by wap: WAP, completion: @escaping ([Place]) -> Void) {
        Server.makeRequest.searchPlaces(by: [wap], completion: { (places) in
            self.showLocalUINotification("Пассивно зачекинились в \(places.first?.name ?? "неизвестном месте")")
            wap.lastSeenDate = Date()
            CoreDataService.instance.save()
            completion(places)
        })
    }
    
    private func showNotification(with p: Place?, wapUUID uuid: String) {
        guard let place = p else { return }
        
        guard BeaconManager.instance.shownedNotifications[uuid] == nil else { return }
        
        if Date().timeIntervalSince1970 - (Array(BeaconManager.instance.shownedNotifications.values).max()?.timeIntervalSince1970 ?? 0) >= 10 { //проверили шо  с последнего уведомления прошло 10с
            async {
                BeaconManager.instance.shownedNotifications[uuid] = Date()
                NotificationsManager.shared.showNotification(with: place)
                NotificationsManager.shared.postPlaceUpdatedNotifications()
            }
        } else {
            //если с последнего увеедомления не прошло 10с - пробуем заново через 5с
            async(after: 5000, {
                self.showNotification(with: p, wapUUID: uuid)
            })
        }

    }
    

    func nearPlacesHadBeenShown() {
        for wap in self.activeWAPs {
            BeaconManager.instance.shownedNotifications[wap.uuid] = Date()
        }
        let activeUUIDs: Set<String> = Set(Server.makeRequest.activePlaces.map({$0.uuid}))
        Server.makeRequest.viewedPlacesUUIDsToday = Server.makeRequest.viewedPlacesUUIDsToday.union(activeUUIDs)
    }
    
    
    func showLocalUINotification(_ text: String, needsSound: Bool = false) {
//        let n = UILocalNotification()
//        n.alertTitle = "Тестируем беконы"
//        n.alertBody = text
//        if needsSound {
//            n.soundName = UILocalNotificationDefaultSoundName
//        }
//
//        UIApplication.shared.presentLocalNotificationNow(n)
    }
    
    
    private func pauseRetrievingBSSID() {
        scanningTimer?.invalidate()
        scanningTimer = nil
    }
    
    
}



fileprivate class WapDowloader {
    
    private var currentPage = -1
    private var hasNext = true
    private var waps: [WAP] = []
    
    func downloadAllWaps(completion: @escaping ([WAP])->Void) {
        downloadPage(currentPage) {
            if self.hasNext {
                self.downloadAllWaps(completion: completion)
            } else {
                completion(self.waps)
                return;
            }
        }
    }
    
    private var lastWapsFetch: Double {
        get {
            return UserDefaults.standard.double(forKey: "lastWapsFetch0")
        } set {
            UserDefaults.standard.set(newValue, forKey: "lastWapsFetch0")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    private func downloadPage(_ page: Int, completion: @escaping ()->Void) {
        currentPage += 1
        guard currentPage != 0 || Date().timeIntervalSince1970 - lastWapsFetch > 60*60*24/2 else {
            self.waps = CoreDataService.instance.allWaps()
            self.hasNext = false
            completion()
            return
            
        }
        
        if currentPage == 0 {
            CoreDataService.instance.clearWaps()
        }
        
        Server.makeRequest.getAvailableWAPs(page: currentPage) { (_waps, _hasNext) in
            self.waps += _waps
            self.hasNext = _hasNext
            if !self.hasNext {
                self.lastWapsFetch = Date().timeIntervalSince1970
            }
            completion()
        }
    }
}
