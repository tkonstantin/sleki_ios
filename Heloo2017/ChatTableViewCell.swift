//
//  ChatTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 18.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class ChatTableViewCell: CommentTableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var profileButton: ProfileButton!
    @IBOutlet weak var unread_count_label: UILabel!
    @IBOutlet weak var unread_count_view: UIView!
    
    func update(with chat: Chat) {
        self.name_label.text = chat.displayTitle
        (self.name_label as? UsernameLabel)?.user = chat.users.first
        self.user_icon.sd_setImage(with: URL(string: chat.displayAvatar ?? ""), placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
        
        let sender = (chat.users.first(where: {$0.uuid == chat.realyLatestMessage?.sender?.uuid}) ?? chat.realyLatestMessage?.sender)
        var senderText = sender?.firstName
        if UserDataManager.instance.isMe(sender) {
            senderText = "Вы"
        }
        var textParts = [senderText, chat.realyLatestMessage?.text].compactMap({$0 ?? ""})
        
        if !textParts.first!.isEmpty {
            textParts[0] += ":"
        }
        
        self.text_label.text = textParts.joined(separator: " ")
        
    
        if let date = chat.realyLatestMessage?.date {
            if Calendar.current.isDateInToday(date) {
                self.dateLabel.text = date.parseTime()
            } else {
                self.dateLabel.text = date.parseDateShort(dots: true)
            }
        } else {
            self.dateLabel.text = ""
        }
        
        self.unread_count_label.text = chat.unreaded_count == 0 ? "" : "\(chat.unreaded_count)"
        self.unread_count_view.isHidden = chat.unreaded_count == 0
    }
}
