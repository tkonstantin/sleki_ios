//
//  InterestsViewer.swift
//  Heloo2017
//
//  Created by Константин on 04.07.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


protocol InterestViewerDelegate: class {
    func currentInterests() -> [TreeItem]
    func item(at index: Int) -> TreeItem?
}

class InterestsViewer {
    
    private var activeInterests: [TreeItem] = []
    
    func isActive(_ interest: TreeItem) -> Bool {
        return activeInterests.contains(where: {$0.key == interest.key})
    }
    
    
    private var _currentInterests: [TreeItem] = []
    
    fileprivate var current: [TreeItem] {
        get {
            return _currentInterests
        } set {
            let filteredInterests = newValue.makeFlat().filter({ interest in
                if isActive(interest) {
                    return true
                } else if interest.associatedItems.contains(where: { isActive($0) }) {
                    return true
                }
                return false
            })
            
            _currentInterests = filteredInterests.sorted(by: {
                
                let first = $0.parent?.title ?? $0.title
                let second = $1.parent?.title ?? $1.title
                
                if first == second {
                    if $0.associatedItems.count == $1.associatedItems.count {
                        return $0.title < $1.title
                    } else {
                        return $0.associatedItems.count > $1.associatedItems.count
                    }
                } else {
                    return first < second
                }
            })
        }
    }
    
    
    init(comparingInterests: [TreeItem], mainSource: [TreeItem]) {
        activeInterests = comparingInterests
        current = mainSource
    }
    
    init(mainSource: [TreeItem]) {
        activeInterests = UserDataManager.instance.currentUser?.interests ?? []
        current = mainSource
    }
    
    var mainSource: [TreeItem] = []
    

}

extension InterestsViewer: InterestViewerDelegate {
    
    func currentInterests() -> [TreeItem] {
        return current
    }
    
    func item(at index: Int) -> TreeItem? {
        guard current.count > index, index >= 0 else { return nil }
        return current[index]
    }
}
