//
//  NotificationsViewController.swift
//  Heloo2017
//
//  Created by Константин on 19.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class NotificationOffer {
    
    var offer_uuid: String!
    var offer_name: String!
    var place_uuid: String!
    var place_avatar: String!
    var place_name: String!
    
    init(userInfo: [AnyHashable: Any]) {
        if let offer_dict = userInfo["offer"] as? NSDictionary {
            initialize(offer_dict: offer_dict)
        } else if let offer_string = userInfo["offer"] as? String {
            if let offer_dict = try? JSONSerialization.jsonObject(with: offer_string.data(using: .utf8) ?? Data(), options: []) as? NSDictionary, offer_dict != nil {
                initialize(offer_dict: offer_dict!)
            }
        }
        
        if let place_dict = userInfo["place"] as? NSDictionary {
            initialize(place_dict: place_dict)
        } else if let place_string = userInfo["place"] as? String {
            if let place_dict = try? JSONSerialization.jsonObject(with: place_string.data(using: .utf8) ?? Data(), options: []) as? NSDictionary, place_dict != nil {
                initialize(place_dict: place_dict!)
            }
        }
        
    }
    
    private func initialize(offer_dict: NSDictionary) {
        offer_uuid = offer_dict["uuid"] as? String ?? ""
        offer_name = offer_dict["title"] as? String ?? ""
    }
    
    private func initialize(place_dict: NSDictionary) {
        place_uuid = place_dict["uuid"] as? String ?? ""
        place_name = (place_dict["name"] as? String ?? "").uppercasedByFirstLetter()
        place_avatar = place_dict["avatar_url"] as? String ?? ""
    }
    
}

class NotificationCheckinComment {
    var comment_uuid = ""
    var checkin_uuid = ""
    var person: User!
    
    init(userInfo: [AnyHashable: Any]) {
        comment_uuid = userInfo["checkIn_comment_uuid"] as? String ?? ""
        checkin_uuid = userInfo["checkin_uuid"] as? String ?? ""
        person = User(userInfo: userInfo)
    }
}

class NotificationAcceptComment {
    var comment_uuid = ""
    var accept_uuid = ""
    var person: User!
    
    init(userInfo: [AnyHashable: Any]) {
        comment_uuid = userInfo["accepted_event_comment_uuid"] as? String ?? ""
        accept_uuid = userInfo["accepted_event_uuid"] as? String ?? ""
        person = User(userInfo: userInfo)
    }
}

class NotificationsViewController: UIViewController {
    
    class func makeOne(place: Place?=nil, user: User?=nil, offer: NotificationOffer?=nil, checkinComment: NotificationCheckinComment?=nil, acceptComment: NotificationAcceptComment?=nil, message: Message?=nil, isOverMPC: Bool = false) -> NotificationsViewController {
        let storyboardIdentifier: String = {
            if place != nil {
                return "place"
            } else if user != nil {
                return "user"
            } else if offer != nil {
                return "offer"
            } else {
                return "comment"
            }
        }()
        let new = UIStoryboard(name: "NotificationControllers", bundle: nil).instantiateViewController(withIdentifier: storyboardIdentifier) as! NotificationsViewController
        new.isOverMPC = isOverMPC
        new.place = place
        new.message = message
        new.user = user
        new.offer = offer
        new.checkinComment = checkinComment
        new.acceptComment = acceptComment
        return new
    }
    
    fileprivate let defaultTop: CGFloat = 8
    
    fileprivate var isOverMPC = false
    
    fileprivate var hiddenTop: CGFloat = -150
    
    
    @IBOutlet weak var notificationsView: NotificationView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    
    private var place: Place?
    private var user: User?
    private var offer: NotificationOffer?
    private var checkinComment: NotificationCheckinComment?
    private var acceptComment: NotificationAcceptComment?
    private var message: Message?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationsView?.place = place
        notificationsView?.user = user
        notificationsView?.offer = offer
        notificationsView?.checkinComment = checkinComment
        notificationsView?.acceptComment = acceptComment
        notificationsView?.message = message
        notificationsView?.isOverMPC = isOverMPC
        notificationsView?.update()
        
    }
    
    @IBAction func panned(_ sender: UIPanGestureRecognizer) {
        guard self.notificationsView != nil else { return }
        
        let translation = sender.translation(in: self.notificationsView!).y
        let newOriginY = self.defaultTop + translation
        
        if translation < -16 && (sender.state == .ended || sender.state == .cancelled || sender.state == .failed) {
            sender.isEnabled = false
            NotificationsManager.shared.removeNotification()
        } else {
            self.topConstraint.constant = min(150, newOriginY)
            NotificationsManager.shared.userInteractionOccured()
            if sender.state == .ended || sender.state == .cancelled || sender.state == .failed {
                self.presentNotificationAnimated(duration: 0.25)
            }
        }
        
        
        
    }
    
    @IBAction func tapped(_ sender: UITapGestureRecognizer) {
        sender.isEnabled = false
        
        NotificationsManager.shared.onTapped(place: place, user: user, offer: offer, checkin_comment: checkinComment, accept_comment: acceptComment, message: message)
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        NotificationsManager.shared.removeNotification()
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return UIApplication.shared.keyWindow?.rootViewController?.prefersStatusBarHidden ?? false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return UIApplication.shared.keyWindow?.rootViewController?.preferredStatusBarStyle ?? .default
        }
    }
}

extension NotificationsViewController { //animations
    
    func presentNotificationAnimated(duration: Double = 0.4, completion: (()->Void)?=nil) {
        async {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            self.topConstraint.constant = self.defaultTop
            UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: { _ in
                completion?()
            })
        }
        
    }
    
    func dismissNotificationAnimated(completion: @escaping ()->Void) {
        async {
            
            self.topConstraint.constant = self.hiddenTop
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: {_ in
                completion()
            })
        }
        
    }
}
