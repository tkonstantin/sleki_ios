//
//  AvatarCropperDelegate.swift
//  Heloo2017
//
//  Created by Константин on 17.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol AvatarCropperDelegate: class {
    func cropperDidPick(image: UIImage, smallImage: UIImage)
    func cropperDidCancel()
}
