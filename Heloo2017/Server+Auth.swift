//
//  Server+Auth.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire


extension Server {
    
    var header: HTTPHeaders? {
        get {
            if let data = KeychainService.loadData(for: auth_token_key) {
                return NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! HTTPHeaders?
            } else {
                return nil
            }
        }
        set {
            if newValue != nil {
                KeychainService.save(data: NSKeyedArchiver.archivedData(withRootObject: newValue!) as NSData, for: auth_token_key)
            } else {
                KeychainService.save(data: NSData(), for: auth_token_key)
            }
        }
    }
    
    
    func auth(username: String="admin", password: String="admin", completion: @escaping ((Bool)->Void)) {
        if self.checkNetwork() {
            var params = [url_fields.auth.username : username]
            params[url_fields.auth.password] = password
            
            Alamofire.request(url_paths.base_auth, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                if let json = response.json, let token = json["token"] as? String {
                    self.header = ["Authorization": token]
                    UserDataManager.instance.currentUser = User(with: [:])
                    self.getAvailableInterests()
                    self.getAvailablePlaceTypes()
                    sendPushToken()
                    KeychainService.saveAuthCredentials(username: username, password: password)
                    MPCManager.shared.startSearchingIfNeeded()
                    completion(true)
                } else {
                    completion(false)
                }
            }
        } else {
            completion(false)
        }
    }
    

    
    
    func reg(email: String, pass: String, completion: @escaping (Bool, String?)->Void) {
        if self.checkNetwork() {
            var params = [url_fields.reg.email: email]
            params[url_fields.reg.password] = pass
            params[url_fields.reg.passwordConfirm] = pass
            
            Alamofire.request(url_paths.base_reg, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                if let json = response.json, let token = json["token"] as? String {
                    UserDataManager.instance.isRegInProgress = true
                    self.header = ["Authorization": token]
                    UserDataManager.instance.currentUser = User(with: json)
                    self.getAvailableInterests()
                    self.getAvailablePlaceTypes()
                    sendPushToken()
                    MPCManager.shared.startSearchingIfNeeded()
                    completion(true, nil)
                } else {
                    completion(false, (response.result.value as? NSDictionary)?["errorMessage"] as? String)
                }
            })
            
        } else {
            completion(false, "Нет сети")
        }
    }
    
    
    func vk_auth(token: String, completion: @escaping (Bool, Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.vk_auth, method: .post, parameters: ["token": token], encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if let json = response.json{
                self.header = ["Authorization": json["token"] as! String]
                let user = User(with: json)
                UserDataManager.instance.currentUser = user
                self.getAvailableInterests()
                self.getAvailablePlaceTypes()
                sendPushToken()
                completion(true, json["already_exists"] as? Bool ?? false)
            } else {
                completion(false, false)
            }
        }
    }
    
    func passwordRecovery_request(email: String, completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.password.request, method: .post, parameters: ["email": email], encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            completion(response.json == nil)
        }
    }
    
    func passwordRecovery_complete(pass: String, token: String, completion: @escaping (Bool)->Void) {
        var params: [String: String] = [:]
        params["password1"] = pass
        params["password2"] = pass
        params["confirmationToken"] = token
        Alamofire.request(url_paths.base + url_paths.password.complete, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if let json = response.json, let token = json["token"] as? String {
                self.header = ["Authorization": token]
                UserDataManager.instance.currentUser = User(with: [:])
                self.getAvailableInterests()
                self.getAvailablePlaceTypes()
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    
    func refreshToken(completion: @escaping (Bool)->Void) {
        Alamofire.request("http://185.5.251.55/refresh", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let token = response.json?["token"] as? String {
                self.header = ["Authorization": token]
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    
    
    
    func logout(completion: ()->Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        UserDataManager.instance.currentUser = nil
        CoreDataService.instance.eraseChatsAndMessages()
        self.header = nil
        completion()
    }
    
    
    
}
