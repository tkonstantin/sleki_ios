//
//  CardViewController.swift
//  Heloo2017
//
//  Created by Константин on 08.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class TableViewContainingViewController: UIViewController {
    weak var table: UITableView?
}

class CardViewController: TableViewContainingViewController, UITableViewDelegate, UITableViewDataSource {
    
    var previousStatusBarStyle: UIStatusBarStyle!
    
    var canResetFooter: Bool {
        get {
            return true
        }
    }
    
    override weak var table: UITableView? {
        didSet {
            if canResetFooter {
                table?.tableFooterView = UIView()
            }
        }
    }
    weak var scroll: UIScrollView?
    
    var isWatching = true
    var animatableViews: [UIView: CGFloat] = [:]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        async(after: 300) { [weak self] in
            self?.animateAll(alpha: 1)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.presentingViewController?.navigationController?.navigationBar.alpha = 1.0
        (self.presentingViewController as? UINavigationController)?.navigationBar.alpha = 1.0
        
    }
    
    
    func updateFooter() {
        self.view.layoutIfNeeded()
        self.table?.tableFooterView?.frame.size.height = (self.table?.frame.size.height ?? 0) - (self.table?.contentSize.height ?? 0)
    }
    
    
    func configureTable(needFooter: Bool=true) {
        self.table?.delegate = self
        self.table?.dataSource = self
        let footer = UIView()
        footer.backgroundColor = .white
        self.table?.tableFooterView = footer
        if needFooter {
            self.view.layoutIfNeeded()
            self.table?.tableFooterView?.frame.size.height = (self.table?.frame.size.height ?? 0) - (self.table?.contentSize.height ?? 0)
        }
        
    }
    
    
    func animateAll(alpha: CGFloat, duration: TimeInterval=0.3) {
        UIView.animate(withDuration: duration) { [weak self] in
            guard let `self` = self else { return }
            for views in Array(self.animatableViews.keys) {
                views.alpha = alpha/self.animatableViews[views]!
            }
        }
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table?.dequeueReusableCell(withIdentifier: "cell") ?? UITableViewCell()
        return cell
    }
    
    
}
