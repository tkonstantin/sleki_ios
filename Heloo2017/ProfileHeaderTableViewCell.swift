//
//  ProfileHeaderTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 21.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ProfileHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var txtLabel: UILabel!
    
    
    func update(with text: String?) {
        txtLabel.text = text
    }

}
