//
//  SettingsViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SelectCoutry, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SDatePickerDelegate {
    
    class func make() -> SettingsViewController {
        let new = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        return new
    }
    
    @IBOutlet weak var save_button: UIButton!
    @IBOutlet weak var tableItems: RefreshableTable!
    
    var savedLeftBarButtons: [UIBarButtonItem]?
    var savedRightBarButtons: [UIBarButtonItem]?
    
    
    var hasEditedSomething: Bool = false {
        didSet {
            save_button.isHidden = !hasEditedSomething
        }
    }
    
    var regData: regData = (name: "", lastName: "", birthday: nil, gender: nil, city: "", city_tag: "", country: "", country_tag: "") {
        didSet {
            hasEditedSomething = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.set(title: "НАСТРОЙКИ")
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        self.addTapOutsideGestureRecognizer()
        reload()
    }
    
    
    
    
    func reload() {
        regData.name = UserDataManager.instance.currentUser.firstName
        regData.lastName = UserDataManager.instance.currentUser.lastName
        regData.birthday = UserDataManager.instance.currentUser.date_of_birth
        regData.gender = UserDataManager.instance.currentUser.gender
        regData.city = UserDataManager.instance.currentUser.city
        regData.city_tag = UserDataManager.instance.currentUser.city_tag
        regData.country = UserDataManager.instance.currentUser.country
        regData.country_tag = UserDataManager.instance.currentUser.country_tag
        self.tableItems.reloadData()
        self.hasEditedSomething = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reload()
        self.navigationController?.navigationBar.tintColor = .black
    }
    
    @IBAction func save(_ sender: UIButton) {
        self.view.endEditing(true)
        showHUD()
        Server.makeRequest.updateMyProfile(data: regData) {
            dismissHUD()
            self.reload()
            DoneViewController.presentOne(with: "Настройки сохранены")
            self.hasEditedSomething = false
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var shouldBegin = false
        switch textField.tag {
        case 2:
            showBirthdayPicker()
        case 3:
            showGenderPicker()
        case 4:
            showCountryPicker()
        case 7:
            showInterestsPicker()
        default:
            shouldBegin = true
        }
        return shouldBegin
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 5 {
            regData.name = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.tableItems.reloadData()
        } else if textField.tag == 66 {
            regData.lastName = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.tableItems.reloadData()
        }
    }
    
    
    
    func dequeue_photo_cell() -> SettingsTableViewCell {
        return tableItems.dequeueReusableCell(withIdentifier: "photo_cell") as! SettingsTableViewCell
    }
    func deque_button_cell() -> SettingsTableViewCell {
        return tableItems.dequeueReusableCell(withIdentifier: "button_cell") as! SettingsTableViewCell
    }
    func deque_field_cell() -> SettingsTableViewCell {
        return tableItems.dequeueReusableCell(withIdentifier: "field_cell") as! SettingsTableViewCell
    }
    func deque_toogle_cell() -> SettingsTableViewCell {
        return tableItems.dequeueReusableCell(withIdentifier: "switch_cell") as! SettingsTableViewCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 11 : section == 1 ? 2 : 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = deque_button_cell()
                cell.black_title?.text = "Приватность"
                return cell
            }
            let cell = deque_toogle_cell()
            cell.black_title?.text = "Видимость в \"Рядом\" (Wi-Fi/Bluetooth)"
            cell.toogle?.isOn = UserDataManager.instance.isVisibleOverMPC
            cell.toogle?.addTarget(self, action: #selector(self.mpcVisibleSwitched), for: .valueChanged)
            return cell
            
        } else if indexPath.section == 2 {
            let cell = tableItems.dequeueReusableCell(withIdentifier: "button_cell") as! SettingsTableViewCell
            cell.black_title?.textColor = .red
            cell.black_title?.text = "Выход"
            return cell
        } else {
            var cell: SettingsTableViewCell!
            
            
            switch indexPath.row {
            case 0:
                cell = deque_button_cell()
                cell.black_title?.text = "Профиль"
                
            case 1:
                cell = dequeue_photo_cell()
                if let url = URL(string: UserDataManager.instance.currentUser.image_small) {
                    cell.user_icon?.sd_setImage(with: url)
                } else if let url = URL(string: UserDataManager.instance.currentUser.image) {
                    cell.user_icon?.sd_setImage(with: url)
                }
                
            case 2:
                cell = deque_field_cell()
                cell.gray_title?.text = "Возраст"
                let age = Int((Date().timeIntervalSince1970 - (regData.birthday?.timeIntervalSince1970 ?? 0))/Double(365*86400))
                cell.black_field?.text = age == 0 ? "Укажите возраст" : String(Int(age))
                cell.black_field?.delegate = self
                
            case 3:
                cell = deque_field_cell()
                cell.gray_title?.text = "Пол"
                cell.black_field?.text = regData.gender?.title
                cell.black_field?.delegate = self
                
                //            case 4:
                //                cell = deque_field_cell()
                //                cell.gray_title?.text = "Страна"
                //                cell.black_field?.text = regData.country ?? "Укажите страну"
                //                cell.black_field?.delegate = self
                
            case 4:
                cell = deque_field_cell()
                cell.gray_title?.text = "Город"
                cell.black_field?.text = regData.city ?? "Укажите город"
                cell.black_field?.delegate = self
                
            case 5:
                cell = deque_field_cell()
                cell.gray_title?.text = "Имя"
                if #available(iOS 10.0, *) {
                    cell.black_field?.textContentType = .givenName
                }
                cell.black_field?.text = regData.name
                cell.black_field?.delegate = self
                
            case 6:
                cell = deque_field_cell()
                cell.gray_title?.text = "Фамилия"
                if #available(iOS 10.0, *) {
                    cell.black_field?.textContentType = .familyName
                }
                cell.black_field?.text = regData.lastName
                cell.black_field?.delegate = self
                
            case 7:
                cell = deque_field_cell()
                cell.gray_title?.text = "Интересы"
                cell.black_field?.text = UserDataManager.instance.currentUser.interests.map({$0.title}).joined(separator: ", ")
                cell.black_field?.delegate = self
                
            case 8:
                cell = deque_button_cell()
                cell.black_title?.text = "Уведомления"
                
            case 9:
                cell = deque_toogle_cell()
                cell.black_title?.text = "Оповещать о новых комментариях"
                cell.toogle?.isOn = UserDataManager.instance.currentUser.notificate_on_comments
                cell.toogle?.addTarget(self, action: #selector(self.changeCommentsNotification), for: .valueChanged)
                
                
            case 10:
                cell = deque_toogle_cell()
                cell.black_title?.text = "Оповещать о новых подписчиках"
                cell.toogle?.isOn = UserDataManager.instance.currentUser.notificate_on_subscribers
                cell.toogle?.addTarget(self, action: #selector(self.changeSubscribersNotification), for: .valueChanged)
                
                //            case 11:
                //                cell = deque_button_cell()
                //                cell.black_title?.text = "Службы"
                //
                //            case 12:
                //                cell = deque_toogle_cell()
                //                cell.black_title?.text = "Геолокация в приложении"
                
            default:
                cell = deque_field_cell()
                cell.gray_title?.text = ""
                cell.black_field?.text = ""
                cell.black_field?.isUserInteractionEnabled = false
                
            }
            
            cell.black_field?.tag = indexPath.row == 6 ? 66 : indexPath.row
            
            return cell
        }
    }
    
    func changeCommentsNotification(_ sender: UISwitch) {
        showHUD()
        Server.makeRequest.updateNotificationFlags(data: self.regData, onComments: sender.isOn) {
            async {
                dismissHUD()
                DoneViewController.presentOne(with: "Настройки сохранены")
                self.hasEditedSomething = false
            }
        }
    }
    
    func changeSubscribersNotification(_ sender: UISwitch) {
        showHUD()
        Server.makeRequest.updateNotificationFlags(data: self.regData, onSubscriptions: sender.isOn) {
            async {
                dismissHUD()
                DoneViewController.presentOne(with: "Настройки сохранены")
                self.hasEditedSomething = false
            }
            
        }
        
    }
    
    static func logout() {
        Server.makeRequest.logout {
            let main = UIStoryboard(name: "SplashScreen", bundle: nil).instantiateInitialViewController()!
            UIView.transition(from: UIApplication.shared.keyWindow!.rootViewController!.view, to: main.view, duration: 0.5, options: .transitionFlipFromRight, completion: { _ in
                UIApplication.shared.keyWindow?.rootViewController = main
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            return
        }
        if indexPath.section == 2 {
            let alert = UIAlertController(title: "Вы уверены, что хотите выйти?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Выйти", style: .destructive, handler: { (_) in
                SettingsViewController.logout()
            }))
            alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            switch indexPath.row {
            case 0: break;//goToUsersProfile()
            case 1: showImagePicker()
            default: break;
            }
        }
    }
    
    
    @objc private func mpcVisibleSwitched(_ sender: UISwitch) {
        UserDataManager.instance.isVisibleOverMPC = sender.isOn
    }
    
    
    func goToUsersProfile() {
        
        let transferringBlock = {
//            let menu = (self.sideMenuViewController.leftMenuViewController) as! MenuContentViewController
//            menu.show_my_profile(menu.my_profile_button)
//            async(after: 300) {
//                UIApplication.shared.statusBarStyle = .lightContent
//            }
        }
        
        if hasEditedSomething {
            let alert = UIAlertController(title: "Вы уверены, что хотите уйти?", message: "Вы не сохранили изменения в настройках профиля", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: { (_) in
                showHUD()
                Server.makeRequest.updateMyProfile(data: self.regData) {
                   dismissHUD()
                    DoneViewController.presentOne(with: "Настройки сохранены") {
                        self.hasEditedSomething = false
                        transferringBlock()
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Не сохранять", style: .destructive, handler: { (_) in
                transferringBlock()
            }))
            alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            transferringBlock()
        }
        
    }
    
    func showImagePicker() {
        var picker: UIImagePickerController! = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Альбомы", style: .default, handler: { (_) in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Моменты", style: .default, handler: { (_) in
            picker.sourceType = .savedPhotosAlbum
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (_) in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: { (_) in
            picker = nil
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showBirthdayPicker() {
        showDatePicker()
    }
    
    func showDatePicker() {
        let picker = SDatePicker.make(self, currentDate: self.regData.birthday)
        self.present(picker, animated: false)
    }
    
    func sdatePicker_cancelled() { }
    
    func sdatePicker_selected(_ date: Date) {
        self.regData.birthday = date
        self.tableItems.reloadData()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: {
            AvatarCropperViewController.present(with: info[UIImagePickerControllerOriginalImage] as? UIImage, from: self)
        })
    }
    
    
    
    func showGenderPicker() {
        let alert = UIAlertController(title: "Выберите пол", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Мужской", style: .default, handler: { (_) in
            self.regData.gender = .male
            
            self.tableItems.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "Женский", style: .default, handler: { (_) in
            self.regData.gender = .female
            
            self.tableItems.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showCountryPicker() {
        self.hideKeyboard()
        Server.makeRequest.getCitiesList { (result) in
            if let coutryAndCityVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "coutryAndCity") as? SelectCountryViewController {
                coutryAndCityVC.modalPresentationStyle = .overCurrentContext
                self.present(coutryAndCityVC, animated: true, completion: nil)
                coutryAndCityVC.isCoutry = false
                coutryAndCityVC.array = result
                coutryAndCityVC.delegate = self
            }
        }
        //        Server.makeRequest.getCountryList(completionHadnler: { (error, countries) in
        //            if error == nil {
        //                if let coutryAndCityVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "coutryAndCity") as? SelectCountryViewController {
        //                    coutryAndCityVC.modalPresentationStyle = .overCurrentContext
        //                    self.present(coutryAndCityVC, animated: true, completion: nil)
        //                    coutryAndCityVC.isCoutry = true
        //                    coutryAndCityVC.array = countries
        //                    coutryAndCityVC.delegate = self
        //                }
        //
        //            } else {
        //                Presenter.instance.showErrorAlert(nil, message: "Возможно, отсутствует связь. Проверьте соединение")
        //            }
        //        })
    }
    
    func showInterestsPicker() {
        self.savedLeftBarButtons = self.navigationItem.leftBarButtonItems
        self.savedRightBarButtons = self.navigationItem.rightBarButtonItems
        
        self.navigationItem.setLeftBarButtonItems([], animated: true)
        self.navigationItem.setRightBarButtonItems([], animated: true)
        
        let edit = InterestsEditorViewController.makeOne()
        edit.completion_block = {
                if let items = self.savedLeftBarButtons {
                    self.navigationItem.setLeftBarButtonItems(items, animated: true)
                    self.savedLeftBarButtons = nil
                }
                
                if let items = self.savedRightBarButtons {
                    self.navigationItem.setRightBarButtonItems(items, animated: true)
                    self.savedRightBarButtons = nil
                }
            self.tableItems.reloadData()

        }
        edit.previousStatusBarStyle = UIApplication.shared.statusBarStyle
        self.present(edit, animated: true, completion: nil)
    }
    
    var selectedCountry = ""
    var selectedCity = ""
    
    func itemIsSelected(with tag: String, title: String, isCountry: Bool) {
        if isCountry {
            //            self.regData.country = andTitle
            //
            //            self.selectedCountry = id
            //            Server.makeRequest.getCitiesList(completionhandler: { (result) in
            //                <#code#>
            //            })
            //            Server.makeRequest.getCityList(countryID: self.selectedCountry, query: "", completionHadnler: { (error, cities) in
            //                if error == nil {
            //                    if let coutryAndCityVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "coutryAndCity") as? SelectCountryViewController {
            //                        coutryAndCityVC.modalPresentationStyle = .overCurrentContext
            //                        self.present(coutryAndCityVC, animated: true, completion: nil)
            //                        coutryAndCityVC.isCoutry = false
            //                        coutryAndCityVC.array = cities
            //                        coutryAndCityVC.delegate = self
            //                        coutryAndCityVC.countryID = self.selectedCountry
            //
            //                    }
            //                }
            //            })
        } else {
            self.regData.city = title
            self.regData.city_tag = tag
            self.selectedCity = tag
            self.tableItems.reloadData()
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presented = self.presentedViewController {
                return presented.preferredStatusBarStyle
            } else {
                return .default
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string != " " else { return true }
        var newString = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        newString = newString.filterAllowedNameCharacters()
        textField.text = newString
        return false
    }
    
}

extension SettingsViewController: AvatarCropperDelegate {
    func cropperDidCancel() {
        
    }
    
    func cropperDidPick(image: UIImage, smallImage: UIImage) {
        showHUD()
        self.uploadImages(smallImage, image: image)
    }
    
    func uploadImages(_ smallImage: UIImage, image: UIImage) {
        Server.makeRequest.fileUpload(file: smallImage, target: .profile_small) { (link, errorMessage) in
            guard link != nil else { dismissHUD(); return }
            let user = UserDataManager.instance.currentUser
            user?.image_small = link!
            UserDataManager.instance.currentUser = user
            Server.makeRequest.fileUpload(file: image, target: .profile, completion: { (link, errorMessage) in
                dismissHUD()
                guard link != nil else { return }
                let user = UserDataManager.instance.currentUser
                user?.image_small = link!
                UserDataManager.instance.currentUser = user
                self.tableItems.reloadData()
                DoneViewController.presentOne(with: "Фото загружено")
            })
        }
    }
    
    
}
