//
//  Server+Notifications.swift
//  Heloo2017
//
//  Created by Константин on 02.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire


var currentFCMToken = ""

extension Server {
    func registerNotificationsToken(_ token: String?) {
        guard token != nil else { currentFCMToken = ""; return}
        currentFCMToken = token!
        Alamofire.request(url_paths.base + url_paths.notifications.token, method: .post, parameters: ["token": token!], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in }
    }
    
    func requestTestNotification(message: String) {
        Alamofire.request(url_paths.base + url_paths.notifications.main, method: .get, parameters: ["message": message], encoding: URLEncoding.default, headers: header).responseJSON { (response) in }
    }
    
    
}
