//
//  Accept.swift
//  Heloo2017
//
//  Created by Константин on 09.05.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class Accept: NSObject {
    enum fields {
        static let id = "uuid"
        static let date  = "created_time"
        static let likes_count = "likes_count"
        static let comments_count  = "comments_count"
        static let isLiked  = "is_liked"
        static let artwork  = "image"
        static let descript  = "description"
        static let event = "event"
        static let user = "from"
    }
    
    var id = ""
    var date: Date = Date()
    var likes_count = 0
    var comments_count = 0
    var isLiked = false
    var artwork: ImageObject?
    var descript: String = ""
    var event: Event?
    var user: User?
    
    var isMine: Bool {
        get {
            return user?.uuid == UserDataManager.instance.currentUser.uuid
        }
    }
    
    init(with json: NSDictionary) {
        if let someValue = json[fields.id] as? String {
            self.id = someValue
        }
        if let someValue = json[fields.date] as? String {
            self.date = someValue.stringToDate()
        }
        
        if let someValue = json[fields.likes_count] as? Int {
            self.likes_count = someValue
        }
        if let someValue = json[fields.comments_count] as? Int {
            self.comments_count = someValue
        }
        if let someValue = json[fields.isLiked] as? Bool {
            self.isLiked = someValue
        }
        if let someValue = json[fields.artwork] as? NSDictionary {
            self.artwork = ImageObject(with: someValue)
        }
        if let someValue = json[fields.descript] as? String {
            self.descript = someValue
        }
        if let someValue = json[fields.event] as? NSDictionary {
            self.event = Event(with: someValue)
        }
        if let someValue = json[fields.user] as? NSDictionary {
            self.user = User(with: someValue)
        }
    }
}
