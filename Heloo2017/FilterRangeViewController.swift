//
//  FilterAgesViewController.swift
//  Heloo2017
//
//  Created by Константин on 19.06.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class FilterRangeViewController: UIViewController {
    
    
    @IBOutlet weak var slider: RangeSlider!
    @IBOutlet weak var fromField: UITextField!
    @IBOutlet weak var toField: UITextField!
    
    
    var leftValue: Int = 18
    var rightValue: Int = 50
    
    
    var minValue: Int = 18
    var maxValue: Int = 50
        
    
    var onSelected: ((Int?, Int?)->Void)?=nil
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        update(updateSlider: true)
    }
    
    func update(updateSlider: Bool = true) {
        fromField?.text = String(leftValue)
        toField?.text = rightValue == maxValue ? "∞" : String(rightValue)
        guard updateSlider else { return }
        slider?.leftThumbValue = CGFloat(leftValue)
        slider?.rightThumbValue = CGFloat(rightValue)
        slider?.minValue = CGFloat(minValue)
        slider?.maxValue = CGFloat(maxValue)
    }
    
    @IBAction func sliderChanged(_ sender: RangeSlider) {
        leftValue = Int(sender.leftThumbValue)
        rightValue = Int(sender.rightThumbValue)
        update(updateSlider: false)
    }
    
    @IBAction func selectTapped(_ sender: UIButton) {
        let actualLeft = (leftValue == minValue && minValue == 0) ? nil : leftValue
        let actualRight = rightValue == maxValue ? nil : rightValue
        onSelected?(actualLeft, actualRight)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FilterRangeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let int = Int(textField.text ?? "") {
            if textField == fromField {
                leftValue = min(max(int, minValue), rightValue - Int(slider.possibleDistanceValue))
            } else  {
                rightValue = max(min(int, maxValue), leftValue + Int(slider.possibleDistanceValue))
            }
        }
        update()
        textField.resignFirstResponder()
        return true
    }
}
