//
//  ProfileRowTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 21.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ProfileRowTableViewCell: UITableViewCell {

    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    @IBOutlet weak var corneredView: UIView!
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var right: TappableLabel!
    @IBOutlet weak var left: UILabel!
    
    func update(left l: String, right r: String?, cornersBottom: Bool) {
        left.text = l
        right.text = r
        line.isHidden = r == nil
        corneredView.cornerRadius = cornersBottom ? 4 : 0
        bottomSpace.constant = cornersBottom ? 12 : 0
    }
    

}
