//
//  LocationViewerViewController.swift
//  YzerChat
//
//  Created by Константин on 25.09.17.
//  Copyright © 2017 INTERO SOLUTIONS LP. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationViewerViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl! {
        didSet {
            segmentedControl.setTitle("Карта", forSegmentAt: 0)
            segmentedControl.setTitle("Спутник", forSegmentAt: 1)
            segmentedControl.setTitle("Гибрид", forSegmentAt: 2)

            segmentedControl.layer.cornerRadius = 4
        }
    }
    
    private var wasNavBarHidden = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wasNavBarHidden = self.navigationController?.isNavigationBarHidden ?? false
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !wasNavBarHidden {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    @IBOutlet weak var moveButton: UIButton!
    @IBOutlet weak var map: MKMapView!
    var coordinates: CLLocationCoordinate2D!
    var pinTitle: String?
    
    static func present(from: UINavigationController?, with latitude: Double, longitude: Double, pinTitle title: String?=nil) {
        let viewer = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "LocationViewerViewController") as! LocationViewerViewController
        viewer.coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        viewer.pinTitle = title
        from?.pushViewController(viewer, animated: true)
        
    }
    
    var currentLocationSetted = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Карта"
        
        if var attr = self.navigationController?.navigationBar.titleTextAttributes {
            attr[NSForegroundColorAttributeName] = UIColor.black
            self.navigationController?.navigationBar.titleTextAttributes = attr
        }
        
        
        map.setRegion(MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 0.025, longitudeDelta: 0.025)), animated: true)
        
        let point = MKPointAnnotation()
        point.coordinate = coordinates
        point.title = pinTitle
        if let loc = CLLocationManager().location {
            point.subtitle = String((CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude).distance(from: loc)/1000).round(to: 2)) + " км"
        }
        self.map.addAnnotation(point)
        self.map.selectAnnotation(point, animated: true)
        self.title = "Карта"
        
        requestLocations()
        
    }
    
    var locationManager: CLLocationManager!
    
    func requestLocations() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        if (locationManager.location?.coordinate.latitude ?? 1) != 0 {
            currentLocationSetted = true
            moveButton.alpha = 1
        }
    }
    
    @IBAction func changeCardMode(_ sender: UISegmentedControl) {
        map.mapType = sender.selectedSegmentIndex == 0 ? .standard : sender.selectedSegmentIndex == 1 ? .satellite : .hybrid
    }
    
    @IBAction func move(_ sender: UIButton) {
        guard currentLocationSetted else { return }
        if sender.image(for: .normal) == #imageLiteral(resourceName: "TrackingLocationOff") {
        sender.setImage(#imageLiteral(resourceName: "TrackingLocation"), for: .normal)
        map.setRegion(MKCoordinateRegion(center: map.userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.025, longitudeDelta: 0.025)), animated: true)
        } else {
            sender.setImage(#imageLiteral(resourceName: "TrackingLocationOff"), for: .normal)
        map.setRegion(MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 0.025, longitudeDelta: 0.025)), animated: true)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .default
        }
    }
    
}

extension LocationViewerViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocationSetted = true
        moveButton.alpha = 1
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //
    }
}


extension Double {
    func round(to:Int) -> Double {
        let divisor = pow(10.0, Double(to))
        return (self * divisor).rounded() / divisor
    }
}

