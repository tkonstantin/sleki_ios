//
//  AppDelegate+OrientationLocking.swift
//  YzerChat
//
//  Created by Константин on 09.01.2018.
//  Copyright © 2018  Yzer Lab LLC.. All rights reserved.
//

import UIKit

extension AppDelegate {
    
    fileprivate func lockPortrait()  {
        isPortraitLocked = true
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    fileprivate func unlockPortrait() {
        isPortraitLocked = false
    }
    
}

extension UIApplication {
    
    static func lockPortrait() {
        (UIApplication.shared.delegate as? AppDelegate)?.lockPortrait()
    }
    
    static func unlockPortrait() {
        (UIApplication.shared.delegate as? AppDelegate)?.unlockPortrait()
    }
}

