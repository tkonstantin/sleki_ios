//
//  NewsViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SDWebImage
import Canvas

enum post_types {
    case event_created
    case action_created
    case checkin_created
    case user_will_go
    case place_Favorite
    case placeholder
    
    var stringValue: String {
        get {
            switch self {
            case .event_created: return "Event_Created"
            case .action_created: return "Action_Created"
            case .checkin_created: return "Checkin_Created"
            case .user_will_go: return "User_Will_Go"
            case .place_Favorite: return "Place_Favorite"
            case .placeholder: return "Placeholder"
            }
        }
    }
}


class NewsViewController: TableViewContainingViewController {
    
    var posts: [Post] = []
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var search_button: UIButton!
    @IBOutlet weak var tableItems: RefreshableTable! {
        didSet {
            self.table = tableItems
        }
    }
    
    var computedCellHeights: [Int: CGFloat] = [:]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        
        for cell in self.tableItems.visibleCells {
            if let postCell = cell as? PostCell {
                postCell.hideMenu()
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.set(title: "Лента")
        self.navigationController?.navigationBar.makeClear()
        self.configureTable()
        self.loadPosts()
    }
    
    
    var expandedIndexPaths: [IndexPath] = []
    
    var currentPage = -1
    var hasNext = true
    
    var only_subscriptions = false {
        didSet {
            async {
                self.search_button.setImage(self.only_subscriptions ? #imageLiteral(resourceName: "filter_button_active") : #imageLiteral(resourceName: "filter_button"), for: .normal)
                self.tableItems.reloadData()
                self.currentPage = -1
                self.hasNext = true
                showHUD()
                self.loadPosts()
            }
        }
    }
    
    var isLoadingNext = false
    func loadPosts(completion: (()->Void)?=nil) {
        guard hasNext, !isLoadingNext else {
            dismissHUD()
            return
        }
        isLoadingNext = true
        currentPage += 1
        Server.makeRequest.getFeed(page: currentPage, only_subscriptions: only_subscriptions) { (result, hasNextItems) in
            DispatchQueue.main.async {
                dismissHUD()
                if self.currentPage == 0 {
                    self.posts = []
                }
                self.hasNext = hasNextItems
                let newResults = result.map({Post(with: $0)}).flatMap({$0}).filter({ item in
                    return !self.posts.contains(where: {$0.searchable_item?.uuid == item.searchable_item?.uuid})
                })
                self.posts += newResults
                self.posts.sort(by: {$0.real_date > $1.real_date})
                self.tryReloadTable()
                self.isLoadingNext = false
                completion?()
            }
        }
    }
    
    func tryReloadTable() {
        guard !self.tableItems.isDragging && !self.tableItems.isTracking && !self.tableItems.isDecelerating else {
            async(after: 300, {
                self.tryReloadTable()
            });
            return;
        }
        async {
            self.tableItems.reloadData()
        }
    }
    
    func configureTable() {
        
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.registerAllCells()
        
        tableItems.addPullToRefresh(color: blueColor) { [weak self] (refreshControl) in
            self?.currentPage = -1
            self?.hasNext = true
            self?.loadPosts {
                refreshControl?.endRefreshing()
            }
        }
        
    }
    
    var isSearchingNow = false
    
    @IBAction func search(_ sender: UIButton) {
//        isSearchingNow = !isSearchingNow
//        UIView.animate(withDuration: 0.3) {
//            self.searchView.frame.size.height = self.isSearchingNow ? 44 : 0
//            self.tableItems.frame.origin.y += self.isSearchingNow ? 44 : -44
//            self.tableTopConstraint.constant = self.isSearchingNow ? 44 : 0
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? FilterNewsViewController {
            dest.set(only_subscriptions: self.only_subscriptions)
            dest.onSelected = { result in
                self.only_subscriptions = result
            }
        }
    }
    
}

extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result = (posts.count == 0 && !hasNext) ? 1 : posts.count
        tableView.tableFooterView?.frame.size.height = hasNext ? 128 : 0
        return result
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if posts.count == 0 && !hasNext || posts.count <= indexPath.row {
            let cell = tableItems.dequeueReusableCell(withIdentifier: post_types.placeholder.stringValue, for: indexPath)
            return cell
        }
        let currentPost = posts[indexPath.row]
        let cell = tableItems.dequeueReusableCell(withIdentifier: currentPost.type.stringValue, for: indexPath) as! PostCell
        configure(postCell: cell, with: currentPost, commentsCountChanged: { [weak self] (newCount) in
            self?.posts[indexPath.row].comments_count = newCount
        })
        
        cell.setCanDelete(currentPost.canDelete) { [weak self] in
            Server.makeRequest.deletePost(currentPost) { success in
                if success {
                    async {
                        self?.posts.remove(at: indexPath.row)
                        self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                    }
                } else {
                    (self?.tableItems.cellForRow(at: indexPath) as? PostCell)?.hideMenu()
                }
            }
        }
        cell.setCanComplain(currentPost.canComplain) { [weak self] in
            guard let checkin = currentPost.searchable_item?.checkin else { return }
            Server.makeRequest.complain(to: checkin, completion: {
                async {
                    self?.posts.remove(at: indexPath.row)
                    self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                }
            })
        }
        
        cell.likes_button?.addTarget(self, action: #selector(self.animate_like(_:)), for: .touchDown)
        cell.likes_button?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        cell.likes_button?.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        computedCellHeights[indexPath.row] = cell.frame.size.height
        
        if indexPath.row > self.posts.count - 3 {
            self.loadPosts()
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.computedCellHeights[indexPath.row] {
            return height
        } else {
            return UITableViewAutomaticDimension
        }
    }

    //
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        if let height = self.computedCellHeights[indexPath.row] {
    //            return height
    //        } else {
    //            return UITableViewAutomaticDimension
    //        }
    //
    //    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.posts.count > indexPath.row else { return }
        self.posts[indexPath.row].didSelect(from: self)
    }
    
    
    func animate_like(_ sender: UIButton) {
        let anim = (sender.superview as? CSAnimationView)
        anim?.duration = 0.3
        anim?.delay = 0
        anim?.type = CSAnimationTypePop
        anim?.startCanvasAnimation()
    }
    
    func like(_ sender: UIButton) {
        Server.makeRequest.like(post: posts[sender.tag]) { (success, liked, count) in
            self.posts[sender.tag].isLiked = liked
            sender.setImage(liked ? #imageLiteral(resourceName: "liked_button") : #imageLiteral(resourceName: "myprofile_likes"), for: .normal)
            guard var likesCount = Int(sender.title(for: .normal) ?? "") else { return }
            likesCount = count
            self.posts[sender.tag].likes_count = count
            sender.setTitle(String(likesCount), for: .normal)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presented = self.presentedViewController {
                return presented.preferredStatusBarStyle
            } else {
                return .default
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableItems.visibleCells.compactMap({$0 as? PostCell}).forEach({$0.hideMenuWithScrollIntent()})
    }
    
}


