//
//  FilterTreeViewController.swift
//  Heloo2017
//
//  Created by Константин on 23.06.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class FilterTreeViewController: UIViewController {
    
    func setItems(_ _items: [TreeItem], mainSource _mainSource: [TreeItem]) {
        self.items = _items
        self.mainSource = _mainSource
    }
    
    fileprivate var items: [TreeItem] = []
    
    var editor: TreeEditor!
    
    var searchText: String = "" {
        didSet {
            collection.reloadData()
        }
    }
    
    var mainSource: [TreeItem] = []
    
    var filteredItems: [TreeItem] {
        get {
            guard !searchText.isEmpty else { return editor?.currentItems() ?? [] }
            
            let source = mainSource.makeFlat()
            return source.filter({$0.title.lowercased().contains(searchText.lowercased())})
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collection.contentInset.bottom = 100
        self.collection.scrollIndicatorInsets.bottom = 100
        self.collection.contentInset.top = 16
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        load()
    }
    
    func load() {
        editor = TreeEditor(interests: items, dataSource: self, mainSource: mainSource)
        self.collection.reloadData()
    }
    
    
    var onPicked: (([TreeItem])->Void)?=nil
    
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var collection: UICollectionView!
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        searchField.text = nil
        searchText = ""
        items = []
        load()
    }
    
    @IBAction func selectTapped(_ sender: UIButton) {
        onPicked?(editor.selectedInterests)
        self.dismiss(animated: true, completion: nil)
        
    }
}




extension FilterTreeViewController: TreeEditorDataSource {
    
    func onItemsInserted(at indexes: [Int]) {
        
        guard searchText.isEmpty else {
            self.collection.reloadData();
            return
        }
        
        let paths = indexes.map({IndexPath(item: $0, section: 0)})
        self.collection.performBatchUpdates({
            self.collection.insertItems(at: paths)
        }) { (_) in
            async {
                if let first = paths.first {
                    self.collection.scrollToItem(at: first, at: .centeredVertically, animated: true)
                }
            }
        }
    }
    
    func onItemsRemoved(at indexes: [Int]) {
        guard searchText.isEmpty else { self.collection.reloadData(); return }

        let paths = indexes.map({IndexPath(item: $0, section: 0)})
        self.collection.performBatchUpdates({
            self.collection.deleteItems(at: paths)
            if let first = paths.first, first.item != 0 {
                self.collection.scrollToItem(at: IndexPath(item: first.item - 1, section: 0), at: .centeredVertically, animated: true)
            }
        }) { (_) in
        }
    }
    
    func onItemsUpdated(at indexes: [Int]) {
        guard searchText.isEmpty else { self.collection.reloadData(); return }

        UIView.setAnimationsEnabled(false)
        self.collection.reloadItems(at: indexes.map({IndexPath(item: $0, section: 0)}))
        UIView.setAnimationsEnabled(true)
    }
    
    
}


extension FilterTreeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let current = filteredItems[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! InterestCollectionViewCell
        cell.update(with: current, isSelected: editor.isSelected(current))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

        guard filteredItems.count > indexPath.item else { return }
        let oldCurrent = filteredItems[indexPath.item]
        
        editor?.selectionChanged(for: oldCurrent)
        
        guard filteredItems.count > indexPath.item else { return }
        let current = filteredItems[indexPath.item]
        
        
        if let cell = collectionView.cellForItem(at: indexPath) as? InterestCollectionViewCell {
            cell.pop()
            cell.update(with: current, isSelected: current.isSelected)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard filteredItems.count > indexPath.item else { return .zero }
        let current = filteredItems[indexPath.item]
        
        let hasBackground =  current.parent == nil
        return CGSize(width: current.title.requiredWidth(14, font: UIFont.systemFont(ofSize: 14, weight: hasBackground ? UIFontWeightMedium : UIFontWeightRegular), height: 24 + 10) + 40, height: 36 + 10)
    }
}


extension FilterTreeViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        searchText = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        return true
    }
}

