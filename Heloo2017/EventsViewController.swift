//
//  EventsViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Canvas

class EventsViewController: TableViewContainingViewController {

    @IBOutlet weak var tableItems: RefreshableTable! {
        didSet {
            self.table = tableItems
        }
    }
    
    
    var posts: [Post] = []
    var expandedIndexPaths: [IndexPath] = []
    
    
    var isEvents = true
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.set(title: isEvents ? "СОБЫТИЯ" : "АКЦИИ")
        self.configureTable()
        tableItems.addPullToRefresh(color: blueColor) { [weak self] (refreshControl) in
            self?.currentPage = -1
            self?.hasNext = true
            self?.load({
                refreshControl?.endRefreshing()
            })
        }
        self.load()
        searchField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = .black

        
        for cell in self.tableItems.visibleCells {
            if let postCell = cell as? PostCell {
                postCell.hideMenu()
            }
        }
        
    }
    
    var loaded = false {
        didSet {
            if loaded == true {
                self.tableItems.alpha = 1
                self.loadingView.alpha = 0
            }
        }
    }
    
    var currentPage = -1
    var hasNext = true
    var isLoadingNextPage = false
    

    
    func load(_ completion: (()->Void)?=nil) {
        guard hasNext, !isLoadingNextPage else { return }
        isLoadingNextPage = true
        currentPage += 1
        Server.makeRequest.getEventsOffers(page: currentPage, isEvents: self.isEvents) { (result, hasNextItems) in
            if self.currentPage == 0 {
                self.posts = []
            }
            self.hasNext = hasNextItems
            let newPosts = result.map({Post(with: $0)}).flatMap({$0}).filter({ item in
                return !self.posts.contains(where: {$0.searchable_item?.uuid == item.searchable_item?.uuid})
            })
            self.posts += newPosts
            self.posts.sort(by: {$0.real_date > $1.real_date})
            self.loaded = true
            self.insertCells(with: newPosts)
            self.isLoadingNextPage = false
            completion?()
       }
    }
    
    
    
    var previousUpdateRowsCount = 0
    
    func insertCells(with requestResult: [Post]) {
        async {
            guard !self.tableItems.isDragging && !self.tableItems.isTracking && !self.tableItems.isDecelerating else { async(after: 300, {
                self.insertCells(with: requestResult); })
                return;
            }
            guard self.posts.count > self.previousUpdateRowsCount else { self.tableItems.reloadData(); return }
            guard self.posts.count == self.previousUpdateRowsCount + requestResult.count else { self.tableItems.reloadData(); return }
            
            self.previousUpdateRowsCount = self.posts.count
            self.tableItems.beginUpdates()
            let previousRowsCount = self.posts.count - requestResult.count
            let insertedRowsRange = previousRowsCount..<self.posts.count
            self.tableItems.insertRows(at: insertedRowsRange.map({IndexPath(row: $0, section: 0)}), with: .none)
            self.tableItems.endUpdates()
            
        }
    }
        
    

    func configureTable() {
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.registerAllCells()
    }
    
    var isSearchingNow = false
    
    @IBAction func showSearch(_ sender: UIButton) {
        isSearchingNow = !isSearchingNow
        UIView.animate(withDuration: 0.3) {
            self.searchView.frame.size.height = self.isSearchingNow ? 44 : 0
            self.tableItems.frame.origin.y += self.isSearchingNow ? 44 : -44
            self.tableTopConstraint.constant = self.isSearchingNow ? 44 : 0
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableItems.visibleCells.compactMap({$0 as? PostCell}).forEach({$0.hideMenuWithScrollIntent()})
    }
    
}

extension EventsViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result = posts.count == 0 && loaded ? 1 : posts.count
        tableView.tableFooterView?.frame.size.height = self.hasNext ? 128 : 0
        return result
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if posts.count == 0 {
            let cell = tableItems.dequeueReusableCell(withIdentifier: post_types.placeholder.stringValue)!
            (cell.viewWithTag(1) as? UILabel)?.text = "Пока что нет доступных событий или акций"
            return cell
        }
        let currentPost = posts[indexPath.row]
        let cell = tableItems.dequeueReusableCell(withIdentifier: currentPost.type.stringValue) as! PostCell
        configure(postCell: cell, with: currentPost, commentsCountChanged: { [weak self] (newCount) in
            self?.posts[indexPath.row].comments_count = newCount
        })
        cell.setCanDelete(currentPost.canDelete) { [weak self] in
            Server.makeRequest.deletePost(currentPost) { success in
                if success {
                    async {
                        self?.posts.remove(at: indexPath.row)
                        self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                    }
                } else {
                    (self?.tableItems.cellForRow(at: indexPath) as? PostCell)?.hideMenu()
                }
            }
        }
        cell.setCanComplain(currentPost.canComplain) { [weak self] in
            guard let checkin = currentPost.searchable_item?.checkin else { return }
            Server.makeRequest.complain(to: checkin, completion: {
                async {
                    self?.posts.remove(at: indexPath.row)
                    self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                }
            })
        }
        cell.likes_button?.addTarget(self, action: #selector(self.animate_like(_:)), for: .touchDown)
        cell.likes_button?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        cell.likes_button?.tag = indexPath.row
        return cell
    }
    
    func animate_like(_ sender: UIButton) {
        let anim = (sender.superview as? CSAnimationView)
        anim?.duration = 0.3
        anim?.delay = 0
        anim?.type = CSAnimationTypePop
        anim?.startCanvasAnimation()
    }
    
    func like(_ sender: UIButton) {
        Server.makeRequest.like(post: posts[sender.tag]) { (success, liked, count) in
            self.posts[sender.tag].isLiked = liked
            sender.setImage(liked ? #imageLiteral(resourceName: "liked_button") : #imageLiteral(resourceName: "myprofile_likes"), for: .normal)
            guard var likesCount = Int(sender.title(for: .normal) ?? "") else { return }
            likesCount = count
            self.posts[sender.tag].likes_count = count
            sender.setTitle(String(likesCount), for: .normal)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.posts.count > indexPath.row {
            self.posts[indexPath.row].didSelect(from: self)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= self.posts.count - 1 && self.posts.count != 0 {
            self.load()
        }
    }


    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presented = self.presentedViewController {
                return presented.preferredStatusBarStyle
            } else {
                return .default
            }
        }
    }
    
}

