//
//  FilterCitiesViewController.swift
//  Heloo2017
//
//  Created by Константин on 19.06.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class PickableThing {
    var title: String = ""
    var tag: String = ""
    
    init(title: String, tag: String) {
        self.title = title
        self.tag = tag
    }
}

class FilterPickerViewController: UIViewController {
    
    var maxSelectionCount = 100500

    var filteredItems: [PickableThing] = [] {
        didSet {
            self.tableView?.reloadData()
        }
    }
    var items: [PickableThing] = [] {
        didSet {
            isLoading = items.count < 2
        }
    }
    var placeholderText: String? = "Ничего не найдено"
    
    var selectedItemTags: [String] = []
    
    var onSelected: (([PickableThing])->Void)? = nil
    
    var isLoading = true
    
    var searchText: String? = nil {
        didSet {
            if (searchText ?? "").isEmpty {
                filteredItems = items.sorted(by: {$0.title < $1.title})
            } else {
                filteredItems = items.filter({$0.title.lowercased().contains(searchText!.lowercased())}).sorted(by: {$0.title < $1.title})
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.contentInset.bottom = 88
            tableView.tableFooterView = UIView()
        }
    }
    
    func setItems(_ items: [PickableThing]) {
        self.items = items
        self.filteredItems = items.sorted(by: {$0.title < $1.title})
        self.tableView?.reloadData()
    }

    @IBAction func selectTapped(_ sender: UIButton) {
        onSelected?(items.filter({ selectedItemTags.contains($0.tag) }))
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearTapped(_ sender: UIButton) {
        selectedItemTags = []
        self.tableView?.reloadData()
    }
    
    
}

extension FilterPickerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(1, filteredItems.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoading && indexPath.row == 0 && filteredItems.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loading", for: indexPath)
            (cell.viewWithTag(1) as? UIActivityIndicatorView)?.startAnimating()
            return cell
        }
        guard filteredItems.count != 0 else {
            tableView.separatorStyle = .none
            let cell = tableView.dequeueReusableCell(withIdentifier: "placeholder") as! PostCell
            cell.placeHolderLabel?.text = items.count == 0 ? placeholderText : "Ничего не найдено"
            return cell
        }
        
        tableView.separatorStyle = .singleLine
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FilterCheckCell
        cell.title.text = filteredItems[indexPath.row].title
        cell.checkmarkIcon.image = selectedItemTags.contains(filteredItems[indexPath.row].tag) ? maxSelectionCount == 1 ? #imageLiteral(resourceName: "checkmark_filled") : #imageLiteral(resourceName: "checkmark_filled_multiple") : maxSelectionCount == 1 ? #imageLiteral(resourceName: "checkmark_empty") : #imageLiteral(resourceName: "checkmark_empty_multiple")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tappedTag = filteredItems[indexPath.row].tag
        
        if let index = selectedItemTags.index(of: tappedTag) {
            selectedItemTags.remove(at: index)
        } else {
            if selectedItemTags.count + 1 <= maxSelectionCount {
                selectedItemTags.append(tappedTag)
            } else if selectedItemTags.count == 1 && maxSelectionCount == 1 {
                selectedItemTags = [tappedTag]
            }
        }
        
        tableView.reloadData()
    }
}


extension FilterPickerViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        searchText = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
