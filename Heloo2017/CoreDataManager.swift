//
//  CoreDataManager.swift
//  Heloo2017
//
//  Created by Константин on 29.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    func currentChatsList() -> [Chat] {
        let req: NSFetchRequest<Chat> = NSFetchRequest(entityName: "Chat")
        req.sortDescriptors = [NSSortDescriptor.init(key: "lastMessageDate", ascending: false)]
        do {
            return try CoreDataService.instance.privateContext.fetch(req)
        } catch {
            return []
        }
    }
    
    func chatsListController() -> NSFetchedResultsController<Chat> {
        let req: NSFetchRequest<Chat> = NSFetchRequest(entityName: "Chat")
        req.sortDescriptors = [NSSortDescriptor(key: "lastMessageDate", ascending: false)]
        req.predicate = NSPredicate(format: "lastMessageDate != 0")
        let controller: NSFetchedResultsController<Chat> = NSFetchedResultsController(fetchRequest: req, managedObjectContext: CoreDataService.instance.privateContext, sectionNameKeyPath: nil, cacheName: nil)
        return controller
    }
    
    func messagesController(for chat: Chat) -> NSFetchedResultsController<Message> {
        let req: NSFetchRequest<Message> = NSFetchRequest(entityName: "Message")
        req.predicate = NSPredicate(format: "chat_uuid = %@", chat.uuid)
        req.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        let controller: NSFetchedResultsController<Message> = NSFetchedResultsController(fetchRequest: req, managedObjectContext: CoreDataService.instance.privateContext, sectionNameKeyPath: "day", cacheName: nil)
        return controller
    }
    
    func chat(with uuid: String) -> Chat? {
        let req: NSFetchRequest<Chat> = NSFetchRequest(entityName: "Chat")
        req.sortDescriptors = [NSSortDescriptor.init(key: "uuid", ascending: false)]
        req.fetchLimit = 1
        req.predicate = NSPredicate(format: "uuid = %@", uuid)
        do {
            return try CoreDataService.instance.privateContext.fetch(req).first
        } catch {
            return nil
        }
    }
    
    func message(with uuid: String) -> Message? {
        let req: NSFetchRequest<Message> = NSFetchRequest(entityName: "Message")
        req.sortDescriptors = [NSSortDescriptor.init(key: "date", ascending: false)]
        req.fetchLimit = 1
        req.predicate = NSPredicate(format: "uuid = %@", uuid)
        do {
            return try CoreDataService.instance.privateContext.fetch(req).first
        } catch {
            return nil
        }
    }
}
