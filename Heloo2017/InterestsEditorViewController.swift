//
//  InterestsEditorViewController.swift
//  Heloo2017
//
//  Created by Константин on 12.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class InterestsEditorViewController: CardViewController {
    
    class func makeOne() -> InterestsEditorViewController {
        let new = UIStoryboard(name: "InterestsEditor", bundle: nil).instantiateViewController(withIdentifier: "InterestsEditorViewController") as! InterestsEditorViewController
        new.modalPresentationStyle = .overCurrentContext
        new.modalTransitionStyle = .crossDissolve
        return new
    }
    
    @IBOutlet weak var skip_button: UIButton!
    @IBOutlet weak var done_button: UIButton!
    @IBOutlet weak var cardViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var background_card: UIView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var comments_label: UILabel!
    @IBOutlet weak var tableItemsTop: NSLayoutConstraint!
    
    let tableItemsNormalTop: CGFloat = 18
    
    var bigSL: CAShapeLayer = CAShapeLayer()
    var smallSL: CAShapeLayer = CAShapeLayer()
    
    var level: CGFloat = 0.75
    
    var isPartOfReg = false
    
    
    var completion_block: (()->Void)?
    
    @IBOutlet weak var collection: UICollectionView! {
        didSet {
            (collection.collectionViewLayout as? CenterAlignedCollectionViewFlowLayout)?.minimumLineSpacing = 0
            collection.contentInset.bottom = UIScreen.main.bounds.width*318/1125 + 16
        }
    }
    
    var editor: TreeEditor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        editor = TreeEditor(interests: UserDataManager.instance.currentUser?.interests ?? [], dataSource: self, mainSource: UserDataManager.instance.availableInterests)
        self.collection.reloadData()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3) {
            self.comments_label.alpha = 1
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateAppearance()
    }
    
    var isAnimatingAppearance = true
    
    func animateAppearance() {
        async {
            UIView.animate(withDuration: 0.3, delay: 0.3, options: [], animations: {
                self.background_card.alpha = 0.5
            }, completion: nil)
            UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
                self.tableItemsTop.constant = self.tableItemsNormalTop
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.isAnimatingAppearance = false
            })
        }
    }
    
    
    @IBAction func close(_ sender: UIButton) {
        showHUD()
        self.saveInterests {
            dismissHUD()
            
            self.animateAll(alpha: 0, duration: 0.1)
            if self.isPartOfReg {
                let bg = UIImageView(image: UIImage(named: "blue_blur_rect"))
                bg.frame = self.view.bounds
                bg.contentMode = .scaleAspectFill
                let parent = self.presentingViewController
                parent?.view.bringSubview(toFront: bg)
                parent?.view.addSubview(bg)
                UIApplication.goMain()
                BeaconManager.instance.startSearchingBeacons()
                WAPManager.instance.startScanning()
                ChattingService.shared.getChats { }
                
            } else {
                self.makeCoverVerticalDismiss()
            }
        }
    }
    
    
    func saveInterests(completion: @escaping ()->Void) {
        let selectedInterests = self.editor.currentItems().filter({$0.isSelected})
        Server.makeRequest.updateInterests(selectedInterests) {
            
            let user = UserDataManager.instance.currentUser
            user?.interests = selectedInterests
            UserDataManager.instance.currentUser = user
            
            async {
                self.completion_block?()
                completion()
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        let presenting = self.presentingViewController
        super.dismiss(animated: flag) {
            presenting?.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
}

extension InterestsEditorViewController: TreeEditorDataSource {
    
    func onItemsInserted(at indexes: [Int]) {
        let paths = indexes.map({IndexPath(item: $0, section: 0)})
        self.collection.performBatchUpdates({
            self.collection.insertItems(at: paths)
        }) { (_) in
            async {
                if let first = paths.first {
                    self.collection.scrollToItem(at: first, at: .centeredVertically, animated: true)
                }
            }
        }
    }
    
    func onItemsRemoved(at indexes: [Int]) {
        let paths = indexes.map({IndexPath(item: $0, section: 0)})
        self.collection.performBatchUpdates({
            self.collection.deleteItems(at: paths)
            if let first = paths.first, first.item != 0 {
                self.collection.scrollToItem(at: IndexPath(item: first.item - 1, section: 0), at: .centeredVertically, animated: true)
            }
        }) { (_) in
        }
    }
    
    func onItemsUpdated(at indexes: [Int]) {
        UIView.setAnimationsEnabled(false)
        self.collection.reloadItems(at: indexes.map({IndexPath(item: $0, section: 0)}))
        UIView.setAnimationsEnabled(true)
    }
    
}

extension InterestsEditorViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return editor?.currentItems().count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let current = editor?.item(at: indexPath.item)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! InterestCollectionViewCell
        cell.update(with: current, isSelected: current?.isSelected ?? false)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let current = editor?.item(at: indexPath.item) {
            if current.associatedItems.count == 0 {
//                self.collection.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
            }
        }

        guard let oldCurrent = editor?.item(at: indexPath.item) else { return }

        editor?.selectionChanged(for: oldCurrent)
        
        guard let current = editor?.item(at: indexPath.item) else { return }

        if let cell = collectionView.cellForItem(at: indexPath) as? InterestCollectionViewCell {
            cell.pop()
            cell.update(with: current, isSelected: current.isSelected)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let current = editor?.item(at: indexPath.item) else { return .zero }
        let hasBackground =  current.parent == nil
        return CGSize(width: current.title.requiredWidth(14, font: UIFont.systemFont(ofSize: 14, weight: hasBackground ? UIFontWeightMedium : UIFontWeightRegular), height: 24 + 10) + 40, height: 36 + 10)
    }
}
