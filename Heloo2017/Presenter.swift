//
//  Presenter.swift
//  Smolnyi
//
//  Created by Константин Черкашин on 30.08.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation
import UIKit
import CRRefresh
import SVProgressHUD

let blueColor = UIColor(red: 0.0, green: 181/255, blue: 231/255, alpha: 1.0)
let yellowColor = UIColor(red: 1.0, green: 189/255, blue: 62/255, alpha: 1.0)
let greenColor = UIColor(red: 99/255, green: 201/255, blue: 93/255, alpha: 1.0)
let redColor = UIColor(red: 1.0, green: 108/255, blue: 73/255, alpha: 1.0)
let grayColor = UIColor(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)


class Presenter {
    
    static let instance = Presenter()
    
    let mainDarkGrayColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
    
    //MARK: - Simple alert
    func showAlert(title: String, message: String, completionHandler: @escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: {_ in
            completionHandler()
        })
        alert.addAction(ok)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    func showErrorAlert(_ title: String?, message: String, _completion: @escaping (()->())={}) {
        let alert = UIAlertController(title: title == nil ? "Ошибка" : title!, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
            _completion()
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func configureNavbarAppearance() {
        let appearance = UINavigationBar.appearance()
        
       appearance.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.black, NSFontAttributeName : UIFont(name: "AvenirNext-Medium", size: 17)!];
        UINavigationBar.appearance().tintColor = .black
        appearance.backIndicatorImage = #imageLiteral(resourceName: "backArrow_black")
        appearance.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "backArrow_black")
        appearance.setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        appearance.shadowImage = UIImage();
        appearance.barTintColor = UIColor.white
        appearance.backgroundColor = UIColor.white
  
        appearance.isTranslucent = false
        appearance.isOpaque = true
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    
    private var mainTabSubscribers: [(()->Void)] = []
    
    func subscribeForTabbarAppearance(_ subscriptionBlock: @escaping ()->Void) {
        mainTabSubscribers.append(subscriptionBlock)
        if let _ = rootTab as? MainTabsController  {
            mainTabSubscribers.forEach({$0()})
            mainTabSubscribers = []
        }
    }
    
    
    @objc private func mainTabOpened() {
    }
}

// MARK: - Custom storyboard properties
@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    
}


@IBDesignable class ShadowedButton: ResizableButton {
    
    @IBInspectable var color: String? = nil {
        didSet {
            update()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        update()
    }
    
    func update() {
        self.layer.shadowColor = UIColor(hex: color ?? "1F77BD").cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 8 //blur
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = 0.56
    
    }
}

extension UIView {
    func firstResponderView() -> UIView? {
        if self.isFirstResponder {
            return self
        }
        for subview in self.subviews {
            if let responder = subview.firstResponderView() {
                return responder
            }
        }
        return nil
    }
    
}



//MARK: - Keyboard appearance's logic
extension UIViewController {
    
    
    func showComments(_ sender: ShowCommentsButton) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            return
        }
//        if (sender.post.post_image?.url.isEmpty ?? true) {
            let nav = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "NavCommentsViewController") as! UINavigationController
            let comments = nav.viewControllers.first as! CommentsViewController
            comments.commentCompletionBlock = { [weak sender] (newCount) in
                sender?.setTitle(String(newCount), for: .normal)
                sender?.commentCompletionBlock?(newCount)
            }
            comments.post = sender.post
            comments.previousStatusBarStyle = UIApplication.shared.statusBarStyle
            nav.modalPresentationStyle = .overCurrentContext
            nav.modalTransitionStyle = .crossDissolve
            topController()?.present(nav, animated: true, completion: nil)
            //            self.present(nav, animated: true, completion: nil)
//        } else {
//            let nav = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "NavPhotoCommentsViewController") as! UINavigationController
//            let comments = nav.viewControllers.first as! PhotoCommentsViewController
//            comments.commentCompletionBlock = { [weak sender] (newCount) in
//                sender?.setTitle(String(newCount), for: .normal)
//                sender?.commentCompletionBlock?(newCount)
//            }
//            comments.post = sender.post
//            comments.previousStatusBarStyle = UIApplication.shared.statusBarStyle
//            nav.modalPresentationStyle = .overCurrentContext
//            nav.modalTransitionStyle = .crossDissolve
//            topController()?.present(nav, animated: true, completion: nil)
//            //            self.present(nav, animated: true, completion: nil)
//        }
    }
    
    
    func addTapOutsideGestureRecognizer() {
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapOutSide.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapOutSide)
    }
    
    func subscribeToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeShown(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeShown(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    
    func set(title: String?=nil) {
        let uppercasedTitle = title?.lowercased().uppercasedByFirstLetter()
        self.title = uppercasedTitle
        self.tabBarItem?.title = uppercasedTitle
    }
  
    
    func keyboardWillBeShown (_ notification: Notification) {
        let info = (notification as NSNotification).userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -keyboardSize.height
            })
        }
    }
    
    func keyboardWillBeHidden() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame.origin.y = 0
        })
    }
    
    
    
    
}

extension UIImage {
    func requiredStatusBarStyle() -> UIStatusBarStyle {
        var bitmap = [UInt8](repeating: 0, count: 4)
        
        // Get average color.
        let context = CIContext()
        let inputImage: CIImage = ciImage ?? CoreImage.CIImage(cgImage: cgImage!)
        let extent = inputImage.extent
        let inputExtent = CIVector(x: extent.origin.x, y: extent.origin.y, z: extent.size.width, w: extent.size.height)
        let filter = CIFilter(name: "CIAreaAverage", withInputParameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: inputExtent])!
        let outputImage = filter.outputImage!
        let outputExtent = outputImage.extent
        assert(outputExtent.size.width == 1 && outputExtent.size.height == 1)
        
        context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: kCIFormatRGBA8, colorSpace: CGColorSpaceCreateDeviceRGB())
        
        let brightness = ((CGFloat(bitmap[0])/255.0)*299 + (CGFloat(bitmap[1])/255.0)*587 + (CGFloat(bitmap[2])/255.0)*114)/1000
        
        return brightness < 0.5 ? .lightContent : .default
    }
}


class ShowCommentsButton: ResizableButton {
    var post: Post!
    
    var commentCompletionBlock: ((Int)->Void)?
}


@IBDesignable class TailIndentLabel: UILabel {
    
    @IBInspectable var isRightInset: Bool = true {
        didSet {
            update()
        }
    }
    @IBInspectable var inset: CGFloat = 0 {
        didSet {
            update()
        }
    }
    
    
    func update() {
        let paragraph = NSMutableParagraphStyle()
        if isRightInset {
            paragraph.baseWritingDirection = .rightToLeft
        }
        paragraph.firstLineHeadIndent = inset
        paragraph.alignment = .left
        self.attributedText = NSAttributedString(string: self.text!, attributes: [NSParagraphStyleAttributeName : paragraph])
    }
    
}

extension UIColor {
    convenience init(hex: String) {
        let color: UIColor = {
            var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines as NSCharacterSet) as CharacterSet).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
            }
            
            if ((cString.count) != 6) {
                return UIColor.clear
            }
            
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }()
        
        self.init(cgColor: color.cgColor)
    }
}

class RefreshableTable: UITableView, UIScrollViewDelegate {
    
    private(set) var isRefreshing = false
    
    func addPullToRefresh(color: UIColor = .white, _ completion: @escaping (UIRefreshControl?)->Void) {
        let animator = NormalHeaderAnimator()
        animator.setColor(color)
        
        self.cr.addHeadRefresh(animator: animator) { [weak self] in
            self?.isRefreshing = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000), execute: {
                self?.cr.endHeaderRefresh()
                self?.isRefreshing = false
                completion(nil)
            })
        }
        
    }

    
}

extension NormalHeaderAnimator {
    open func setColor(_ color: UIColor) {
        titleLabel.textColor = color
        imageView.tintColor = color
        indicatorView.color = color
    }
}



extension UINavigationBar {
    
    func makeClear() {
        let bar = self
        
        bar.tintColor = .white
        bar.setBackgroundImage(UIImage(), for: .default)
        bar.shadowImage = UIImage();
        bar.barTintColor = UIColor.clear
        bar.backgroundColor = UIColor.clear
        
        bar.isTranslucent = true
        bar.isOpaque = false
    }
}


var isHudAllowed = true

func showHUD() {
    if isHudAllowed {
        SVProgressHUD.show()
    } else {
        dismissHUD()
    }
}

func dismissHUD() {
    SVProgressHUD.dismiss()
}
