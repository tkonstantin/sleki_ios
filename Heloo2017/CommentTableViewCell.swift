//
//  CommentTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 27.08.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var text_label: UILabel!
    @IBOutlet weak var date_label: UILabel!
    @IBOutlet weak var user_icon: UIImageView!
    @IBOutlet weak var name_label: UILabel!

}
