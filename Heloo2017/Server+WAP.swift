//
//  Server+WAP.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import SystemConfiguration.CaptiveNetwork

extension Server {
    
    func getAvailableWAPs(page: Int=0, completion: @escaping (([WAP], Bool)->Void)) {
        Alamofire.request(url_paths.base + url_paths.waps.main, method: .get, parameters: ["page": page, "size": 100], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var hasNext = false
            var waps: [WAP] = []
            
            print("loading waps...")
            if let json = response.result.value as? NSDictionary, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    waps.append(WAP(true).loaded(with: item))
                }
                hasNext = json["hasNext"] as? Bool ?? false
            }
            completion(waps, hasNext)
        }
    }
    
    
    func getСurrentBSSID() -> String? {
        var currentBSSID: String? = nil
        guard let interfaces: CFArray = CNCopySupportedInterfaces() else { return nil }
        
        for i in 0..<CFArrayGetCount(interfaces) {
            
            let interfaceName: UnsafeRawPointer =  CFArrayGetValueAtIndex(interfaces, i)
            let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
            let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
            
            if let dict = unsafeInterfaceData as? Dictionary<AnyHashable, Any>, let bssid = dict["BSSID"] as? String {
                currentBSSID = bssid
            }
        }
        
        return currentBSSID
    }
    
    
    func searchPlaces(by waps: [WAP], completion: @escaping ([Place])->Void) {
        let params: [String: String] = ["wifiUuid": waps.map({$0.uuid}).joined(separator: ",")]
        Alamofire.request(url_paths.base + url_paths.places.searchByWAPS, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            var places: [Place] = []
            if let json = response.json, let resultSet = json["result_set"] as? [NSDictionary] {
                for result in resultSet {
                    places.append(Place(with: result))
                }
            }
            
            var currentFoundedPlaces = self.foundedPlaces
            
            for place in places {
                if let existingPlace = self.foundedPlaces.keys.first(where: {$0.uuid == place.uuid}) {
                    currentFoundedPlaces[existingPlace] = nil
                }
                currentFoundedPlaces[place] = Date()
            }
            
            self.foundedPlaces = currentFoundedPlaces
            
            completion(places)
        }
    }
    
    func searchPersons(by waps: [WAP], completion: @escaping ([User])->Void) {
        guard waps.count != 0 else { completion([]); return }
        let params: [String: String] = ["wifiUuid": waps.map({$0.uuid}).joined(separator: ",")]
        Alamofire.request(url_paths.base + url_paths.persons.searchByWAPS, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var users: [User] = []
            if let json = response.json, let resultSet = json["result_set"] as? [NSDictionary] {
                for result in resultSet {
                    users.append(User(with: result))
                }
            }
            completion(users)
        }
    }
    
    
    
}


