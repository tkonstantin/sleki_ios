//
//  ErrorPopupsManager.swift
//  Heloo2017
//
//  Created by Константин on 02.07.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import UIKit

class ErrorPopupManager {
    enum types {
        case connection
        case unknown
        case fields
        case invalid_data
        case fields_custom
        case access
    }
    
    static func showError(type: types, from: UIViewController?=nil, subtitle: String?=nil, title: String?=nil) {
        guard type != .fields_custom || subtitle != nil else { print("type == .fields_custom, but subtitle is empty!"); return }
        
        guard type != .access else {
            let vc = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "AccessViewController")
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            (from ?? UIApplication.shared.keyWindow?.rootViewController)?.present(vc, animated: true, completion: nil)
            return;
        }
        
        let vc = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "ErrorPopupViewController") as! ErrorPopupViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext

        vc.loadView()



        switch type {
        case .connection:
            vc.title_label?.text = title ?? "НЕТ СЕТИ"
            vc.subtitle?.text = subtitle ?? "Повторите попытку позже"
            vc.error_icon?.image = #imageLiteral(resourceName: "error_red_icon")
//            vc.main_icon?.image = #imageLiteral(resourceName: "no_connection_icon")
        case .unknown:
            vc.title_label?.text = title ?? "УПС! ОШИБКА"
            vc.subtitle?.text = subtitle ?? "Мы уже работаем над ней"
            vc.error_icon?.image = #imageLiteral(resourceName: "error_yellow_icon")
//            vc.main_icon?.image = #imageLiteral(resourceName: "error_icon")
        case .invalid_data:
            vc.title_label?.text = title ?? "ОШИБКА"
            vc.subtitle?.text = subtitle ?? "Введены неверные данные"
            vc.error_icon?.image = #imageLiteral(resourceName: "error_yellow_icon")
//            vc.main_icon?.image = #imageLiteral(resourceName: "fields_error_icon")
        case .fields_custom:
            vc.title_label?.text = title ?? "ОШИБКА"
            vc.subtitle?.text = subtitle
            vc.error_icon?.image = #imageLiteral(resourceName: "error_yellow_icon")
//            vc.main_icon?.image = #imageLiteral(resourceName: "fields_error_icon")
        default:
            vc.title_label?.text = title ?? "ОШИБКА"
            vc.subtitle?.text = subtitle ?? "Заполните все поля"
            vc.error_icon?.image = #imageLiteral(resourceName: "error_yellow_icon")
//            vc.main_icon?.image = #imageLiteral(resourceName: "fields_error_icon")
        }
        
        

        (from ?? UIApplication.shared.keyWindow?.rootViewController)?.present(vc, animated: true, completion: nil)

    }
}
