//
//  MediaViewerPhotoCell.swift
//  yzer-ios
//
//  Created by Константин on 07.08.17.
//  Copyright © 2017  Yzer Lab LLC. All rights reserved.
//

import UIKit
import SDWebImage


class MediaViewerPhotoCell: UICollectionViewCell {
    
    var item: Int = 0
    fileprivate var lastZoomScale: CGFloat = -1
    
    
    @IBOutlet weak var imageConstraintTop: NSLayoutConstraint!
    @IBOutlet weak var imageConstraintRight: NSLayoutConstraint!
    @IBOutlet weak var imageConstraintLeft: NSLayoutConstraint!
    @IBOutlet weak var imageConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView!


    
    fileprivate var previousDeviceOrientation: UIDeviceOrientation = .portrait
    
    var isFullsizeLoaded = false {
        didSet {
            if isFullsizeLoaded {
                self.activity?.stopAnimating()
            }
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    
    func update(with urlString: String) {
        self.activity?.startAnimating()

        self.imageView.sd_setImage(with: URL(string: urlString)) { (img, _, _, _) in
            self.setImage(img)
        }
    }
    
    private func setImage(_ img: UIImage?) {
        guard img != nil else { return }
        
        async {
            self.imageView.image = img
            self.isFullsizeLoaded = img != nil
            self.updateAll()
        }

    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.updateAll()
    }
    
    var doubleTapGR: UITapGestureRecognizer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.updateAll()
        
        subscribeToOrientationChanges()
        if doubleTapGR == nil || !(self.gestureRecognizers?.contains(doubleTapGR) ?? false) {
            doubleTapGR = UITapGestureRecognizer(target: self, action: #selector(self.doubleTapped(_:)))
            doubleTapGR.numberOfTapsRequired = 2
            doubleTapGR.numberOfTouchesRequired = 1
            self.addGestureRecognizer(doubleTapGR)
        }
    }
    
    @objc private func doubleTapped(_ gr: UITapGestureRecognizer) {
        guard isFullsizeLoaded else { return }
        if scrollView.zoomScale == scrollView.minimumZoomScale {
            let touchCoordinate = gr.location(ofTouch: 0, in: imageView)
            guard touchCoordinate != .zero else { return }
            let rectSide = min(self.imageView.image!.size.width, self.imageView.image!.size.height)/2
            let targetRect = CGRect(x: touchCoordinate.x - rectSide/2, y: touchCoordinate.y - rectSide/2, width: rectSide, height: rectSide)
            scrollView.zoom(to: targetRect, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        }
    }
    
    
    fileprivate func updateAll() {
        DispatchQueue.main.async {
            self.previousDeviceOrientation = .portrait
            self.lastZoomScale = -1
            self.updateZoom()
            self.updateConstraints()
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        if let image = imageView.image {
            let imgSize = image.size
            
            let scrollSize = scrollView.bounds.size
            
            var horizontalInset = (scrollSize.width - scrollView.zoomScale * imgSize.width)/2
            if horizontalInset < 0 { horizontalInset = 0 }
            
            var verticalInset = (scrollSize.height - scrollView.zoomScale * imgSize.height)/2
            if verticalInset < 0 { verticalInset = 0 }
            
            imageConstraintLeft.constant = horizontalInset
            imageConstraintRight.constant = horizontalInset
            
            imageConstraintTop.constant = verticalInset
            imageConstraintBottom.constant = verticalInset
            
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    
    fileprivate func updateZoom() {
        previousDeviceOrientation = validated(from: UIDevice.current.orientation)
        
        if let image = imageView.image {
            
            var minZoom = min(scrollView.bounds.size.width / image.size.width,
                              scrollView.bounds.size.height / image.size.height)
            if minZoom == lastZoomScale { minZoom += 0.000001 }
            
            let maxZoom = minZoom * (isFullsizeLoaded ? 3 : 1)
            
            scrollView.minimumZoomScale = minZoom
            scrollView.maximumZoomScale = maxZoom
            
            self.scrollView.zoomScale = minZoom
            self.lastZoomScale = minZoom
        }
    }
    
    
    
    
    private func subscribeToOrientationChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: .UIDeviceOrientationDidChange, object: nil)
    }
    
    
    @objc private func rotated() {
        
        if previousDeviceOrientation != validated(from: UIDevice.current.orientation) {
            updateZoom()
        }
        
    }
    
    private func validated(from: UIDeviceOrientation) -> UIDeviceOrientation {
        if from.isFlat || from == .portraitUpsideDown {
            return previousDeviceOrientation
        } else {
            return from
        }
    }
    
}

extension MediaViewerPhotoCell: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateConstraints()
    }
}



