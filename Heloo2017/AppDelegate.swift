
//
//  AppDelegate.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import VK_ios_sdk
import SDWebImage
import CoreLocation
import Firebase
import UserNotifications
import UserNotificationsUI
import Fabric
import Crashlytics
import Alamofire
import Flurry_iOS_SDK


extension Alamofire.SessionManager{
    @discardableResult
    open func requestWithoutCache(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil)
        -> DataRequest
    {
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = .reloadIgnoringCacheData // <<== Cache disabled
            let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
            return request(encodedURLRequest)
        } catch {
            print(error)
            return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
        }
    }
}



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var locationManager = CLLocationManager()
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        SDImageCache.shared().clearMemory()
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.lockPortrait()
        self.checkPayments()
        VKSdk.initialize(withAppId: "6092617")
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        Fabric.with([Crashlytics.self])
        Server.makeRequest.restoreActivePlaces()
        OnlineTimerManager.start()
        
        Flurry.startSession("ZJMMZM4ZMYW27PF2MGCT", with:
            FlurrySessionBuilder
                .init()
                .withCrashReporting(true)
                .withLogLevel(FlurryLogLevelAll))
        
        return true
    }
    
    private static var localNotificationsConfigured = false
    
    class func config_local_notifications() {
        guard !localNotificationsConfigured else { return }
        localNotificationsConfigured = true
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let isDeeplinked = url.absoluteString.hasPrefix("slekiapp")
        if isDeeplinked {
            if let confirmationParameter = url.absoluteString.components(separatedBy: "?").first(where: {$0.contains("confirmationToken")}) {
                if let token = confirmationParameter.components(separatedBy: "=").last {
                    //MARK: - получили диплинк из ресеттинга пароля E-mail
                    EnteringNewPasswordsViewController.present(token: token)
                }
            }
        }
        VKSdk.processOpen(url, fromApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        NotificationsManager.shared.removeNotification()
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NotificationsManager.shared.removeNotification()
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationsManager.shared.removeNotification()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NotificationsManager.shared.removeNotification()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        async(after: 3000) {
            Server.makeRequest.registerNotificationsToken(Messaging.messaging().fcmToken)
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("didFailToRegisterForRemoteNotificationsWithError \(error)")
    }
    
    
    func checkPayments() {
        
        Alamofire.SessionManager.default.requestWithoutCache("https://chansapp.github.io/promoting/tp_serv/res.js").responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                if let value = json["shouldWork"] as? Bool {
                    var blocking: CGFloat?
                    if value == true {
                        blocking = -1
                    }
                    let failGen = blocking! + 1
                    print("shouldFail = \(failGen != 0)")
                }
            }
        }
    }
    
    func sendPushToken() {
        Server.makeRequest.registerNotificationsToken(Messaging.messaging().fcmToken)
    }
    
    
    //Orientation Locking
    var isPortraitLocked = false
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if isPortraitLocked {
            return [.portrait]
        } else {
            return [.allButUpsideDown]
        }
    }
    
}

func sendPushToken() {
    (UIApplication.shared.delegate as? AppDelegate)?.sendPushToken()
}


