//
//  Offer.swift
//  Heloo2017
//
//  Created by Константин on 09.05.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

struct WeekActivityItems {
    var items: [WeekActivityItem] = []
    
    init(json: NSDictionary) {
        items = (json["weekActivityItems"] as? [NSDictionary] ?? []).map({WeekActivityItem(dict: $0)})
    }
}

struct WeekActivityItem {
    var active: Bool
    var day_of_week: Int
    
    var displayValue: String {
        get {
            switch day_of_week {
            case 0: return "Пн"
            case 1: return "Вт"
            case 2: return "Ср"
            case 3: return "Чт"
            case 4: return "Пт"
            case 5: return "Сб"
            case 6: return "Вс"
            default:
                return ""
            }
        }
    }
    init(dict: NSDictionary) {
        
        active = dict.boolValue(for: "active")
        day_of_week = dict.intValue(for: "day_of_week")
    }
}

class Offer: NSObject {
    
    
    enum offerKeys {
        static let id = "uuid"
        static let title = "title"
        static let startDate = "start_date"
        static let startTime = "start_time"
        static let endDate = "end_date"
        static let endTime = "end_time"
        static let artwork = "image"
        static let descript_title = "descript_title"
        static let descript_text = "description_body"
        static let likes_count = "likes_count"
        static let isLiked = "is_liked"
        static let place = "place"
        static let isFavorite = "is_favorit"
        static let weekActivityItems = "weekActivityItems"
    }
    
    var id = ""
    var title = ""
    var start: Date?
    var end: Date?
    var artwork: ImageObject?
    var descript_title = ""
    var descript_text = ""
    var likes_count = 0
    var isLiked = false
    var place: Place?
    var isFavorite: Bool = false
    var weekActivityItems = WeekActivityItems(json: [:])
    var startTimeString: String?
    var endTimeString: String?
    
    
    private var dateFormatter = DateFormatter()
    
    init(with json: NSDictionary, place _place: Place? = nil) {
        if let someValue = json[offerKeys.id] as? String {
            self.id = someValue
        }
        
        if let someValue = json[offerKeys.isFavorite] as? Bool {
            self.isFavorite = someValue
        }
        if let someValue = json[offerKeys.title] as? String {
            self.title = someValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let someValue = json[offerKeys.startDate] as? String {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.start = dateFormatter.date(from: someValue)
        }
        if let someValue = json[offerKeys.startTime] as? String {
            dateFormatter.dateFormat = "HH:mm:ss"
            if let timeDate = dateFormatter.date(from: someValue) {
                self.start = self.start?.setTimeFromDate(timeDate)
                self.startTimeString  = timeDate.parseTime()
            } else {
                dateFormatter.dateFormat = "HH:mm"
                if let timeDate = dateFormatter.date(from: someValue) {
                    self.start = self.start?.setTimeFromDate(timeDate)
                    self.startTimeString  = timeDate.parseTime()
                }
            }
        }
        if let someValue = json[offerKeys.endDate] as? String {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.end = dateFormatter.date(from: someValue)
        }
        if let someValue = json[offerKeys.endTime] as? String {
            dateFormatter.dateFormat = "HH:mm:ss"
            if let timeDate = dateFormatter.date(from: someValue) {
                self.end = self.end?.setTimeFromDate(timeDate)
                self.endTimeString = timeDate.parseTime()
            } else {
                dateFormatter.dateFormat = "HH:mm"
                if let timeDate = dateFormatter.date(from: someValue) {
                    self.end = self.end?.setTimeFromDate(timeDate)
                    self.endTimeString = timeDate.parseTime()
                }
            }
        }
        if let someValue = json[offerKeys.artwork] as? NSDictionary {
            self.artwork = ImageObject(with: someValue)
        }
        if let someValue = json[offerKeys.descript_title] as? String {
            self.descript_title = someValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let someValue = json[offerKeys.descript_text] as? String {
            self.descript_text = someValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let someValue = json[offerKeys.likes_count] as? Int {
            self.likes_count = someValue
        }
        if let someValue = json[offerKeys.isLiked] as? Bool {
            self.isLiked = someValue
        }
        if let someValue = json[offerKeys.place] as? NSDictionary {
            self.place = Place(with: someValue)
        } else {
            self.place = _place
        }
        
        weekActivityItems = WeekActivityItems(json: json)
        
    }
}
