//
//  Address.swift
//  Heloo2017
//
//  Created by Константин on 08.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

class Address: NSObject {
    enum fields {
        static let city = "city"
        static let city_tag = "city_tag"
        static let country = "country"
        static let country_tag = "country_tag"
        static let value = "value"
        static let number = "number"
        static let street = "street"
        static let uuid = "uuid"
        static let zip = "zip"
    }
    
    var city: String?
    var city_tag: String?
    var country: String?
    var country_tag: String?
    var number = ""
    var street = ""
    var uuid = ""
    var zip = ""
    
    
    init(with json: NSDictionary) {
        if let cityDict = json[fields.city] as? NSDictionary {
            self.city_tag = cityDict.stringValue(for: fields.city_tag)
            self.city = cityDict.stringValue(for: fields.value)

            if let countryDict = cityDict[fields.country] as? NSDictionary {
                self.country_tag = countryDict.stringValue(for: fields.country_tag)
                self.country = countryDict.stringValue(for: fields.value)
            }
        }

        self.number = json.stringValue(for: fields.number)
        self.street = json.stringValue(for: fields.street)
        self.uuid = json.stringValue(for: fields.uuid)
        self.zip = json.stringValue(for: fields.zip)

    }
}
