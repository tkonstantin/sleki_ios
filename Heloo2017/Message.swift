//
//  Message.swift
//  Heloo2017
//
//  Created by Константин on 29.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import CoreData

@objc(Message)
class Message: NSManagedObject, NSCoding {
    
    func encode(with aCoder: NSCoder) {
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(false)
    }
    
    convenience init(_ insert: Bool) {
        self.init(entity: Message.entitys, insertInto: insert ? CoreDataService.instance.privateContext : nil)
    }
    
    class var entitys: NSEntityDescription {
        var entity = NSEntityDescription()
        entity = NSEntityDescription.entity(forEntityName: "Message", in: CoreDataService.instance.privateContext)!
        return entity
    }
    
    class func outgoingMessage(with text: String, in chat: Chat) -> Message {
        let new = Message(false)
        new.uuid = "temp_local_message"
        new.sender = UserDataManager.instance.currentUser
        new.text = text
        new.date = Date()
        new.isReaded = true
        return new
    }
    
    class func makeMessage(with json: NSDictionary, in _chat: Chat, isValid: Bool = true) -> Message? {
        guard let someUUID = json["uuid"] as? String, CoreDataManager.shared.message(with: someUUID) == nil else {
            return nil
        }
        let new = Message.init(true)

        new.date = (json["created_time"] as? String)?.stringToDate() ?? Date()
        
        if let sender_dict = json["sender"] as? NSDictionary {
            new.sender = User(with: sender_dict)
        }
        
        
        new.text = json["text"] as? String
        new.uuid = json["uuid"] as? String
        new.valid = isValid
        new.isReaded = currentOpenedChat == new.uuid
        
        new.chat = _chat
        new.chat_uuid = _chat.uuid
        _chat.appendMessage(new)
        return new
    }
    
    
    class func makeMessage(userInfo: [AnyHashable: Any]) -> Message? {
        guard let someUUID = userInfo["chat_message_uuid"] as? String, CoreDataManager.shared.message(with: someUUID) == nil else {
            return nil
        }
        let new = Message.init(true)
        
        new.uuid = userInfo["chat_message_uuid"] as? String
        new.chat_uuid = userInfo["chat_uuid"] as? String
        new.sender = User(userInfo: userInfo)
        new.text = userInfo["chat_message_text"] as? String ?? ""
        new.date = Date(timeIntervalSince1970: (Double(userInfo["chat_message_created_time"] as? String ?? "0") ?? 0)/1000)
        new.isReaded = false
        
        if let found_chat = CoreDataManager.shared.chat(with: new.chat_uuid) {
            new.chat_uuid = found_chat.uuid
            found_chat.appendMessage(new)
        } else {
            let newChat = Chat(true)
            newChat.isLocal = false
            newChat.uuid = new.chat_uuid
            newChat.users = [new.sender]
            newChat.messages_count = 1
            newChat.appendMessage(new)
        }
        
        CoreDataService.instance.save()
        return new
    }

    
    @NSManaged var uuid: String!
    @NSManaged var chat_uuid: String!
    @NSManaged var sender: User!
    @NSManaged var text: String!
    @NSManaged var date: Date!
    @NSManaged var isReaded: Bool
    @NSManaged var chat: Chat?
    @NSManaged var valid: Bool
    
    var day: Date {
        get {
            return date.dateToZero
        }
    }
    
    
    var isIncoming: Bool {
        get {
            return !UserDataManager.instance.isMe(sender)
        }
    }
}
