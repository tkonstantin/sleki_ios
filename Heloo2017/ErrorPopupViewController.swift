//
//  ErrorPopupViewController.swift
//  Heloo2017
//
//  Created by Константин on 02.07.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ErrorPopupViewController: UIViewController {

    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var error_icon: UIImageView!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var main_icon: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()



    }

    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
}
