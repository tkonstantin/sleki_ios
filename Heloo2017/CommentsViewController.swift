//
//  CommentsViewController.swift
//  Heloo2017
//
//  Created by Константин on 26.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class CommentsViewController: CardViewController, UITextFieldDelegate {
    
    

    @IBOutlet weak var field_view_bottom: NSLayoutConstraint!
    @IBOutlet weak var toogle_button: UIButton!
    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var field_view: UIView!
    @IBOutlet weak var background_card: UIView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var comments_label: UILabel!
    
    @IBOutlet weak var place_label: UILabel?
    @IBOutlet weak var action_label: UILabel!
    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var tableItemsTop: NSLayoutConstraint!
    
    var commentCompletionBlock: ((Int)->Void)?
    
    let normalTableItemsTop: CGFloat = 44 //(98 + 44) было 50
    
    var post: Post?
    
    var _comments: [Comment] = []
    var comments: [Comment] {
        get {
            return _comments
        } set {
            _comments = newValue.sorted(by: {$0.date > $1.date})
        }
    }
    
    @IBOutlet weak var tableItems: RefreshableTable!
    
    @IBAction func toogle(_ sender: UIButton) {
        let wasFirstResponder = field.isFirstResponder
        guard wasFirstResponder else { field.becomeFirstResponder(); return }
        
        if wasFirstResponder && (self.field.text == nil || self.field.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
            field.resignFirstResponder()
        }
        self.leaveComment(text: self.field.text!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    var isAnimatingAppearance = true
    func animateAppearance() {
        dismissHUD()
        async {
            UIView.animate(withDuration: 0.3, delay: 0.3, options: [], animations: {
                self.background_card.alpha = 0.5
            }, completion: nil)
            UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
                self.tableItemsTop.constant = self.normalTableItemsTop
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.isAnimatingAppearance = false
            })
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
        if self.comments.count > post?.comments_count ?? 0 {
            self.commentCompletionBlock?(self.comments.count)
        }

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table = tableItems
        self.animatableViews = [comments_label: 1.0, close_button: 1.0]
        
        tableItems.addPullToRefresh(color: blueColor) { [weak self] (refreshControl) in
            self?.currentPage = -1
            self?.hasNext = true
            self?.comments = []
            self?.load {
                refreshControl?.endRefreshing()
            }
        }
        
        //        self.configureTable()
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        tableItems.estimatedRowHeight = 93
        tableItems.rowHeight = UITableViewAutomaticDimension
        field.delegate = self
        self.subscribeToKeyboard()
//        self.addTapOutsideGestureRecognizer()
        
        self.name_label.text = post?.big_top_label
        (self.name_label as? UsernameLabel)?.user = post?.searchable_item?.user
        
        let mutableAttributed = NSMutableAttributedString(string: "")
        if let action = post?.action_title {
            
            mutableAttributed.append(NSAttributedString(string: action.string + (post?.type == .checkin_created ? " в" : "")))
        }
        mutableAttributed.append(NSAttributedString(string: ": " + (post?.small_top_label ?? ""), attributes: [NSForegroundColorAttributeName : UIColor(hex: "3AABFF")]))
        
        self.action_label.attributedText = mutableAttributed
        self.place_label?.text = post?.small_top_label
        
//        self.tableItems.tableHeaderView?.frame.size.height = self.tableItems.tableHeaderView!.sizeThatFits(CGSize(width: self.view.bounds.width, height: 256)).height
        async {
            self.tableItems.tableHeaderView?.frame.size.height = self.tableItems.tableHeaderView!.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        }

        showHUD()

        self.load(from: nil)
        
    }
    
    func leaveComment(text: String) {
        guard !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            self.field?.text = ""
            self.field?.resignFirstResponder()
            return
        }
        Server.makeRequest.leaveComment(text: text, for: post) { (comment) in
            FeebackService.designate.message_sent()
            self.field.text = ""
            self.view.endEditing(true)
            if let comm = comment {
                self.comments.append(comm)
                guard self.comments.count > 1 else { self.tableItems.reloadData(); return }
                let newPath = IndexPath(row: 0, section: 0)
                self.tableItems.insertRows(at: [newPath], with: .automatic)
                self.tableItems.scrollToRow(at: newPath, at: .bottom, animated: true)
            } else {
                ErrorPopupManager.showError(type: .unknown, from: self, subtitle: "Лимит: не больше 1 комментария в минуту")
            }
        }
    }
    
    
    private var tempTapGR: UITapGestureRecognizer?
    
    private func addTapGR() {
        guard tempTapGR == nil else { return }
        tempTapGR = UITapGestureRecognizer(target: self, action: #selector(self.tapped))
        tempTapGR?.numberOfTapsRequired = 1
        tempTapGR?.numberOfTouchesRequired = 1
        tempTapGR?.cancelsTouchesInView = true
        self.tableItems.addGestureRecognizer(tempTapGR!)
    }
    
    private func removeTapGR() {
        if let tap =  tempTapGR {
            self.tableItems.removeGestureRecognizer(tap)
        }
        tempTapGR = nil
    }
    
    @objc private func tapped() {
        self.field?.resignFirstResponder()
    }
    
    
    override func keyboardWillBeShown(_ notification: Notification) {
        async {
            self.addTapGR()
            if (self.field?.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                self.toogle_button.setImage(self.field.isFirstResponder ? #imageLiteral(resourceName: "toogle_close_button") : #imageLiteral(resourceName: "toogle_button"), for: .normal)
            } else {
                self.toogle_button.setImage(#imageLiteral(resourceName: "comments_send_button"), for: .normal)
            }
        }
        let info = (notification as NSNotification).userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.3) {
                self.field_view_bottom.constant = keyboardSize.height
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                let keyboardFrame = self.tableItems.convert(keyboardSize, from: nil)
                let intersect = keyboardFrame.intersection(self.tableItems.bounds)
                UIView.animate(withDuration: (info?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.0, animations: {
                    self.tableItems.contentInset = UIEdgeInsetsMake(0, 0, intersect.size.height + 8, 0)
                    self.tableItems.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, intersect.size.height + 8, 0)
//                    self.tableItems.scrollRectToVisible(CGRect(x: 0, y: self.tableItems.contentSize.height, width: self.view.bounds.width, height: keyboardSize.height), animated: true)
                })
            }
        }
    }
    
    
    
    override func keyboardWillBeHidden() {
        onKeyboardHidden()
    }

    fileprivate func onKeyboardHidden() {
        async {
            self.removeTapGR()
        }
        UIView.animate(withDuration: 0.25) {
            self.toogle_button.setImage(#imageLiteral(resourceName: "toogle_button"), for: .normal)
            self.field_view_bottom.constant = 0
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.tableItems.contentInset = .zero
            self.tableItems.scrollIndicatorInsets = .zero
        }
    }
    
    var currentPage = -1
    var hasNext = true
    
    func load(from uuid: String? = nil, _ completion: (()->Void)?=nil) {
        guard hasNext || uuid != nil else { return }
        if uuid == nil {
            currentPage += 1
        }

        Server.makeRequest.getComments(uuid: uuid, page: currentPage, for: post) { (result) in
            self.comments += result.0
            self.hasNext = result.1
            self.tableItems.reloadData()
            self.animateAppearance()
            completion?()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count == 0 ? 1 : comments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if comments.count == 0 {
            tableView.separatorStyle = .none
            
            let cell = tableItems.dequeueReusableCell(withIdentifier: "placeholder")!
            return cell
        } else {
            tableView.separatorStyle = .singleLine
            
            let cell = tableItems.dequeueReusableCell(withIdentifier: "cell") as! CommentTableViewCell
            cell.configure(with: comments[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.comments.count > indexPath.row else { return }
        if let user = self.comments[indexPath.row].user {

            let profile = MyProfileViewController.makeOne(with: user, mode: UserDataManager.instance.isMe(user) ? .my : .other)

            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard self.comments.count > indexPath.row, UserDataManager.instance.isMe(comments[indexPath.row].user) || (post?.isMine ?? false) else { return UISwipeActionsConfiguration(actions: []); }

        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (action, view, completionBlock) in
            let activity = UIActivityIndicatorView(frame: view.bounds)
            activity.backgroundColor = action.backgroundColor
            activity.color = .white
            view.addSubview(activity)
            activity.startAnimating()
            Server.makeRequest.deleteComment(self.comments[indexPath.row].uuid, for: self.post, completion: { (success) in
                async {
                    completionBlock(success)
                    if success {
                        tableView.beginUpdates()
                        self.comments.remove(at: indexPath.row)
                        tableView.deleteRows(at: [indexPath], with: .automatic)
                        if self.comments.count == 0 {
                            tableView.insertRows(at: [indexPath], with: .automatic)
                        }
                        tableView.endUpdates()
                    }
                }
            })
        }
        deleteAction.image = #imageLiteral(resourceName: "delete_cell_icon")
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        return config
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row > tableItems.numberOfRows(inSection: 0) - 2 && tableItems.numberOfRows(inSection: 0) != 0 {
            self.load()
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        
        if newText.isEmpty {
            self.toogle_button.setImage(#imageLiteral(resourceName: "toogle_close_button"), for: .normal)
        } else {
            self.toogle_button.setImage(#imageLiteral(resourceName: "comments_send_button"), for: .normal)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.leaveComment(text: textField.text!)
        return true
    }

    @IBAction func close(_ sender: UIButton) {
        self.makeCoverVerticalDismiss()
    }
    

    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        let presenting = self.presentingViewController
        super.dismiss(animated: flag) {
            presenting?.setNeedsStatusBarAppearanceUpdate()
        }
    }
}
extension UIViewController {
    func makeCoverVerticalDismiss() {
        if self is CommentsViewController == false && self is PhotoCommentsViewController == false {
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.view.subviews.filter({$0 is UITableView == false && $0 is UICollectionView == false && $0 is UIScrollView == false}).forEach({$0.alpha = 0})
            }
        }
        (self as? CardViewController)?.animateAll(alpha: 0, duration: 0.1)
        UIView.animate(withDuration: 0.25, delay: 0, animations: { [weak self] in
            self?.view.frame.origin.y = self!.view.bounds.height*1.2
        }, completion: { [weak self] (_) in
            self?.dismiss(animated: false)
        })
    }
}


extension CommentTableViewCell {
    
    func configure(with comment: Comment) {
        self.date_label.text = comment.date_string
        self.name_label.text = (comment.user?.firstName ?? "") + " " + (comment.user?.lastName ?? "")
        (self.name_label as? UsernameLabel)?.user = comment.user
        self.text_label.text = comment.text
        if let url = URL(string: comment.user?.image_small ?? "") {
            self.user_icon.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"), options: .scaleDownLargeImages)
        } else {
            self.user_icon.image = #imageLiteral(resourceName: "user_feed_placeholder")
        }
    }
}

