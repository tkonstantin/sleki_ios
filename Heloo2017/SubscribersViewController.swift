//
//  CommentsViewController.swift
//  Heloo2017
//
//  Created by Константин on 26.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol SubscribersSelectionDelegate: class {
    func didSelect(user: User)
}
protocol SubscribersSelectionDataSource: class {
    func filteredUsers(_ users: [User]) -> [User]
}

class SubscribersViewController: CardViewController {
    
    @IBOutlet weak var background_card: UIView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var comments_label: UILabel!
    @IBOutlet weak var blue_blur: UIImageView!
    @IBOutlet weak var user_icon: UIImageView!
    @IBOutlet weak var tableTop: NSLayoutConstraint!
    @IBOutlet weak var inviteButton: ResizableButton!
    
    var profileReloadingBlock: (()->Void)?
    
    weak var selectionDelegate: SubscribersSelectionDelegate?
    weak var selectionDataSource: SubscribersSelectionDataSource?
    
    
    enum modes {
        case subscribers
        case subscriptions
    }
    
    
    var isOtherProfile = false
    
    var _mode: modes = .subscribers
    
    var mode: modes {
        get {
            return _mode
        } set {
            _mode = newValue
        }
    }
    
    
    var uuid: String?
    
    var currentPage = -1
    var hasNextPages = true
    var isLoadingNextPage = false
    var loaded = false {
        didSet {
            if loaded {
                dismissHUD()
            } else {
                showHUD()
            }
        }
    }
    
    var users: [User] = []
    var bg_img: UIImage?
    
    var normalTableItemsTop: CGFloat = 50
    
    @IBOutlet weak var tableItems: RefreshableTable!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table = tableItems
        self.tableItems.contentInset.bottom = 88
        self.loaded = false
        
        
        self.user_icon.image = bg_img
        
        
        tableItems.addPullToRefresh(color: blueColor) { [weak self] (refreshControl) in
            self?.users = []
            self?.currentPage = -1
            self?.hasNextPages = true
            self?.loadItems {
                refreshControl?.endRefreshing()
            }
            
        }
        
        UIView.animate(withDuration: 0.3) {
            self.user_icon.alpha = 1
            self.blue_blur.alpha = 0.8
        }
        
        self.animatableViews = [comments_label: 1.0, close_button: 1.0]
        self.comments_label.text = self.mode == .subscribers ? "ПОДПИСЧИКИ" : "ПОДПИСКИ"
        //        self.configureTable()
        self.tableItems.delegate = self
        self.tableItems.dataSource = self
        
        let footer = UITableView()
        footer.backgroundColor = .white
        self.tableItems.tableFooterView = footer
        
        //        self.scrollViewDidScroll(self.tableItems)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.loadItems()
        
    }
    
//
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        profileReloadingBlock?()
        super.dismiss(animated: false, completion: nil)
    }
    
    var isAnimatingAppearance = true
    
    func animateAppearance() {
        async {
            UIView.animate(withDuration: 0.2, delay: 0.3, options: [], animations: {

                self.background_card.alpha = 0.75
            })
            UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
                self.tableTop.constant = self.normalTableItemsTop
                self.inviteButton.alpha = 1
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: { _ in

                self.isAnimatingAppearance = false
            })
        }
        
    }
    
    
    func loadItems(_ completion: (()->Void)?=nil) {
        guard hasNextPages, !isLoadingNextPage else { return }
        currentPage += 1
        isLoadingNextPage = true
        if let id = uuid {
            if mode == .subscriptions {
                Server.makeRequest.getSubscriptions(uuid: id, page: currentPage) { (result, hasNext) in
                    async {
                        self.hasNextPages = hasNext
                        self.isLoadingNextPage = false
                        self.users += result
                        if let ds = self.selectionDataSource {
                            self.users = ds.filteredUsers(self.users)
                        }
                        self.loaded = true
                        self.animateAppearance()
                        self.tableItems.reloadData()
                        //                    self.configureTable()
                        completion?()
                    }

                }
            } else {
                Server.makeRequest.getSubscribers(uuid: id, page: currentPage, completion: { (result, hasNext) in
                    async {
                        self.hasNextPages = hasNext
                        self.isLoadingNextPage = false
                        self.users += result
                        if let ds = self.selectionDataSource {
                            self.users = ds.filteredUsers(self.users)
                        }
                        self.loaded = true
                        self.animateAppearance()
                        self.tableItems.reloadData()
                        //                    self.configureTable()
                        completion?()
                    }

                    
                })
            }
        } else {
            if mode == .subscriptions {
                Server.makeRequest.getSubscriptions(page: currentPage) { (result, hasNext) in
                    async {
                        self.hasNextPages = hasNext
                        self.isLoadingNextPage = false
                        self.users += result
                        if let ds = self.selectionDataSource {
                            self.users = ds.filteredUsers(self.users)
                        }
                        self.loaded = true
                        self.animateAppearance()
                        self.tableItems.reloadData()
                        //                    self.configureTable()
                        completion?()
                    }

                    
                }
            } else {
                Server.makeRequest.getSubscribers(page: currentPage, completion: { (result, hasNext) in
                    async {
                        self.hasNextPages = hasNext
                        self.isLoadingNextPage = false
                        self.users += result
                        if let ds = self.selectionDataSource {
                            self.users = ds.filteredUsers(self.users)
                        }
                        self.loaded = true
                        self.animateAppearance()
                        self.tableItems.reloadData()
                        //                    self.configureTable()
                        completion?()
                    }

                    
                })
            }
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.makeCoverVerticalDismiss()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count == 0 ? 1 : users.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.users.count > indexPath.row else { return }
        guard selectionDelegate == nil else {
            self.makeCoverVerticalDismiss()
            self.selectionDelegate?.didSelect(user: self.users[indexPath.row])
            return
        }
        
        guard !UserDataManager.instance.isMe(self.users[indexPath.row]) else { return }
        
        let profile = MyProfileViewController.makeOne(with: self.users[indexPath.row], mode: .other)

            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.pushViewController(profile, animated: true)
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if users.count == 0 {
            tableView.separatorStyle = .none
            return tableItems.dequeueReusableCell(withIdentifier: "placeholder")!
        }
        
        tableView.separatorStyle = .singleLine
        guard users.count > indexPath.row else { return UITableViewCell() }
        let currentUser = users[indexPath.row]
        let cell = tableItems.dequeueReusableCell(withIdentifier: mode == .subscribers || isOtherProfile ? "cell_wo_button" : "cell") as! SubscribersTableViewCell
        cell.unfollow_button?.tag = indexPath.row
        cell.unfollow_button?.addTarget(self, action: #selector(self.unfollowUser(_:)), for: .touchUpInside)
        cell.unfollow_button?.setImage(currentUser.is_subscribed ? #imageLiteral(resourceName: "unsubscribeButton") : #imageLiteral(resourceName: "subscribeButton"), for: .normal)
        cell.unfollow_button?.tintColor = currentUser.is_subscribed ? grayColor : blueColor
        cell.unfollow_button?.layer.borderColor = currentUser.is_subscribed ? grayColor.cgColor : blueColor.cgColor
        cell.city.text = currentUser.city
        cell.name.text = currentUser.firstName + " " + currentUser.lastName
        (cell.name as? UsernameLabel)?.user = currentUser
        if let url = URL(string: currentUser.image_small.isEmpty ? currentUser.image : currentUser.image_small) {
            cell.icon.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
        } else {
            cell.icon.image = #imageLiteral(resourceName: "user_feed_placeholder")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= self.users.count - 2 && self.users.count != 0  {
            self.loadItems()
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if users.count == 0 {
            return 208
        } else {
            return 68
        }
    }
    
    func unfollowUser(_ sender: UIButton) {
        
        sender.isEnabled = false
        let currentUser = users[sender.tag]
        if currentUser.is_subscribed {
            let alert = UIAlertController(title: "Вы уверены, что хотите отписаться?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Отписаться", style: .destructive, handler: { (_) in
                Server.makeRequest.unsibscribeFromPerson(uuid: currentUser.uuid, completion: { (success) in
                    if success {
                        sender.setImage(#imageLiteral(resourceName: "subscribeButton"), for: .normal)
                        sender.tintColor = blueColor
                        sender.layer.borderColor = blueColor.cgColor
                        sender.isEnabled = true
                        self.users[sender.tag].is_subscribed = false
                    }
                })
            }))
            alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: {_ in
                sender.isEnabled = true
            }))
            self.present(alert, animated: true, completion: nil)

        } else {
            Server.makeRequest.subscribeToPerson(uuid: currentUser.uuid, completion: { (success) in
                if success {
                    sender.setImage(#imageLiteral(resourceName: "unsubscribeButton"), for: .normal)
                    sender.isEnabled = true
                    sender.tintColor = grayColor
                    sender.layer.borderColor = grayColor.cgColor
                    self.users[sender.tag].is_subscribed = true
                    
                }
            })
        }
    }
    
    @IBAction func inviteFriends(_ sender: UIButton) {
        let act = UIActivityViewController(activityItems: ["Узнай новые места и людей рядом с приложением Sleki\n","https://sleki.com"], applicationActivities: nil)
        self.present(act, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
}
