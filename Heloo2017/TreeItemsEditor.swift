//
//  InterestsEditor.swift
//  Heloo2017
//
//  Created by Константин on 12.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


protocol TreeEditorDelegate: class {
    func selectionChanged(for interest: TreeItem)
    func currentItems() -> [TreeItem]
    func item(at index: Int) -> TreeItem?
    
}

protocol TreeEditorDataSource: class {
    func onItemsInserted(at indexes: [Int])
    func onItemsRemoved(at indexes: [Int])
    func onItemsUpdated(at indexes: [Int])
}

class TreeEditor {
    
    weak fileprivate var dataSource: TreeEditorDataSource!
    var selectedInterests: [TreeItem] = []
    
    private var _currentInterests: [TreeItem] = []
    
    fileprivate var current: [TreeItem] {
        get {
            return _currentInterests
        } set {
            
            var allInterests = mainSource
            
            for interest in allInterests {
                if isSelected(interest) || interest.associatedItems.contains(where: {isSelected($0)}) {
                    allInterests += interest.associatedItems
                }
            }
            
            _currentInterests = allInterests.sorted(by: {
                
                let first = $0.parent?.title ?? $0.title
                let second = $1.parent?.title ?? $1.title
                
                if first == second {
                    if $0.associatedItems.count == $1.associatedItems.count {
                        return $0.title < $1.title
                    } else {
                        return $0.associatedItems.count > $1.associatedItems.count
                    }
                } else {
                    return first < second
                }
                
            })
            
            _currentInterests.forEach({ element in element.isSelected = isSelected(element)})
        }
    }
    
    fileprivate var mainSource: [TreeItem] = []
    
    init(interests: [TreeItem], dataSource ds: TreeEditorDataSource, mainSource ms: [TreeItem]) {
        mainSource = ms
        selectedInterests = interests.compactMap({$0.copy() as? TreeItem})
        current = []
        dataSource = ds
    }
    
    func isSelected(_ interest: TreeItem) -> Bool {
        return selectedInterests.contains(where: {$0.key == interest.key})
    }

}

extension TreeEditor: TreeEditorDelegate {
        
    func currentItems() -> [TreeItem] {
        return current
    }
    
    func item(at index: Int) -> TreeItem? {
        guard current.count > index, index >= 0 else { return nil }
        return current[index]
    }
    
    func selectionChanged(for interest: TreeItem) {
        interest.isSelected = !interest.isSelected
        
        if let index = selectedInterests.index(where: {$0.key == interest.key}) {
            selectedInterests.remove(at: index)
        } else {
            selectedInterests.append(interest)
        }

        let assosiated = interest.associatedItems
        
        guard assosiated.count != 0 else {
            if let parent = interest.parent, !parent.isSelected, parent.associatedItems.filter({$0.isSelected}).count == 0 {
                var indexes: [Int] = []
                
                for int in parent.associatedItems {
                    if let index = current.index(of: int) {
                        indexes.append(index)
                    }
                }
                
                for int in parent.associatedItems {
                    if let index = current.index(of: int) {
                        current.remove(at: index)
                    }
                }
                
                if indexes.count != 0 {
                    dataSource.onItemsRemoved(at: indexes)
                }
            }
            return
            
        }
        
        if interest.isSelected {
            let newAssosiated = assosiated.filter({!current.contains($0)})
            current += newAssosiated
            var indexes: [Int] = []
            for int in newAssosiated {
                if let index = current.index(where: ({$0.key == int.key})) {
                    indexes.append(index)
                }
            }
            if indexes.count != 0 {
                dataSource.onItemsInserted(at: indexes)
            }
        } else if interest.associatedItems.filter({$0.isSelected}).count == 0 {

            var indexes: [Int] = []
            
            for int in assosiated {
                if let index = current.index(where: ({$0.key == int.key})) {
                    indexes.append(index)
                }
            }
            
            for int in assosiated {
                if let index = current.index(where: ({$0.key == int.key})) {
                    current.remove(at: index)
                }
            }
            
            if indexes.count != 0 {
                dataSource.onItemsRemoved(at: indexes)
            }
            
        }
    }
    
}
