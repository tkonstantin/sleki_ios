//
//  KCKoloda.swift
//  Heloo2017
//
//  Created by Константин on 06.08.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol KCKolodaDelegate:class {
    func kc_koloda(_ koloda: KCKoloda, didSelectItem at: Int)
    func kc_koloda(_ koloda: KCKoloda, didShowItem at: Int)
    func kc_koload(_ koloda: KCKoloda, didFinishScrolling forced: Bool)
}

protocol KCKolodaDataSource: class {
    func numberOfCards(_ koloda: KCKoloda) -> Int
    func card(for item: Int, in koloda: KCKoloda) -> KCCardView
}


class KCCardView: UIView {
    
    var customView: UIView?
    
    
    convenience init(customView _customView: UIView?, target_size: CGSize) {
        self.init()
        customView = _customView
        
        self.backgroundColor = .clear
        guard let custom = customView else { return }
        
        async(after: 300) { [weak self] in
            custom.bounds.size = CGSize(width: target_size.width - (KCKoloda.horizontalOffsetLeft + KCKoloda.horizontalOffsetRight), height: target_size.height - 2*KCKoloda.verticalOffset )
            custom.center = CGPoint(x: target_size.width/2 - (KCKoloda.horizontalOffsetLeft + KCKoloda.horizontalOffsetRight)/2, y: target_size.height/2 - KCKoloda.verticalOffset)
            custom.setNeedsLayout()
            custom.layoutIfNeeded()
            
            self?.addSubview(custom)
        }
    }
    
    
}

//

@IBDesignable class KCKoloda: UIView {
    
    weak var delegate: KCKolodaDelegate!
    weak var dataSource: KCKolodaDataSource! = nil {
        didSet {
            self.addTapGesture()
            self.addPanGesture()
            self.addMainPanGesture()
        }
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self._reload()
    }
    
    enum modes {
        case centered
        case rightsided
    }
    
    var mode: modes = .centered
    
    var allowsRotation = true
    
    var numberOfItems: Int {
        get {
            return cards.count
        }
    }
    
    private func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tap.cancelsTouchesInView = true
        self.addGestureRecognizer(tap)
    }
    
    @objc private func tapped(_ sender: UITapGestureRecognizer) {
        guard mainCardsArea.contains(sender.location(in: self)) else {
            if sender.location(in: self).x < KCKoloda.horizontalOffsetLeft && !isRestoringRemovedNow {
                currentCardIndex = max(0, currentCardIndex - 1)
                isDragginRemovedCard = false
            }
            return
        }
        delegate.kc_koloda(self, didSelectItem: currentCardIndex)
    }
    
    lazy var removedCardOriginX: CGFloat = { -self.cardSize.width + KCKoloda.horizontalOffsetLeft }()
    static var horizontalOffsetLeft: CGFloat = 36
    static var horizontalOffsetRight: CGFloat = 12
    
    static var verticalOffset: CGFloat = 0//12
    
    var horizontalCardTranslation: CGFloat = 8
    var verticalCardTranslation: CGFloat = 8
    
    
    var cardSize: CGSize {
        get {
            return CGSize(width: self.bounds.width - (KCKoloda.horizontalOffsetLeft + KCKoloda.horizontalOffsetRight), height: self.bounds.height - KCKoloda.verticalOffset*2)
        }
    }
    
    
    private var _currentCardIndex: Int = 0
    private(set) var currentCardIndex: Int {
        get {
            return _currentCardIndex
        }
        set {
            if newValue >= cards.count {
                didFinished()
            } else {
                if newValue > _currentCardIndex {
                    _currentCardIndex = newValue
                    self.removeSwipedCards()
                } else {
                    _currentCardIndex = newValue
                    self.restoreRemovedCards()
                }
            }
            updateCardFrames()
            delegate?.kc_koloda(self, didShowItem: currentCardIndex)
        }
    }
    
    
    private func didFinished() {
        
    }
    
    func resetCurrentCardIndex() {
        removedCards = []
        currentCardIndex = 0
    }
    
    private var cards: [KCCardView?] = []
    
    
    private func drawCards(from position: Int=0) {
        guard position < cards.count else { return }
        for index in position..<cards.count {
            if let card = cards[index] {
                updateCard(card, at: index)
            }
            
        }
    }
    
    private func updateCard(_ card: KCCardView, at index: Int) {
        card.alpha = alphaForCard(at: index - self.currentCardIndex)
        let frame = frameForCard(at: index - self.currentCardIndex)
        self.addSubview(card)
        self.sendSubview(toBack: card)
        //
        if self.mode == .rightsided {
            card.transform = .identity
            card.frame = frame
        } else {
            card.transform = .identity
            card.center = CGPoint(x: frame.midX, y: frame.midY)
            card.bounds.size = self.cardSize
            let scale = (self.cardSize.width - 2*self.horizontalCardTranslation*CGFloat(index - self.currentCardIndex))/self.cardSize.width
            card.transform = CGAffineTransform(scaleX: scale, y: scale).translatedBy(x: -(frame.size.width - frame.size.width*scale)/2, y: 0)
        }
    }
    
    
    fileprivate func updateCardFrames(animated: Bool = true) {
        
        let block = { [weak self] in
            guard let `self` = self else { return }
            
            for index in 0..<self.cards.count {
                guard let card = self.cards[index] else { return }
                
                if index >= self.currentCardIndex {
                    card.alpha = self.alphaForCard(at: index - self.currentCardIndex)
                    
                    let frame = self.frameForCard(at: index - self.currentCardIndex)
                    if self.mode == .rightsided {
                        card.transform = .identity
                        card.frame = frame
                    } else {
                        card.transform = .identity
                        card.center = CGPoint(x: frame.midX, y: frame.midY)
                        card.bounds.size = self.cardSize
                        let scale = (self.cardSize.width - 2*self.horizontalCardTranslation*CGFloat(index - self.currentCardIndex))/self.cardSize.width
                        card.transform = CGAffineTransform(scaleX: scale, y: scale).translatedBy(x: -(frame.size.width - frame.size.width*scale)/2, y: 0)
                    }
                }
            }
        }
        if animated {
            UIView.animate(withDuration: 0.3) {
                block()
            }
        } else {
            block()
        }
        
    }
    
    func alphaForCard(at index: Int) -> CGFloat {
        switch index {
        case 0,1: return 1
        case 2: return 0.75
        default: return 0
        }
    }
    
    func frameForCard(at index: Int) -> CGRect {
        switch mode {
        case .centered:
            var defaultFrame = CGRect(origin: CGPoint(x: KCKoloda.horizontalOffsetLeft + horizontalCardTranslation*CGFloat(index), y: KCKoloda.verticalOffset + 2*verticalCardTranslation*CGFloat(index)), size: cardSize)
            if currentCardIndex == 0 || (currentCardIndex == 1 && isReloading) {
                defaultFrame = defaultFrame.offsetBy(dx: (KCKoloda.horizontalOffsetRight - KCKoloda.horizontalOffsetLeft)/2, dy: 0)
            }
            return defaultFrame
        case .rightsided: return CGRect(origin: CGPoint(x: KCKoloda.horizontalOffsetLeft + horizontalCardTranslation*CGFloat(index), y: KCKoloda.verticalOffset + verticalCardTranslation*CGFloat(index)), size: cardSize)
        }
        
    }
    
    func reload(appending: Bool=false) {
        if (self.cards.count < 2 || self.cards.count > dataSource.numberOfCards(self)) || (!appending) {
            self._reload()
        } else {
            let cardsCount = self.cards.count
            for index in cardsCount..<dataSource.numberOfCards(self) {
                self.cards.append(dataSource.card(for: index, in: self))
            }
            self.drawCards(from: cardsCount)
            self.updateCardFrames()
        }
    }
    
    private var isReloading = false
    private func _reload() {
        guard dataSource != nil else { return }
        
        resetCurrentCardIndex()
        
        let snapshot = self.snapshotView(afterScreenUpdates: false)
        
        if snapshot != nil {
            snapshot!.frame = self.bounds
            self.addSubview(snapshot!)
        }
        
        self.cards.forEach({ card in
            card?.removeFromSuperview()
        })
        
        self.cards = []
        for index in 0..<self.dataSource.numberOfCards(self) {
            self.cards.append(self.dataSource.card(for: index, in: self))
        }
        self.drawCards()
        
        self.isReloading = true
        self.currentCardIndex = 1
        
        async { [weak self] in

            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                snapshot?.alpha = 0
                self?.alpha = 0.25
            }, completion: { (_) in
                snapshot?.removeFromSuperview()
            })
            snapshot?.removeFromSuperview()
            guard self != nil else { return }
            UIView.transition(with: self!, duration: 0.5, options: .transitionFlipFromLeft, animations: { [weak self] in
                self?.alpha = 1
            }, completion: {[weak self] (_) in
                self?.isReloading = false
                self?.currentCardIndex = 0
            })
        }
        
    }
    
    private var isTrackingTopItem: Bool = false
    private var isDragginRemovedCard: Bool = false
    fileprivate var panSuperview: UIView!
    
    private func addPanGesture() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(_:)))
        panSuperview = UIView()
        panSuperview.frame = CGRect(origin: .zero, size: CGSize(width: KCKoloda.horizontalOffsetLeft, height: self.bounds.height))
        panSuperview.backgroundColor = .clear
        panSuperview.addGestureRecognizer(pan)
        self.addSubview(panSuperview)
        self.bringSubview(toFront: panSuperview)
    }
    
    @objc private func pan(_ sender: UIPanGestureRecognizer) {
        
        guard cards.count > currentCardIndex, currentCardIndex != 0 else { return }
        let translationX = sender.translation(in: sender.view!).x
        guard sender.state != .began || translationX >= 0 else { return }
        let card = cards[currentCardIndex-1]
        
        let newOffset = sender.translation(in: sender.view)
        if self.allowsRotation {
            card?.transform = CGAffineTransform(rotationAngle: angle(for: sender))
        } else {
            card?.transform = .identity
        }
        card?.transform = card!.transform.concatenating(CGAffineTransform(translationX: newOffset.x, y: newOffset.y))
        
        //        card.transform = .identity
        //        card.frame.origin.x = removedCardOriginX + translationX
        
        card?.alpha = 1
        isDragginRemovedCard = true
        
        if sender.state == .ended || sender.state == .cancelled || sender.state == .failed {
            
            let velocity = sender.velocity(in: self).x
            animateToClosestPosition(cards[currentCardIndex - 1], velocity: velocity, reportOnNonRemoving: true) { [weak self] in
                if velocity >= 0 {
                    self?.currentCardIndex -= 1
                }
            }
            isDragginRemovedCard = false
            
        }
    }
    
    private func addMainPanGesture() {
        let mainPan = UIPanGestureRecognizer(target: self, action: #selector(self.pannedMain(_:)))
        self.addGestureRecognizer(mainPan)
    }
    
    
    var mainCardsArea: CGRect {
        get {
            return CGRect(x: KCKoloda.horizontalOffsetLeft, y: 0, width: self.bounds.width, height: self.bounds.height)
        }
    }
    
    @objc private func pannedMain(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .began:
            if mainCardsArea.contains(sender.location(in: self)) {
                isTrackingTopItem = true
            }
        case .changed:
            
            guard let card = cards[currentCardIndex] else { return }
            let newOffset = sender.translation(in: self)
            
            
            if self.allowsRotation {
                card.transform = CGAffineTransform(rotationAngle: angle(for: sender))
            } else {
                card.transform = .identity
            }
            card.transform = card.transform.concatenating(CGAffineTransform(translationX: newOffset.x, y: newOffset.y))
        //            card.transform = CGAffineTransform(translationX: newOffset.x, y: newOffset.y)
        case .cancelled, .ended, .failed:
            finishDragging(velocity: sender.velocity(in: self).x)
        default: break;
        }
    }
    
    func angle(for sender: UIPanGestureRecognizer) -> CGFloat {
        let newOffset = sender.translation(in: self)
        let senderViewHeight = cardSize.height
        
        
        let touchVerticalPosition = sender.location(in: sender.view).y - newOffset.y
        let touchVerticalPositionPercent = touchVerticalPosition/senderViewHeight
        let angle = (((newOffset.x)/self.bounds.width) + ((newOffset.y)/self.bounds.height)) * .pi/24
        
        let multiplier = 1 - (touchVerticalPositionPercent - 1/3)*3
        
        return angle*multiplier

        
    }
    
    
    private func finishDragging(velocity: CGFloat) {
        isDragginRemovedCard = false
        
        isTrackingTopItem = false
        if cards.count > currentCardIndex {
            animateToClosestPosition(cards[currentCardIndex], velocity: velocity) { [weak self] in
                self?.currentCardIndex += 1
            }
        }
    }
    
    func viewForCard(at index: Int) -> KCCardView? {
        guard cards.count > index else { return nil }
        return self.cards[index]
    }
    
    private func animateToClosestPosition(_ card: KCCardView?, velocity: CGFloat, reportOnNonRemoving: Bool = false, shouldIncreaseIndex: (()->Void)?=nil) {
        let shouldRemoveCard = velocity < 0 && card != cards.last
        
        let xDelta = (card?.frame.origin.x ?? 0) - self.removedCardOriginX + abs(card?.transform.tx ?? 0)
        
        let springVelocity = abs(velocity/xDelta)
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: springVelocity, options: [.curveEaseIn], animations: { [weak self] in
            if shouldRemoveCard {
                card?.transform = CGAffineTransform.identity
                if self != nil {
                    card?.frame = CGRect(x: self!.removedCardOriginX, y: KCKoloda.verticalOffset, width: self!.cardSize.width, height: self!.cardSize.height)
                }
                shouldIncreaseIndex?()
            } else {
                self?.updateCardFrames(animated: false)
                if reportOnNonRemoving {
                    shouldIncreaseIndex?()
                }
            }
        })
    }
    
    private var removedCards: Set<Int> = []
    
    private func removeSwipedCards() {
        guard currentCardIndex > 1 else { return }
        let index = currentCardIndex - 2
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.cards[index]?.frame.origin.x = -self!.cardSize.width
        }, completion: {[weak self] (_) in
            self?.cards[index]?.removeFromSuperview()
            self?.cards[index] = KCCardView()
            self?.removedCards.insert(index)
        })
    }
    
    private var isRestoringRemovedNow = false
    private func restoreRemovedCards() {
        for index in removedCards.sorted() {
            if index >= currentCardIndex - 1 && self.cards.count >= index + 1 {
                self.isRestoringRemovedNow = true
                cards[index] = self.dataSource.card(for: index, in: self)
                self.delegate.kc_koloda(self, didShowItem: index)
                self.updateCard(self.cards[index]!, at: index+1)
                self.cards[index]?.alpha = 1
                self.insertSubview(self.cards[index]!, at: self.cards.count - index - 1)
                self.cards[index]?.frame.origin.x = -self.cardSize.width
                self.removedCards = Set(self.removedCards.filter({$0 != index}))
                self.bringSubview(toFront: self.panSuperview)
                
                async(after: 300) { [weak self] in
                    UIView.animate(withDuration: 0.2, animations: { [weak self] in
                        if (self?.cards.count ?? 0) > index {
                            self?.cards[index]?.frame.origin.x = self!.removedCardOriginX
                        }
                    }, completion: {[weak self] (_) in
                        self?.isRestoringRemovedNow = false
                    })
                }
            }
        }
    }
    
}



func -(_ f: CGPoint, _ s: CGPoint) -> CGPoint {
    return CGPoint(x: f.x - s.x, y: f.y - s.y)
}

