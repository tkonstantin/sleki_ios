//
//  ProfilePhotosTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 15.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

protocol PhotosCellDelegate:  class {
    func shouldOpenPhoto(_ link: String?)
}

class ProfilePhotosTableViewCell: UITableViewCell {
    
    static let id = "photosCell"
    fileprivate weak var delegate: PhotosCellDelegate!
    
    func update(with _photos: [PlacePhotoObject], delegate d: PhotosCellDelegate) {
        delegate = d
        photos = _photos
        self.collection.reloadData()
    }

    fileprivate var photos: [PlacePhotoObject] = []
    
    @IBOutlet weak var collection: UICollectionView! {
        didSet {
            collection.delegate = self
            collection.dataSource = self
        }
    }
    
}

extension ProfilePhotosTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UICollectionViewCell
        (cell.viewWithTag(1) as? UIImageView)?.sd_setImage(with: URL(string: photos[indexPath.item].avatar_url ?? ""))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.shouldOpenPhoto(photos[indexPath.item].image_url)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = min((self.collection.bounds.width - 24)/4 - 8, collectionView.bounds.height)
        return CGSize(width: side, height: side)
    }
}
