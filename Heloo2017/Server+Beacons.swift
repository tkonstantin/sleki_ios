//
//  Server+Beacons.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

extension Server {
    
    func getAvailableBeacons(page: Int=0, completion: @escaping (([Beacon], Bool)->Void)) {
        Alamofire.request(url_paths.base + url_paths.beacons.main, method: .get, parameters: ["page": page, "size": 100], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var hasNext = false
            var beacons: [Beacon] = []
            print("getAvailableBeacons response = \(response)")
            print("loading beacons...")
            
            if let json = response.result.value as? NSDictionary, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    beacons.append(Beacon(true).loaded(with: item))
                }
                CoreDataService.instance.save()
                hasNext = json["hasNext"] as? Bool ?? false
            }
            completion(beacons, hasNext)
        }
    }
    
    
    
    func searchPlaces(by beacons: [Beacon], completion: @escaping ([Place])->Void) {
        let params: [String: String] = ["beaconUuid": beacons.map({$0.uuid}).joined(separator: ",")]
        Alamofire.request(url_paths.base + url_paths.places.searchByBeacons, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            var places: [Place] = []
            if let json = response.json, let resultSet = json["result_set"] as? [NSDictionary] {
                for result in resultSet {
                    places.append(Place(with: result))
                }
            }
            
            var currentFoundedPlaces = self.foundedPlaces
            
            for place in places {
                if let existingPlace = self.foundedPlaces.keys.first(where: {$0.uuid == place.uuid}) {
                    currentFoundedPlaces[existingPlace] = nil
                }
                currentFoundedPlaces[place] = Date()
            }
            
            self.foundedPlaces = currentFoundedPlaces
            
            completion(places)
        }
    }

    
    func searchPersons(by beacons: [Beacon], completion: @escaping ([User])->Void) {
        guard beacons.count != 0 else { completion([]); return }
        let params: [String: String] = ["beaconUuid": beacons.map({$0.uuid}).joined(separator: ",")]
        Alamofire.request(url_paths.base + url_paths.persons.searchByBeacons, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var users: [User] = []
            if let json = response.json, let resultSet = json["result_set"] as? [NSDictionary] {
                for result in resultSet {
                    users.append(User(with: result))
                }
            }
            completion(users)
        }
    }
    

    
}
