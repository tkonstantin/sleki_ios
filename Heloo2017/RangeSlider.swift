//
//  RangeSlider.swift
//  SnowQueen
//
//  Created by Константин on 24.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class RangeSlider: UIControl {
    
    @IBInspectable var infiniteOnMax: Bool = false {
        didSet {
            update()
        }
    }
    
    @IBInspectable var minValue: CGFloat = 1 {
        didSet {
            update()
        }
    }
    
    @IBInspectable var maxValue: CGFloat = 100 {
        didSet {
            update()
        }
    }
    
    @IBInspectable var leftThumbValue: CGFloat = 0 {
        didSet {
            update()
        }
    }
    
    @IBInspectable var rightThumbValue: CGFloat = 0 {
        didSet {
            update()
        }
    }
    
    
    private func update() {
        updateViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateViews()
        
    }
    
    private var progressLine: UIView!
    
    private var leftThumbLabel: UILabel!
    private var leftThumb: ShadowedView!
    
    private var rightThumbLabel: UILabel!
    private var rightThumb: ShadowedView!
    
    private var centerView: UIView!
    
    private var panGR: UIPanGestureRecognizer!
    
    
    var leftThumbPosition: CGFloat {
        get {
            return 5 + leftPercentage*(self.bounds.width - 10)
        }
    }
    
    var rightThumbPosition: CGFloat {
        get {
            return 5 + rightPercentage*(self.bounds.width - 10)
        }
    }
    
    
    var leftPercentage: CGFloat {
        get {
            return (leftThumbValue-minValue)/(maxValue - minValue)
        }
    }
    
    var rightPercentage: CGFloat {
        get {
            return (rightThumbValue-minValue)/(maxValue - minValue)
        }
    }
    
    var leftLabelOffset: CGFloat {
        get {
            return (leftThumbLabel.intrinsicContentSize.width/2)*(1/4 - leftPercentage*2) - 4
        }
    }
    
    
    var rightLabelOffset: CGFloat {
        get {
            return (rightThumbLabel.intrinsicContentSize.width/2)*(5/4 - rightPercentage*2) + 10
        }
    }
    
    private func updateViews() {
        
        if progressLine == nil {
            progressLine = UIView()
            progressLine.backgroundColor = UIColor(hex: "F1F1F8")
            progressLine.cornerRadius = 2.5
            self.addSubview(progressLine)
        }
        progressLine.frame.size = CGSize(width: self.bounds.width - 10, height: 5)
        progressLine.center = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
        
        if centerView == nil {
            centerView = UIView()
            centerView.backgroundColor = UIColor(hex: "F1F1F8")
            centerView.clipsToBounds = true

            let img = UIImageView(image: #imageLiteral(resourceName: "gradient_violet"))
            img.clipsToBounds = true
            centerView.addSubview(img)
            
            self.addSubview(centerView)
        }
        
        
        if leftThumb == nil {
            leftThumb = ShadowedView()
            leftThumb.shadowColor = "C6C9D6"
            leftThumb.shadowAlpha = 1
            leftThumb.shadowRadius = 10
            leftThumb.shadowOffset = CGSize(width: 0, height: 2)
            
            leftThumb.backgroundColor = .white//self.tintColor
            leftThumb.layer.cornerRadius = 14
            leftThumb.frame.size = CGSize(width: 28, height: 28)
            leftThumb.center.y = self.bounds.height/2
            self.addSubview(leftThumb)
        }
        
        if leftThumbLabel == nil {
            leftThumbLabel = UILabel()
            leftThumbLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            leftThumbLabel.textAlignment = .center
            leftThumbLabel.textColor = .black
            leftThumbLabel.frame.size = CGSize(width: 128, height: 20)
            leftThumbLabel.center.y = self.bounds.height/2 - 28
            self.addSubview(leftThumbLabel)
        }
        
        leftThumbLabel.text = String(Int(leftThumbValue))
        leftThumbLabel.center.x = leftThumbPosition + leftLabelOffset
        leftThumb.center.x = leftThumbPosition
        
        if rightThumb == nil {
            rightThumb = ShadowedView()
            rightThumb.shadowColor = "C6C9D6"
            rightThumb.shadowAlpha = 1
            rightThumb.shadowRadius = 10
            rightThumb.shadowOffset = CGSize(width: 0, height: 2)
            
            rightThumb.backgroundColor = .white//self.tintColor
            rightThumb.layer.cornerRadius = 14
            rightThumb.frame.size = CGSize(width: 28, height: 28)
            rightThumb.center.y = self.bounds.height/2
            self.addSubview(rightThumb)
        }
        
        
        if rightThumbLabel == nil {
            rightThumbLabel = UILabel()
            rightThumbLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            rightThumbLabel.textAlignment = .center
            rightThumbLabel.textColor = .black
            rightThumbLabel.frame.size = CGSize(width: 128, height: 20)
            rightThumbLabel.center.y = self.bounds.height/2 - 28
            self.addSubview(rightThumbLabel)
        }
        

        centerView.frame = progressLine.frame
        centerView.frame.origin.x = leftThumb.frame.origin.x + leftThumb.frame.size.width - 1
        centerView.frame.size.width = rightThumb.frame.origin.x - leftThumb.frame.origin.x - leftThumb.frame.size.width + 2
        centerView.translatesAutoresizingMaskIntoConstraints = true
        
        if infiniteOnMax && rightThumbValue == maxValue {
//            rightThumbLabel.text = "∞"
            rightThumbLabel.text = "\(Int(maxValue))+"
        } else {
            rightThumbLabel.text = String(Int(rightThumbValue))
        }
        rightThumbLabel.center.x = rightThumbPosition + rightLabelOffset
        rightThumb.center.x = rightThumbPosition
        
        if panGR == nil {
            panGR = UIPanGestureRecognizer(target: self, action: #selector(self.panned(_:)))
            self.addGestureRecognizer(panGR)
        }
    }
    
    
    private var panningThumb: UIView?
    private var previousThumbValue: CGFloat = 0
    
    @objc private func panned(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .cancelled, .ended, .failed:
            
            panningThumb = nil
            
        case .began:
            if self.leftThumb.frame.insetBy(dx: -24, dy: -44).contains(sender.location(in: self)) {
                panningThumb = leftThumb
                previousThumbValue = leftThumbValue
            } else if rightThumb.frame.insetBy(dx: -24, dy: -44).contains(sender.location(in: self)) {
                panningThumb = rightThumb
                previousThumbValue = rightThumbValue
            }
        default:
            switch panningThumb {
            case  leftThumb:
                leftThumbValue = max(0, min(rightThumbValue - possibleDistanceValue, max(minValue, previousThumbValue + (maxValue - minValue)*(sender.translation(in: self).x/(self.bounds.width-10)))))
            case rightThumb:
                rightThumbValue = min(maxValue, max(leftThumbValue + possibleDistanceValue, previousThumbValue + (maxValue - minValue)*(sender.translation(in: self).x/(self.bounds.width-10))))
            default: return;
            }
            
            self.sendActions(for: .valueChanged)
            updateViews()
        }
    }
    
    
    let possibleDistancePoints: CGFloat = 30
    
    var possibleDistanceValue: CGFloat {
        get {
            return possibleDistancePoints*(maxValue - minValue)/(self.bounds.width - 10)
        }
    }
}
