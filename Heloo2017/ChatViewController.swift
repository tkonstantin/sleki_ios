//
//  ChatViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import CoreData


var currentOpenedChat: String? = nil

extension UIViewController {
    var safeAreaBottom: CGFloat {
        get {
            if #available(iOS 11.0, *) {
                return self.view.safeAreaInsets.bottom
            } else {
                return 0
            }
        }
    }
    
    var safeAreaTop: CGFloat {
        get {
            if #available(iOS 11.0, *) {
                return self.view.safeAreaInsets.top
            } else {
                return UIApplication.shared.statusBarFrame.height
            }
        }
    }
}





class ChatViewController: UIViewController {
    
    class func makeOne(with chat: Chat) -> ChatViewController {
        let new = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        new.configure(with: chat)
        return new
    }
    
    fileprivate lazy var fetchController: NSFetchedResultsController<Message> = {
        let fetch = CoreDataManager.shared.messagesController(for: self.chat)
        fetch.delegate = self
        try? fetch.performFetch()
        return fetch
    }()
    
    fileprivate(set) var chat: Chat! {
        didSet {
            currentOpenedChat = chat?.uuid
            guard chat != nil else { return }
            self.markReaded()
            
            updateUI()
            simpleHistoryLoading()
            
            if chat.isLocal {
                guard let user = chat.users.first else { return }
                //MARK: - initializing chat
                Server.makeRequest.initializeChat(to: user) { [weak self] (createdChat) in
                    guard createdChat != nil else { return }
                    
                    self?.chat = createdChat
                    self?.markReaded()
                    self?.fetchController = CoreDataManager.shared.messagesController(for: self!.chat)
                    self?.fetchController.delegate = self
                    try? self?.fetchController.performFetch()
                    self?.configure(with: createdChat ?? self!.chat)
                }
            }
        }
    }
    
    
    
    @IBOutlet weak var field_view_bottom: NSLayoutConstraint!
    @IBOutlet weak var toogleButton: UIButton!
    @IBOutlet weak var inputPlaceholder: UILabel!
    @IBOutlet weak var inputField: VerticallyCenteredTextView! {
        didSet {
            inputField.delegate = self
            inputField.text = chat?.pendingText
            
            if inputField.text != nil && !inputField.text.isEmpty {
                async {
                    self.inputPlaceholder?.isHidden = true
                }
            }
        }
    }
    @IBOutlet weak var inputHeight: NSLayoutConstraint!
    @IBOutlet weak var chatName: UILabel!
    @IBOutlet weak var chatIcon: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var isUpdating = false
    fileprivate var editingSections = Set<Int>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor(hex: "454545")
        tableView.register(UINib(nibName: MessageCell.text_incoming_id, bundle: nil), forCellReuseIdentifier: MessageCell.text_incoming_id)
        tableView.register(UINib(nibName: MessageCell.text_id, bundle: nil), forCellReuseIdentifier: MessageCell.text_id)
        
        tableView.transform = CGAffineTransform(rotationAngle: .pi)
        tableView.tableFooterView?.transform = CGAffineTransform(rotationAngle: .pi)
        tableView.tableHeaderView?.transform = CGAffineTransform(rotationAngle: .pi)
        
        subscribeToKeyboard()
        addTapOutsideGestureRecognizer()
        updateUI()
        tableView.tableFooterView?.frame.size.height = 0
        
        
    }
    
    override func addTapOutsideGestureRecognizer() {
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapOutSide.cancelsTouchesInView = false
        self.tableView.addGestureRecognizer(tapOutSide)
    }
    
    func simpleHistoryLoading() {
        guard self.chat?.isLocal == false else { return }
        ChattingService.shared.getNewMessages(for: self.chat!) { [weak self] (_) in
            self?.tableView?.tableHeaderView?.frame.size.height = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(hex: "454545")
        UIApplication.shared.statusBarStyle = .default
        
        async {
            if let input = self.inputField {
                self.inputField?.frame = input.frame
                self.textViewDidChange(input)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, self.tableView.bounds.size.width-8);
    }
    
    
    
    func markReaded() {
        guard !self.isBeingDismissed else { return }
        
        self.chat.messages.forEach({$0.isReaded = true})
        CoreDataService.instance.save()
        
        
        let _fetch = CoreDataManager.shared.chatsListController()
        try? _fetch.performFetch()
        
        let unreadedMessagesCount = _fetch.fetchedObjects?.reduce(0, { (res, chat) -> Int in
            return res + chat.unreaded_count
        }) ?? 0
        UIApplication.shared.applicationIconBadgeNumber = unreadedMessagesCount
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.tintColor = .white
        ChattingService.shared.onLeavedConcreteChat()
        
        savePengingTextIfNeeded()
        self.markReaded()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        currentOpenedChat = nil
    }
    
    
    func savePengingTextIfNeeded() {
        
        if (chat?.pendingText == nil || chat.pendingText!.isEmpty) && (inputField?.text == nil || inputField.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
            return;
        }
        
        guard chat?.pendingText != inputField?.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        
        chat?.pendingText = inputField?.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        CoreDataService.instance.save()
    }
    
    
    func configure(with _chat: Chat) {
        chat = _chat
        
    }
    
    func updateUI() {
        guard chat != nil, chatName != nil else {
            return
        }
        
        async {
            self.chatName.text = self.chat.displayTitle
            (self.chatName as? UsernameLabel)?.user = self.chat.users.first
            self.chatIcon.sd_setImage(with: URL(string: self.chat.displayAvatar ?? ""), placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
        }
        
    }
    
    @IBAction func chat_avatar_tapped(_ sender: Any) {
        if let user = chat.users.first {
            let profile = MyProfileViewController.makeOne(with: user, mode: .other)
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        let wasFirstResponder = inputField.isFirstResponder
        guard wasFirstResponder else { inputField.becomeFirstResponder(); return }
        
        if wasFirstResponder && (self.inputField.text == nil || self.inputField.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty) {
            inputField.resignFirstResponder()
        }
        self.sendMessage()
    }
    
    @IBAction func tapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    func sendMessage() {
        guard let text = self.inputField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !text.isEmpty else { return  }
        
        self.toogleButton.isEnabled = false
        
        let tinyActivity = UIActivityIndicatorView(frame: self.toogleButton.frame)
        tinyActivity.backgroundColor = .clear
        tinyActivity.color = blueColor
        tinyActivity.hidesWhenStopped = true
        self.toogleButton.setImage(nil, for: .normal)
        self.toogleButton.superview?.addSubview(tinyActivity)
        tinyActivity.startAnimating()
        
        let message = Message.outgoingMessage(with: text, in: self.chat)
        
        let completionBlock = {
            FeebackService.designate.message_sent()
            self.inputField.text = nil
            self.textViewDidChange(self.inputField)
            self.toogleButton.isEnabled = true
            self.toogleButton.setImage(#imageLiteral(resourceName: "toogle_button"), for: .normal)
            tinyActivity.stopAnimating()
            tinyActivity.removeFromSuperview()
        }
        
        if chat.isLocal {
            completionBlock()
            scrollDown()
        } else {
            //MARK: - just sending message
            Server.makeRequest.sendMessage(message, to: self.chat) { [weak self] (_createdMessage) in
                completionBlock()
                self?.scrollDown()
            }
        }
        
    }
    
    
    func scrollDown(animated: Bool = true) {
        async {
            guard self.tableView.numberOfSections > 0 && self.tableView.numberOfRows(inSection: 0) > 0 else { return }
            
            let lastPath = IndexPath(row: 0, section: 0)
            self.tableView.scrollToRow(at: lastPath, at: .top, animated: animated)
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .default
        }
    }
    
}

extension ChatViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        isUpdating = true
        self.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
        isUpdating = false
        editingSections = []
        async(after: 1000) { [weak self] in
            if self?.isUpdating == false {
                self?.tableView.reloadData()
            }
        }
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            guard indexPath != nil else { return }
            self.tableView.deleteRows(at: [indexPath].compactMap({$0}), with: self.editingSections.contains(indexPath!.section) ? .none : .top)
        case .insert:
            guard newIndexPath != nil else { return }
            self.tableView.insertRows(at: [newIndexPath].compactMap({$0}), with: self.editingSections.contains(newIndexPath!.section) ? .none : .top)
            (self.tableView.cellForRow(at: newIndexPath!) as? MessageCell)?.shape?.removeFromSuperlayer()
        case .move:
            if indexPath != nil && newIndexPath != nil {
                self.tableView.moveRow(at: indexPath!, to: newIndexPath!)
            }
        case .update:
            guard indexPath != nil else { return }
            self.tableView.reloadRows(at: [indexPath].compactMap({$0}), with: .none)
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .delete:
            editingSections.insert(sectionIndex)
            self.tableView.deleteSections([sectionIndex], with: .automatic)
        case .insert:
            editingSections.insert(sectionIndex)
            self.tableView.insertSections([sectionIndex], with: .automatic)
        case .move:
            break;
        case .update:
            self.tableView.reloadSections([sectionIndex], with: .none)
        }
    }
    
}


extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableHeaderView?.frame.size.height = (fetchController.fetchedObjects ?? []).count == 0 ? 64 : 0
        return fetchController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchController.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentMessage = fetchController.object(at: indexPath)
        
        let nextPath: IndexPath! = nextMessagePath(for: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: currentMessage.isIncoming ? MessageCell.text_incoming_id : MessageCell.text_id) as! MessageCell
        cell.update(with: currentMessage, needsTail: nextPath == nil || indexPath.row == 0 || fetchController.object(at: nextPath).isIncoming != currentMessage.isIncoming)
        return cell
    }
    
    func previousMessagePath(for indexPath: IndexPath) -> IndexPath! {
        if indexPath.row + 1 < tableView.numberOfRows(inSection: indexPath.section) {
            return IndexPath(row: indexPath.row + 1, section: indexPath.section)
        } else if indexPath.section + 1 < tableView.numberOfSections {
            return IndexPath(row: tableView.numberOfRows(inSection: indexPath.section + 1) - 1, section: indexPath.section + 1)
        } else {
            return nil
        }
    }
    
    func nextMessagePath(for indexPath: IndexPath) -> IndexPath! {
        if indexPath.row > 0 {
            return IndexPath(row: indexPath.row - 1, section: indexPath.section)
        } else if indexPath.section > 0 {
            return IndexPath(row: tableView.numberOfRows(inSection: indexPath.section - 1) - 1, section: indexPath.section - 1)
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == (fetchController.sections?.count ?? 0) - 1 && indexPath.row == (fetchController.sections?.last?.numberOfObjects ?? 0) - 1 && !self.chat.isHistoryLoaded {
            tableView.tableFooterView?.frame.size.height = 64
            ChattingService.shared.getHistoryMessages(for: self.chat) { [weak tableView] (hasNext) in
                DispatchQueue.main.async {
                    tableView?.tableFooterView?.frame.size.height = 0
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UINib(nibName: "HeaderView", bundle: nil).instantiate(withOwner: self, options: nil).first as? HeaderView
        footer?.title.text = fetchController.object(at: IndexPath(row: 0, section: section)).date.parseDateShort(dots: true)
        footer?.transform = CGAffineTransform(rotationAngle: .pi)
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 56
    }
}

extension ChatViewController: UITextViewDelegate {
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        inputPlaceholder?.isHidden = !(textView.text as NSString).replacingCharacters(in: range, with: text).isEmpty || textView.isFirstResponder
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        UIView.performWithoutAnimation {
            self.toogleButton?.setImage(textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ? #imageLiteral(resourceName: "toogle_close_button") : #imageLiteral(resourceName: "comments_send_button"), for: .normal)
            let newHeight = max(newSize.height, 35)
            self.inputField?.isScrollEnabled = newHeight > 200
            self.inputHeight?.constant = min(newHeight, 200)
            self.inputField?.setNeedsLayout()
            self.inputField?.layoutIfNeeded()
        }
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        inputPlaceholder.isHidden = !textView.text.isEmpty || textView.isFirstResponder
        self.inputField?.frame = self.inputField.frame
        textViewDidChange(inputField)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        inputPlaceholder.isHidden = !textView.text.isEmpty || textView.isFirstResponder
        
    }
}


extension ChatViewController { //keyboard stuff
    override func keyboardWillBeShown(_ notification: Notification) {
        
        let info = (notification as NSNotification).userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: (info?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.3, animations: {
                self.field_view_bottom.constant = keyboardSize.height - self.safeAreaBottom
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            })
            
        }
    }
    
    override func keyboardWillBeHidden() {
        onKeyboardHidden()
    }
    
    fileprivate func onKeyboardHidden() {
        
        UIView.animate(withDuration: 0.3) {
            self.toogleButton.setImage(#imageLiteral(resourceName: "toogle_button"), for: .normal)
            self.field_view_bottom.constant = 0
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            
        }
        
        
    }
}

class VerticallyCenteredTextView: UITextView {
    
    override var contentSize: CGSize {
        didSet {
            correctInset()
        }
    }
    
    override var frame: CGRect {
        didSet {
            correctInset()
        }
    }
    
    func correctInset() {
        UIView.performWithoutAnimation {
            var topCorrection = (frame.size.height - contentSize.height * zoomScale) / 2.0
            topCorrection = max(0, topCorrection)
            contentInset = UIEdgeInsets(top: topCorrection, left: 4, bottom: 0, right: 4)
        }
        
    }
}

