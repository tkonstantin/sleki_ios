//
//  TutorialViewController.swift
//  Heloo2017
//
//  Created by Константин on 13/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import CoreLocation


var currentActiveController: UIViewController? {
    get {
        return currentActiveNavigation?.viewControllers.first
    }
}

var currentActiveNavigation: UINavigationController? {
    get {
        return rootTab?.selectedViewController as? UINavigationController
    }
}



class TutorialViewController: UIViewController {
    
    @IBOutlet weak var feedView: UIView!
    @IBOutlet weak var placesMainView: UIView!
    @IBOutlet weak var placesFavoritesView: UIView!
    @IBOutlet weak var placesFilterView: UIView!
    @IBOutlet weak var placesPermissionView: UIView!
    @IBOutlet weak var placesCheckinView: UIView!
    @IBOutlet weak var peopleMainView: UIView!
    @IBOutlet weak var peopleInterestsView: UIView!
    
    
    enum stages {
        case news
        case places
        case people
    }
    
    var currentStage: stages = .news
    
    var allViews: [UIView] {
        get {
            switch currentStage {
            case .news:
                return [feedView]
            case .places:
                return [placesMainView, placesFavoritesView, placesFilterView, placesPermissionView, placesCheckinView]
            case .people:
                return [peopleMainView, peopleInterestsView]
            }
        }
    }
    
    var manager: CLLocationManager!
    var pendingActionBlock: (()->Void)?=nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        areNotificationsAllowed = false
        isHudAllowed = false
        self.allViews.forEach({$0.isHidden = $0.tag != self.position.rawValue})

    }
    
    @IBAction func nextTapped(_ sender: UIButton) {
        
        let actionBlock = {
            self.incrementPosition()
            self.allViews.forEach({$0.isHidden = $0.tag != self.position.rawValue})
        }
        
        if position == .placesPermission {
            pendingActionBlock = actionBlock
            manager = CLLocationManager()
            manager.delegate = self
            manager.requestAlwaysAuthorization()
        } else {
            actionBlock()
        }

    }
    
    
    enum positions: Int {
        case feed = 0
        case placesMain = 1
        case placesFavorites
        case placesFilter
        case placesPermission
        case placesCheckin
        case peopleMain
        case peopleInterests
        case completed
        
        
        var center: CGPoint {
            get {
                guard rootTab != nil else { return .zero }
                
                switch self {
                case .feed:
                    return CGPoint(x: UIScreen.main.bounds.width/5*(CGFloat(0) + 0.5), y: UIScreen.main.bounds.height - rootTab!.tabBar.frame.size.height + 25)
                    
                case .placesMain:
                   return CGPoint(x: UIScreen.main.bounds.width/5*(CGFloat(1) + 0.5), y: UIScreen.main.bounds.height - rootTab!.tabBar.frame.size.height + 25)
                    
                case .peopleMain:
                   return CGPoint(x: UIScreen.main.bounds.width/5*(CGFloat(2) + 0.5), y: UIScreen.main.bounds.height - rootTab!.tabBar.frame.size.height + 25)
                    
                case .placesFavorites:
                    if let vc = currentActiveController as? PeoplePlacesViewController {
                        if let favButton = (vc.koloda.viewForCard(at: 0)?.customView as? KolodaCard)?.addToFavorites_button {
                            return favButton.superview!.convert(favButton.center, to: UIApplication.shared.keyWindow!)
                        }
                    }
                    
                case .placesFilter:
                    if let vc = currentActiveController as? PeoplePlacesViewController {
                        return vc.filterButton.superview!.convert(vc.filterButton.center, to: UIApplication.shared.keyWindow!)
                    }
                    
                case .placesCheckin:
                    if let vc = currentActiveNavigation?.viewControllers.last as? PlaceProfileViewController {
                        return vc.checkin_button.superview!.convert(vc.checkin_button.center, to: UIApplication.shared.keyWindow!)
                    }
                    
                case .peopleInterests:
                    if let vc = currentActiveController as? PeoplePlacesViewController {
                        if let icon = (vc.koloda.viewForCard(at: 0)?.customView as? KolodaCard)?.interests_icon {
                            return icon.superview!.convert(icon.center, to: UIApplication.shared.keyWindow!)
                        }
                    }
                case .placesPermission:
                    if let vc = currentActiveController as? PeoplePlacesViewController {
                        _ = vc.service?.changeModeIfPossible(to: .nearby)
                        if let aroundButton = vc.around_mode_button {
                            var center = aroundButton.center
                            center.x -= 12
                            return aroundButton.superview!.convert(center, to: UIApplication.shared.keyWindow!)
                        }
                    }
                default:
                    return .zero
                }
                return .zero
            }
        }
        
        var radius: CGFloat? {
            get {
                switch self {
                case .peopleInterests: return 72
                case .placesCheckin: return 64
                case .placesPermission: return 64
                default: return nil
                }
            }
        }
    }
    
    var position: positions = .feed
    
    func makePositionChanges(newValue: positions, _ completion: @escaping ()->Void) {
        position = newValue
        
        if position == .completed {
            AppDelegate.configureNotifications()
            switch currentStage {
            case .news:
                UserDataManager.instance.isTutorialCompletedOnNews = true
            case .people:
                UserDataManager.instance.isTutorialCompletedOnPeople = true
            case .places:
                UserDataManager.instance.isTutorialCompletedOnPlaces = true
            }
            
            let parent = self.presentingViewController
            self.dismiss(animated: true, completion: {
                parent?.setNeedsStatusBarAppearanceUpdate()
                areNotificationsAllowed = true
                isHudAllowed = true
            })
            async(after: 50) {
                completion()
            }
        } else if position == .placesMain {
            rootTab?.selectedIndex = 1
            async(after: 50) {
                completion()
            }
        } else if position == .placesCheckin {
            if let stacksController = currentActiveController as? PeoplePlacesViewController {
               _ =  stacksController.service?.changeModeIfPossible(to: .nearby)
                async(after: 150) {
                    stacksController.kc_koloda(stacksController.koloda, didSelectItem: 0)
                    async(after: 350) {
                        completion()
                    }
                }
            }
        } else if position == .peopleMain {
            update(with: position.center, side: 0, animated: false)
            currentActiveNavigation?.popToRootViewController(animated: true)
            async(after: 450) {
                rootTab?.selectedIndex = 2
                async(after: 50) {
                    completion()
                }
            }
        } else {
            completion()
        }
    }
   
    
    func incrementPosition() {
        var nextPosition = positions(rawValue: position.rawValue + 1) ?? .completed
        
        
        if nextPosition == .placesPermission && CLLocationManager.authorizationStatus() != .notDetermined {
            nextPosition = .placesCheckin
        }
        
        if position == .placesCheckin && currentStage == .places {
            nextPosition = .completed
        } else if position == .peopleInterests && currentStage == .people {
            nextPosition = .completed
        } else if position == .feed && currentStage == .news {
            nextPosition = .completed
        }
  
        
        makePositionChanges(newValue: nextPosition) {
            self.update(with: self.position.center, side: self.position.radius ?? 50)
        }
    }

    class func make(with stage: stages = .news) -> TutorialViewController {
        let new = UIStoryboard(name: "Tutorial", bundle: nil).instantiateInitialViewController() as!  TutorialViewController
        new.currentStage = stage
        var position: positions!
        
        switch stage {
        case .news:
            position = .feed
        case .places:
            position = .placesMain
        case .people:
            position = .peopleMain
            
        }
        new.position = position
        new.update(with: position.center, animated: false)
        return new
    }
    
    func update(with center: CGPoint, side: CGFloat = 50, animated: Bool = true) {
        
        let path = CGMutablePath()
        path.addArc(center: center, radius: side/2, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.addRect(CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        
        let maskLayer = self.view.layer.mask as? CAShapeLayer ?? CAShapeLayer()
        
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        
        let anim = CABasicAnimation(keyPath: "path")
        
        anim.fromValue = maskLayer.path
        
        anim.toValue = path
        
        anim.duration = animated ? 0.5 : 0
        
        anim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        maskLayer.add(anim, forKey: nil)
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        maskLayer.path = path
        CATransaction.commit()
        
        
        if self.view.layer.mask != maskLayer {
            self.view.layer.mask = maskLayer
        }
        
    }
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}

extension TutorialViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .notDetermined {
            pendingActionBlock?()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(#function, error)
        pendingActionBlock?()

    }
}
