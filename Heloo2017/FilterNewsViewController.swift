//
//  FilterNewsViewController.swift
//  Heloo2017
//
//  Created by Константин on 25.06.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class FilterNewsViewController: UIViewController {
    
    @IBOutlet weak var subscribersSwitch: UISwitch!
    @IBOutlet weak var doneButton: ShadowedButton!
    
    var onSelected: ((Bool)->Void)? = nil
    private var only_subscriptions: Bool = false
    
    func set(only_subscriptions _only: Bool) {
        only_subscriptions = _only
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribersSwitch?.setOn(only_subscriptions, animated: false)
    }

    @IBAction func selectTapped(_ sender: UIButton) {
        onSelected?(only_subscriptions)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func subscribersTapped(_ sender: UISwitch) {
        only_subscriptions = sender.isOn
    }

}
