//
//  SNavigationController.swift
//  Heloo2017
//
//  Created by Константин on 19.08.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class SNavigationController: UINavigationController {


    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        var newControllersSet = self.viewControllers

        
        if let profileController = viewController as? MyProfileViewController {
            while let sameUserControllerIndex = newControllersSet.index(where: {($0 as? MyProfileViewController)?.user.uuid == profileController.user.uuid}) {
                newControllersSet.remove(at: sameUserControllerIndex)
            }
        } else if let chatController = viewController as? ChatViewController {
            while let sameUserControllerIndex = newControllersSet.index(where: {($0 as? ChatViewController)?.chat.uuid == chatController.chat.uuid}) {
                newControllersSet.remove(at: sameUserControllerIndex)
            }
        }
        
        if self.viewControllers != newControllersSet {
            newControllersSet.append(viewController)
            self.setViewControllers(newControllersSet, animated: true)
        } else {
            super.pushViewController(viewController, animated: animated);
        }
        
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            let result = self.presentedViewController?.preferredStatusBarStyle ?? self.topViewController?.presentedViewController?.preferredStatusBarStyle ?? self.topViewController?.preferredStatusBarStyle ?? .default
            return result
        }
    }

}
