//
//  SelectCountryViewController.swift
//  FashionFan
//
//  Created by Константин Черкашин on 19.04.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import UIKit

class SelectCountryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
  

    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchingArray: [(title: String, tag: String)] = []
    var isCoutry: Bool = true
    var countryID: Int!
    var delegate: SelectCoutry?
    var array: [(title: String, tag: String)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableItems.tag = 731367
        tableItems.delegate = self
        tableItems.dataSource = self
        searchBar.delegate = self
        searchingArray = array
        self.tableItems.reloadData()
        self.helpView.isHidden = true
        let footer = UIView()
        footer.backgroundColor = .black
        self.tableItems.tableFooterView = footer
        let blurEffect: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView: UIVisualEffectView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.view.addSubview(blurView)
        self.view.sendSubview(toBack: blurView)
        let textField = searchBar.value(forKey: "_searchField") as! UITextField
        textField.textColor = UIColor.white

    }
    
    @IBOutlet weak var helpView: UIView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchingArray = array
        self.tableItems.reloadData()
        self.helpView.isHidden = true


    }
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
//    override func preferredStatusBarStyle() -> UIStatusBarStyle {
//       return .LightContent
//    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchingArray = array
        self.tableItems.reloadData()
        self.view.endEditing(true)
    }
    var searchTxt = ""
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchTxt = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if searchTxt == "" || searchTxt == " " {
          searchingArray = array
        } else {
        searchingArray = []
        for item in array {
            if item.title.contains(searchTxt) {
                searchingArray.append(item)
            }
        }
            self.tableItems.reloadData()

        }
    }
    
  
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        if !isCoutry {
//        Server.makeRequest.getCityList(countryID: countryID!, query: searchTxt, completionHadnler: { (error, cities) in
//            if error == nil {
//                self.searchingArray = cities
//                self.tableItems.reloadData()
//            }
//        })
//        }
        self.view.endEditing(true)
        self.tableItems.reloadData()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            self.helpView.isHidden = !(searchingArray.count == 0)
        
        return searchingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableItems.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = searchingArray[indexPath.row].title
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name: "AvenirNext-Regular", size: 17)
        cell.backgroundView = UIView()
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate!.itemIsSelected(with: searchingArray[indexPath.row].tag, title: searchingArray[indexPath.row].title, isCountry: self.isCoutry)
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var tableItems: UITableView!
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
protocol SelectCoutry {
    func itemIsSelected(with tag: String, title: String, isCountry: Bool)
}
