//
//  ResizableButton.swift
//  Heloo2017
//
//  Created by Константин on 03.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


class ResizableButton: UIButton {
    
    var originalCenter: CGPoint! = nil
    let animationStep: Double = 0.15
    
    override var isHighlighted: Bool {
        didSet {
//
//            if originalCenter == nil {
//                originalCenter = self.center
//            }
            
            
            if state == .highlighted {
//                originalCenter = self.center
                decreaseAnimated()
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(animationStep*1000)), execute: {
                    self.increaseAnimated()
                })
            }
        }
    }
    
    
    @objc func decreaseAnimated() {
        UIView.animate(withDuration: animationStep) {
            self.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
//            self.center = self.originalCenter
        }
    }
    
    @objc func increaseAnimated() {
        UIView.animate(withDuration: animationStep*2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
            self.transform = .identity
//            self.center = self.originalCenter
        }, completion: { _ in
        })
    }
    

    override var cornerRadius: CGFloat {
        didSet {
            if self.layer.cornerRadius != 0, let img = self.backgroundImage(for: .normal) {
                self.setBackgroundImage(img, for: .normal)
            }
        }
    }
    
    override func setBackgroundImage(_ image: UIImage?, for state: UIControlState) {
        
        guard self.layer.cornerRadius != 0 else {
            super.setBackgroundImage(image, for: state)
            return
        }
        
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 1.0)
        
        UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).addClip()
        
        image?.draw(in: self.bounds)
        
        let clippedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        super.setBackgroundImage(clippedImage, for: state)

    }
        
}

