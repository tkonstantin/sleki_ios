//
//  Chat.swift
//  Heloo2017
//
//  Created by Константин on 29.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import CoreData

@objc(Chat)
class Chat: NSManagedObject {
    
    convenience init(_ insert: Bool) {
        self.init(entity: Chat.entitys, insertInto: insert ? CoreDataService.instance.privateContext : nil)
    }

    class var entitys: NSEntityDescription {
        var entity = NSEntityDescription()
        entity = NSEntityDescription.entity(forEntityName: "Chat", in: CoreDataService.instance.privateContext)!
        return entity
    }
    
    class func localChat(with user: User) -> Chat {
        let new = Chat(false)
        new.uuid = "localchat"
        new.users = [user]
        new.isLocal = true
        return new
    }
    
    convenience init?(with json: NSDictionary) {
        guard let someUUID = json["uuid"] as? String, (CoreDataManager.shared.chat(with: someUUID) == nil || CoreDataManager.shared.chat(with: someUUID)!.isDeleted) else {
            if let someUUID = json["uuid"] as? String, let chat = CoreDataManager.shared.chat(with: someUUID) {
                chat.update(with: json)
            }
            return nil
            
        }
        self.init(true)
        
        uuid = json["uuid"] as? String
        messages_count = json["messages_count"] as? Int ?? 0
        users = [User(with: json["member"] as? NSDictionary ?? [:])]
        
        if let last = Message.makeMessage(with: json["last_message"] as? NSDictionary ?? [:], in: self) {
            appendMessage(last)
        }
    }

    
    func appendMessage(_ message: Message) {
        messages.insert(message)
        lastMessageDate = realyLatestMessage?.date.timeIntervalSince1970 ?? 0
    }
    
    func update(with json: NSDictionary) {
        messages_count = json["messages_count"] as? Int ?? 0
        users = [User(with: json["member"] as? NSDictionary ?? [:])]
        if let last = Message.makeMessage(with: json["last_message"] as? NSDictionary ?? [:], in: self,isValid: false) {
            appendMessage(last)
        }
    }
    
    @NSManaged var uuid: String!
    @NSManaged var users: [User]
    @NSManaged var title: String?
    @NSManaged var lastMessageDate: Double
    @NSManaged var isLocal: Bool
    @NSManaged var isHistoryLoaded: Bool
    @NSManaged var messages: Set<Message>
    @NSManaged var avatar: String?
    @NSManaged var messages_count_raw: Int64
    @NSManaged var pendingText: String?
    
    
    var messages_objects: [Message] {
        get {
            return Array(messages)
        }
    }
    
    var messages_count: Int {
        get {
            return Int(messages_count_raw % Int64(Int32.max))
        } set {
            messages_count_raw = Int64(messages_count_raw)
        }
    }
    
    var unreaded_count: Int {
        get {
            return self.messages.filter({!$0.isReaded && $0.isIncoming}).count
        }
    }
    
    var realyLatestMessage: Message? {
        get {
            let latest = messages_objects.sorted(by: {$0.date > $1.date}).first
            return latest
        }
    }
    
    
    var displayTitle: String {
        get {
            if title != nil {
                return title!
            } else if let user = users.first {
                return [user.firstName, user.lastName].filter({!$0.isEmpty}).joined(separator: " ")
            } else {
                return "?"
            }
        }
    }
    
    var displayAvatar: String? {
        get {
            if avatar != nil {
                return avatar
            } else if let user = users.first {
                return user.image_small
            } else {
                return nil
            }
        }
    }

}


extension Array where Element: Chat {
    func hasChanges(to: [Chat]) -> Bool {
        
        if to.contains(where: { chat in
            return !self.contains(where: {$0.uuid == chat.uuid})
        }) {
            return true
        }
        let currentChats = self.filter({ chat in
            return to.contains(where: {$0.uuid == chat.uuid})
        })
        
        guard currentChats.compactMap({$0.lastMessageDate}).sorted() == to.compactMap({$0.lastMessageDate}).sorted() else { return true }
        guard currentChats.compactMap({$0.messages_count_raw}).sorted() == to.compactMap({$0.messages_count_raw}).sorted() else { return true }
        guard currentChats.compactMap({$0.users.first?.uuid}).sorted() == to.compactMap({$0.users.first?.uuid}).sorted() else { return true }
        
        return false
    }
}
