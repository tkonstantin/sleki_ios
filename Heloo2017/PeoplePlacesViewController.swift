//
//  SearchViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import CoreLocation


class PeoplePlacesViewController: UIViewController {
    
    @IBOutlet weak var background_oval_hack: UIImageView!
    @IBOutlet weak var koloda: KCKoloda!
    @IBOutlet weak var mode_image_hack: UIImageView!
    @IBOutlet weak var around_mode_button: UIButton!
    @IBOutlet weak var aroundCounterView: UIView!
    @IBOutlet weak var aroundCounterLabel: UILabel!
    @IBOutlet weak var city_mode_button: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!

    
    var loaded = false
    var items: [Searchable] = []
    var type: PeoplePlacesService.types = .people
    var shouldOpenAround = false
    var lastCoordinates: [Date: CLLocationCoordinate2D] = [:]
    var service: PeoplePlacesService!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeForCounter()
        modalPresentationCapturesStatusBarAppearance = true
        self.set(title: type == .people ? "ЛЮДИ" : "МЕСТА")
        self.topLabel?.text = type == .people ? "Люди" : "Места"
        self.navigationController?.navigationBar.makeClear()
        self.navigationItem.title = nil
        
        koloda.delegate = self
        koloda.dataSource = self
  
        service = PeoplePlacesService(type: type, mode: shouldOpenAround ? .nearby : .normal, delegate: self)
        getGPS()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        for index in 0..<koloda.numberOfItems {
            guard let card = self.koloda.viewForCard(at: index + koloda.currentCardIndex)?.customView as? KolodaCard else { return }
            var searchable_item: Searchable!
            
            if self.items.count == 0 {
                searchable_item = Searchable(user: UserDataManager.instance.currentUser)
            } else if self.items.count > index + koloda.currentCardIndex {
                searchable_item = self.items[index + koloda.currentCardIndex]
            }
            configure(card: card, with: searchable_item)
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if type == .places {
            if !UserDataManager.instance.isTutorialCompletedOnPlaces {
                let tutorial = TutorialViewController.make(with: .places)
                async {
                    self.tabBarController?.present(tutorial, animated: true, completion: nil)
                }
            }
        } else {
            if !UserDataManager.instance.isTutorialCompletedOnPeople {
                let tutorial = TutorialViewController.make(with: .people)
                async {
                    self.tabBarController?.present(tutorial, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        dismissHUD()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presented = self.presentedViewController {
                return presented.preferredStatusBarStyle
            } else {
                return .default
            }
        }
    }
    
    
    var currentPlaceFilter: placeFilterObject? = nil {
        didSet {
            onPlaceFilterUpdated()
        }
    }
    
    
    var currentPersonFilter: personFilterObject? = nil {
        didSet {
            onPersonFilterUpdated()
        }
    }

    

    var actualCoordinates: CLLocationCoordinate2D? {
        get {
            if let lastDate = lastCoordinates.keys.sorted(by: {$0 > $1}).first {
                return lastCoordinates[lastDate]
            }
            return nil
        }
    }
    
    func getGPS() {
        BeaconManager.instance.startTrackingLocation { [weak self] (coordinates) in
            if let newCoordinates = coordinates {
                if let previousActual = self?.actualCoordinates, CLLocation(latitude: previousActual.latitude, longitude: previousActual.longitude).distance(from: CLLocation(latitude: newCoordinates.latitude, longitude: newCoordinates.longitude)) > 300 {
                    self?.lastCoordinates = [Date(): newCoordinates]
                    self?.service?.reportLocationChanged()
                } else {
                    self?.lastCoordinates = [Date(): newCoordinates]
                }
            }
        }

    }

    
    @IBAction func changeMode(_ sender: UIButton) {
        
        let newValue: PeoplePlacesService.modes = sender == around_mode_button ? .nearby : .normal
        
        if service.changeModeIfPossible(to: newValue) {
            self.filterButton?.isHidden = newValue == .nearby
            
            let activeColor = UIColor(hex: "3AABFF")
            let inactiveColor = UIColor(hex: "DADBE1")
            
            self.city_mode_button?.setTitleColor(newValue == .normal ? activeColor : inactiveColor, for: .normal)
            self.around_mode_button?.setTitleColor(newValue == .nearby ? activeColor : inactiveColor, for: .normal)
        }
    }
    
    
    func showComparison() {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            return
        }
        let compare = CompareInterestsViewController.createOne(with: items[koloda.currentCardIndex].user?.interests ?? [])
        compare.previousStatusBarStyle = UIApplication.shared.statusBarStyle
        (topController() ?? self).present(compare, animated: false, completion: nil)
        compare.user_icon?.image = UIImage(named: "blue_blur_rect")
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        for touch in touches {
            
            if koloda.frame.contains(touch.location(in: koloda.superview)) {
                
                if let currentCard = koloda.viewForCard(at: koloda.currentCardIndex)?.customView as? KolodaCard {
                    if let compare = currentCard.compareButton {
                        if compare.frame.contains(touch.location(in: self.koloda)) {
                            currentCard.showComparison(compare)
                        } else {
                            selectKoloda(at: koloda.currentCardIndex)
                        }
                    } else if let favorites = currentCard.addToFavorites_button {
                        if favorites.frame.contains(touch.location(in: self.koloda)) {
                            self.addToFavorites(favorites)
                        } else {
                            selectKoloda(at: koloda.currentCardIndex)
                        }
                    } else {
                        selectKoloda(at: koloda.currentCardIndex)
                    }
                }
            }
        }
    }
    
    func selectKoloda(at index: Int) {
        if type == .people {
            self.openPeopleProfile(at: index)
        } else {
            self.openPlaceProfile(at: index)
        }
    }
    
    func openPeopleProfile(at index: Int) {
        if items.count != 0 {
            let currentItem = items[index]
            let profile = MyProfileViewController.makeOne(with: currentItem.user!, mode: .other)
            self.navigationController?.pushViewController(profile, animated: true)
        }
        
        
    }
    
    func openPlaceProfile(at index: Int) {
        if items.count != 0 {
            let currentItem = items[index]
            if let place = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "PlaceProfileViewController") as? PlaceProfileViewController {
                place.place = currentItem.place
                self.navigationController?.pushViewController(place, animated: true)
            }
        }
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? PlaceFilter {
            dest.restore(with: currentPlaceFilter)
            dest.delegate = self
        } else if let dest = segue.destination as? PersonFilter {
            dest.restore(with: currentPersonFilter)
            dest.delegate = self
        }
    }
    
    
    
    @IBAction func addToFavorites(_ sender: OfferFavoriteButton) {
        
        guard let uuid = sender.uuid else {  return }
        
        if sender.isFavorite {
            Server.makeRequest.removePlaceFromFavorites(uuid: uuid) { [weak self] (success) in
                if success {
                    sender.isFavorite = false
                    self?.items.first(where: {$0.place?.uuid == uuid})?.place?.isFavorite = false
                }
            }
        } else {
            Server.makeRequest.addPlaceToFavorites(uuid: uuid) { [weak self] (success) in
                if success {
                    sender.isFavorite = true
                    self?.items.first(where: {$0.place?.uuid == uuid})?.place?.isFavorite = true
                }
            }
        }
    }
    
    
    @IBAction func showFilter(_ sender: UIButton) {
        self.performSegue(withIdentifier: type == .places ? "PlaceFilter" : "PersonFilter", sender: nil)
    }
    
    
}

extension PeoplePlacesViewController: PPServiceDelegate {
    
    func pp_onContentChanged(appending: Bool) {
        self.items = self.service.items
        self.loaded = true
        async { [weak self] in
            self?.koloda.reload(appending: appending)
            if self?.items.count != 0 {
                self?.setImage(to: self!.koloda.viewForCard(at: 0)?.customView as? KolodaCard, item: self!.items[0])
            }
        }
    }
    
    func pp_getPlaceFilter() -> placeFilterObject? {
        return self.currentPlaceFilter
    }
    
    func pp_getPersonFilter() -> personFilterObject? {
        return self.currentPersonFilter
    }
    
    func pp_getCurrentLocation() -> CLLocationCoordinate2D? {
        return self.actualCoordinates
    }
    
}



extension PeoplePlacesViewController: KCKolodaDelegate {
    
    func kc_koload(_ koloda: KCKoloda, didFinishScrolling forced: Bool) {
        print("did finish scrolling")
    }
    
    func kc_koloda(_ koloda: KCKoloda, didSelectItem at: Int) {
        selectKoloda(at: at)
    }
    
    func kc_koloda(_ koloda: KCKoloda, didShowItem at: Int) {
        
        if let card = koloda.viewForCard(at: at) {
            if let kolodaCard = card.customView as? KolodaCard {
                kolodaCard.animateRings()
                if items.count > at {
                    setImage(to: kolodaCard, item: self.items[at])
                }
            }
        }
        
        if let card = koloda.viewForCard(at: at + 1) {
            if let kolodaCard = card.customView as? KolodaCard {
                if items.count > at + 1 {
                    setImage(to: kolodaCard, item: self.items[at + 1])
                }
            }
        }
        if at >= self.items.count - 3 {
            service.shouldLoadNextPage()
        }
    }
}


extension PeoplePlacesViewController: KCKolodaDataSource {
    
    func numberOfCards(_ koloda: KCKoloda) -> Int {
        if items.count == 0 && loaded {
            return 1
        }
        return items.count
    }
    
    
    func card(for item: Int, in koloda: KCKoloda) -> KCCardView {
        let customView: UIView = {
            if let card = UINib(nibName: type == .people ? "KolodaCard" : "KolodaPlaceCard", bundle: nil).instantiate(withOwner: self, options: nil).first as? KolodaCard {
                card.frame.size = CGSize(width: self.view.frame.size.width/2, height: 100)
                card.searchRef = self
                async {
                    var searchable_item: Searchable?
                    if self.items.count == 0 {
                        searchable_item = Searchable(user: UserDataManager.instance.currentUser)
                    } else if self.items.count > item {
                        searchable_item = self.items[item]
                    }
                    if searchable_item != nil {
                        self.configure(card: card, with: searchable_item!)
                    }
                }
                return card
            } else {
                return UIView()
            }
        }()
        
        return KCCardView(customView: customView, target_size: koloda.bounds.size)
    }
    
    
    func configure(card: KolodaCard, with item: Searchable) {
        if items.count == 0 {
            
            if !Server.isNetworkAvailable {
                card.no_network_view?.isHidden = false
                card.no_results_view?.isHidden = true
            } else {
                card.no_network_view?.isHidden = true
                card.no_results_view?.isHidden = false
            }
        } else {
            card.no_network_view?.isHidden = true
            card.no_results_view?.isHidden = true
            
            if type == .places {
                card.uuid = item.place?.uuid ?? ""
                if let place = item.place {
                    card.isFavorite = Server.makeRequest.isLocallyFavorite(place: place)
                }
                card.addToFavorites_button?.uuid = item.place?.uuid ?? ""
                card.addToFavorites_button?.isFavorite = card.isFavorite
                card.addToFavorites_button?.addTarget(self, action: #selector(self.addToFavorites(_:)), for: .touchUpInside)
                
                card.name_label?.text = item.place?.name
                (card.name_label as? UsernameLabel)?.user = nil
                
                if service.mode == .normal {
                    card.place_label?.text = item.place?.types.filter({$0.parent == nil}).map({$0.title}).joined(separator: ", ")
                } else {
                    if let geo_distance = item.place?.geo_distance, geo_distance != -1 {
                        card.place_label?.text = geo_distance.distanceString
                    } else {
                        card.place_label?.text = "Ближе 25 метров"
                    }
                }
            } else {
                card.uuid = item.user?.uuid ?? ""
                card.interests = item.user?.interests ?? []
                
                
                let age = item.user?.age ?? 0
                let age_string = item.user?.age_string ?? ""
                card.name_label?.text = [item.user?.firstName, item.user?.age_string].compactMap({$0}).joined(separator: ", ")
                (card.name_label as? UsernameLabel)?.user = item.user
                card.ages_label?.text = "Возраст: " + (age == 0 ? "" : age_string)
                card.place_label?.text = [item.user?.city, item.user?.age_string].compactMap({$0}).filter({!$0.isEmpty}).joined(separator: ", ")
            }
        }
    }
}



extension PeoplePlacesViewController {
    
    fileprivate func setImage(to card: KolodaCard?, item: Searchable) {
        guard let kolodaCard = card else { return }
        if kolodaCard.user_icon.sd_imageURL() == nil {
            if item.place != nil {
                kolodaCard.placeholder_image.isHidden = false
                
                
                if let url = URL(string: item.place?.image_large ?? "") {
                    kolodaCard.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_gray")
                    kolodaCard.cardView.backgroundColor = UIColor.init(hex: "F1F1F1")
                    
                    kolodaCard.user_icon.sd_setImageAnimated(with: url, placeholderImage: nil, options: [], completed: { (img, _, _, _) in
                        kolodaCard.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_huge")
                        kolodaCard.cardView.backgroundColor = UIColor.init(hex: "43A3F1")
                        
                        kolodaCard.placeholder_image.isHidden = img != nil
                    })
                } else if let url = URL(string: item.place?.image_small ?? "") {
                    kolodaCard.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_gray")
                    kolodaCard.cardView.backgroundColor = UIColor.init(hex: "F1F1F1")
                    kolodaCard.user_icon.sd_setImageAnimated(with: url, placeholderImage: nil, options: [], completed: { (img, _, _, _) in
                        kolodaCard.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_huge")
                        kolodaCard.cardView.backgroundColor = UIColor.init(hex: "43A3F1")
                        
                        kolodaCard.placeholder_image.isHidden = img != nil
                    })
                }
            } else {
                kolodaCard.placeholder_image.image = item.user?.gender == .male ? #imageLiteral(resourceName: "male_placeholder_huge") : #imageLiteral(resourceName: "female_placeholder_huge")
                kolodaCard.placeholder_image.isHidden = false
                
                if let url = URL(string: item.user?.image ?? "") {
                    kolodaCard.placeholder_image.image = item.user?.gender == .male ? #imageLiteral(resourceName: "user_male_placeholder_gray") : #imageLiteral(resourceName: "user_female_placeholder_gray")
                    kolodaCard.cardView.backgroundColor = UIColor.init(hex: "F1F1F1")
                    kolodaCard.user_icon.sd_setImageAnimated(with: url, placeholderImage: nil, options: [], completed: { (img, _, _, _) in
                        kolodaCard.placeholder_image.image = item.user?.gender == .male ? #imageLiteral(resourceName: "male_placeholder_huge") : #imageLiteral(resourceName: "female_placeholder_huge")
                        kolodaCard.cardView.backgroundColor = UIColor.init(hex: "43A3F1")
                        
                        kolodaCard.placeholder_image.isHidden = img != nil
                    })
                } else if let url = URL(string: item.user?.image_small ?? "") {
                    kolodaCard.placeholder_image.image = item.user?.gender == .male ? #imageLiteral(resourceName: "user_male_placeholder_gray") : #imageLiteral(resourceName: "user_female_placeholder_gray")
                    kolodaCard.cardView.backgroundColor = UIColor.init(hex: "F1F1F1")
                    kolodaCard.user_icon.sd_setImageAnimated(with: url, placeholderImage: nil, options: [], completed: { (img, _, _, _) in
                        kolodaCard.placeholder_image.image = item.user?.gender == .male ? #imageLiteral(resourceName: "male_placeholder_huge") : #imageLiteral(resourceName: "female_placeholder_huge")
                        kolodaCard.cardView.backgroundColor = UIColor.init(hex: "43A3F1")
                        kolodaCard.placeholder_image.isHidden = img != nil
                    })
                }
            }
        }
    }
}


extension PeoplePlacesViewController: PlaceFilterDelegate, PersonFilterDelegate  { //Filters
    
    var isPlaceFilterActive: Bool {
        get {
            if currentPlaceFilter == nil {
                return false
            }
            return !((self.currentPlaceFilter!.city == nil || self.currentPlaceFilter!.city!.tag == UserDataManager.instance.currentUser.city_tag) && self.currentPlaceFilter!.categories.count == 0 && self.currentPlaceFilter!.fromPrice == nil && self.currentPlaceFilter!.toPrice == nil && self.currentPlaceFilter!.subways.count == 0 && (self.currentPlaceFilter!.text == nil || self.currentPlaceFilter!.text!.isEmpty))
        }
    }
    
    var isPersonFilterActive: Bool {
        get {
            if currentPersonFilter == nil {
                return false
            }
            return !((self.currentPersonFilter?.selectedCity?.tag == UserDataManager.instance.currentUser?.city_tag  || self.currentPersonFilter?.selectedCity?.tag == nil) && self.currentPersonFilter!.selectedInterests.count == 0 && self.currentPersonFilter!.fromAge == nil && self.currentPersonFilter!.toAge == nil && (self.currentPersonFilter!.text == nil || self.currentPersonFilter!.text!.isEmpty) && (self.currentPersonFilter!.gender == .undefined || self.currentPersonFilter!.gender == nil))
        }
    }
    

    
    fileprivate func onPlaceFilterUpdated() {
        async {
            let isCityTheSame = self.currentPlaceFilter?.city?.tag == UserDataManager.instance.currentUser?.city_tag || self.currentPlaceFilter?.city?.tag == nil
            self.cityLabel.text = isCityTheSame ? "" : self.currentPlaceFilter?.city?.title ?? ""
            self.filterButton.setImage(self.isPlaceFilterActive ? #imageLiteral(resourceName: "filter_button_active") : #imageLiteral(resourceName: "filter_button"), for: .normal)
            
            self.service.reportFilterChanged()
        }
    }
    
    
    fileprivate func onPersonFilterUpdated() {
        async {
            let isCityTheSame = self.currentPersonFilter?.selectedCity?.tag == UserDataManager.instance.currentUser?.city_tag  || self.currentPersonFilter?.selectedCity?.tag == nil
            self.cityLabel.text = isCityTheSame ? "" : self.currentPersonFilter?.selectedCity?.title ?? ""
            self.filterButton.setImage(self.isPersonFilterActive ? #imageLiteral(resourceName: "filter_button_active") : #imageLiteral(resourceName: "filter_button"), for: .normal)
            
            self.service.reportFilterChanged()
        }
    }
    
    func filtersChanged(filter: placeFilterObject) {
        if currentPlaceFilter != filter {
            currentPlaceFilter = filter
        }
    }
    func filtersChanged(filter: personFilterObject) {
        if currentPersonFilter != filter {
            currentPersonFilter = filter
        }
    }
    
    
}




extension PeoplePlacesViewController { //KVOs

    
    fileprivate func observeForCounter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCounter), name: .places_update_notification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCounter), name: .viewed_places_update_notification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCounter), name: .people_update_notification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCounter), name: .viewed_people_update_notification, object: nil)
        
        
        reloadCounter()
    }
    
    @objc private func reloadCounter() {
        async {
            let count = self.type == .places ? Server.makeRequest.freshPlaces.count : MPCManager.shared.freshUsers.count
            self.aroundCounterView.isHidden = count == 0
            self.aroundCounterLabel.text = String(count)
            
            //todo reload
        }
    }

    
}
