//
//  SoundsAndTaptics.swift
//  Cguide
//
//  Created by Константин on 12.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import AudioToolbox

func vibrateFeedback() {
    guard #available(iOS 10.0, *) else {  return }
        let generator = UIImpactFeedbackGenerator.init(style: .medium)
        generator.prepare()
        generator.impactOccurred()
}

func vibrateSuccess() {
    guard #available(iOS 10.0, *) else {  return }
    UINotificationFeedbackGenerator.init().notificationOccurred(.success)
}

func vibrateWarning() {
    guard #available(iOS 10.0, *) else {  return }
    UINotificationFeedbackGenerator.init().notificationOccurred(.warning)
}

func vibrateError() {
    guard #available(iOS 10.0, *) else {  return }
    UINotificationFeedbackGenerator().notificationOccurred(.error)
}

class FeebackService {
    
    static let designate = FeebackService()
    
    func message_sent() {
        guard UIApplication.shared.applicationState == .active else { return }
        
        AudioServicesPlaySystemSound(messageSentSound);
        vibrateFeedback()
    }
    
    func message_received() {
        guard UIApplication.shared.applicationState == .active else { return }

        AudioServicesPlaySystemSound(messageReceivedSound);
        vibrateSuccess()
    }
    
    func place_located() {
        guard UIApplication.shared.applicationState == .active else { return }

        AudioServicesPlaySystemSound(placeLocatedSound);
        vibrateSuccess()
    }
    
    
    private let messageSentSound: SystemSoundID = FeebackService.makeSound(filename: "Tab 1", type:"m4a");
    private let messageReceivedSound: SystemSoundID = FeebackService.makeSound(filename: "Success 1", type:"m4a");
    private let placeLocatedSound: SystemSoundID = FeebackService.makeSound(filename: "Success 2", type:"m4a");

    
    private static func makeSound(filename:String!, type:String!) -> SystemSoundID
    {
        var soundID: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: filename, withExtension: type) {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &soundID)
        }
        
        return soundID
    }
    
}

