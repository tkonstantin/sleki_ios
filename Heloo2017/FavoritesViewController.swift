//
//  FavoritesViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Canvas

class FavoritesViewController: TableViewContainingViewController {
    
    @IBOutlet weak var places_button: UIButton!
    @IBOutlet weak var actions_button: UIButton!
    @IBOutlet weak var tableItems: RefreshableTable! {
        didSet {
            self.table = tableItems
        }
    }
    
    enum modes {
        case actions
        case places
    }
    
    
    var posts: [Post] = []
    
    let darkColor = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha: 1.0)
    let lightColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0)
    
    var _mode: modes = .actions
    
    var mode: modes {
        get {
            return _mode
        } set {
            var shouldResetPaging = false
            if _mode != newValue {
                shouldResetPaging = true
            }
            _mode = newValue
            self.actions_button.setTitleColor(newValue == .actions ? darkColor : grayColor, for: .normal)
            self.actions_button.tintColor = newValue == .actions ? darkColor : grayColor
            
            self.places_button.setTitleColor(newValue == .places ? darkColor : grayColor, for: .normal)
            self.places_button.tintColor = newValue == .places ? darkColor : grayColor
            
            if shouldResetPaging {
                self.resetPaging()
            }
        }
    }
    
    
    private func resetPaging() {
        self.computedCellHeights = [:]
        self.loaded = false
        self.hasNext = true
        self.currentPage = -1
        self.load(animated: true)
    }
    
    
    var expandedIndexPaths: [IndexPath] = []
    
    var computedCellHeights: [Int: CGFloat] = [:]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = .black
        UIApplication.shared.statusBarStyle = .default
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.set(title: "ИЗБРАННОЕ")
        self.configureTable()
        
        tableItems.addPullToRefresh(color: blueColor) { [weak self] (refreshControl) in
            self?.computedCellHeights = [:]
            self?.loaded = false
            self?.hasNext = true
            self?.currentPage = -1
            self?.load(animated: false) {
                refreshControl?.endRefreshing()
            }
        }
        self.load(animated: false)
    }
    var loaded = false
    
    func configureTable() {
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        tableItems.registerAllCells()
        
    }
    
    var places: [Place?] = []
    let fields: [String] = []//["uuid","name","smallImageUrl"]
    
    var hasNext = true
    var currentPage: Int = -1
    var isLoadingNextPage = false
    
    func load(animated: Bool, _ completion: (()->Void)?=nil) {
        guard hasNext, !isLoadingNextPage else { return }
        currentPage += 1
        isLoadingNextPage = true
        Server.makeRequest.getFavorites(page: currentPage, isOffers: self.mode == .actions, fields: fields) { [weak self] (items, hasNextPages) in
            
            if self?.currentPage == 0 {
                self?.places = []
                self?.posts = []
            }
            
            if self?.mode == .places {
                self?.places += items.map({$0.place}).filter({$0 != nil})
            }
            items.forEach({
                if let post = Post(with: $0) {
                    self?.posts.append(post)
                }
            })
            self?.hasNext = hasNextPages
            self?.loaded = true
            async {
                if self?.tableItems.isRefreshing == true || !animated {
                    self?.tableItems.reloadData()
                } else {
                    self?.tableItems.beginUpdates()
                    self?.tableItems.reloadSections([0], with: self?.mode == .places ? .left : .right)
                    self?.tableItems.endUpdates()
                }
                
                self?.isLoadingNextPage = false
                completion?()

            }
        }
    }
    
    
    @IBAction func change_mode(_ sender: UIButton) {
        self.mode = sender.tag == 1 ? .actions : .places
    }
    
}


extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count == 0 && self.loaded ? 1 : posts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if posts.count == 0 {
            let cell = tableItems.dequeueReusableCell(withIdentifier: post_types.placeholder.stringValue)!
            (cell.viewWithTag(1) as? UILabel)?.text = self.mode == .actions ? "Вы еще не добавили ни одной акции в избранное" : "Вы еще не добавили ни одного места в избранное"
            return cell
        }
        let currentPost = posts[indexPath.row]
        let cell = tableItems.dequeueReusableCell(withIdentifier: currentPost.type.stringValue) as! PostCell
        configure(postCell: cell, with: currentPost, commentsCountChanged: { [weak self] (newCount) in
            self?.posts[indexPath.row].comments_count = newCount
        })
        cell.likes_button?.addTarget(self, action: #selector(self.animate_like(_:)), for: .touchDown)
        cell.likes_button?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        cell.likes_button?.tag = indexPath.row
        cell.favorites_button?.tag = indexPath.row
        cell.favorites_button?.addTarget(self, action: #selector(self.favoriteTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    func  favoriteTapped(_ sender: UIButton) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if mode == .places && places.count > indexPath.row {
            let currentItem = places[indexPath.row]
            if let place = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "PlaceProfileViewController") as? PlaceProfileViewController {
                UIApplication.shared.statusBarStyle = .lightContent
                place.place = currentItem
                self.navigationController?.pushViewController(place, animated: true)
            }
        } else {
            guard self.posts.count > indexPath.row else { return }
            self.posts[indexPath.row].didSelect(from: self)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        computedCellHeights[indexPath.row] = cell.frame.size.height
        if indexPath.row >= self.posts.count - 2 && hasNext {
            self.load(animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = computedCellHeights[indexPath.row] {
            return height
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    
    
    func animate_like(_ sender: UIButton) {
        let anim = (sender.superview as? CSAnimationView)
        anim?.duration = 0.3
        anim?.delay = 0
        anim?.type = CSAnimationTypePop
        anim?.startCanvasAnimation()
    }
    
    func like(_ sender: UIButton) {
        Server.makeRequest.like(post: posts[sender.tag]) { [weak self] (success, liked, count) in
            self?.posts[sender.tag].isLiked = liked
            sender.setImage(liked ? #imageLiteral(resourceName: "liked_button") : #imageLiteral(resourceName: "myprofile_likes"), for: .normal)
            guard var likesCount = Int(sender.title(for: .normal) ?? "") else { return }
            likesCount = count
            self?.posts[sender.tag].likes_count = count
            sender.setTitle(String(likesCount), for: .normal)
        }
    }
    
    
}
