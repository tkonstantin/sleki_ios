//
//  ShadowedView.swift
//  Heloo2017
//
//  Created by Константин on 24.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ShadowedView: UIView {
    
    @IBInspectable var shadowColor: String = "36393C" {
        didSet {
            self._update()
        }
    }
    
    @IBInspectable var shadowAlpha: CGFloat = 0.13 {
        didSet {
            self._update()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 6 {
        didSet {
            self._update()
        }
    }
    
    @IBInspectable var shadowOffset:CGSize =  CGSize(width: 1, height: 1)  {
        didSet {
            self._update()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self._update()
    }
    
    private func _update() {
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor(hex: shadowColor).withAlphaComponent(shadowAlpha).cgColor
        self.layer.masksToBounds = false
    }
    
}
