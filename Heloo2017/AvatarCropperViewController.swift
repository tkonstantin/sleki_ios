//
//  AvatarCropperViewController.swift
//  Heloo2017
//
//  Created by Константин on 17.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


class AvatarCropperViewController: UIViewController {
    
    var image: UIImage!
    
    var state: states = .first
    
    weak var delegate: AvatarCropperDelegate?
    
    enum states {
        case first
        case second
    }
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var commentLabel: ShadowBlurLabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var profileInfoView: UIImageView!
    @IBOutlet weak var viewsBottomConstraint: NSLayoutConstraint!
    
    fileprivate var firstCroppedImage: UIImage!
    fileprivate var secondCroppedImage: UIImage!
    
    static func present(with image: UIImage?, from: AvatarCropperDelegate) {
        guard image != nil else { return }
        let new = UIStoryboard(name: "AvatarCropper", bundle: nil).instantiateInitialViewController() as! AvatarCropperViewController
        new.image = image!
        new.delegate = from
        new.modalTransitionStyle = .coverVertical
        (from as? UIViewController)?.present(new, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
        scrollView.delegate = self
        
        scrollView.minimumZoomScale = max(self.view.bounds.width/image.size.width, self.view.bounds.height/image.size.height)
        scrollView.zoomScale = scrollView.minimumZoomScale
        
        
        self.imageView.image = self.image.rotatedImageWithTransform(CGAffineTransform(rotationAngle: 0))
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
    @IBAction func next(_ sender: UIButton) {
        if state == .first {
            UIView.animate(withDuration: 0.1, animations: {
                sender.setImage(#imageLiteral(resourceName: "cropper_done_button"), for: .normal)
                sender.transform = sender.transform.scaledBy(x: 1.2, y: 1.2)
            }, completion: {_ in
                UIView.animate(withDuration: 0.1, animations: {
                    sender.transform = sender.transform.scaledBy(x: 0.83, y: 0.83)
                })
            })
            
            cropFirst()
            prepareForSecondState()
        } else {
            cropSecond()
            self.dismiss(animated: true)
        }
    }
    
    private func prepareForSecondState() {
        viewsBottomConstraint.constant = 16 - profileInfoView.bounds.height
        profileInfoView.alpha = 0
        commentLabel.text = "Двигайте и масштабируйте круг, чтобы настроить миниатюру"
        state = .second
        scrollView.isUserInteractionEnabled = false
        imageView.image = firstCroppedImage
        scrollView.setZoomScale(self.view.bounds.height/firstCroppedImage.size.height, animated: false)
        scrollView.setContentOffset(.zero, animated: false)
        addCroppingCircle()
    }
    
    
    var cropArea: CGRect {
        get {
            let cropAreaViewFrame: CGRect! = state == .first ? CGRect(origin: .zero, size: self.view.bounds.size) : self.circle_superview.currentCircleFrame
            
            let factor: CGFloat = 1//self.imageView.image!.size.width/view.frame.width
            let scale = 1/scrollView.zoomScale
            let imageFrame = imageView.imageFrame()
            let x = (scrollView.contentOffset.x + cropAreaViewFrame.origin.x - imageFrame.origin.x) * scale * factor
            let y = (scrollView.contentOffset.y + cropAreaViewFrame.origin.y - imageFrame.origin.y) * scale * factor
            let width = cropAreaViewFrame.size.width * scale * factor
            let height = cropAreaViewFrame.size.height * scale * factor
            return CGRect(x: x, y: y, width: width, height: height)
        }
    }
    
    private func cropFirst() {
        let roundedCropArea = CGRect(x: Int(cropArea.origin.x), y: Int(cropArea.origin.y), width: Int(cropArea.size.width), height: Int(cropArea.size.height))
        let img = image.crop(to: roundedCropArea)
        
        firstCroppedImage = img
    }
    
    private func cropSecond() {
        let roundedCropArea = CGRect(x: Int(cropArea.origin.x), y: Int(cropArea.origin.y), width: Int(cropArea.size.width), height: Int(cropArea.size.height))
        let img = firstCroppedImage.crop(to: roundedCropArea)
        secondCroppedImage = img
        
        delegate?.cropperDidPick(image: firstCroppedImage, smallImage: secondCroppedImage)
        
    }
    
    fileprivate var circle_superview: DraggableView!
    
    private func addCroppingCircle() {
        circle_superview = DraggableView(frame: self.view.bounds)
        circle_superview.backgroundColor = .clear
        circle_superview.backgroundColor =  UIColor.black.withAlphaComponent(0.5)
        
        let path = CGMutablePath()
        path.addArc(center: CGPoint(x: self.view.center.x, y: self.view.center.y/2), radius: 104, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.addRect(CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        
        let maskLayer = CAShapeLayer()
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.path = path;
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        circle_superview.layer.setAffineTransform(CGAffineTransform(scaleX: 5, y: 5))
        circle_superview.layer.mask = maskLayer
        circle_superview.clipsToBounds = true
        circle_superview.alpha = 1
        
        circle_superview.layer.setAffineTransform(CGAffineTransform(scaleX: 5, y: 5))
        
        
        self.view.insertSubview(circle_superview, at: 1)
        
        UIView.animate(withDuration: 0.5) {
            self.circle_superview.layer.setAffineTransform(.identity)
        }
        
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
        delegate?.cropperDidCancel()
    }
    
}

extension AvatarCropperViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    
}


extension UIImageView{
    func imageFrame()->CGRect{
        let imageViewSize = self.frame.size
        guard let imageSize = self.image?.size else{return CGRect.zero}
        let imageRatio = imageSize.width/imageSize.height
        let imageViewRatio = imageViewSize.width/imageViewSize.height
        
        if imageRatio < imageViewRatio {
            let scaleFactor = imageViewSize.height/imageSize.height
            let width = imageSize.width * scaleFactor
            let topLeftX = (imageViewSize.width - width)/2
            return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
        }else{
            let scalFactor = imageViewSize.width / imageSize.width
            let height = imageSize.height * scalFactor
            let topLeftY = (imageViewSize.height - height)/2
            return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
        }
    }
    
}


extension UIImage {
    func crop(to rect: CGRect) -> UIImage? {
        let img = self.rotatedImageWithTransform(CGAffineTransform(rotationAngle: 0))
        guard let cgImage = img.cgImage?.cropping(to: rect) else { return self }
        return UIImage(cgImage: cgImage)
    }
    
    fileprivate func rotatedImageWithTransform(_ transform: CGAffineTransform) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, true, scale)
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: size.width / 2.0, y: size.height / 2.0)
        context?.concatenate(transform)
        context?.translateBy(x: size.width / -2.0, y: size.height / -2.0)
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rotatedImage!
    }
    
}


class DraggableView: UIView {
    
    private var maskCircleCenter: CGPoint!
    private var lastMaskCircleCenter: CGPoint!
    private var lastZoom: CGFloat!
    
    private var currentTranslation: CGPoint!
    private var currentZoom: CGFloat = 1
    
    private  var actualAccuranceZoom: CGFloat {
        get {
            return min(max(currentZoom, 0.75), self.bounds.width/208.0)
        }
    }
    
    var currentCircleFrame: CGRect {
        get {
            let side = actualAccuranceZoom*104
            guard maskCircleCenter != nil else { return .zero }
            return CGRect(x: maskCircleCenter.x - side, y: maskCircleCenter.y - side, width: 2*side, height: 2*side)
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panned(_:)))
        self.addGestureRecognizer(panGesture)
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinched(_:)))
        self.addGestureRecognizer(pinchGesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc private func panned(_ sender: UIPanGestureRecognizer) {
        guard maskCircleCenter == nil || CGRect(x: maskCircleCenter.x - 104*actualAccuranceZoom, y: maskCircleCenter.y - 104*actualAccuranceZoom, width: 208*actualAccuranceZoom, height: 208*actualAccuranceZoom).contains(sender.location(in: self)) else { return }
        currentTranslation = sender.translation(in: self)
        self.update()
        
        switch sender.state {
        case .cancelled, .ended, .failed: lastMaskCircleCenter = maskCircleCenter
        default: break;
        }
        
    }
    
    @objc private func pinched(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
        case .cancelled, .ended, .failed: lastZoom = min(max(sender.scale, 0.75), self.bounds.width/208.0)
        default:
            if lastZoom != nil {
                sender.scale = lastZoom
                lastZoom = nil
            }
        }
        //        guard maskCircleCenter == nil || CGRect(x: maskCircleCenter.x - 104*actualAccuranceZoom, y: maskCircleCenter.y - 104*actualAccuranceZoom, width: 208*actualAccuranceZoom, height: 208*actualAccuranceZoom).contains(sender.location(in: self)) else { return }
        currentZoom = sender.scale
        self.update(zoomOnly: true)
        
    }
    
    var maxCenterPosition: CGPoint {
        get {
            return CGPoint(x: self.bounds.width - currentCircleFrame.width/2, y: self.bounds.height - currentCircleFrame.height/2)
        }
    }
    
    var minCenterPosition: CGPoint {
        get {
            return CGPoint(x: currentCircleFrame.width/2, y: currentCircleFrame.height/2)
        }
    }

    private func safeCircleCenter() {
            if maskCircleCenter == nil {
                maskCircleCenter = CGPoint(x: self.bounds.width/2, y: self.bounds.height/4)
            } else {
                let safeX = max(min(maskCircleCenter.x, maxCenterPosition.x), minCenterPosition.x)
                let safeY = max(min(maskCircleCenter.y, maxCenterPosition.y), minCenterPosition.y)
                maskCircleCenter = CGPoint(x: safeX, y: safeY)
            }
    }
    
    private func update(zoomOnly: Bool = false) {
        
        let translationX = (currentTranslation ?? .zero).x
        let translationY = (currentTranslation ?? .zero).y
        
        
        if maskCircleCenter == nil {
            maskCircleCenter = CGPoint(x: self.bounds.width/2, y: self.bounds.height/4)
        } else if !zoomOnly {
            let newCenterX = (lastMaskCircleCenter?.x ?? self.bounds.width/2) + translationX
            let newCenterY = (lastMaskCircleCenter?.y ?? self.bounds.height/4) + translationY
            maskCircleCenter = CGPoint(x: newCenterX, y: newCenterY)
        }
        
        safeCircleCenter()
        
        
        
        let path = CGMutablePath()
        path.addArc(center: maskCircleCenter, radius: 104*actualAccuranceZoom, startAngle: 0.0, endAngle: 2 * 3.14, clockwise: false)
        path.addRect(CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        
        let maskLayer = CAShapeLayer()
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.path = path;
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        self.layer.mask = maskLayer
    }
    
}



