//
//  ChattingService.swift
//  Heloo2017
//
//  Created by Константин on 23.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import CoreData


class SimplePollingService {
    
    enum targets {
        case chatList
        case chatMessages
        
        var updateInterval: Double {
            get {
                switch self {
                case .chatList: return 60*2
                case .chatMessages:
                    return 60*1/2
                }
            }
        }
    }
    
    enum modes {
        case active
        case background
        case inactive
        
        var multiplier: Double {
            get {
                switch self {
                case .active: return 1
                case .background: return 3
                case .inactive: return 1000
                }
            }
        }
    }
    
    private var mode: modes = .active {
        didSet {
            currentUpdateInterval = target.updateInterval*mode.multiplier
            if mode == .inactive {
                self.stopTimer()
            } else {
                if mode == .active {
                    runImmediately = true
                }
                self.startTimer()
            }
        }
    }
    
    private var target: targets!
    private var currentUpdateInterval: Double = targets.chatList.updateInterval
    private var updateTimer: Timer!
    private var actionBlock: (()->Void)?=nil
    private var runImmediately = true
    
    init(_ _target: targets, actionBlock block: @escaping ()->Void) {
        actionBlock = block
        target = _target
        currentUpdateInterval = target.updateInterval*mode.multiplier
        self.startTimer()
    }
    
    func setMode(_ _mode: modes) {
        mode = _mode
    }
    
    private func stopTimer() {
        updateTimer?.invalidate()
        updateTimer = nil
    }
    
    private func startTimer() {
        stopTimer()
        updateTimer = Timer.scheduledTimer(timeInterval: currentUpdateInterval, target: self, selector: #selector(self.makeUpdate), userInfo: nil, repeats: true)
        RunLoop.main.add(updateTimer!, forMode: .commonModes)

        if runImmediately {
            updateTimer.fire()
            runImmediately = false
        }
    }
    
    @objc private func makeUpdate() {
        actionBlock?()
    }
}

class ChattingService {
    static let shared = ChattingService()
    
    private var currentChatsPage = 0
    
    private var currentLoadNewMessagesAfterMessage: Message?
    private var currentLoadOldMessagesBeforeMessage: Message?
    
    private static var chatsPolling: SimplePollingService!
    private static var chatMessagesPolling: SimplePollingService!
    
    
    func getChats(forced: Bool = false, completion: @escaping ()->Void) {

        guard ChattingService.chatsPolling == nil || forced else {
            ChattingService.chatsPolling?.setMode(.active)
            return }
        
        ChattingService.chatsPolling = SimplePollingService(.chatList, actionBlock: {
            
            Server.makeRequest.loadChats(page: self.currentChatsPage) { (chats, hasNext) in
                if hasNext {
                    self.currentChatsPage += 1
                    self.getChats(completion: completion)
                } else {
                    self.currentChatsPage = 0
                    completion()
                }
            }
        })
    }
    
    func onLeavedChats() {
        ChattingService.chatsPolling?.setMode(.background);
    }
    
    func getNewMessages(for chat: Chat, completion: @escaping ()->Void) {
        
        ChattingService.chatMessagesPolling?.setMode(.inactive)
        ChattingService.chatMessagesPolling = nil
        
        ChattingService.chatMessagesPolling = SimplePollingService(.chatMessages, actionBlock: {
            guard self.currentLoadNewMessagesAfterMessage == nil else { return }
            
            self.currentLoadNewMessagesAfterMessage = chat.messages_objects.sorted(by: {$0.date > $1.date}).filter({$0.valid}).first
            
            Server.makeRequest.getMessages(from: chat, after: self.currentLoadNewMessagesAfterMessage) { (messages, hasNext) in
                self.currentLoadNewMessagesAfterMessage = nil
                completion()
            }
        })

    }
    
    func onLeavedConcreteChat() {
        ChattingService.chatMessagesPolling?.setMode(.inactive)
    }
    
    func getHistoryMessages(for chat: Chat, completion: @escaping ()->Void) {
        
        guard currentLoadOldMessagesBeforeMessage == nil else {  completion(); return }
        guard !chat.isHistoryLoaded else { completion(); return }
        
        currentLoadOldMessagesBeforeMessage = chat.messages_objects.sorted(by: {$0.date < $1.date}).filter({$0.valid}).first
        
        Server.makeRequest.getMessages(from: chat, before: currentLoadOldMessagesBeforeMessage) { (messages, hasNext) in
            chat.isHistoryLoaded = !hasNext
            async {
                CoreDataService.instance.save()
            }
            self.currentLoadOldMessagesBeforeMessage = nil
            completion()
        }
    }
}
