//
//  PhotoCommentsViewController.swift
//  Heloo2017
//
//  Created by Константин on 02.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//


import UIKit

class PhotoCommentsViewController: CardViewController, UITextFieldDelegate {
    
    @IBOutlet weak var field_view_bottom: NSLayoutConstraint!
    @IBOutlet weak var action_label: UILabel!
    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var post_image: UIImageView!
    @IBOutlet weak var toogle_button: UIButton!
    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var field_view: UIView!
    @IBOutlet weak var background_card: UIView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var comments_label: UILabel!
    @IBOutlet weak var tableItemsTop: NSLayoutConstraint!
    @IBOutlet weak var place_label: UILabel!
    @IBOutlet weak var tableHeaderHeight: NSLayoutConstraint!
    
    var commentCompletionBlock: ((Int)->Void)?
    
    let normalTableItemsTop: CGFloat = 50
    
    var post: Post?
    var _comments: [Comment] = []
    var comments: [Comment] {
        get {
            return _comments
        } set {
            _comments = newValue.sorted(by: {$0.date > $1.date})
        }
    }
    
    @IBAction func photoTapped(_ sender: UIButton) {
        if let img = post?.post_image, URL(string: img.url) != nil {
            PhotoViewerViewController.present(with: [img.url], from: self)
        }
    }
    
    
    @IBOutlet weak var tableItems: RefreshableTable!
    
    @IBAction func toogle(_ sender: UIButton) {
        guard field.isFirstResponder else { field.becomeFirstResponder(); return }
        if !(self.field.text?.isEmpty ?? true) {
            self.leaveComment(text: self.field.text!)
        }
        self.field.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    var isAnimatingAppearance = true
    func animateAppearance() {
        dismissHUD()
        async {
            UIView.animate(withDuration: 0.3, delay: 0.3, options: [], animations: {
                self.background_card.alpha = 0.5
            }, completion: nil)
            
            UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
                self.tableItemsTop.constant = self.normalTableItemsTop
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.isAnimatingAppearance = false
            })
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.comments.count > post?.comments_count ?? 0 {
            self.commentCompletionBlock?(self.comments.count)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table = tableItems
        self.tableItems.backgroundView = UIView()
        self.tableItems.backgroundView?.backgroundColor = .white
        self.animatableViews = [tableItems.backgroundView!: 1.0, comments_label: 1.0, close_button: 1.0]
        self.tableItems.contentInset = UIEdgeInsetsMake(242, 0, 0, 0)
        
        
        tableItems.addPullToRefresh(color: blueColor) { [weak self] (refreshControl) in
            self?.currentPage = -1
            self?.hasNext = true
            self?.comments = []
            self?.load {
                refreshControl?.endRefreshing()
            }
        }
        
        //        self.configureTable(needFooter: false)
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        tableItems.estimatedRowHeight = 100
        tableItems.rowHeight = UITableViewAutomaticDimension
        
        async {
            self.tableItems.contentOffset.y = -242
        }
        
        if let url = URL(string: post?.post_image?.url ?? "") {
            post_image.sd_setImage(with: url)
        } else {
            post_image.image = nil
        }
        
        name_label.text = post?.big_top_label
        self.action_label.attributedText = post?.action_title
        place_label.text = post?.small_top_label
        field.delegate = self
        self.subscribeToKeyboard()
//        self.addTapOutsideGestureRecognizer()
        
        showHUD()
        
        self.load(from: nil)
        
    }
    
    func leaveComment(text: String) {
        Server.makeRequest.leaveComment(text: text, for: post) { (comment) in
            FeebackService.designate.message_sent()
            self.field.text = ""
            self.view.endEditing(true)
            if let comm = comment {
                self.comments.append(comm)
                guard self.comments.count > 1 else { self.tableItems.reloadData(); return }
                let newPath = IndexPath(row: 0, section: 0)
                self.tableItems.insertRows(at: [newPath], with: .automatic)
                self.tableItems.scrollToRow(at: newPath, at: .bottom, animated: true)
            } else {
                ErrorPopupManager.showError(type: .unknown, from: self, subtitle: "Лимит: не больше 1 комментария в минуту")
            }
        }
    }
    
    override func keyboardWillBeShown(_ notification: Notification) {
        let info = (notification as NSNotification).userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.3) {
                self.toogle_button.setImage(#imageLiteral(resourceName: "comments_send_button"), for: .normal)
                self.field_view_bottom.constant = keyboardSize.height
                self.tableHeaderHeight.constant = 72
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                let keyboardFrame = self.tableItems.convert(keyboardSize, from: nil)
                let intersect = keyboardFrame.intersection(self.tableItems.bounds)
                UIView.animate(withDuration: (info?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.0, animations: {
                    
                    self.tableItems.contentInset = UIEdgeInsetsMake(72, 0, intersect.size.height + 8, 0)
                    self.tableItems.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, intersect.size.height + 8, 0)
                    self.tableItems.scrollRectToVisible(CGRect(x: 0, y: self.tableItems.contentSize.height, width: self.view.bounds.width, height: keyboardSize.height), animated: true)
                })
            }
        }
    }
    
    override func keyboardWillBeHidden() {
        UIView.animate(withDuration: 0.3) {
            self.toogle_button.setImage(#imageLiteral(resourceName: "toogle_button"), for: .normal)
            self.field_view_bottom.constant = 0
            self.tableHeaderHeight.constant = 250
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            
            
            self.tableItems.contentInset = UIEdgeInsetsMake(242, 0, 0, 0)
            self.tableItems.scrollIndicatorInsets = .zero
            
        }
    }
    
    
    var currentPage = -1
    var hasNext = true
    
    func load(from uuid: String? = nil, _ completion: (()->Void)?=nil) {
        guard hasNext || uuid != nil else { return }
        if uuid == nil {
            currentPage += 1
        }
        
        Server.makeRequest.getComments(uuid: uuid, page: currentPage, for: post) { (result) in
            self.comments += result.0
            self.hasNext = result.1
            self.tableItems.reloadData()
            self.animateAppearance()
            completion?()
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count == 0 ? 1 : comments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if comments.count == 0 {
            tableView.separatorStyle = .none
            let cell = tableView.dequeueReusableCell(withIdentifier: "placeholder")!
            return cell
        } else {
            tableView.separatorStyle = .singleLine
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CommentTableViewCell
            cell.configure(with: comments[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.comments.count > indexPath.row else { return }
        if let user = self.comments[indexPath.row].user {
            
            let profile = MyProfileViewController.makeOne(with: user, mode: UserDataManager.instance.isMe(user) ? .my : .other)
            
            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.pushViewController(profile, animated: true)
            
        }
    }
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard self.comments.count > indexPath.row, UserDataManager.instance.isMe(comments[indexPath.row].user) || (post?.isMine ?? false) else { return UISwipeActionsConfiguration(actions: []); }
        
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (action, view, completionBlock) in
            let activity = UIActivityIndicatorView(frame: view.bounds)
            activity.backgroundColor = action.backgroundColor
            activity.color = .white
            view.addSubview(activity)
            activity.startAnimating()
            Server.makeRequest.deleteComment(self.comments[indexPath.row].uuid, for: self.post, completion: { (success) in
                async {
                    completionBlock(success)
                    if success {
                        tableView.beginUpdates()
                        self.comments.remove(at: indexPath.row)
                        tableView.deleteRows(at: [indexPath], with: .automatic)
                        if self.comments.count == 0 {
                            tableView.insertRows(at: [indexPath], with: .automatic)
                        }
                        tableView.endUpdates()
                    }
                }

            })
        }
        deleteAction.image = #imageLiteral(resourceName: "delete_cell_icon")
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        return config
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        guard textField.text != nil && !textField.text!.isEmpty else { return true }
        self.leaveComment(text: textField.text!)
        self.view.endEditing(true)
        return true
    }
    
    
    @IBAction func close(_ sender: UIButton) {
        self.makeCoverVerticalDismiss()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        let presenting = self.presentingViewController
        super.dismiss(animated: flag) {
            presenting?.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    
    //     override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        if isWatching {
    //            self.animateAll(alpha: 1.0 + scrollView.contentOffset.y/144, duration: 0.0)
    //
    //            if scrollView.contentOffset.y < -128 {
    //                scrollView.contentOffset.y = -128
    //            }
    //        }
    //    }
    
}
