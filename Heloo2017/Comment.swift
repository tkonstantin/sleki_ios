//
//  Comment.swift
//  Heloo2017
//
//  Created by Константин on 02.07.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

class Comment: NSObject {
    
    var text = ""
    var user: User?
    var date: Date = Date()
    var uuid: String = ""
    
    var date_string: String {
        get {
            return date.commentDate()
        }
    }
    
    init?(with _json: NSDictionary?) {
        guard let json = _json else { return nil }
        if let someValue = json["uuid"] as? String {
            self.uuid = someValue
        } else {
            return nil
        }
        if let someValue = json["text"] as? String {
            self.text = someValue
        }
        if let someValue = json["from"] as? NSDictionary {
            self.user = User(with: someValue)
        }
        
        if let someValue = json["created_time"] as? String {
            self.date = someValue.stringToDate()
        }
    }
}
