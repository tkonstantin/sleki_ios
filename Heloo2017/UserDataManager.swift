//
//  UserDataManager.swift
//  Heloo2017
//
//  Created by Константин on 08.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class UserDataManager {
    static let instance = UserDataManager()
    
    static var isAuthorized: Bool {
        get {
            return !instance.currentUser.uuid.isEmpty
        }
    }
    
    let kInterests: NSString = NSString(string: "kInterestsDict")
    var availableInterests: [TreeItem] = []
    var availablePlaceTypes = [TreeItem]()
    var availableSubways: [String: String] = [:]

    
    func isMe(_ user: User?) -> Bool {
        if user == nil || currentUser == nil { return false }
        return user?.uuid == currentUser?.uuid
    }
    
    
    let isRegInProgressKey = NSString(string: "registrationisinprogress")
    let isRegInProgressYes = NSString(string: "registrationisinprogressnow")
    let isRegInProgressNo =  NSString(string: "registrationisnotinprogressnow")
    var isRegInProgress: Bool {
        get {
            return KeychainService.loadString(for: isRegInProgressKey) == isRegInProgressYes
        } set {
            KeychainService.save(string: newValue ? isRegInProgressYes : isRegInProgressNo, for: isRegInProgressKey)

        }
    }
    
    let kCurrentUser: NSString = NSString(string: "kCurrentUser")
    var currentUser: User! {
        get {
            if let data =  KeychainService.loadData(for: kCurrentUser) as Data? {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            } else {
                return nil
            }
        } set {
            if newValue != nil {
                KeychainService.save(data: NSKeyedArchiver.archivedData(withRootObject: newValue!) as NSData, for: kCurrentUser)
            } else {
                KeychainService.save(data: NSData(), for: kCurrentUser)
            }
            NotificationCenter.default.post(name: .userInfoChanged, object: nil)
        }
    }
    
    var isOnboardingCompleted: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isOnboardingCompleted_0") as? Bool ?? false
        } set {
            UserDefaults.standard.set(newValue, forKey: "isOnboardingCompleted_0")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    var isTutorialCompletedOnNews: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isTutorialCompletedOnNews_0") as? Bool ?? false
        } set {
            UserDefaults.standard.set(newValue, forKey: "isTutorialCompletedOnNews_0")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    var isTutorialCompletedOnPlaces: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isTutorialCompletedOnPlaces_0") as? Bool ?? false
        } set {
            UserDefaults.standard.set(newValue, forKey: "isTutorialCompletedOnPlaces_0")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    var isTutorialCompletedOnPeople: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isTutorialCompletedOnPeople_0") as? Bool ?? false
        } set {
            UserDefaults.standard.set(newValue, forKey: "isTutorialCompletedOnPeople_0")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    
    var isVisibleOverMPC: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isVisibleOverMPC_0") as? Bool ?? true
        } set {
            UserDefaults.standard.set(newValue, forKey: "isVisibleOverMPC_0")
            _ = UserDefaults.standard.synchronize()
            if newValue {
                MPCManager.shared.startSearchingIfNeeded()
            } else {
                MPCManager.shared.stop()
            }
        }
    }
    
}
