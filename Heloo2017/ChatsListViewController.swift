//
//  ChatsListViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import CoreData

class ChatsListViewController: UIViewController {
    
    fileprivate lazy var fetchController: NSFetchedResultsController<Chat>! = {
        let fetch = CoreDataManager.shared.chatsListController()
        fetch.delegate = self
        try? fetch.performFetch()
        return fetch
    }()
    
    fileprivate var isUpdating = false

    @IBOutlet weak var placeholderView: UIView!
    
    @IBOutlet weak var tableView: RefreshableTable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.set(title: "СООБЩЕНИЯ")
        
       self.navigationController?.navigationBar.makeClear()
        
        self.tableView.addPullToRefresh(color: blueColor) { [weak self] (refreshControl) in
            self?.load()
            async {
                refreshControl?.endRefreshing()
            }
        }
    }
    
    deinit {
        ChattingService.shared.onLeavedChats()
    }
    
    @objc private func load() {
        ChattingService.shared.getChats { }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(hex: "454545")
        self.load()
        if let selected = self.tableView?.indexPathForSelectedRow {
            self.tableView?.deselectRow(at: selected, animated: true)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.load), name: .UIApplicationDidBecomeActive, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let selected = self.tableView?.indexPathForSelectedRow {
            self.tableView?.deselectRow(at: selected, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let cell = sender as? UITableViewCell else { return }
        if let chat = segue.destination as? ChatViewController, let selectedIndexPath = tableView.indexPath(for: cell) {
            chat.configure(with: fetchController.object(at: selectedIndexPath))
        }
    }
    
    @IBAction func subscriptionsTapped(_ sender: UIBarButtonItem) {
        let nav = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "NavSubscribersViewController") as! UINavigationController
        let subcs = nav.viewControllers.first as! SubscribersViewController
        nav.modalTransitionStyle = .crossDissolve
//        nav.modalPresentationStyle = .overCurrentContext
        subcs.selectionDelegate = self
        subcs.selectionDataSource = self
        subcs.bg_img = UIImage(named: "blue_blur_rect")
        subcs.isOtherProfile = true
        subcs.mode = .subscriptions
        subcs.uuid = nil
        subcs.previousStatusBarStyle = .default
        self.tabBarController?.present(nav, animated: false)
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presented = self.presentedViewController, !presented.isBeingDismissed {
                return presented.preferredStatusBarStyle
            } else {
                return .default
            }
        }
    }
    
}

extension ChatsListViewController: SubscribersSelectionDelegate, SubscribersSelectionDataSource {
    func didSelect(user: User) {
        if let found_chat = CoreDataManager.shared.currentChatsList().first(where: {$0.users.contains(where: {$0.uuid == user.uuid})}) {
            let chat = ChatViewController.makeOne(with: found_chat)
            self.navigationController?.pushViewController(chat, animated: true)
        } else {
            let chat = ChatViewController.makeOne(with: Chat.localChat(with: user))
            self.navigationController?.pushViewController(chat, animated: true)
        }
    }
    
    func filteredUsers(_ users: [User]) -> [User] {
        var usersWithChat: [User] = []
        for chat in self.fetchController.fetchedObjects ?? [] {
            usersWithChat += chat.users
        }
        return users.filter({ user in !usersWithChat.contains(where: {$0.uuid == user.uuid})})
    }
    
}

extension ChatsListViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard !isUpdating else { return }
        self.isUpdating = true
        self.tableView.beginUpdates()
        UIView.setAnimationsEnabled(false)

    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

        guard isUpdating else { return }
        self.isUpdating = false
        self.tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        switch type {
        case .delete:
            guard let path = indexPath else { return }
            self.tableView.deleteRows(at: [path], with: .automatic)
        case .insert:
            guard let path = newIndexPath else { return }

            self.tableView.insertRows(at: [path], with: .automatic)

        case .move:
            if indexPath != nil && newIndexPath != nil && indexPath != newIndexPath {
//                self.tableView.moveRow(at: indexPath!, to: newIndexPath!)
                self.tableView.deleteRows(at: [indexPath!], with: .none)
                self.tableView.insertRows(at: [newIndexPath!], with: .none)
            }
        case .update:
            guard let path = indexPath else { return }
            self.tableView.reloadRows(at: [path], with: .none)
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {

        switch type {
        case .delete:
            self.tableView.deleteSections([sectionIndex], with: .automatic)

        case .insert:
            self.tableView.insertSections([sectionIndex], with: .automatic)

        case .move:

            break;
        case .update:
            self.tableView.reloadSections([sectionIndex], with: .none)

        }
    }
}

extension ChatsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let shouldHideFooter = (fetchController.fetchedObjects ?? []).count != 0
        placeholderView.isHidden = shouldHideFooter
        placeholderView.frame.size.height = shouldHideFooter ? 0 : 240
        return fetchController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchController.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let currentChat = fetchController.object(at: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ChatTableViewCell
        cell.update(with: currentChat)
        cell.profileButton.chat = currentChat
        cell.profileButton.addTarget(self, action: #selector(self.openProfile(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (action, view, completionBlock) in
            let activity = UIActivityIndicatorView(frame: view.bounds)
            activity.backgroundColor = action.backgroundColor
            activity.color = .white
            view.addSubview(activity)
            activity.startAnimating()
            Server.makeRequest.clearChat(self.fetchController.object(at: indexPath), completion: { (success) in
                async {
                    completionBlock(success)
                }
            })
        }
        
        deleteAction.image = #imageLiteral(resourceName: "delete_cell_icon")
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        return config
    }
    
    @objc func openProfile(_ button: ProfileButton) {
        if let user = button.chat?.users.first {
            let profile = MyProfileViewController.makeOne(with: user, mode: .other)
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
}

class ProfileButton: UIButton {
    weak var chat: Chat?
}
