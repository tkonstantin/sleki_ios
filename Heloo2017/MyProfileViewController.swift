//
//  MyProfileViewController.swift
//  Heloo2017
//
//  Created by Константин on 18.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Canvas

extension String {
    func cropped(to symbolsCount: Int) -> String {
        guard self.count > symbolsCount else { return self }
        return self.substring(to: String.Index.init(symbolsCount)) + "..."
    }
}


class MyProfileViewController: TableViewContainingViewController {
    
    class func makeOne(with user: User, mode: profile_modes) -> MyProfileViewController {
        let new = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        new.user = user
        new.profile_mode = mode
        return new
    }
    
    @IBOutlet weak var posts_count_label: UILabel?
    @IBOutlet weak var subscribers_count: UILabel!
    @IBOutlet weak var subscriptions_count: UILabel!
    @IBOutlet weak var interests_count: UILabel!
    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var address_label: UILabel?
    @IBOutlet weak var placeholder_image: UIImageView!
    @IBOutlet weak var subscribe_button: UIButton!
    @IBOutlet weak var user_icon: UIImageView!
    @IBOutlet weak var tableItems: RefreshableTable! {
        didSet {
            self.table = tableItems
        }
    }
    @IBOutlet weak var blue_blur: UIImageView!
    @IBOutlet var action_labels: [UILabel]!
    @IBOutlet weak var chat_button: UIButton!
    @IBOutlet weak var actionButtonsView: UIView!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var profileBoard: ShadowedView!
    @IBOutlet weak var editingButton: UIButton!
    
    @IBAction func arrowTapped(_ sender: UIButton) {
        if sender.transform == .identity {
            self.tableItems.setContentOffset(CGPoint(x: 0, y: self.profileBoard.bounds.height), animated: true)
        } else {
            self.tableItems.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
    
 
    @IBAction func editTapped(_ sender: UIButton) {
        let settings = SettingsViewController.make()
        self.navigationController?.pushViewController(settings, animated: true)
    }
    
    
    enum profile_modes {
        case my
        case other
    }
    
    var posts: [Post] = []
    
    var expandedIndexPaths: [IndexPath] = []
    
    var profile_mode: profile_modes = .my
    
    var _user: User?
    var user: User {
        get {
            let someUser = self.profile_mode == .my ? UserDataManager.instance.currentUser : _user
            guard someUser != nil else { SettingsViewController.logout(); return User(with: [:]) }
            return someUser!
        } set {
            _user = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.makeClear()

        tableItems.addPullToRefresh { [weak self] (refreshControl) in
            self?.requestFullProfile(true)
            self?.currentPage = -1
            self?.hasNext = true
            self?.loadPosts {
                refreshControl?.endRefreshing()
            }
        }
        
        tableItems.delegate = self
        tableItems.dataSource = self
        
        tableItems.registerAllCells()
        fill()
        
        async {
            self.tableItems.tableHeaderView?.frame.size.height = self.tableItems.bounds.height + self.profileBoard.bounds.height - self.name_label.bounds.height - 10
            self.tableItems.reloadData()
        }
        
        self.actionButtonsView.isHidden = profile_mode == .my
        
        self.loadPosts()
        
    }
    

    
    
    
    
    
    var currentPage = -1
    var hasNext = true
    var isLoadingNextPage = false
    
    func loadPosts(_ completion: (()->Void)?=nil) {
        guard hasNext && !isLoadingNextPage else { return }
        self.isLoadingNextPage = true
        currentPage += 1
        Server.makeRequest.getWall(page: currentPage, uuid: user.uuid) { (result, hasNextItems, totalCount) in
            async {
                if self.currentPage == 0 {
                    self.posts = []
                }
                self.posts_count_label?.text = String(totalCount)
                self.hasNext = hasNextItems
                let newPosts = result.map({Post(with: $0)}).flatMap({$0}).filter({ item in
                    return !self.posts.contains(where: {$0.searchable_item?.uuid == item.searchable_item?.uuid})
                })
                self.posts += newPosts
                self.posts.sort(by: {$0.real_date > $1.real_date})
                self.insertCells(with: newPosts)
                self.isLoadingNextPage = false
                completion?()
            }
            
        }
    }
    
    var previousUpdateRowsCount = 0
    
    func insertCells(with requestResult: [Post]) {
        async {
            guard !self.tableItems.isDragging && !self.tableItems.isTracking && !self.tableItems.isDecelerating else { async(after: 300, {
                self.insertCells(with: requestResult); })
                return;
            }
            guard self.posts.count > self.previousUpdateRowsCount else { self.tableItems.reloadData(); return }
            guard self.posts.count == self.previousUpdateRowsCount + requestResult.count else { self.tableItems.reloadData(); return }
            
            self.previousUpdateRowsCount = self.posts.count
            let previousRowsCount = self.posts.count - requestResult.count
            if previousRowsCount == 0 {
                self.tableItems.reloadData()
                return;
            }
            self.tableItems.beginUpdates()
            let insertedRowsRange = previousRowsCount..<self.posts.count
            self.tableItems.insertRows(at: insertedRowsRange.map({IndexPath(row: $0, section: 0)}), with: .none)
            self.tableItems.endUpdates()
        }
        
    }
    
    
    
    func requestFullProfile(_ forced: Bool = false) {
        if self.profile_mode == .my {
            if forced {
                Server.makeRequest.getMyProfile {
                    async {
                        self.user = UserDataManager.instance.currentUser
                        self.fill()
                    }
                    
                }
            }
        } else {
            Server.makeRequest.getPerson(uuid: user.uuid) { (user_full) in
                async {
                    if user_full != nil {
                        self.user = user_full!
                        self.fill()
                    }
                }
                
            }
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.makeClear()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fill()
        
        self.requestFullProfile(true)
        self.currentPage = -1
        self.hasNext = true
        self.loadPosts {}
        
        
        for cell in self.tableItems.visibleCells {
            if let postCell = cell as? PostCell {
                postCell.hideMenu()
            }
        }
        async {
            self.tableItems.setContentOffset(CGPoint(x: 0, y: self.profileBoard.bounds.height), animated: false)
        }

    }
    
    
    func fill() {
        self.editingButton?.isHidden = profile_mode != .my
        self.subscribe_button.setTitle(user.is_subscribed ? "Подписаны" : "Подписаться", for: .normal)
        self.subscribe_button.backgroundColor = user.is_subscribed ? UIColor(hex: "B2B1C2") : UIColor(hex: "3AAAFE")
        (self.subscribe_button as? ShadowedButton)?.color = user.is_subscribed ? "B2B1C2" : "3AAAFE"


        self.interests_count.text = String(user.interests.count)
        self.subscribers_count.text = String(user.subscribers_count)
        self.subscriptions_count.text = String(user.subscriptions_count)
        self.fillActionLabels()
        
        
        self.placeholder_image.isHidden = false
        self.placeholder_image.image = self.user.gender == .male ? #imageLiteral(resourceName: "male_placeholder_huge") : #imageLiteral(resourceName: "female_placeholder_huge")
        self.chat_button.isHidden = profile_mode == .my
        
        
        if let url = URL(string: user.image) {
            self.placeholder_image.image = self.user.gender == .male ? #imageLiteral(resourceName: "user_male_placeholder_gray") : #imageLiteral(resourceName: "user_female_placeholder_gray")
            self.view.backgroundColor = UIColor.init(hex: "F1F1F1")
            self.user_icon.sd_setImage(with: url, placeholderImage: nil, options: [], completed: { img,_,_,_ in
                self.view.backgroundColor = UIColor.init(hex: "43A3F1")
                self.placeholder_image.image = self.user.gender == .male ? #imageLiteral(resourceName: "male_placeholder_huge") : #imageLiteral(resourceName: "female_placeholder_huge")
                self.placeholder_image.isHidden = img != nil
            })
        } else if let url = URL(string: user.image_small) {
            self.placeholder_image.image = self.user.gender == .male ? #imageLiteral(resourceName: "user_male_placeholder_gray") : #imageLiteral(resourceName: "user_female_placeholder_gray")
            self.view.backgroundColor = UIColor.init(hex: "F1F1F1")
            self.user_icon.sd_setImage(with: url, placeholderImage: nil, options: [], completed: { img,_,_,_ in
                self.view.backgroundColor = UIColor.init(hex: "43A3F1")
                self.placeholder_image.image = self.user.gender == .male ? #imageLiteral(resourceName: "male_placeholder_huge") : #imageLiteral(resourceName: "female_placeholder_huge")
                self.placeholder_image.isHidden = img != nil
            })
        } else {
            self.user_icon.image = nil
            self.placeholder_image.isHidden = false
        }
        
        self.address_label?.text = user.city
        if self.address_label?.text?.isEmpty ?? true {
            self.address_label?.text = "Город не указан"
        }
        var name = [user.firstName, user.lastName].flatMap({$0}).filter({!$0.isEmpty}).joined(separator: " ").replacingOccurrences(of: "\"", with: "'")
        if name.isEmpty {
            name = "Имя не указано"
        }
        
        self.name_label.text = [name, user.age == 0 ? "" : user.age_string].filter({!$0.isEmpty}).joined(separator: ", ")
        (self.name_label as? UsernameLabel)?.user = user

    }
    
    private func fillActionLabels() {
        for label in action_labels {
            switch label.tag {
            case 1:
                var defaultText = "Интерес"
                switch (user.interests.count % 100) {
                case 10...20: defaultText += "ов"
                default:
                    switch user.interests.count % 10 {
                    case 1: break;
                    case 2,3,4: defaultText += "а"
                    default: defaultText += "ов"
                    }
                }
                label.text = defaultText
                
            case 2:
                var defaultText = "Подпис"
                switch (user.subscriptions_count % 100) {
                case 10...20: defaultText += "ок"
                default:
                    switch user.subscriptions_count % 10 {
                    case 1: defaultText += "ка"
                    case 2,3,4: defaultText += "ки"
                    default: defaultText += "ок"
                    }
                }
                label.text = defaultText
                
            default:
                var defaultText = "Подписчик"
                switch (user.subscribers_count % 100) {
                case 10...20: defaultText += "ов"
                default:
                    switch user.subscribers_count % 10 {
                    case 1: break;
                    case 2,3,4: defaultText += "а"
                    default: defaultText += "ов"
                    }
                }
                label.text = defaultText
            }
        }
    }

    
    @IBAction func subscribe(_ sender: UIButton) {
        
        sender.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.15, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }, completion: { _ in
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                sender.transform = .identity
            }, completion: { _ in
                sender.isUserInteractionEnabled = true
            })
        })

        if user.is_subscribed {
            Server.makeRequest.unsibscribeFromPerson(uuid: user.uuid, completion: { (success) in
                if success {
                    self.apply_subscribe_changes()
                }
            })
        } else {
            Server.makeRequest.subscribeToPerson(uuid: user.uuid, completion: { (success) in
                if success {
                    self.apply_subscribe_changes()
                }
            })
        }
    }
    
    @IBAction func openChat(_ sender: UIButton) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            return
        }
        
        if let found_chat = CoreDataManager.shared.currentChatsList().first(where: {$0.users.contains(where: {$0.uuid == user.uuid})}) {
            let chat = ChatViewController.makeOne(with: found_chat)
            self.navigationController?.pushViewController(chat, animated: true)
        } else {
            let chat = ChatViewController.makeOne(with: Chat.localChat(with: user))
            self.navigationController?.pushViewController(chat, animated: true)
        }
        
    }
    
    private func apply_subscribe_changes() {
        self.requestFullProfile(true)
    }

    
    @IBAction func show_subscribers(_ sender: UIButton) {
        let nav = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "NavSubscribersViewController") as! UINavigationController
        let subcs = nav.viewControllers.first as! SubscribersViewController
        subcs.bg_img = self.user_icon.image ?? UIImage(named: "blue_blur_rect")
        nav.modalTransitionStyle = .crossDissolve
        nav.modalPresentationStyle = .overCurrentContext
        subcs.profileReloadingBlock = {
            self.requestFullProfile(true)
        }
        subcs.isOtherProfile = profile_mode == .other
        subcs.mode = sender.tag == 1 ? .subscriptions : .subscribers
        subcs.uuid = self.profile_mode == .my ? nil : user.uuid
        
        subcs.previousStatusBarStyle = UIApplication.shared.statusBarStyle
        self.present(nav, animated: true, completion: nil)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
            
        }
    }

    func back() {
        self.navigationController?.popToViewController(self.navigationController!.viewControllers.dropLast().last!, animated: true)
    }

    
    private var isShowingInterests = false
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableItems.visibleCells.compactMap({$0 as? PostCell}).forEach({$0.hideMenuWithScrollIntent()})
        
        if scrollView.contentOffset.y > self.profileBoard.bounds.height*4/5 {
            UIView.animate(withDuration: 0.2) {
                self.arrowButton.transform = CGAffineTransform(rotationAngle: -.pi)
            }
        } else if scrollView.contentOffset.y < self.profileBoard.bounds.height*1/5 {
            UIView.animate(withDuration: 0.2) {
                self.arrowButton.transform = .identity
            }
        }
        
        var currentOffset = scrollView.contentOffset.y
        if currentOffset >= 0 && currentOffset <= self.profileBoard.bounds.height {
            currentOffset = 0
        } else if currentOffset > self.profileBoard.bounds.height {
            currentOffset -= self.profileBoard.bounds.height
        } else {
            currentOffset *= 2
        }
        
        self.blue_blur.alpha = abs(currentOffset/(self.tableItems.bounds.height - self.profileBoard.bounds.height))
    }
   
    
   
    @IBAction func showInterests(_ sender: UIButton) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            return
        }
        
        if profile_mode == .other {
            let compare = CompareInterestsViewController.createOne(with: user.interests)
            compare.previousStatusBarStyle = UIApplication.shared.statusBarStyle
            
            (topController() ?? self).present(compare, animated: false, completion: nil)
            compare.user_icon?.image = self.user_icon.image ?? UIImage(named: "blue_blur_rect")
        } else {
            let mys = MyInterestsViewController.createOne()
            mys.previousStatusBarStyle = UIApplication.shared.statusBarStyle
            (topController() ?? self).present(mys, animated: false, completion: nil)
        }
    }
    
}

extension MyProfileViewController: UITableViewDelegate,  UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result = (posts.count == 0 && !self.hasNext) ? 1 : posts.count
        tableView.tableFooterView?.frame.size.height = self.hasNext ? 128 : 16
        tableView.tableFooterView?.alpha = self.hasNext ? 1 : 0
        return result
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if posts.count == 0 && !self.hasNext {
            let cell = tableItems.dequeueReusableCell(withIdentifier: "profile_placeholder")!
            (cell.viewWithTag(1) as? UILabel)?.text = self.profile_mode == .my ? "Пока что вы не оставили ни одной записи" : "Пока что пользователь не оставил ни одной записи"
            return cell
        }
        guard posts.count > indexPath.row else { return UITableViewCell(); }
        
        let currentPost = posts[indexPath.row]
        let cell = tableItems.dequeueReusableCell(withIdentifier: currentPost.type.stringValue) as! PostCell
        
        self.configure(postCell: cell, with: currentPost, commentsCountChanged: { [weak self] (newCount) in
            self?.posts[indexPath.row].comments_count = newCount
        })
        
        cell.setCanDelete(currentPost.canDelete && self.posts.count > indexPath.row) { [weak self] in
            Server.makeRequest.deletePost(currentPost) { success in
                async {
                    (self?.tableItems.cellForRow(at: indexPath) as? PostCell)?.hideMenu()
                    if success {
                        self?.tableItems?.beginUpdates()
                        self?.posts.remove(at: indexPath.row)
                        self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                        if self?.posts.count == 0 {
                            self?.tableItems?.insertRows(at: [indexPath], with: .automatic)
                        }
                        self?.tableItems?.endUpdates()
                        
                        async(after: 300, {
                            self?.tableItems?.reloadData()
                        })
                    }
                }
            }
        }
        
        cell.likes_button?.addTarget(self, action: #selector(self.animate_like(_:)), for: .touchDown)
        cell.likes_button?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        cell.likes_button?.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.posts.count > indexPath.row else { return }
        self.posts[indexPath.row].didSelect(from: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= self.posts.count - 1 && self.posts.count != 0 {
            self.loadPosts()
        }
    }
    
    
    func animate_like(_ sender: UIButton) {
        let anim = (sender.superview as? CSAnimationView)
        anim?.duration = 0.3
        anim?.delay = 0
        anim?.type = CSAnimationTypePop
        anim?.startCanvasAnimation()
    }
    
    func like(_ sender: UIButton) {
        Server.makeRequest.like(post: posts[sender.tag]) { (success, liked, count) in
            self.posts[sender.tag].isLiked = liked
            sender.setImage(liked ? #imageLiteral(resourceName: "liked_button") : #imageLiteral(resourceName: "myprofile_likes"), for: .normal)
            guard var likesCount = Int(sender.title(for: .normal) ?? "") else { return }
            likesCount = count
            self.posts[sender.tag].likes_count = count
            sender.setTitle(String(likesCount), for: .normal)
        }
    }
    
    
    
    
}
