////
////  CheckinCreateViewController.swift
////  Heloo2017
////
////  Created by Константин on 09.04.17.
////  Copyright © 2017 Константин. All rights reserved.
////
//
import UIKit
//
class CheckinCreateViewController: CardViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
}
//
//    @IBOutlet weak var separator: UIView!
//    @IBOutlet weak var user_icon_border: UIView!
//    @IBOutlet weak var checkin_image: UIImageView!
//    @IBOutlet weak var txtViewBottomConstraint: NSLayoutConstraint!
//    @IBOutlet weak var txtView: UITextView!
//    @IBOutlet weak var subtitle: UILabel!
//    @IBOutlet weak var username: UILabel!
//    @IBOutlet weak var cardViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var done_button: UIButton!
//    @IBOutlet weak var upload_button: UIButton!
//    @IBOutlet weak var scrollView: UIScrollView!
//    @IBOutlet weak var cardView: UIView!
//    @IBOutlet weak var background_card: UIView!
//    @IBOutlet weak var true_blur: UIVisualEffectView!
//    @IBOutlet weak var close_button: UIButton!
//    @IBOutlet weak var comments_label: UILabel!
//    @IBOutlet weak var blue_blur: UIImageView!
//    @IBOutlet weak var user_icon: UIImageView!
//    @IBOutlet weak var moving_view: UIView!
//    @IBOutlet weak var moving_view_top_constraint: NSLayoutConstraint!
//
//
//    var checkinCompletionBlock: ((Int)->Void)?=nil
//
//
//    var place: Place!
//
//    var _attachedImage: UIImage?
//    var attachedImage: UIImage? {
//        get {
//            return _attachedImage
//        } set {
//            _attachedImage = newValue
//            self.checkin_image.image = newValue
//        }
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.scrollView.delegate = self
//        self.txtView.subviews.forEach({$0.layer.masksToBounds = true})
//        self.view.layoutIfNeeded()
//        if self.view.frame.size.height-69 > 362 {
//            self.cardViewHeight.constant = self.view.frame.size.height-69
//        }
//        self.animatableViews = [blue_blur: 1.0, comments_label: 1.0, close_button: 1.0, true_blur: 2.0, background_card: 3.0]
//        self.subscribeToKeyboard()
//        self.addTapOutsideGestureRecognizer()
//        self.txtView.delegate = self
//        self.fill()
//        self.txtView.isScrollEnabled = false
//
//    }
//
//    private func fill() {
//        guard let user = UserDataManager.instance.currentUser else { return }
//        let imgString = user.image_small.isEmpty ? user.image : user.image_small
//        user_icon.sd_setImage(with: URL(string: imgString), placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
//        username.text = user.firstName + " " + user.lastName
//        let mutableString = NSMutableAttributedString(attributedString: NSAttributedString(string: [place.address?.street, place.address?.number].flatMap({$0}).joined(separator: ", "), attributes: [NSForegroundColorAttributeName : UIColor(hex: "C0C0C0")]))
//        mutableString.append(NSAttributedString(string: place.name, attributes: [NSForegroundColorAttributeName : UIColor(hex: "42A9F0")]))
//        subtitle.attributedText = mutableString
//
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        checkinCompletionBlock?(place.checkins_count)
//    }
//
//
//    override func keyboardWillBeShown(_ notification: Notification) {
//        UIView.animate(withDuration: 0.3, animations: {
//            self.txtViewBottomConstraint.constant = 260
//        })
//    }
//
//    override func keyboardWillBeHidden() {
//        UIView.animate(withDuration: 0.3, animations: {
//            self.txtViewBottomConstraint.constant = 75
//        })
//    }
//
//    @IBAction func close(_ sender: UIButton) {
//        self.makeCoverVerticalDismiss()
//
//    }
//
//    @IBAction func upload(_ sender: UIButton) {
//
//        let sheet = UIAlertController(title: "Выберите источник", message: nil, preferredStyle: .actionSheet)
//        sheet.addAction(UIAlertAction(title: "Снять фото или видео", style: .default, handler: { (_) in
//            self.presentPicker(with: .camera)
//        }))
//        sheet.addAction(UIAlertAction(title: "Альбомы", style: .default, handler: { (_) in
//            self.presentPicker(with: .savedPhotosAlbum)
//        }))
//        sheet.addAction(UIAlertAction(title: "Все фото", style: .default, handler: { (_) in
//            self.presentPicker(with: .photoLibrary)
//        }))
//        sheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
//        self.present(sheet, animated: true, completion: nil)
//    }
//
//
//    func presentPicker(with type: UIImagePickerControllerSourceType) {
//        let picker = UIImagePickerController()
//        picker.navigationBar.tintColor = .white
//        picker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
//        picker.navigationBar.isTranslucent = false
//        picker.navigationBar.barTintColor = UIColor(red: 57/255, green: 152/255, blue: 226/255, alpha: 1.0)
//        picker.sourceType = type
//        picker.delegate = self
//        self.present(picker, animated: true, completion: nil)
//    }
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        picker.dismiss(animated: true, completion: nil)
//
//    }
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            self.attachedImage = img
//        }
//        picker.dismiss(animated: true, completion: nil)
//    }
//
//    @IBAction func done(_ sender: UIButton) {
//        guard txtView.text != "Напишите что-нибудь…" && !txtView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { return }
//        Server.makeRequest.createCheckin(place: place, text: txtView.text, image: attachedImage) { (success) in
//            if success {
//                self.place.checkins_count += 1
//                self.dismiss(animated: true, completion: {_ in
//                    DoneViewController.presentOne(with: "Чекин выполнен")
//                })
//            } else {
//                ErrorPopupManager.showError(type: .unknown, from: self, subtitle: "Лимит: не больше 1 чекина в минуту")
//            }
//        }
//    }
//}
//
//
//extension CheckinCreateViewController: UITextViewDelegate {
//    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
//        if textView.attributedText.string == "Напишите что-нибудь…" {
//            let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
//            mutable.mutableString.setString("")
//            textView.attributedText = mutable
//        }
//        return true
//    }
//
//    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
//        if textView.attributedText.string.isEmpty {
//            let mutable = NSMutableAttributedString(attributedString: textView.attributedText)
//            mutable.mutableString.setString("Напишите что-нибудь…")
//            textView.attributedText = mutable
//        }
//        return true
//    }
//
//    func textViewDidChange(_ textView: UITextView) {
//        let delta = (textView.attributedText.requiredHeightForTextView(textView.font!, width: textView.frame.size.width) - textView.frame.size.height)
//
//        if self.moving_view.frame.origin.y - delta >= 96 && self.moving_view.frame.origin.y - delta <= 210 {
//            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveLinear, animations: {
//
//                //                UIView.transition(with: self.view, duration: 0.1, options: [.beginFromCurrentState, .curveLinear], animations: {
//                self.moving_view.frame.origin.y -= delta
//                self.moving_view_top_constraint.constant -= delta
//                self.view.setNeedsLayout()
//                self.view.layoutIfNeeded()
//            }, completion: nil)
//
//        } else {
//            textView.isScrollEnabled = true
//        }
//    }
//
//
//}

