//
//  Server.swift
//  Heloo2017
//
//  Created by Константин on 19.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation




extension DataResponse {
    var json: NSDictionary? {
        get {
            return self.result.value as? NSDictionary ?? nil
        }
    }
}

class Server {
    
    static let defaultPageSize = 20
    
    let auth_token_key: NSString = "auth_token_key"
    
    
    var addedToFavorites: [String] = []
    var removedFromFavorites: [String] = []
    
    ///не трогать вне Server
    var foundedPlaces: [Place: Date] = [:]  {
        didSet {
            guard foundedPlaces != oldValue else { return }
            NotificationCenter.default.post(name: .places_update_notification, object: nil)
        }
    }
    
    var viewedPlacesUUIDsToday: Set<String> {
        get {
            var result: [String] = []
            if let uuids = UserDefaults.standard.dictionary(forKey: "viewedPlacesUUIDs") as? [String: Date] {
                for (key, value) in uuids {
                    if Calendar.current.isDateInToday(value) {
                        result.append(key)
                    }
                }
            }
            return Set(result)
        } set {
            var dict: [String: Date] = UserDefaults.standard.dictionary(forKey: "viewedPlacesUUIDs") as? [String: Date] ?? [:]
            
            let reallyNewValues = newValue.filter({ !viewedPlacesUUIDsToday.contains($0) })
            
            for uuid in reallyNewValues {
                dict[uuid] = Date()
            }
            
            UserDefaults.standard.set(dict, forKey: "viewedPlacesUUIDs")
            _ = UserDefaults.standard.synchronize()
            
            NotificationCenter.default.post(name: .viewed_places_update_notification, object: nil)
        }
    }
    
    
    var notificatedPlacesUUIDsToday: Set<String> {
        get {
            var result: [String] = []
            if let uuids = UserDefaults.standard.dictionary(forKey: "notificatedPlacesUUIds") as? [String: Date] {
                for (key, value) in uuids {
                    if Calendar.current.isDateInToday(value) {
                        result.append(key)
                    }
                }
            }
            return Set(result)
        } set {
            var dict: [String: Date] = UserDefaults.standard.dictionary(forKey: "notificatedPlacesUUIds") as? [String: Date] ?? [:]

            let reallyNewValues = newValue.filter({ !notificatedPlacesUUIDsToday.contains($0) })

            for uuid in reallyNewValues {
                dict[uuid] = Date()
            }

            UserDefaults.standard.set(dict, forKey: "notificatedPlacesUUIds")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    
    var freshPlaces: [Place] {
        get {
            return activePlaces.filter({ viewedPlacesUUIDsToday.contains($0.uuid) == false })
        }
    }
    
    ///активные на данный момент плейсы
    var activePlaces: [Place] {
        get {
            var result: [Place] = []
            
            if foundedPlaces.count != 0 {
                var simpifiedDict: [String: Date] = [:]
                
                for (place, date) in foundedPlaces {
                    if Date().timeIntervalSince1970 - date.timeIntervalSince1970 < beacon_timeout_seconds {
                        simpifiedDict[place.uuid] = date
                        result.append(place)
                    }
                }
                
                UserDefaults.standard.set(simpifiedDict, forKey: "foundedPlacesUUIDs")
                _ = UserDefaults.standard.synchronize()
            }
            
            return result.sorted(by: {first, second in
                return (foundedPlaces[first]?.timeIntervalSince1970 ?? 0) < (foundedPlaces[second]?.timeIntervalSince1970 ?? 1)
            })
        }
    }
    
    ///скачать последние активные места, если они всё еще актуальны
    func restoreActivePlaces() {
        if let dict = UserDefaults.standard.value(forKey: "foundedPlacesUUIDs") as? [String: Date] {
            for (uuid, date) in dict {
                if Date().timeIntervalSince1970 - date.timeIntervalSince1970 < beacon_timeout_seconds {
                    Server.makeRequest.getPlace(uuid: uuid, completion: { (place) in
                        if place != nil {
                            self.foundedPlaces[place!] = date
                        }
                    })
                }
            }
        }
    }
    
    static let makeRequest = Server()
    
    
    lazy var hardParsingQueue: DispatchQueue = {
        return DispatchQueue.global(qos: .userInitiated)
    }()
    
    
    func searchPersons(coordinates: CLLocationCoordinate2D?, completion: @escaping ([User])->Void) {
        var result: [User] = []
        self.searchPersons(by: BeaconManager.instance.activeBeacons) { (beacon_users) in
            result = beacon_users
            self.searchPersons(by: WAPManager.instance.activeWAPs) { wap_users in
                for user in wap_users {
                    if !result.contains(where: {$0.uuid == user.uuid}) {
                        result.append(user)
                    }
                }
                
                self.getPersonsByGPS(location: coordinates, completion: { (gps_users) in
                    for user in gps_users {
                        if !result.contains(where: {$0.uuid == user.uuid}) {
                            result.append(user)
                        }
                    }
                    completion(result)
                    
                })
            }
        }
    }
    
    func searchPlaces(coordinates: CLLocationCoordinate2D?, completion: @escaping ([Place])->Void) {
        var result: [Place] = []
        self.searchPlaces(by: BeaconManager.instance.activeBeacons) { (beacon_places) in
            result = beacon_places
            self.searchPlaces(by: WAPManager.instance.activeWAPs) { wap_places in
                for place in wap_places {
                    if !result.contains(where: {$0.uuid == place.uuid}) {
                        result.append(place)
                    }
                }
                
                Server.makeRequest.getPlacesByGPS(location: coordinates, completion: { (gps_places) in
                    for place in gps_places {
                        if !result.contains(where: {$0.uuid == place.uuid}) {
                            result.append(place)
                        }
                    }
                    completion(result)
                })
            }
        }
    }
    
    
    func getPlaces(page: Int, filter: placeFilterObject?, completion: @escaping (([Place], Bool)->Void)) {
        var params: [String: Any] = ["page": page, "size": Server.defaultPageSize]
        if let currentFilter = filter {
            if currentFilter.fromPrice != nil || currentFilter.toPrice != nil {
                var billString: String!
                if currentFilter.fromPrice == nil {
                    billString = "LESS_THAN_\(currentFilter.toPrice!)"
                } else {
                    if currentFilter.toPrice == nil {
                        billString = "GREATER_THAN_\(currentFilter.fromPrice!)"
                    } else {
                        billString = "FROM_\(currentFilter.fromPrice!)_TO_\(currentFilter.toPrice!)"
                    }
                }
                params["average_of_check"] = billString
            }
            
            if currentFilter.subways.count != 0 {
                params["subway"] = currentFilter.subways.map({$0.tag}).joined(separator: ",")
            }
            
            if currentFilter.categories.count != 0 {
                params["type"] = currentFilter.categories.map({$0.key}).joined(separator: ",")
            }
            
            if currentFilter.city != nil {
                params["city"] = currentFilter.city!.tag
            }
            
            if let text = currentFilter.text, !text.isEmpty {
                params["name"] = text
            }
        }
        
        Alamofire.request(url_paths.base + url_paths.places.main, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            ErrorHandler.instance.handleError(with: response)
            var places: [Place] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    places.append(Place(with: item))
                }
            }
            completion(places, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    ///returns new blocked value
    func changeBlockState(for place: Place, completion: @escaping (Bool)->Void) {
        let path = String(format: url_paths.base + (place.blocked ? url_paths.places.unblock :  url_paths.places.block), place.uuid)
        
        Alamofire.request(path, method: place.blocked ? .put : .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(!place.blocked)
        }
    }
    
    func complain(to checkin: Checkin, completion: @escaping ()->Void) {
        let path = String(format: url_paths.checkin.block, checkin.id)
        Alamofire.request(path, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion()
        }
    }
    
    
    func getPersons(page: Int, filter: personFilterObject?, completion: @escaping (([User], Bool)->Void)) {
        var params: [String: Any] = ["page": page, "size": Server.defaultPageSize]
        
        if let currentFilter = filter {
            
            if currentFilter.fromAge != nil {
                params["age_from"] = currentFilter.fromAge!
            }
            if currentFilter.toAge != nil {
                params["age_to"] = currentFilter.toAge!
            }
            
            if currentFilter.selectedInterests.count != 0 {
                params["interest"] = currentFilter.selectedInterests.map({$0.tag}).joined(separator: ",")
            }
            
            if currentFilter.selectedCity != nil {
                params["city"] = currentFilter.selectedCity!.tag
            }
            
            if currentFilter.gender != nil, currentFilter.gender != .undefined {
                params["gender"] = currentFilter.gender!.apiValue.uppercased()
            }
            if let text = currentFilter.text, !text.isEmpty {
                params["name"] = text
            }
        }
        
        
        Alamofire.request(url_paths.base + url_paths.persons.main, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            ErrorHandler.instance.handleError(with: response)
            var users: [User] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    users.append(User(with: item))
                }
            }
            completion(users, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    func getFavorites(page: Int, isOffers: Bool, fields: [String]=[], completion: @escaping (([Searchable], Bool)->Void)) {
        var params:[String: String] = [:]
        if fields.count != 0 {
            params = ["fields": fields.joined(separator: ",")]
        }
        params["page"] = String(page)
        
        
        Alamofire.request(url_paths.base + (isOffers ? url_paths.favorites.offers : url_paths.favorites.places), method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            ErrorHandler.instance.handleError(with: response)
            var search_items: [Searchable] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    if isOffers {
                        search_items.append(Searchable(offer: Offer(with: item))!)
                    } else {
                        search_items.append(Searchable(place: Place(with: item))!)
                    }
                }
            }
            completion(search_items, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    func getOffers(completion: @escaping (([Offer])->Void)) {
        Alamofire.request(url_paths.base + url_paths.offers.main, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            ErrorHandler.instance.handleError(with: response)
            var offers: [Offer] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    offers.append(Offer(with: item))
                }
            }
            completion(offers)
        }
        
    }
    
    func getOffers(page: Int = 0, place: Place, completion: @escaping (([Offer], Bool)->Void)) {
        Alamofire.request(url_paths.base + url_paths.places.main + "/" + place.uuid + "/offers", method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            var offers: [Offer] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary]{
                for item in items {
                    offers.append(Offer(with: item, place: place))
                }
            }
            completion(offers, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    func createSearchable(with dict: NSDictionary, forceType: String?=nil) -> Searchable? {
        guard let type = dict["type"] as? String ?? forceType else { return nil }
        switch type {
        case "EVENT": return Searchable(event: Event(with: dict["object"] as? NSDictionary ?? dict))
        case "OFFER": return Searchable(offer: Offer(with: dict["object"] as? NSDictionary ?? dict))
        case "CHECKIN": return Searchable(checkin: Checkin(with: dict["object"] as? NSDictionary ?? dict))
        case "ACCEPTED_EVENT": return Searchable(accept: Accept(with: dict["object"] as? NSDictionary ?? dict))
        default: return nil
        }
    }
    
    func getEventsOffers(page: Int = 0, isEvents: Bool, completion: @escaping ([Searchable], Bool)->Void) {
        Alamofire.request(url_paths.base + (isEvents ? url_paths.eventsOffsers.events : url_paths.eventsOffsers.offers), method: .get, parameters: ["page": page, "size": Server.defaultPageSize, (isEvents ? "only_last_events" : "only_last_offers"): true], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            print("getEventsOffers for page \(page) response = \(response)")
            var searchables: [Searchable?] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary]{
                for item in items {
                    searchables.append(self.createSearchable(with: item, forceType: isEvents ? "EVENT": "OFFER"))
                }
            }
            completion(searchables.flatMap({$0}), response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    
    func getCheckins(page: Int = 0, place: Place, completion: @escaping (([Checkin], Bool)->Void)) {
        Alamofire.request(url_paths.base + url_paths.places.main + "/" + place.uuid + "/checkins", method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var checkins: [Checkin] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary]{
                for item in items {
                    checkins.append(Checkin(with: item, place: place))
                }
            }
            completion(checkins, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    func getCheckin(by uuid: String, completion: @escaping (Checkin?)->Void)  {
        Alamofire.request(url_paths.base + url_paths.checkin.main + uuid, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var checkin: Checkin? = nil
            if let json = response.json {
                checkin = Checkin(with: json)
            }
            completion(checkin)
        }
    }
    
    func getAcceptedEvent(by uuid: String, completion: @escaping (Accept?)->Void) {
        Alamofire.request(url_paths.base + url_paths.favorites.events + "/" + uuid, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var accept: Accept?
            if let json = response.json {
                accept = Accept(with: json)
            }
            completion(accept)
        }
    }
    
    
    func getEvents(page: Int = 0, place: Place, completion: @escaping (([Event], Bool)->Void)) {
        Alamofire.request(url_paths.base + url_paths.places.main + "/" + place.uuid + "/events", method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var events: [Event] = []
            
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    events.append(Event(with: item, place: place))
                }
            }
            completion(events, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    
    func createCheckin(place: Place, text: String, image: UIImage?, completion: @escaping (Bool,String?)->Void) {
        Alamofire.request(url_paths.base + url_paths.places.main + "/" + place.uuid + "/checkins", method: .post, parameters: ["description": text], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if let checkin_url = response.response?.allHeaderFields["Location"] as? String {
                guard image != nil else { completion(true, nil); return }
                let uuidStart = checkin_url.range(of: "checkins/")!.upperBound
                let uuid = checkin_url.substring(from: uuidStart)
                self.fileUpload(file: image!, type: .jpg, target: .checkin, checkin_id: uuid, completion: { (img_url, errorMessage) in
                    completion(img_url != nil, errorMessage)
                })
            } else {
                completion(false, response.json?["errorMessage"] as? String)
            }
        }
    }
    
    func getMyProfile(completion: @escaping ()->Void) {
        Alamofire.request(url_paths.base + url_paths.profile.main, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            ErrorHandler.instance.handleError(with: response, completion: { (success, canRetry) in
                if !success && canRetry {
                    self.getMyProfile {
                        completion()
                    }
                } else {
                    self.getAvailableInterests()
                    self.getAvailablePlaceTypes()
                    
                    if let json = response.json {
                        let user = User(with: json)
                        UserDataManager.instance.currentUser = user
                    }
                    completion()
                }
            })
            
        }
    }
    
    
    func like(post: Post, completion: @escaping (Bool, Bool, Int)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false, post.isLiked, post.likes_count)
            return
        }
        var key_part = ""
        var uuid = ""
        var method = ""
        //        var canBeLiked = true
        if let checkin = post.searchable_item?.checkin {
            key_part = "/checkins/"
            uuid = checkin.id
        } else if let offer = post.searchable_item?.offer {
            key_part = "/offers/"
            uuid = offer.id
        } else if let event = post.searchable_item?.event {
            key_part = "/events/"
            uuid = event.id
        } else if let accept = post.searchable_item?.accept {
            key_part = "/events/accepted/"
            uuid = accept.id
        }
        if post.isLiked {
            method = "/dislike"
        } else {
            method = "/like"
        }
        
        //        if canBeLiked {
        
        Alamofire.request(url_paths.base + key_part + uuid + method, method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            
            let success = response.response?.statusCode ?? 0 == 200
            completion(success, success ? !post.isLiked : post.isLiked, (response.result.value as? NSDictionary)?["count"] as? Int ?? 0)
            //        }
        }
    }
    
    func getComments(uuid lastUuid: String?, page: Int, for post: Post?, completion: @escaping ([Comment], Bool) -> Void) {
        var key_part = ""
        var uuid = ""
        if let checkin = post?.searchable_item?.checkin {
            key_part = "/checkins"
            uuid = checkin.id
        } else if let offer = post?.searchable_item?.offer {
            key_part = "/offers"
            uuid = offer.id
        } else if let event = post?.searchable_item?.event {
            key_part = "/events"
            uuid = event.id
        } else if let accept = post?.searchable_item?.accept {
            key_part = "/events/accepted"
            uuid = accept.id
        }
        
        var params: [String: Any] = ["page": page, "size": Server.defaultPageSize]
        if lastUuid != nil {
            params["lastUuid"] = lastUuid!
        }
        
        
        Alamofire.request(url_paths.base + key_part + "/" + uuid + "/" + "comments", method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var comments: [Comment] = []
            
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                items.forEach({comments.append(Comment(with: $0)!)})
            }
            completion(comments, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    func leaveComment(text: String, for post: Post?, completion: @escaping (Comment?)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(nil)
            return
        }
        var key_part = ""
        var uuid = ""
        
        if let checkin = post?.searchable_item?.checkin {
            key_part = "/checkins"
            uuid = checkin.id
        } else if let offer = post?.searchable_item?.offer {
            key_part = "/offers"
            uuid = offer.id
        } else if let event = post?.searchable_item?.event {
            key_part = "/events"
            uuid = event.id
        } else if let accept = post?.searchable_item?.accept {
            key_part = "/events/accepted"
            uuid = accept.id
        }
        
        Alamofire.request(url_paths.base + key_part + "/" + uuid + "/" + "comments", method: .post, parameters: ["text": text], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            
            //
            guard response.json != nil else { completion(nil); return }
            completion(Comment(with: response.json))
            
        }
    }
    
    func deleteComment(_ id: String, for post: Post?, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard post != nil else { completion(false); return }
        
        var key_part = ""
        var uuid = ""
        
        if let checkin = post?.searchable_item?.checkin {
            key_part = "/checkins"
            uuid = checkin.id
        } else if let offer = post?.searchable_item?.offer {
            key_part = "/offers"
            uuid = offer.id
        } else if let event = post?.searchable_item?.event {
            key_part = "/events"
            uuid = event.id
        } else if let accept = post?.searchable_item?.accept {
            key_part = "/events/accepted"
            uuid = accept.id
        }
        
        
        Alamofire.request(url_paths.base + key_part + "/" + uuid + "/" + "comments/" + id, method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            print("response = \(response)")
            completion((response.response?.statusCode ?? 0) / 100 == 2)
        }
        
    }
    
    func updateMyProfile(data: regData, completion: @escaping ()->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion()
            return
        }
        var params: [String: AnyObject] = [:]
        var profile: [String: AnyObject] = [:]
        profile["city"] = ["city_tag": data.city_tag] as AnyObject
        profile["display_name"] = UserDataManager.instance.currentUser.displayName as AnyObject
        profile["gender"] = data.gender!.rawValue as AnyObject
        
        if let changedName = data.name, !changedName.isEmpty {
            profile["first_name"] = changedName as AnyObject
        } else {
            profile["first_name"] = UserDataManager.instance.currentUser.firstName as AnyObject
        }
        
        if let changedLastname = data.lastName, !changedLastname.isEmpty {
            profile["last_name"] = changedLastname as AnyObject
        } else {
            profile["last_name"] = UserDataManager.instance.currentUser.lastName as AnyObject
        }
        
        profile["date_of_birth"] = data.birthday?.parseDateShort() as AnyObject
        params[User.fields.notificate_on_comments] = UserDataManager.instance.currentUser.notificate_on_comments as AnyObject
        params[User.fields.notificate_on_subscribers] = UserDataManager.instance.currentUser.notificate_on_subscribers as AnyObject
        
        params["person"] = profile as AnyObject
        
        
        
        Alamofire.request(url_paths.base + url_paths.profile.main, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            UserDataManager.instance.isRegInProgress = false
            
            if let json = response.json {
                UserDataManager.instance.currentUser = User(with: json)
            }
            completion()
        }
    }
    
    ///only one flag at the time
    func updateNotificationFlags(data: regData, onSubscriptions: Bool?=nil, onComments: Bool?=nil, completion: @escaping ()->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion()
            return
        }
        
        guard onSubscriptions != nil || onComments != nil else { return }
        var params:  [String: AnyObject] = [:]
        var profile: [String: AnyObject] = [:]
        
        if onSubscriptions != nil {
            params[User.fields.notificate_on_subscribers] = onSubscriptions! as AnyObject
            params[User.fields.notificate_on_comments] = UserDataManager.instance.currentUser.notificate_on_comments as AnyObject
            
            let user = UserDataManager.instance.currentUser
            user?.notificate_on_subscribers = onSubscriptions!
            UserDataManager.instance.currentUser = user
            
        } else if onComments != nil {
            params[User.fields.notificate_on_comments] = onComments! as AnyObject
            params[User.fields.notificate_on_subscribers] = UserDataManager.instance.currentUser.notificate_on_subscribers as AnyObject
            
            let user = UserDataManager.instance.currentUser
            user?.notificate_on_comments = onComments!
            UserDataManager.instance.currentUser = user
            
        } else {
            return;
        }
        
        profile["city"] = ["city_tag": data.city_tag] as AnyObject
        profile["display_name"] = UserDataManager.instance.currentUser.displayName as AnyObject
        profile["gender"] = data.gender!.rawValue as AnyObject
        profile["first_name"] = data.name as AnyObject
        profile["last_name"] = data.lastName as AnyObject
        profile["date_of_birth"] = data.birthday?.parseDateShort() as AnyObject
        
        //
        params["person"] = profile as AnyObject
        
        Alamofire.request(url_paths.base + url_paths.profile.main, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            completion()
            
        }
    }
    
    
    func getFeed(page: Int=0, only_subscriptions: Bool, completion: @escaping ([Searchable], Bool)->Void) {
        
        var params: [String: AnyObject] = [:]
        params["page"] = page as AnyObject
        params["only_subscriptions"] = only_subscriptions as AnyObject
        
        Alamofire.request(url_paths.base + url_paths.feed.main, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            var searchables: [Searchable?] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    searchables.append(self.createSearchable(with: item))
                }
            }
            completion(searchables.flatMap({$0}).sorted(by: {$0.sortDate > $1.sortDate}), response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    
    
    func addPlaceToFavorites(uuid: String, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        self.addedToFavorites.append(uuid)
        if let index = self.removedFromFavorites.index(of: uuid) {
            self.removedFromFavorites.remove(at: index)
        }
        Alamofire.request(url_paths.base + url_paths.favorites.places, method: .post, parameters: ["uuid": uuid], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(true)
        }
    }
    
    func removePlaceFromFavorites(uuid: String, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        self.removedFromFavorites.append(uuid)
        if let index = self.addedToFavorites.index(of: uuid) {
            self.addedToFavorites.remove(at: index)
        }
        
        Alamofire.request(url_paths.base + url_paths.favorites.places + "/\(uuid)", method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(true)
        }
    }
    
    func acceptEvent(uuid: String, completion: @escaping (Bool) -> Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        Alamofire.request(url_paths.base + url_paths.favorites.events, method: .post, parameters: ["uuid": uuid], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print("accept event request url = \(url_paths.base + url_paths.favorites.events)")
            print("accept event request params = \(["uuid": uuid])")
            
            print("accept event response = \(response)")
            completion(true)
        }
    }
    
    func discardEvent(uuid: String, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        Alamofire.request(url_paths.base + url_paths.favorites.events + "/\(uuid)", method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            completion(true)
            
        }
    }
    
    func deleteCheckin(uuid: String, completion: @escaping (Bool)->Void) {
        guard !uuid.isEmpty else { completion(false); return }
        Alamofire.request(url_paths.base + url_paths.checkin.main + uuid, method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            print("deleteCheckin response = \(response)")
            completion((response.response?.statusCode ?? 0) / 100 == 2) //проканает любой двухсотый статус
        }
        
    }
    
    func addOfferToFavorites(uuid: String, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        Alamofire.request(url_paths.base + url_paths.favorites.offers, method: .post, parameters: ["uuid": uuid], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            completion(true)
        }
    }
    
    func removeOfferFromFavorites(uuid: String, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        Alamofire.request(url_paths.base + url_paths.favorites.offers + "/\(uuid)", method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            completion(true)
        }
        
    }
    
    
    
    func getPerson(uuid: String, completion: @escaping (User?)->Void) {
        Alamofire.request(url_paths.base + url_paths.persons.main + "/" + uuid, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.json {
                completion(User(with: json))
            } else {
                completion(nil)
            }
        }
    }
    
    
    func getPlace(uuid: String, completion: @escaping (Place?)->Void) {
        Alamofire.request(url_paths.base + url_paths.places.main + "/" + uuid, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            if let json = response.json {
                completion(Place(with: json))
            } else {
                completion(nil)
            }
        }
    }
    
    
    func deletePost(_ post: Post, _ completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        
        switch post.type {
        case .user_will_go:
            if let event = post.searchable_item?.accept?.event {
                self.discardEvent(uuid: event.id, completion: completion)
            }
        case .checkin_created:
            if let checkin = post.searchable_item?.checkin {
                deleteCheckin(uuid: checkin.id, completion: completion)
            }
        default:
            print("deletion of this post's type is not implemented yet")
        }
    }
    
    
    
    func getWall(page: Int=0, uuid: String, completion: @escaping ([Searchable], Bool, Int)->Void) {
        hardParsingQueue.async {
            
            if UserDataManager.instance.currentUser.uuid == uuid {
                Alamofire.request(url_paths.base + url_paths.wall.main, method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
                    
                    var searchables: [Searchable?] = []
                    
                    if let json = response.json, let items = json["items"] as? [NSDictionary] {
                        for item in items {
                            searchables.append(self.createSearchable(with: item))
                        }
                    }
                    
                    completion(searchables.flatMap({$0}).sorted(by: {$0.sortDate > $1.sortDate}), response.json?["hasNext"] as? Bool ?? false, response.json?["totalResultSize"] as? Int ?? 0)
                }
            } else {
                Alamofire.request(url_paths.base + url_paths.persons.main + "/\(uuid)/wall", method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: self.header).responseJSON(completionHandler: { (response) in
                    var searchables: [Searchable?] = []
                    if let json = response.json, let items = json["items"] as? [NSDictionary] {
                        for item in items {
                            searchables.append(self.createSearchable(with: item))
                        }
                    }
                    completion(searchables.flatMap({$0}).sorted(by: {$0.sortDate > $1.sortDate}), response.json?["hasNext"] as? Bool ?? false, response.json?["totalResultSize"] as? Int ?? 0)
                })
            }
        }
    }
    
    
    
    func updateInterests(_ interests: [TreeItem], completion: @escaping ()->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion()
            return
        }
        
        var request = URLRequest(url: URL(string: url_paths.base + url_paths.profile.interests)!)
        request.httpMethod = "POST"
        request.setValue(header!["Authorization"], forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: interests.map({$0.key}))
        
        Alamofire.request(request).responseJSON { (response) in
            ErrorHandler.instance.handleError(with: response)
            completion()
        }
    }
    
}



extension Server { //MARK: - VK requests
    
    func getCitiesList(completionhandler: @escaping ([(title: String, tag: String)])->Void) {
        Alamofire.request(url_paths.base + url_paths.stuff.cities, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            var result: [(title: String, tag: String)] = []
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    result.append((title: dict["value"] as! String, tag: dict["city_tag"] as! String))
                }
            }
            completionhandler(result)
        }
    }
    
    func getVKCountryList(completionHadnler:@escaping (_ error: Error?, _ countries:[(title: String, cid: Int)]) -> ()) {
        let request = NSURLRequest(url: NSURL(string: "https://api.vk.com/method/database.getCountries?need_all=0&count=1000")! as URL)
        var countries:[(title: String, cid: Int)] = []
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main, completionHandler: { (response: URLResponse?, data: Data?, error: Error?) in
            if error == nil && data != nil {
                let json = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                if json != nil {
                    if let response = json!?["response"] as? [NSDictionary] {
                        for item  in response {
                            let title: String = item["title"] as! String
                            let cid: Int = item["cid"] as! Int
                            countries.append((title: title, cid: cid))
                        }
                        completionHadnler(nil, countries)
                    } else {
                        completionHadnler(error, [])
                        
                    }
                    
                } else {
                    completionHadnler(error , [])
                }
                
            } else {
                completionHadnler(error , [])
            }
        })
    }
    
    func getVKCityList(countryID: Int, query: String, completionHadnler:@escaping (_ error: NSError?, _ cities:[(title: String, cid: Int)]) -> ()) {
        let request = URLRequest(url: URL(string: "https://api.vk.com/method/database.getCities?country_id=\(countryID)&count=1000&need_all=0&q=\(query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)")!)
        var cities:[(title: String, cid: Int)] = []
        
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main, completionHandler: { (response: URLResponse?, data: Data?, error: Error?) in
            if error == nil && data != nil {
                let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                if json != nil {
                    if let response = json!["response"] as? [NSDictionary] {
                        for item in response {
                            var title: String = (item["title"] as! String)
                            if let reg = item["region"] as? String {
                                title += ", " + reg
                            }
                            let cid = item["cid"] as! Int
                            cities.append((title: title, cid: cid))
                            
                        }
                        completionHadnler(nil, cities)
                    } else {
                        completionHadnler(error as NSError?, [])
                    }
                } else {
                    completionHadnler(error as NSError?, [])
                    
                }
                
            } else {
                completionHadnler(error as NSError?, [])
            }
        })
    }
    
}








