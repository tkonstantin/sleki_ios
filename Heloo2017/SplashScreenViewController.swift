//
//  SplashScreenViewController.swift
//  Heloo2017
//
//  Created by Константин on 08.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {
    
    @IBOutlet weak var background_city: UIImageView!
    @IBOutlet weak var logo_text: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var logoZeroHeight: NSLayoutConstraint!
    @IBOutlet weak var logoNormalHeight: NSLayoutConstraint!
    @IBOutlet weak var logoVerticalCenter: NSLayoutConstraint!
    @IBOutlet weak var textTop: NSLayoutConstraint!
    @IBOutlet weak var textTopCompact: NSLayoutConstraint!
    
    func animateAppearance() {
        self.logoZeroHeight.isActive = false
        self.logoNormalHeight.isActive = true
        UIView.animate(withDuration: 0.6, delay: 0.2, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }) { (_) in
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.4, options: [], animations: {
                self.logoVerticalCenter.constant -= 44
                self.logo_text.alpha = 1
                self.textTopCompact.isActive = false
                self.textTop.isActive = true
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.appeared = true
                if self.downloaded {
                    self.goNext()
                }
            })
        }
    }
    
    func disappear() {
        
        if !UserDataManager.instance.isOnboardingCompleted {
            let main = UIStoryboard(name: "Onboarding", bundle: nil).instantiateInitialViewController()!
            UIView.transition(from: self.view, to: main.view, duration: 0.5, options: .transitionFlipFromLeft, completion: { _ in
                UIApplication.shared.keyWindow?.rootViewController = main
            })
            return
        }

        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            if UserDataManager.instance.isOnboardingCompleted {
                let logoCenter: CGFloat = 88 + (self.safeAreaTop == 0 ? 20 : self.safeAreaTop)
                self.logo.center.y = logoCenter
                self.logo.frame.size = CGSize(width: 48, height: 68)
                self.logo.center.x = self.view.center.x
                self.logo_text.center.y = logoCenter + 104
                self.logo_text.frame.size.height = 0
                self.logo_text.alpha = 0
            } else {
                self.logo.center.y = self.view.bounds.height/2 - 18 - 64 - 120/2
                self.logo_text.alpha = 0
                self.background_city.alpha = 0
            }
        }) { (_) in
            UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController()
        }
        
    }
    
    var appeared = false
    var downloaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        async {
            self.animateAppearance()
        }
        
        UIApplication.shared.isStatusBarHidden = false
        if Server.makeRequest.header != nil && UserDataManager.instance.currentUser != nil {
            Server.makeRequest.getMyProfile {
                self.downloaded = true
                if self.appeared {
                    self.goNext()
                }
            }
        } else {
            self.downloaded = true
            if appeared {
                goNext()
            }
        }
        
    }
    
    func goNext() {
        if UserDataManager.instance.isRegInProgress {
            UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "RegViewController")
        } else {
            if Server.makeRequest.header != nil && UserDataManager.instance.currentUser != nil {
                BeaconManager.instance.startSearchingBeacons()
                WAPManager.instance.startScanning()
                ChattingService.shared.getChats { }
                UIApplication.goMain()
            } else {
                disappear()
            }
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    
    
}
