//
//  LoginViewController.swift
//  Heloo2017
//
//  Created by Константин on 08.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import VK_ios_sdk

extension UIApplication {
    class func mainController() -> UIViewController? {
        return UIStoryboard(name: "MainTabs", bundle: nil).instantiateInitialViewController()
    }
    
    class func goMain() {
        UIApplication.shared.keyWindow?.backgroundColor = .white
        let main = mainController()!
        
        UIView.transition(from: UIApplication.shared.keyWindow!.rootViewController!.view, to: main.view, duration: 0.5, options: .transitionFlipFromLeft, completion: { _ in
            UIApplication.shared.keyWindow?.rootViewController = main
        })
    }
}

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var instagramLogin: InstagramLoginViewController!
    

    let clientId = "24c6be5887444202ad0fb145df08b5e2"
    let clientSecret = "047b1073b30347c7b6c0914799adfabc"
    let redirectUri = "https://sleki.com"
    
    
    @IBOutlet var defaultConstraints: [NSLayoutConstraint]!
    @IBOutlet var loginConstraints: [NSLayoutConstraint]!
    @IBOutlet var resetConstraints: [NSLayoutConstraint]!
    
    @IBOutlet var noTransparentViews: [UIView]!
    
    
    @IBOutlet weak var logo_text: UIImageView!
    @IBOutlet weak var reset_view: UIView!
    @IBOutlet weak var passwordBlur: UIVisualEffectView!
    @IBOutlet weak var loginBlur: UIVisualEffectView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var login_view: UIView!
    @IBOutlet weak var socials_view: UIView!
    @IBOutlet weak var password_field: UITextField!
    @IBOutlet weak var email_field: UITextField!
    
    fileprivate var last_vk_token_string = ""
    
    let keyboardAnimationDuration: Double = 0.25
    
    var isLoginInProgress: Bool = false {
        didSet {
            if isLoginInProgress {
                activateLoginConstraints()
                animateAlpha()
            }
        }
    }
    
    var isResettingInProgress: Bool = false {
        didSet {
            if isResettingInProgress {
                self.email_field.becomeFirstResponder()
                activateResetConstraints()
                animateAlpha()
            }
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
    func animateAlpha() {
        UIView.animate(withDuration: keyboardAnimationDuration, animations: {
//            self.loginBlur.alpha = self.isLoginInProgress || self.isResettingInProgress ? 0.88 : 0.66
//            self.passwordBlur.alpha =  self.isLoginInProgress || self.isResettingInProgress ? 0.88 : 0.66
            
            self.password_field.alpha = self.isResettingInProgress ? 0 : 1
            self.passwordBlur.alpha = self.isResettingInProgress ? 0 : 0.15
            
            self.socials_view.alpha = (!self.isResettingInProgress && !self.isLoginInProgress) ? 1 : 0
            self.reset_view.alpha = self.isResettingInProgress ? 1 : 0
            self.login_view.alpha = self.isLoginInProgress ? 1 : 0
            self.resetButton.alpha = self.isLoginInProgress ? 0 : 1
        }) { (_) in
            self.password_field.isUserInteractionEnabled = !(self.isResettingInProgress)
            self.passwordBlur.isUserInteractionEnabled = !(self.isResettingInProgress)
            
            self.socials_view.isUserInteractionEnabled = !(self.isResettingInProgress || self.isLoginInProgress)
            self.reset_view.isUserInteractionEnabled = self.isResettingInProgress
            self.login_view.isUserInteractionEnabled = self.isLoginInProgress
        }

    }
    
    @IBAction func reset(_ sender: UIButton) {
        isResettingInProgress = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        animateAppearance()
    }
    
    func animateAppearance() {
        var shouldAnimateBack: [UIView] = []
        for v in self.view.subviews {
            if !self.noTransparentViews.contains(v) && v.alpha == 1 {
                shouldAnimateBack.append(v)
                v.alpha = 0
            }
        }
        
        UIView.animate(withDuration: 0.3) {
            for v in shouldAnimateBack {
                    v.alpha = 1
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        VKSdk.instance().register(self)
        VKSdk.instance().uiDelegate = self
        self.email_field.delegate = self
        self.password_field.delegate = self
        
        self.email_field.tag = 1
        self.password_field.tag = 2
        self.subscribeToKeyboard()
        self.addTapOutsideGestureRecognizer()
    }
    
    
    override func hideKeyboard() {
        self.isLoginInProgress = false
        self.isResettingInProgress = false
        self.view.endEditing(true)
        self.activateDefaultConstraints()
        self.animateAlpha()
    }
    
    
    override func keyboardWillBeShown(_ notification: Notification) {
        
    }
    
    override func keyboardWillBeHidden() {
        
    }
    
    func activateDefaultConstraints() {
        
        defaultConstraints.forEach({$0.isActive = true})
        loginConstraints.forEach({$0.isActive = false})
        resetConstraints.forEach({$0.isActive = false})

        layoutAnimated()
    }
    
    func activateLoginConstraints() {
        defaultConstraints.forEach({$0.isActive = false})
        loginConstraints.forEach({$0.isActive = true})
        resetConstraints.forEach({$0.isActive = false})
        
        layoutAnimated()
        
    }
    
    func activateResetConstraints() {
        defaultConstraints.forEach({$0.isActive = false})
        loginConstraints.forEach({$0.isActive = false})
        resetConstraints.forEach({$0.isActive = true})
        
        layoutAnimated()
    }
    
    func layoutAnimated() {
        UIView.animate(withDuration: keyboardAnimationDuration*2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.logo_text.alpha = self.email_field.frame.minY < self.logo_text.frame.maxY ? 0 : 1
        }, completion: nil)
    }
    
    
    
    @IBAction func vk_auth(_ sender: UIButton) {
        if VKSdk.isLoggedIn() {
            VKSdk.forceLogout()
        }
        async {
            VKSdk.authorize([])
        }
    }
    
    
    @IBAction func fb_auth(_ sender: UIButton) {
    }
    
    @IBAction func insta_auth(_ sender: UIButton) {
        instagramLogin = InstagramLoginViewController(clientId: clientId, redirectUri: redirectUri)
        instagramLogin.delegate = self
        
        instagramLogin.title = "Вход через Instagram"
        
        instagramLogin.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissLoginViewController))
        instagramLogin.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshPage))

        present(UINavigationController(rootViewController: instagramLogin), animated: true)
    }
    
    @objc func dismissLoginViewController() {
        instagramLogin.dismiss(animated: true)
    }
    
    @objc func refreshPage() {
        instagramLogin.reloadPage()
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let next = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            next.becomeFirstResponder()
        } else {
            
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if !self.isLoginInProgress && !self.isResettingInProgress {
            self.isLoginInProgress = true
        }
        return true
    }
    
    
    @IBAction func sendReset(_ sender: UIButton) {
        self.hideKeyboard()
        guard !email_field.text!.isEmpty else { dismissHUD(); ErrorPopupManager.showError(type: .fields, from: self); return }
        guard email_field.text!.isValidEmail else { dismissHUD(); ErrorPopupManager.showError(type: .invalid_data, from: self); return }


        showHUD()
        Server.makeRequest.passwordRecovery_request(email: email_field.text!) { (success) in
            dismissHUD()
            if success {
                self.performSegue(withIdentifier: "reset_popup", sender: nil)
            } else {
                ErrorPopupManager.showError(type: .invalid_data, from: self)
            }
        }
        
    }
    
    @IBAction func sendLogin(_ sender: UIButton) {
        self.hideKeyboard()
        guard !email_field.text!.isEmpty, !password_field.text!.isEmpty else { dismissHUD(); ErrorPopupManager.showError(type: .fields, from: self); return }
        guard email_field.text!.isValidEmail else { dismissHUD(); ErrorPopupManager.showError(type: .invalid_data, from: self); return }


        showHUD()
        Server.makeRequest.auth(username: email_field.text!, password: password_field.text!, completion: { (success) in
            guard success else { dismissHUD(); ErrorPopupManager.showError(type: .invalid_data, from: self); return }
            Server.makeRequest.getMyProfile {
                dismissHUD()
                if success {
                    let bg = UIImageView(image: UIImage(named: "blue_blur_rect"))
                    bg.frame = self.view.bounds
                    bg.contentMode = .scaleAspectFill
                    let parent = self
                    parent.view.bringSubview(toFront: bg)
                    parent.view.addSubview(bg)
                    UIApplication.goMain()
                    BeaconManager.instance.startSearchingBeacons()
                    WAPManager.instance.startScanning()
                    ChattingService.shared.getChats { }
                } else {
                    ErrorPopupManager.showError(type: .invalid_data, from: self)
                    self.email_field.text = ""
                    self.password_field.text = ""
                }
            }
            
        })
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        UserDataManager.instance.currentUser = User(with: [:])
        Server.makeRequest.getAvailableInterests()
        Server.makeRequest.getAvailablePlaceTypes()
        BeaconManager.instance.startSearchingBeacons()
        WAPManager.instance.startScanning()
        let bg = UIImageView(image: UIImage(named: "blue_blur_rect"))
        bg.frame = self.view.bounds
        bg.contentMode = .scaleAspectFill
        let parent = self
        parent.view.bringSubview(toFront: bg)
        parent.view.addSubview(bg)
        UIApplication.goMain()
    }
    
}

extension LoginViewController: InstagramLoginViewControllerDelegate {
    func instagramLoginDidFinish(accessToken: String?, error: InstagramError?) {
        print("insta token = \(accessToken)")
        instagramLogin.dismiss(animated: true)

    }
    
    
}


extension LoginViewController: VKSdkUIDelegate, VKSdkDelegate {
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.present(controller, animated: true, completion:  nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        self.present(VKCaptchaViewController(), animated: true, completion: nil)
    }
    
    func vkSdkDidDismiss(_ controller: UIViewController!) {
        print("did dismiss")
    }
    
    func vkSdkWillDismiss(_ controller: UIViewController!) {
        print("will dismiss")
    }
    
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if let token = result.token {
            print("get token = \(token)")
        } else {
            print("auth error: \(result.error)")
        }
    }
    
    func handleToken(token: String?) {
        guard let newToken = token else { ErrorPopupManager.showError(type: .unknown, from: self); return }
        Server.makeRequest.vk_auth(token: newToken, completion: { (success, registered) in
            if success {
                async {
                    if registered {
                        UIApplication.goMain()
                        BeaconManager.instance.startSearchingBeacons()
                        WAPManager.instance.startScanning()
                        ChattingService.shared.getChats { }

                    } else {
                        UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "RegViewController")
                    }
                }
            } else {
                ErrorPopupManager.showError(type: .unknown, from: self)
            }
        })
    }
    
    func vkSdkUserAuthorizationFailed() {
        ErrorPopupManager.showError(type: .unknown, from: self)

    }
    
    func vkSdkTokenHasExpired(_ expiredToken: VKAccessToken!) {
    }
    
    func vkSdkAuthorizationStateUpdated(with result: VKAuthorizationResult!) {
    }
    
    
    func vkSdkAccessTokenUpdated(_ newToken: VKAccessToken!, oldToken: VKAccessToken!) {
        if newToken != nil && last_vk_token_string != newToken.accessToken {
            last_vk_token_string = newToken.accessToken
            self.handleToken(token: newToken.accessToken)
        }
    }
}

@IBDesignable extension UITextField {
    @IBInspectable var placeHolderColor: UIColor {
        get {
            return self.placeHolderColor
        } set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSForegroundColorAttributeName : newValue])
        }
    }
}
