//
//  InterestCollectionViewCell.swift
//  Heloo2017
//
//  Created by Константин on 13.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class InterestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var title: UILabel!
    
    private var onBubbleImgView: UIImageView!
    
    func update(with int: TreeItem?, isSelected: Bool) {
        
        UIView.performWithoutAnimation {
            self.title.text = int?.title ?? ""
            self.title.font = int?.parent == nil ? UIFont(name: "AvenirNext-DemiBold", size: 14) : UIFont(name: "AvenirNext-Regular", size: 14)
            self.title.textColor = isSelected == true ? .white : .black
            self.bubbleView.backgroundColor = isSelected == true ? int?.parent == nil ? UIColor(hex: "FFFFFF") : UIColor(hex: "4FD5E6") : .white
            self.bubbleView.borderColor = int?.parent == nil ? UIColor(hex: "D9D9D9") : UIColor(hex: "5BA7E5")
            self.bubbleView.borderWidth = isSelected == true ? 0 : 1
            self.bubbleView.cornerRadius = 18
            self.title.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)
            
            let needsBackground = isSelected && int?.parent == nil
            setBackgroundImage(needsBackground ? #imageLiteral(resourceName: "gradient_blue") : nil)
            
        }
    
    }
    
    func setBackgroundImage(_ image: UIImage?) {
        
        title?.font = UIFont.systemFont(ofSize: 14, weight: image == nil ? UIFontWeightRegular : UIFontWeightMedium)
        onBubbleImgView?.removeFromSuperview()
        
        onBubbleImgView = UIImageView(frame: self.bubbleView.bounds)
        onBubbleImgView.contentMode = .scaleToFill
        onBubbleImgView.image = image
        onBubbleImgView.cornerRadius = self.bubbleView.cornerRadius
        onBubbleImgView.clipsToBounds = true
        self.bubbleView.addSubview(onBubbleImgView)
        self.bubbleView.sendSubview(toBack: onBubbleImgView)
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        async {
            self.onBubbleImgView.frame = self.bubbleView.bounds
        }
    }
    
    func pop() {
        UIView.animate(withDuration: 0.1, animations: {
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }) { (_) in
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                self.transform = .identity
            }, completion: nil)
        }
    }
}
