//
//  PlaceFilter.swift
//  Heloo2017
//
//  Created by Константин on 09.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


typealias personFilterObject = (selectedCity: PickableThing?, fromAge: Int?, toAge: Int?, selectedInterests: [PickableThing], gender: PersonFilter.genders?, text: String?)


func ==(lhs: personFilterObject?, rhs: personFilterObject?) -> Bool {
    guard lhs?.text == rhs?.text else { return false }
    guard lhs?.gender == rhs?.gender else { return false }
    guard lhs?.selectedInterests.map({$0.tag}).sorted() == rhs?.selectedInterests.map({$0.tag}).sorted() else { return false }
    guard lhs?.selectedCity?.tag == rhs?.selectedCity?.tag else { return false }
    return lhs?.fromAge == rhs?.fromAge && lhs?.toAge == rhs?.toAge
}

func !=(lhs: personFilterObject?, rhs: personFilterObject?) -> Bool {
    return !(lhs == rhs)
}




protocol PersonFilterDelegate: class {
    func filtersChanged(filter: personFilterObject)
}


class PersonFilter: UIViewController {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var interestsLabel: UILabel!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchButton: ShadowedButton!
    @IBOutlet weak var slider: RangeSlider! {
        didSet {
            slider.minValue = 18
            slider.maxValue = 50
            
            slider.leftThumbValue = self.fromAge == nil ? slider.minValue : CGFloat(fromAge!)
            slider.rightThumbValue = self.toAge == nil ? slider.maxValue : CGFloat(toAge!)
        }
    }
    
    @IBOutlet var genderButtons: [UIButton] = []
    
    weak var delegate: PersonFilterDelegate!
    
    func restore(with filter: personFilterObject?) {
        fromAge = filter?.fromAge
        toAge = filter?.toAge
        gender = filter?.gender ?? .undefined
        selectedInterests = UserDataManager.instance.availableInterests.makeFlat().filter({ interest in
            (filter?.selectedInterests ?? []).contains(where: { thing in
                thing.tag == interest.key
            })
        })
        selectedCity = filter?.selectedCity ?? PersonFilter.myCityItem
        searchText = filter?.text
    }
    
    var searchText: String? = nil

    
    var fromAge: Int? = nil {
        didSet {
            applyAges()
        }
    }
    var toAge: Int? = nil {
        didSet {
            applyAges()
        }
    }
    
    func applyAges() {
        guard ageLabel != nil else { return }
        
        ageLabel.textColor = .black
        if fromAge == nil && toAge == nil {
            ageLabel.text = "Укажите возраст"
            ageLabel.textColor = placeholderColor
        } else if fromAge != nil && toAge != nil {
            ageLabel.text = "от \(fromAge!) до \(toAge!)"
        } else if fromAge != nil {
            ageLabel.text = "от \(fromAge!)"
        } else if toAge != nil {
            ageLabel.text = "до \(toAge!)"
        }
    }
    
    enum genders: Int {
        case undefined = 1
        case male = 2
        case female = 3
        
        var apiValue: String {
            get {
                switch self {
                case .undefined: return "unset"
                case .male: return "m"
                case .female: return "f"
                }
            }
        }
    }
    
    var gender: genders = .undefined {
        didSet {
            updateGender()
        }
    }
    
    func updateGender() {
        for button in genderButtons {
            let isActive = button.tag == gender.rawValue
            button.setBackgroundImage(isActive ? #imageLiteral(resourceName: "gradient_violet") : nil, for: .normal)
            button.setTitleColor(isActive ? .white : .black, for: .normal)
        }
    }
    
    let placeholderColor = UIColor(hex: "9E9DA9")
    
    func updateCity() {
        cityLabel?.text = selectedCity?.title ?? "Выберите город из списка"
        cityLabel?.textColor = selectedCity == nil ? placeholderColor : .black
    }
    
    func updateInterests() {
        let activeInterests = selectedInterests.filter({$0.isSelected})
        interestsLabel?.text = activeInterests.count != 0 ? activeInterests.map({$0.title}).joined(separator: ", ") : "Выберите интересы"
        interestsLabel?.textColor = activeInterests.count == 0 ? placeholderColor : .black
    }
    
    @IBAction func genderTapped(_ sender: UIButton) {
        gender = genders(rawValue: sender.tag) ?? .undefined
    }
    
    private class var myCityItem: PickableThing? {
        get {
            if let city = UserDataManager.instance.currentUser?.city, let city_tag = UserDataManager.instance.currentUser?.city_tag {
                if !city.isEmpty && !city_tag.isEmpty {
                    return PickableThing(title: city, tag: city_tag)
                }
            }
            return nil
        }
    }
    
    
    var selectedCity: PickableThing? = myCityItem {
        didSet {
         updateCity()
        }
    }
   
    @IBAction func searchTapped(_ sender: UIButton) {
        searchText = searchField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        delegate?.filtersChanged(filter: personFilterObject((selectedCity: selectedCity, fromAge: fromAge, toAge: toAge, selectedInterests: selectedInterests.map({PickableThing(title: $0.title, tag: $0.key)}), gender: gender, text: searchText)))
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearTapped(_ sender: UIButton) {
        searchField.text = nil
        fromAge = nil
        toAge = nil
        slider.leftThumbValue = slider.minValue
        slider.rightThumbValue = slider.maxValue
        selectedCity = nil
        gender = .undefined
        selectedInterests = []
    }
    
    var selectedInterests: [TreeItem] = [] {
        didSet {
            updateInterests()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateCity()
        updateGender()
        updateInterests()
        applyAges()
        searchField?.text = searchText
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? FilterPickerViewController {
            dest.selectedItemTags = [self.selectedCity?.tag].compactMap({$0})
            dest.placeholderText = "Города не найдены"
            dest.maxSelectionCount = 1
            dest.onSelected = { city in
                self.selectedCity = city.first == nil ? nil : PickableThing(title: city.first!.title, tag: city.first!.tag)
            }
            Server.makeRequest.getCitiesList { (cities) in
                dest.setItems(cities.map({PickableThing(title: $0.title, tag: $0.tag)}))
            }
        } else if let dest = segue.destination as? FilterRangeViewController {
            dest.leftValue = self.fromAge ?? dest.leftValue
            dest.rightValue = self.toAge ?? dest.rightValue
            dest.minValue = 18
            dest.maxValue = 50
            dest.update(updateSlider: true)
            dest.onSelected = { ages in
                self.fromAge = ages.0
                self.toAge = ages.1
            }
        } else if let dest = segue.destination as? FilterTreeViewController {
            dest.setItems(self.selectedInterests, mainSource: UserDataManager.instance.availableInterests)
            dest.onPicked = { pickedInterests in
                self.selectedInterests = pickedInterests
            }
        }
    }
    
    @IBAction func sliderChanged(_ sender: RangeSlider) {
        fromAge = Int(sender.leftThumbValue)
        toAge = Int(sender.rightThumbValue)
    }
    
}

extension PersonFilter: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchText = textField.text
        return true
    }
}
