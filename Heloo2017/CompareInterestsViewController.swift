//
//  CompareInterestsViewController.swift
//  Heloo2017
//
//  Created by Константин on 01.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class CompareInterestsViewController: CardViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    class func createOne(with interests: [TreeItem]) -> CompareInterestsViewController {
        let new = UIStoryboard(name: "InterestsBase", bundle: nil).instantiateViewController(withIdentifier: "CompareInterestsViewController") as! CompareInterestsViewController
        new.modalPresentationStyle = .overCurrentContext
        new.modalTransitionStyle = .coverVertical
        new.otherInterests = interests
        return new
    }
    
    fileprivate var otherInterests: [TreeItem] = []
    
    @IBOutlet weak var scrollVisibleConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollHiddenConstraint: NSLayoutConstraint!
    @IBOutlet weak var blue_blur: UIImageView!
    @IBOutlet weak var interests_icon_big: UIImageView!
    @IBOutlet weak var cardViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var collectionItems: UICollectionView!
    @IBOutlet weak var interests_icon: UIImageView!
    @IBOutlet weak var level_label: UILabel!
    @IBOutlet weak var background_card: UIView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var comments_label: UILabel!
    @IBOutlet weak var user_icon: UIImageView!
    @IBOutlet weak var card_main_top_space: NSLayoutConstraint!
    
    let card_main_top_space_normal: CGFloat = 50
    
    
    var bigSL: CAShapeLayer = CAShapeLayer()
    var smallSL: CAShapeLayer = CAShapeLayer()
    
    
    lazy var progress: CGFloat = {
        var intersect: CGFloat = 0
        let userInterests = UserDataManager.instance.currentUser?.interests ?? []
        guard userInterests.count != 0 && self.viewer.currentInterests().count != 0 else { return 0 }
        for interest in self.viewer.currentInterests() {
            if userInterests.contains(where: {$0.key == interest.key}) {
                intersect += 1
            }
        }

        return intersect*(1/(2*CGFloat(userInterests.count)) + 1/(2*CGFloat(self.viewer.currentInterests().count)))
    }()
    
    
    var currentColor: UIColor {
        get {
            if self.progress < 0.33 {
                return redColor
            } else if self.progress < 0.66 {
                return yellowColor
            } else {
                return greenColor
            }
        }
    }
    
    
    func animateAppearance() {
        async {
            UIView.performWithoutAnimation {
                self.collectionItems.reloadData()
            }
            
            UIView.animate(withDuration: 0.3) {
                self.user_icon.alpha = 1
                self.blue_blur.alpha = 0.8
            }
            
            self.scrollHiddenConstraint.isActive = false
            self.scrollVisibleConstraint.isActive = true

            UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                self.background_card.alpha = 0.75
            }, completion: nil)
            self.card_main_top_space.constant = self.card_main_top_space_normal
            self.animateChanges()
        }
    }
    
    private func animateChanges() {
        UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateAppearance()
    
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        let presenting = self.presentingViewController
        super.dismiss(animated: flag) {
            presenting?.setNeedsStatusBarAppearanceUpdate()
            completion?()

        }
    }
  
    var viewer: InterestsViewer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewer = InterestsViewer(comparingInterests: self.otherInterests, mainSource: UserDataManager.instance.availableInterests)
        
        self.interests_icon?.image = self.interests_icon!.image!.imageWithColor(self.currentColor)
        self.level_label.textColor = self.currentColor

        self.scroll = self.scrollView
        self.scrollView.delegate = self
        self.view.layoutIfNeeded()
        self.collectionItems.contentInset.bottom = 16
        if viewer.currentInterests().count == 0 || progress < 0.33 {
            self.interests_icon_big.image = #imageLiteral(resourceName: "interests_placeholder")
        }
        self.level_label.text = progress < 0.33 ? "Низкий" : progress < 0.66 ? "Средний" :  "Высокий"
        self.animatableViews = [comments_label: 1.0, close_button: 1.0]
        collectionItems.delegate = self
        collectionItems.dataSource = self
        self.scrollView.delegate = self
        

        
            if self.interests_icon != nil {
                self.bigSL = CAShapeLayer()
                self.smallSL = CAShapeLayer()
                
                let center = CGPoint(x: self.interests_icon!.layer.frame.midX, y: self.interests_icon!.layer.frame.midY)
                let bigP = UIBezierPath(arcCenter: center, radius: 28*1.15, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi/2, clockwise: false)
                let smallP = UIBezierPath(arcCenter: center, radius: 20*1.15, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi/2, clockwise: false)
                
                self.bigSL.path = bigP.cgPath
                self.bigSL.fillColor = UIColor.clear.cgColor
                self.bigSL.strokeColor = self.currentColor.cgColor
                self.bigSL.lineWidth = 4
                self.bigSL.lineCap = kCALineCapRound
                self.bigSL.strokeStart = 0
                self.bigSL.strokeEnd = 0.0
                
                self.smallSL.path = smallP.cgPath
                self.smallSL.fillColor = UIColor.clear.cgColor
                self.smallSL.strokeColor = self.currentColor.withAlphaComponent(0.33).cgColor
                self.smallSL.lineWidth = 3
                self.smallSL.lineCap = kCALineCapRound
                self.smallSL.strokeStart = 0
                self.smallSL.strokeEnd = 0.0
                
                
                self.cardView.layer.addSublayer(self.bigSL)
                self.cardView.layer.addSublayer(self.smallSL)
            }
        
        self.animateRings()
        
    }
    
    func animateRings() {
        async(after: 300) {
            if self.interests_icon != nil {
                let updatedProgress = self.progress == 0 ? 0.1 : self.progress
                
                let center = CGPoint(x: self.interests_icon!.layer.frame.midX, y: self.interests_icon!.layer.frame.midY)
                let bigP = UIBezierPath(arcCenter: center, radius: 28*1.15, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi/2, clockwise: false)
                let smallP = UIBezierPath(arcCenter: center, radius: 20*1.15, startAngle: CGFloat.pi*2.5, endAngle: CGFloat.pi/2, clockwise: false)
                
                self.bigSL.path = bigP.cgPath
                self.smallSL.path = smallP.cgPath
                
                
                self.bigSL.strokeColor = updatedProgress == 0 ? grayColor.withAlphaComponent(0.5).cgColor : self.currentColor.cgColor
                CATransaction.begin()
                
                CATransaction.setAnimationDuration(2.0)
                self.bigSL.strokeEnd = updatedProgress
                CATransaction.commit()
                
                CATransaction.begin()
                CATransaction.setAnimationDuration(1.5)
                self.smallSL.strokeEnd = updatedProgress
                CATransaction.commit()
            }
        }
    }
    
    
    @IBAction func close(_ sender: UIButton) {
        self.makeCoverVerticalDismiss()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewer.currentInterests().count
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentItem = viewer.item(at: indexPath.item)
        let isSelected = self.otherInterests.contains(where: {$0.key == currentItem?.key})
        let cell = collectionItems.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! InterestCollectionViewCell
        cell.update(with: currentItem, isSelected: isSelected)
        
        let isActive = UserDataManager.instance.currentUser?.interests.contains(where: { $0.key == currentItem?.key }) ?? false
        if isActive && isSelected {
            cell.title.textColor = .white
            cell.setBackgroundImage(currentItem?.parent == nil ? #imageLiteral(resourceName: "gradient_yellow") : nil)
            cell.bubbleView.backgroundColor = currentItem?.parent == nil ? .white : UIColor(hex: "FFC300")
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let current = viewer.item(at: indexPath.item) else { return .zero }
        let hasBackground =  current.parent == nil
        return CGSize(width: current.title.requiredWidth(14, font: UIFont.systemFont(ofSize: 14, weight: hasBackground ? UIFontWeightMedium : UIFontWeightRegular), height: 24 + 10) + 40, height: 36 + 10)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
}



