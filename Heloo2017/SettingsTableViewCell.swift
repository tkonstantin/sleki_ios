//
//  SettingsTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 15.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var user_icon: UIImageView?
    @IBOutlet weak var black_field: UITextField?
    @IBOutlet weak var toogle: UISwitch?
    @IBOutlet weak var gray_title: UILabel?
    @IBOutlet weak var black_title: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
