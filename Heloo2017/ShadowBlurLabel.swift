//
//  ShadowBlurLabel.swift
//  Heloo2017
//
//  Created by Константин on 17.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

@IBDesignable class ShadowBlurLabel: UILabel {
    
    @IBInspectable var shadowBlur: CGFloat = 0.0 {
        didSet {
            self.layer.shadowOpacity = 1
            self.layer.shadowRadius = shadowBlur
        }
    }
    
    
}
