//
//  RegViewController.swift
//  Heloo2017
//
//  Created by Константин on 25.06.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import QuickLook

typealias regData = (name: String?, lastName: String?, birthday: Date?, gender: User.genders?, city: String?, city_tag: String?, country: String?, country_tag: String?)


class RegViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SelectCoutry, SDatePickerDelegate {
    
    @IBOutlet var defaultConstraints: [NSLayoutConstraint]!
    @IBOutlet var typingConstraints: [NSLayoutConstraint]!

    
    @IBOutlet weak var nextButton: ShadowedButton!
    @IBOutlet weak var uploadLabel: UILabel!
    @IBOutlet weak var name_field: UITextField!
    @IBOutlet weak var lastName_field: UITextField!
    @IBOutlet weak var birthday_field: UITextField!
    @IBOutlet weak var gender_field: UITextField!
    @IBOutlet weak var city_field: UITextField!
    @IBOutlet weak var upload_button: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    var selectedCountry = ""
    var selectedCity = ""
    
    func itemIsSelected(with tag: String, title: String, isCountry: Bool) {
        if isCountry {
//            self.selectedCountry = tag
//            self.regData.country = title
//
//            Server.makeRequest.getCityList(countryID: self.selectedCountry, query: "", completionHadnler: { (error, cities) in
//                if error == nil {
//                    if let coutryAndCityVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "coutryAndCity") as? SelectCountryViewController {
//                        coutryAndCityVC.modalPresentationStyle = .overCurrentContext
//                        self.present(coutryAndCityVC, animated: true, completion: nil)
//                        coutryAndCityVC.isCoutry = false
//                        coutryAndCityVC.array = cities
//                        coutryAndCityVC.delegate = self
//                        coutryAndCityVC.countryID = self.selectedCountry
//
//                    }
//                }
//            })
        } else {
            self.city_field.text = title
            self.selectedCity = tag
            self.regData.city_tag = tag
            self.regData.city = title
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }

    var regData: regData = (name: "", lastName: "", birthday: Date(), gender: nil, city: "", city_tag: "", country: "", country_tag: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        [name_field, lastName_field, birthday_field, gender_field, city_field].forEach({$0?.delegate = self})
        self.addTapOutsideGestureRecognizer()
        fillFieldsIfNeeded()
        self.subscribeToKeyboard()
    }
    
    override func keyboardWillBeShown(_ notification: Notification) {
        activateTypingConstraints()
    }
    
    override func keyboardWillBeHidden() {
        activateDefaultConstraints()
    }
    
    func fillFieldsIfNeeded() {
        guard let user = UserDataManager.instance.currentUser else { return }
        name_field.text = user.firstName
        lastName_field.text = user.lastName
        birthday_field.text = user.date_of_birth?.parseDateShort()
        gender_field.text = user.gender?.title ?? "Выберите пол"
        city_field.text = user.city
        if let img = URL(string: user.image_small) {
            imgView.sd_setImage(with: img)
        } else if let img = URL(string: user.image) {
            imgView.sd_setImage(with: img)
        }
        regData.name = user.firstName
        regData.lastName = user.lastName
        regData.birthday = user.date_of_birth
        regData.gender = user.gender
        regData.city = user.city
        regData.country = user.country
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    let keyboardAnimationDuration: Double = 0.25
    
    func activateDefaultConstraints() {
        defaultConstraints.forEach({$0.isActive = true})
        typingConstraints.forEach({$0.isActive = false})
        
        UIView.animate(withDuration: keyboardAnimationDuration*2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            self.upload_button.alpha = 1
            self.uploadLabel.alpha = 1
            self.imgView.alpha = 1
        })

        layoutAnimate()
    }
    
    func activateTypingConstraints() {
        defaultConstraints.forEach({$0.isActive = false})
        typingConstraints.forEach({$0.isActive = true})
        
        UIView.animate(withDuration: keyboardAnimationDuration*2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            self.upload_button.alpha = 0
            self.uploadLabel.alpha = 0
            self.imgView.alpha = 0
        })

        layoutAnimate()
    }
    
    func layoutAnimate() {
        UIView.animate(withDuration: keyboardAnimationDuration*2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    
    
    @IBAction func upload(_ sender: UIButton) {
        var picker: UIImagePickerController! = UIImagePickerController()
        picker.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Альбомы", style: .default, handler: { (_) in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Моменты", style: .default, handler: { (_) in
            picker.sourceType = .savedPhotosAlbum
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (_) in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: { (_) in
            picker = nil
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: {
            AvatarCropperViewController.present(with: info[UIImagePickerControllerOriginalImage] as? UIImage, from: self)
        })
    }
    
    func uploadImages(_ smallImage: UIImage, image: UIImage) {
        self.nextButton.isEnabled = false
        Server.makeRequest.fileUpload(file: smallImage, target: .profile_small) { (link, errorMessage) in
            self.nextButton.isEnabled = true
            guard link != nil else { return }
            let user = UserDataManager.instance.currentUser
            user?.image_small = link!
            UserDataManager.instance.currentUser = user
            Server.makeRequest.fileUpload(file: image, target: .profile, completion: { (link, errorMessage) in
                guard link != nil else { return }
                let user = UserDataManager.instance.currentUser
                user?.image_small = link!
                UserDataManager.instance.currentUser = user
            })
        }
    }
    
    func showDatePicker() {
        let picker = SDatePicker.make(self, currentDate: self.regData.birthday)
        self.present(picker, animated: false)
    }
    
    func sdatePicker_cancelled() { }
    
    func sdatePicker_selected(_ date: Date) {
        self.regData.birthday = date
        self.birthday_field.text = self.regData.birthday?.parseDateShort() ?? "Дата рождения"
    }
    
    func showGenderPicker() {
        let alert = UIAlertController(title: "Выберите пол", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: User.genders.male.title, style: .default, handler: { (_) in
            self.regData.gender = .male
            self.gender_field.text = User.genders.male.title
        }))
        alert.addAction(UIAlertAction(title: User.genders.female.title, style: .default, handler: { (_) in
            self.regData.gender = .female
            self.gender_field.text = User.genders.female.title
        }))
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showCityPicker() {
        self.hideKeyboard()
        Server.makeRequest.getCitiesList { (result) in
            if let coutryAndCityVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "coutryAndCity") as? SelectCountryViewController {
                coutryAndCityVC.modalPresentationStyle = .overCurrentContext
                self.present(coutryAndCityVC, animated: true, completion: nil)
                coutryAndCityVC.isCoutry = false
                coutryAndCityVC.array = result
                coutryAndCityVC.delegate = self
            }

        }
//        Server.makeRequest.getCountryList(completionHadnler: { (error, countries) in
//            if error == nil {
//                if let coutryAndCityVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "coutryAndCity") as? SelectCountryViewController {
//                    coutryAndCityVC.modalPresentationStyle = .overCurrentContext
//                    self.present(coutryAndCityVC, animated: true, completion: nil)
//                    coutryAndCityVC.isCoutry = true
//                    coutryAndCityVC.array = countries
//                    coutryAndCityVC.delegate = self
//                }
//
//            } else {
//                Presenter.instance.showErrorAlert(nil, message: "Возможно, отсутствует связь. Проверьте соединение")
//            }
//        })
    }
    
    @IBAction func next(_ sender: UIButton) {
        self.hideKeyboard()
        
        if (self.regData.name?.isEmpty ?? true) || /*(self.regData.lastName?.isEmpty ?? true) ||*/ (self.regData.city?.isEmpty ?? true) || self.regData.birthday == nil || self.regData.gender == nil {
            ErrorPopupManager.showError(type: .fields, from: self)
        } else {
            showHUD()
                Server.makeRequest.updateMyProfile(data: self.regData, completion: {
                    dismissHUD()
                    self.showInterests()
                })
        }
    }
    
    private func showInterests() {
        let interestsVC = InterestsEditorViewController.makeOne()
        interestsVC.isPartOfReg = true
        self.present(interestsVC, animated: true, completion: nil)
    }
    
    
    var previewItem: PreviewItem!
    
    @IBAction func showTerms(_ sender: UIButton) {
        previewItem = PreviewItem()
        
        guard let targetURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Пользовательское соглашение.pdf") else { return }
        
        if let pdfPath = Bundle.main.path(forResource: "sleki_terms", ofType: "pdf"), let pdfData = FileManager.default.contents(atPath: pdfPath) {
            _ = try? FileManager.default.createFile(atPath: targetURL.path, contents: pdfData, attributes: nil)
        }
        previewItem.previewItemURL = targetURL
        
        let ql = QLPreviewController()
        ql.dataSource = self
        ql.delegate = self
        self.present(ql, animated: true, completion: nil)

    }
    
    
}

class PreviewItem: NSObject, QLPreviewItem {
    var previewItemURL: URL?
}


extension RegViewController: QLPreviewControllerDataSource, QLPreviewControllerDelegate {
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return previewItem
    }
    
    func previewControllerDidDismiss(_ controller: QLPreviewController) {
        if let tempURL = previewItem?.previewItemURL {
            try? FileManager.default.removeItem(at: tempURL)
        }
    }
}

extension RegViewController: AvatarCropperDelegate {
    func cropperDidPick(image: UIImage, smallImage: UIImage) {
        self.imgView.image = smallImage
        self.uploadLabel.text = "Загрузить новое фото"
        self.uploadImages(smallImage, image: image)
        

    }
    
    func cropperDidCancel() {
        self.imgView.image = nil
    }
}

extension RegViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == birthday_field {
            self.showDatePicker()
            self.hideKeyboard()
            return false
        } else if textField == gender_field {
            self.showGenderPicker()
            self.hideKeyboard()
            return false
        } else if textField == city_field {
            self.showCityPicker()
            self.hideKeyboard()
            return false
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let next = self.view.viewWithTag(textField.tag + 60) as? UITextField {
            next.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == name_field {
            self.regData.name = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        } else if textField == lastName_field {
            self.regData.lastName = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string != " " else { return true }
        
        var newString = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        newString = newString.filterAllowedNameCharacters()
        textField.text = newString
        return false
    }
}


