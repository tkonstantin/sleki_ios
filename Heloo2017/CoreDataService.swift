//
//  CoreDataService.swift
//  UPS
//
//  Created by Константин Черкашин on 18.10.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation
import CoreData



class CoreDataService: NSObject {
    class var instance: CoreDataService {
        struct Singleton {
            static let instance = CoreDataService()
        }
        return Singleton.instance
    }
    
    let coordinator: NSPersistentStoreCoordinator
    var model: NSManagedObjectModel
    var privateContext: NSManagedObjectContext
    
    fileprivate override init() {
        let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd")!
        model = NSManagedObjectModel(contentsOf: modelURL)!
        
        let fileManager = FileManager.default
        let docsURL = fileManager.urls( for: .documentDirectory, in: .userDomainMask).last as URL!
        let storeURL = docsURL?.appendingPathComponent("model.sqlite")
        
        coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        let store = try? coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        if store == nil {
            if storeURL != nil {
                try? fileManager.removeItem(at: storeURL!)
            }
            let newStore = try? coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            if newStore == nil {
                abort()
            }
        }
        
        try? fileManager.setAttributes([FileAttributeKey.protectionKey : FileProtectionType.completeUntilFirstUserAuthentication], ofItemAtPath: storeURL!.path)
        
        privateContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        privateContext.persistentStoreCoordinator = coordinator
        super.init()
        
    }
    
    func lock(_ block:  @escaping () -> ()) {
        self.privateContext.performAndWait(block)
    }
    
    func save() {
        self.lock({
            try? self.privateContext.save()
        })
        NotificationCenter.default.post(name: .chatsChanged, object: nil)
    }
    
    func clearBeacons(with dictsArr: [NSDictionary]?=nil) {
        if let dicts = dictsArr {
            var ids: Set<String> = []
            for dict in dicts {
                if let id = dict[Beacon.beaconKeys.uuid] as? String {
                    ids.insert(id)
                }
            }
            for prod in self.allBeacons().filter({!ids.contains($0.uuid)}) {
                self.privateContext.delete(prod)
                self.save()
            }
        } else {
            for lmt in self.allBeacons() {
                self.privateContext.delete(lmt)
                self.save()
            }
        }
    }
    
    
    func allBeacons() -> [Beacon] {
        var returns: [Beacon] = []
        self.lock({
            if let results = try? self.privateContext.fetch(NSFetchRequest(entityName: "Beacon")) as? [Beacon] {
                returns = results ?? []
            }
        })
        return returns
    }
    
    
    func clearWaps(with dictsArr: [NSDictionary]?=nil) {
        if let dicts = dictsArr {
            var ids: Set<String> = []
            for dict in dicts {
                if let id = dict[WAP.wapKeys.uuid] as? String {
                    ids.insert(id)
                }
            }
            for prod in self.allWaps().filter({!ids.contains($0.uuid)}) {
                self.privateContext.delete(prod)
                self.save()
            }
        } else {
            for lmt in self.allWaps() {
                self.privateContext.delete(lmt)
                self.save()
            }
        }
    }
    
    
    func allWaps() -> [WAP] {
        var returns: [WAP] = []
        self.lock({
            if let results = try? self.privateContext.fetch(NSFetchRequest(entityName: "WAP")) as? [WAP] {
                returns = results ?? []
            }
        })
        return returns
    }
    
    func eraseChatsAndMessages() {
        let messageRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        _ = try? privateContext.execute(NSBatchDeleteRequest(fetchRequest: messageRequest))
        
        let chatsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Chat")
        _ = try? privateContext.execute(NSBatchDeleteRequest(fetchRequest: chatsRequest))
        
        save()
        
//        for chat in CoreDataManager.shared.currentChatsList() {
//            if chat.messages.count == 0 {
//                privateContext.delete(chat)
//            } else {
//                for index in 0..<chat.messages.count {
//                    privateContext.delete(chat.messages_objects.first!)
//                    if index == chat.messages.count - 1 {
//                        privateContext.delete(chat)
//                    }
//                }
//            }
//        }
//        save()
    }
    
    
}
