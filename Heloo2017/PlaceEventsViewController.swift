//
//  PlaceEventsViewController.swift
//  Heloo2017
//
//  Created by Константин on 01.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Canvas


extension UITableView {
    func registerAllCells() {
        self.register(UINib(nibName: post_types.event_created.stringValue, bundle: nil), forCellReuseIdentifier: post_types.event_created.stringValue)
        self.register(UINib(nibName: post_types.action_created.stringValue, bundle: nil), forCellReuseIdentifier: post_types.action_created.stringValue)
        self.register(UINib(nibName: post_types.checkin_created.stringValue, bundle: nil), forCellReuseIdentifier: post_types.checkin_created.stringValue)
        self.register(UINib(nibName: post_types.user_will_go.stringValue, bundle: nil), forCellReuseIdentifier: post_types.user_will_go.stringValue)
        self.register(UINib(nibName: post_types.placeholder.stringValue, bundle: nil), forCellReuseIdentifier: post_types.placeholder.stringValue)
        self.register(UINib(nibName: post_types.place_Favorite.stringValue, bundle: nil), forCellReuseIdentifier: post_types.place_Favorite.stringValue)
        self.estimatedRowHeight = 370
        self.rowHeight = UITableViewAutomaticDimension
    }
}

class PlaceEventsViewController: CardViewController {
    
    @IBOutlet weak var background_card: UIView!
    @IBOutlet weak var true_blur: UIVisualEffectView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var comments_label: UILabel!
    @IBOutlet weak var blue_blur: UIImageView!
    @IBOutlet weak var user_icon: UIImageView!
    @IBOutlet weak var tableItemsTop: NSLayoutConstraint!
    
    override var canResetFooter: Bool {
        get {
            return false
        }
    }
    
    var expandedIndexPaths: [IndexPath] = []
    let normalTableItemsTop: CGFloat = 50
    
    var place: Place!
    
    enum modes {
        case events
        case actions
        case checkins
        case placeInfo
        
        var title: String {
            get {
                switch self {
                case .events: return "События".uppercased()
                case .actions: return "Акции".uppercased()
                case .checkins: return "Чекины".uppercased()
                case .placeInfo: return "Информация".uppercased()

                }
            }
        }
    }
    
    var _mode: modes = .events
    
    var mode: modes {
        get {
            return _mode
        } set {
            _mode = newValue
        }
    }
    
    var posts: [Post] = []
    
    @IBOutlet weak var tableItems: RefreshableTable!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        //        animateAppearance()
        
        for cell in self.tableItems.visibleCells {
            if let postCell = cell as? PostCell {
                postCell.hideMenu()
            }
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        animateAppearance()
    }
    
    var wasAnimatedAppearance = false
    var isAnimatingAppearance = true
    
    func animateAppearance() {
        guard !wasAnimatedAppearance else { return }
        wasAnimatedAppearance = true
        async {
            UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: { [weak self] in
                self?.tableItemsTop.constant = self!.normalTableItemsTop
                self?.view.setNeedsLayout()
                self?.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                self?.isAnimatingAppearance = false
            })
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table = tableItems
        
        self.tableItems.delegate = self
        self.tableItems.dataSource = self
        self.loaded = false
        
        tableItems.addPullToRefresh() { [weak self] (refreshControl) in
            self?.currentPage = -1
            self?.hasNext = true
            self?.loadPosts { [weak refreshControl] in
                refreshControl?.endRefreshing()
            }
        }
        if mode != .placeInfo {
            table?.registerAllCells()
        }
        if let url = URL(string: place?.image_large ?? "") {
            self.user_icon.sd_setImage(with: url, placeholderImage: UIImage(named: "blue_blur_rect"))
        } else {
            self.user_icon.image = UIImage(named: "blue_blur_rect")
        }
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.user_icon.alpha = 1
            self?.blue_blur.alpha = 0.8
        }
        
        
        self.comments_label.text = self.mode.title
        
        
        
        self.loadPosts()
    }
    
    
    var loaded = false {
        didSet {
            if loaded {
                dismissHUD()
            } else {
                showHUD()
            }
        }
    }
    
    var currentPage: Int = -1
    var hasNext: Bool = true
    var isLoadingNextPage = false
    
    func loadPosts(_ completion: (()->Void)?=nil) {
        guard hasNext, !isLoadingNextPage else { return }
        currentPage += 1
        isLoadingNextPage = true
        if mode == .actions {
            Server.makeRequest.getOffers(page: currentPage,  place: place, completion: { [weak self] (offers, hasNextPages) in
                async {
                    if self?.currentPage == 0 {
                        self?.posts = []
                    }
                    let newOffers = offers.filter({ item in
                        return self?.posts.contains(where: {$0.searchable_item?.uuid == item.id}) == false
                    })
                    newOffers.forEach({self?.posts.append(Post(with: Searchable(offer: $0)!)!)})
                    self?.hasNext = hasNextPages
                    self?.loaded = true
                    self?.isLoadingNextPage = false
                    self?.tryToUpdateTable()
                    self?.animateAppearance()
                    //                self.configureTable()
                    completion?()
                }
                
            })
        } else if mode == .checkins {
            Server.makeRequest.getCheckins(page: currentPage, place: place, completion: { [weak self] (checkins, hasNextPages) in
                async {
                    if self?.currentPage == 0 {
                        self?.posts = []
                    }
                    self?.hasNext = hasNextPages
                    let newCheckins = checkins.filter({ item in
                        return self?.posts.contains(where: {$0.searchable_item?.uuid == item.id}) == false
                    })
                    newCheckins.forEach({self?.posts.append(Post(with: Searchable(checkin: $0)!)!)})
                    self?.loaded = true
                    self?.isLoadingNextPage = false
                    
                    self?.tryToUpdateTable()
                    self?.animateAppearance()
                    //                self.configureTable()
                    completion?()
                }
                
                
            })
        } else {
            Server.makeRequest.getEvents(page: currentPage, place: place, completion: { [weak self] (events, hasNextPages) in
                async {
                    if self?.currentPage == 0 {
                        self?.posts = []
                    }
                    self?.hasNext = hasNextPages
                    let newEvents = events.filter({ item in
                        return self?.posts.contains(where: {$0.searchable_item?.uuid == item.id}) == false
                    })
                    newEvents.forEach({self?.posts.append(Post(with: Searchable(event: $0)!)!)})
                    self?.loaded = true
                    self?.isLoadingNextPage = false
                    
                    self?.tryToUpdateTable()
                    self?.animateAppearance()
                    //                self.configureTable()
                    completion?()
                }
            })
        }
    }
    
    func tryToUpdateTable() {
        guard !self.tableItems.isDragging && !self.tableItems.isTracking && !self.tableItems.isDecelerating else {
            async(after: 300, { [weak self] in
                self?.tryToUpdateTable();
            })
            return
        }
        async { [weak self] in
            self?.tableItems.reloadData()
            let footer = self?.tableItems.tableFooterView
            footer?.frame.size.height = self?.hasNext == true ? 128 : 44
            footer?.alpha = self?.hasNext == true ? 1 : 0
            self?.tableItems.tableFooterView = footer
        }
    }
    
    
    
    @IBAction func close(_ sender: UIButton) {
        self.makeCoverVerticalDismiss()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result = posts.count == 0 && loaded ? 1 : posts.count
        
        return result
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if posts.count == 0 {
            let cell = tableItems.dequeueReusableCell(withIdentifier: post_types.placeholder.stringValue, for: indexPath)
            (cell.viewWithTag(1) as? UILabel)?.text = "Пока что \(self.mode == .events ? "событий" : self.mode == .actions ? "акций" : "чекинов") нет"
            cell.backgroundColor = .clear
            return cell
        }
        
        let currentPost = posts[indexPath.row]
        guard let cell = tableItems.dequeueReusableCell(withIdentifier: currentPost.type.stringValue) as? PostCell else { return UITableViewCell() }
        
        configure(postCell: cell, with: currentPost, commentsCountChanged: { [weak self] (newCount) in
            self?.posts[indexPath.row].comments_count = newCount
        })
        
        cell.setCanDelete(currentPost.canDelete) { [weak self] in
            Server.makeRequest.deletePost(currentPost) { [weak self] (success: Bool) in
                async {
                    (self?.tableItems.cellForRow(at: indexPath) as? PostCell)?.hideMenu()
                    if success {
                        self?.tableItems?.beginUpdates()
                        self?.posts.remove(at: indexPath.row)
                        self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                        if self?.posts.count == 0 {
                            self?.tableItems?.insertRows(at: [indexPath], with: .automatic)
                        }
                        self?.tableItems?.endUpdates()

                        async(after: 300, {
                            self?.tableItems?.reloadData()
                        })
                    }
                }
            }
        }
        
        cell.setCanComplain(currentPost.canComplain) { [weak self] in
            guard let checkin = currentPost.searchable_item?.checkin else { return }
            Server.makeRequest.complain(to: checkin, completion: {
                async {
                    self?.posts.remove(at: indexPath.row)
                    self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                }
            })
        }
        
        cell.likes_button?.addTarget(self, action: #selector(self.animate_like(_:)), for: .touchDown)
        cell.likes_button?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        cell.likes_button?.tag = indexPath.row
        cell.backgroundColor = .clear
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.posts.count > indexPath.row else { return }
        
        if let user = self.posts[indexPath.row].searchable_item?.checkin?.user {
            let profile = MyProfileViewController.makeOne(with: user, mode: UserDataManager.instance.isMe(user) ? .my : .other)
            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.pushViewController(profile, animated: true)
            
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= tableView.numberOfRows(inSection: 0) - 2 && tableView.numberOfRows(inSection: 0) != 0 {
            loadPosts()
        }
    }
    
    
    
    
    func animate_like(_ sender: UIButton) {
        let anim = (sender.superview as? CSAnimationView)
        anim?.duration = 0.3
        anim?.delay = 0
        anim?.type = CSAnimationTypePop
        anim?.startCanvasAnimation()
    }
    
    func like(_ sender: UIButton) {
        sender.isEnabled = false
        Server.makeRequest.like(post: posts[sender.tag]) {  [weak self] (success, liked, count) in
            sender.isEnabled = true
            self?.posts[sender.tag].isLiked = liked
            sender.setImage(liked ? #imageLiteral(resourceName: "liked_button") : #imageLiteral(resourceName: "myprofile_likes"), for: .normal)
            guard var likesCount = Int(sender.title(for: .normal) ?? "") else { return }
            likesCount = count
            self?.posts[sender.tag].likes_count = count
            sender.setTitle(String(likesCount), for: .normal)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableItems.visibleCells.compactMap({$0 as? PostCell}).forEach({$0.hideMenuWithScrollIntent()})
    }
    
    
    
}


extension UIViewController {
    
    func accept(_ sender: AcceptButton) {
        if !sender.isAccepted {
            sender.isAccepted = true
            sender.isEnabled = false
            
            Server.makeRequest.acceptEvent(uuid: sender.uuid, completion: {  [weak sender] (success) in
                sender?.isAccepted = success
                sender?.isEnabled = true
                
            })
        } else {
            sender.isAccepted = false
            sender.isEnabled = false
            
            Server.makeRequest.discardEvent(uuid: sender.uuid, completion: {  [weak sender] (success) in
                sender?.isAccepted = !success
                sender?.isEnabled = true
                
            })
        }
        
    }
    
    func offerFavorite(_ sender: OfferFavoriteButton) {
        if sender.isFavorite {
            sender.isFavorite = false
            sender.isEnabled = false
            
            Server.makeRequest.removeOfferFromFavorites(uuid: sender.uuid, completion: {  [weak sender]  (success) in
                sender?.isFavorite = !success
                sender?.isEnabled = true
                
            })
        } else {
            sender.isFavorite = true
            sender.isEnabled = false
            
            Server.makeRequest.addOfferToFavorites(uuid: sender.uuid, completion: {  [weak sender]  (success) in
                sender?.isFavorite = success
                sender?.isEnabled = true
                
            })
        }
    }
    
    func placeFavorite(_ sender: FavoriteButton) {
        if sender.isFavorite {
            sender.isFavorite = false
            sender.isEnabled = false
            
            Server.makeRequest.removePlaceFromFavorites(uuid: sender.uuid, completion: { [weak sender] (success) in
                sender?.isFavorite = !success
                sender?.isEnabled = true
                
            })
        } else {
            sender.isFavorite = true
            sender.isEnabled = false
            
            Server.makeRequest.addPlaceToFavorites(uuid: sender.uuid, completion: { [weak sender]  (success) in
                sender?.isFavorite = success
                sender?.isEnabled = true
                
            })
        }
    }
    
    func openUser(_ sender: UserButton) {
        sender.post.didSelectUser(from: self)
    }
    
    
    
    func openPostImage(_ sender: PostImageButton) {
        PhotoViewerViewController.present(with: [sender.post_image])
    }
    
    func expandPost(_ sender: ExpandingButton) {
        sender.post.isExpanded = true
        (self as? TableViewContainingViewController)?.table?.reloadData()
        
    }
    
    
    func configure(postCell cell: PostCell, with item: Post, commentsCountChanged: @escaping ((Int)->Void)) {
        
        cell.setCanEdit(item.canEdit)
        cell.weekdaysStack?.update(item: item.searchable_item?.event?.weekActivityItems ?? item.searchable_item?.offer?.weekActivityItems)
        cell.big_top_label?.text = item.big_top_label
        (cell.big_top_label as? UsernameLabel)?.user = item.searchable_item?.user
        cell.small_top_label?.text = item.small_top_label
        cell.post_text?.text = item.post_text
        
        cell.read_more_button_height?.constant = 0
        cell.post_text_height_max?.constant = 100
        cell.post_text_height_max?.isActive = !item.isExpanded
        
        
        if let textLabel = cell.post_text, !item.isExpanded {
            if item.post_text.requiredHeight(textLabel.font.pointSize, font: textLabel.font, width: self.view.bounds.width - 44, lineSpacing: nil) > 100 {
                cell.read_more_button?.post = item
                cell.read_more_button_height?.constant = 32
                cell.read_more_button?.addTarget(self, action: #selector(self.expandPost(_:)), for: .touchUpInside)
            }
        }
        
        
        cell.post_title?.text = item.post_title
        cell.date_label?.text = item.date
        cell.action_label?.attributedText = item.action_title
        
        cell.likes_button?.setTitle(String(item.likes_count), for: .normal)
        cell.comments_button?.setTitle(String(item.comments_count), for: .normal)
        
        cell.likes_button?.setImage(item.isLiked ? #imageLiteral(resourceName: "liked_button") : #imageLiteral(resourceName: "myprofile_likes"), for: .normal)
        cell.comments_button?.post = item
        cell.comments_button?.addTarget(self, action: #selector(self.showComments(_:)), for: .touchUpInside)
        cell.comments_button?.commentCompletionBlock = { (newCount) in
            commentsCountChanged(newCount)
        }
        cell.offer_favorite_button?.uuid = item.searchable_item?.offer?.id ?? ""
        cell.offer_favorite_button?.isFavorite = item.isFavorite
        cell.offer_favorite_button?.addTarget(self, action: #selector(self.offerFavorite(_:)), for: .touchUpInside)
        
        cell.favorites_button?.isFavorite = item.isFavorite
        cell.favorites_button?.uuid = item.searchable_item!.place!.uuid
        cell.favorites_button?.addTarget(self, action: #selector(self.placeFavorite(_:)), for: .touchUpInside)
        cell.post_image_button?.post_image = item.post_image?.url ?? ""
        cell.post_image_button?.addTarget(self, action: #selector(self.self.openPostImage(_:)), for: .touchUpInside)
        
        cell.accept_button?.uuid = item.searchable_item?.event?.id ?? ""
        
        cell.accept_button?.isAccepted = item.isAccepted
        cell.accept_button?.addTarget(self, action: #selector(self.accept(_:)), for: .touchUpInside)
        
        cell.openUserButton?.config(item)
        cell.openUserButton?.addTarget(self, action: #selector(self.openUser(_:)), for: .touchUpInside)
        
        if let url = URL(string: item.big_top_icon) {
            cell.big_top_icon?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "place_feed_placeholder"))
        } else {
            cell.big_top_icon?.image = #imageLiteral(resourceName: "place_feed_placeholder")
        }
        if let url = URL(string: item.small_top_icon) {
            cell.small_top_icon?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "user_feed_placeholder"))
        } else {
            cell.small_top_icon?.image = #imageLiteral(resourceName: "user_feed_placeholder")
        }
        
        if let url = URL(string: item.post_image?.url ?? "") {
            cell.post_image?.backgroundColor = UIColor.init(hex: "F1F1F1")
            cell.loadingLabel?.isHidden = false
            cell.post_image_height?.constant = item.imageHeight
            cell.post_image?.contentMode = .scaleAspectFill
            cell.post_image?.alpha = 1

            cell.imageBottomOffset?.constant = 18

            cell.post_image?.sd_setImageAnimated(with: url, completed: { [weak cell] (img, error,_,_) in
                    cell?.loadingLabel?.isHidden = true

                    if img == nil || error != nil {
                        cell?.post_image?.backgroundColor = UIColor.init(hex: "43A3F1")
                        if item.type == .place_Favorite {
                            cell?.post_image?.contentMode = .scaleAspectFit
                            cell?.post_image?.image = #imageLiteral(resourceName: "place_placeholder_huge")
                        } else {
                            cell?.post_image?.image = #imageLiteral(resourceName: "comments_photo_placeholder")
                        }
                    }
               
            })
        } else {
            cell.post_image_height?.constant = 0
            cell.post_image?.image = item.searchable_item?.checkin != nil ? nil : #imageLiteral(resourceName: "comments_photo_placeholder")
            cell.post_image?.alpha = 0
            cell.imageBottomOffset?.constant = 10
        }
        cell.date_label_view?.isHidden = true
        cell.noImg_date_label?.text = item.date
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()

    }
    

}


class UserButton: UIButton {
    var post: Post!
    
    func config(_ _post: Post) {
        post = _post
    }
}

class ExpandingButton: UIButton {
    var post: Post!
    

    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.tintColor = UIColor(hex: "00B5E7")
        self.setTitle("Читать далее", for: .normal)
    }

}


class PostImageButton: UIButton {
    var post_image: String = ""
}
