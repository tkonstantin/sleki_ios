//
//  MPCManagerr.swift
//  Heloo2017
//
//  Created by Константин on 04/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import MultipeerConnectivity


class MPCManager: NSObject {
    
    static let shared = MPCManager()
    var session: MCSession!
    var advertizer: MCNearbyServiceAdvertiser!
    var browser: MCNearbyServiceBrowser!
    var peerID: MCPeerID!
    let serviceType = "sleki"
    var isRunning = false
    
    var freshUsers: [User] {
        get {
            return locatedUsers.keys.filter({ user in
                return !self.shownUsers.contains(where: {$0.uuid == user.uuid})
            }).filter({
                if let date = self.shownedUsersHistory[$0.uuid] {
                    return !Calendar.current.isDateInToday(date)
                } else {
                    return true
                }
            })
        }
    }
    var locatedUsers = [User: Date]() {
        didSet {
            var simplified: [String: Date] = [:]
            locatedUsers.forEach({simplified[$0.key.uuid] = $0.value})
            locatedUserHistory = simplified
        }
    }
    
    var locatedUserHistory: [String: Date] {
        get {
            return UserDefaults.standard.value(forKey: "mpclocatedusers0") as? [String: Date] ?? [:]
        } set {
            UserDefaults.standard.set(newValue, forKey: "mpclocatedusers0")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    var shownedUsersHistory: [String: Date] {
        get {
            return UserDefaults.standard.value(forKey: "mpcshownusers0") as? [String: Date] ?? [:]
        } set {
            UserDefaults.standard.set(newValue, forKey: "mpcshownusers0")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    func wasLocatedToday(_ user: User) -> Bool {
        guard let date = locatedUserHistory.first(where: {$0.key == user.uuid})?.value else { return false }
        return Calendar.current.isDateInToday(date)
    }
    
    var shownUsers = [User]() {
        didSet {
            for user in shownUsers  {
                if !shownedUsersHistory.contains(where: {$0.key == user.uuid}) {
                    shownedUsersHistory[user.uuid] = Date()
                }
            }
        }
    }
    
    func send(message: String) {
        if let peer = session.connectedPeers.first {
            try? session.send( message.data(using: .utf8)!, toPeers: [peer], with: MCSessionSendDataMode.reliable)
        }
        
    }
    
    func startSearchingIfNeeded() {
        guard UserDataManager.isAuthorized, !isRunning, UserDataManager.instance.isVisibleOverMPC else { return }
        isRunning = true
        peerID = MCPeerID(displayName: UIDevice.current.name)
        session = MCSession(peer: peerID,  securityIdentity: nil, encryptionPreference: .none)
        session.delegate = self
        startAdvertising()
        startBrowsing()
    }
    
    func stop() {
        isRunning = false
        advertizer?.stopAdvertisingPeer()
    }
    
    fileprivate func startAdvertising() {
        advertizer = MCNearbyServiceAdvertiser(peer: peerID, discoveryInfo: UserDataManager.instance.currentUser!.mpcDict, serviceType: serviceType)
        advertizer.delegate = self
        advertizer.startAdvertisingPeer()
    }
    
    fileprivate func startBrowsing() {
        browser = MCNearbyServiceBrowser(peer: peerID, serviceType: serviceType)
        browser.delegate = self
        browser.startBrowsingForPeers()
    }
    
    
    
}
extension MPCManager: MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        if let newUser = User(mpcDict: info ?? [:]) {
            if !locatedUsers.keys.contains(where: {$0.uuid == newUser.uuid}) {
                if !wasLocatedToday(newUser) {
                    NotificationsManager.shared.showNotification(with: newUser, isOverMPC: true)
                }
                locatedUsers[newUser] = Date()

                NotificationCenter.default.post(name: .people_update_notification, object: nil)
                Server.makeRequest.getPerson(uuid: newUser.uuid) { (loadedUser) in
                    if loadedUser != nil {
                        self.locatedUsers[newUser] = nil
                        self.locatedUsers[loadedUser!] = Date()
                        NotificationCenter.default.post(name: .people_update_notification, object: nil)
                    }

                }
            } else {
                if let previous = locatedUsers.first(where: {$0.key.uuid == newUser.uuid})?.key {
                    locatedUsers[previous] = nil
                }
                locatedUsers[newUser] = Date()
            }
        }
        
//        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
//        browser.stopBrowsingForPeers()
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print(#function, peerID)
//        startBrowsing()
    }
}

extension MPCManager: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        print(#function, peerID, context)
        
        invitationHandler(true, session)
//        advertiser.stopAdvertisingPeer()
    }
    
}

extension MPCManager: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print(#function, peerID, state)
        
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        print(#function, peerID, data)
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "received", message: String(data: data, encoding: .utf8), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        print(#function, peerID, streamName, stream)
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        print(#function, peerID, resourceName, progress)
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        print(#function, peerID, resourceName, localURL, error)
        
    }
    
    func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
        print(#function, peerID, certificate)
        certificateHandler(true)
    }
    
}
