//
//  FavoriteButton.swift
//  Heloo2017
//
//  Created by Константин on 26.08.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

@IBDesignable
class FavoriteButton: ResizableButton {
    var uuid: String = ""
    @IBInspectable var isInPlaceProfile: Bool = false 
    
    @IBInspectable var isFavorite: Bool = false {
        didSet {
            if isInPlaceProfile {
                self.setTitle(nil, for: .normal)
                self.backgroundColor = .clear
                self.setImage(isFavorite ? #imageLiteral(resourceName: "offerpost_favorite_enabled") : #imageLiteral(resourceName: "offerpost_favorite_disabled"), for: .normal)
            } else {
                self.setTitle(nil, for: .normal)
                self.backgroundColor = isFavorite ? UIColor(hex: "FFB500") : UIColor(hex: "D2D2D2")
                self.setImage(isFavorite ? #imageLiteral(resourceName: "offerpost_favorite_enabled") : #imageLiteral(resourceName: "offerpost_favorite_disabled"), for: .normal)
                self.tintColor = .white
            }
        }
    }
    
}
