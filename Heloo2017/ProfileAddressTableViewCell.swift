//
//  ProfileAddressTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 21.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import MapKit

class ProfileAddressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var cardButton: UIButton!
    
    var lat: Double = 0
    var lon: Double = 0
    var pinTitle: String?
    
    func update(with place: Place) {
        adressLabel.text = place.address_string
        pinTitle = place.name
        lat = place.lat
        lon = place.lon
        cardButton.addTarget(self, action: #selector(self.openMap), for: .touchUpInside)
    }
    
    @objc private func openMap() {
        LocationViewerViewController.present(from: rootNav?.presentedViewController as? UINavigationController ?? rootNav, with: lat, longitude: lon, pinTitle: pinTitle)
    }
    
}
