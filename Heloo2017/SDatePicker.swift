//
//  SDatePicker.swift
//  Heloo2017
//
//  Created by Константин on 27/09/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


protocol SDatePickerDelegate: class {
    func sdatePicker_selected(_ date: Date)
    func sdatePicker_cancelled()
}

class SDatePicker: UIViewController {
    
    class func make(_ delegate: SDatePickerDelegate, currentDate: Date?) -> SDatePicker {
        let new = UIStoryboard(name: "SDatePicker", bundle: nil).instantiateInitialViewController() as! SDatePicker
        new.modalTransitionStyle = .crossDissolve
        new.modalPresentationStyle = .overCurrentContext
        new.delegate = delegate
        new.currentDate = currentDate
        return new
    }
    weak var delegate: SDatePickerDelegate!
    fileprivate var currentDate: Date?
    
    @IBOutlet weak var picker: UIDatePicker! {
        didSet {
            picker.maximumDate = Date().addingTimeInterval(-365*86400*14)
        }
    }
    @IBOutlet var hiddenConstraint: NSLayoutConstraint!
    
    
    @IBAction func doneTapped(_ sender: UIButton) {
        delegate?.sdatePicker_selected(picker.date)
        self.dismiss(animated: true)
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        delegate?.sdatePicker_cancelled()
        self.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGR()
        picker.date = currentDate ?? Date().addingTimeInterval(-365*86400*18)
        picker.setDate(currentDate ?? Date().addingTimeInterval(-365*86400*18), animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        async {
            UIView.animate(withDuration: 0.25, animations: {
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            })
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: [], animations: {
                self.hiddenConstraint?.isActive = false
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.hiddenConstraint?.isActive = true
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        })
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: [], animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: { _ in
            super.dismiss(animated: false, completion: completion)
        })
    }
    
    
    private func addTapGR() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc private func tapped() {
        self.cancelTapped(UIButton())
    }
}
