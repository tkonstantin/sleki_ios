//
//  PlaceProfileViewController.swift
//  Heloo2017
//
//  Created by Константин on 24.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SDWebImage
import Canvas

class PlaceProfileViewController: TableViewContainingViewController {
    
    class func makeOne(with place: Place, shouldOpenOffers: Bool = false) -> PlaceProfileViewController {
        let profileVC = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "PlaceProfileViewController") as! PlaceProfileViewController
        profileVC.place = place
        profileVC.shouldOpenOffers = shouldOpenOffers
        return profileVC
    }
    
    @IBOutlet weak var typeLabel: UILabel!
    var favorites_button: PlaceFavoriteNavigationButton!
    @IBOutlet weak var checkins_count: UILabel!
    @IBOutlet weak var checkings_count_dubbling: UILabel!
    @IBOutlet weak var events_count: UILabel!
    @IBOutlet weak var offers_count: UILabel!
    @IBOutlet weak var address_label: UILabel?
    @IBOutlet weak var place_name_label: UILabel!
    @IBOutlet weak var placeholder_image: UIImageView!
    @IBOutlet weak var topButton: UIButton?
    @IBOutlet weak var profileBoard: ShadowedView!
    @IBOutlet weak var arrowButton: UIButton!
    
    @IBOutlet weak var checkin_button: ResizableButton!
    @IBOutlet weak var info_button: ShadowedButton!
    
    @IBOutlet weak var user_icon: UIImageView!
    @IBOutlet weak var tableItems: RefreshableTable! {
        didSet {
            self.table = tableItems
            tableItems.registerAllCells()
        }
    }
    @IBOutlet weak var blue_blur: UIImageView!
    @IBOutlet var action_labels: [UILabel]!
    
    fileprivate var shouldOpenOffers = false
    
    var posts: [Post] = []
    
    
    @IBAction func arrowTapped(_ sender: UIButton) {
        if sender.transform == .identity {
            self.tableItems.setContentOffset(CGPoint(x: 0, y: self.profileBoard.bounds.height), animated: true)
        } else {
            self.tableItems.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
    

    @IBAction func addToFavorites(_ sender: PlaceFavoriteNavigationButton) {
        if place.isFavorite {
            Server.makeRequest.removePlaceFromFavorites(uuid: place.uuid, completion: { [weak self] (success) in
                if success {
                    sender.isFavorite = false
                    self?.place.isFavorite = false
                }
            })
        } else {
            Server.makeRequest.addPlaceToFavorites(uuid: place.uuid) { [weak self] (success) in
                if success {
                    sender.isFavorite = true
                    self?.place.isFavorite = true
                    
                }
            }
        }
    }

    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if shouldOpenOffers {
            shouldOpenOffers = false
            self.showAccessory(with: .actions)
        }
    }
    
    
    @IBAction func checkin(_ sender: UIButton) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            return
        }

        if let activePlace  = Server.makeRequest.activePlaces.first(where: {$0.uuid == place.uuid}), activePlace.geo_distance < 1/2 {
            let createCheckin = CreateCheckinViewController.createOne(with: place)
            createCheckin.checkinCompletionBlock = { [weak self] (newCount) in
                self?.place.checkins_count = newCount
                self?.fill()
            }
            self.present(createCheckin, animated: true, completion: nil)
        } else {
            ErrorPopupManager.showError(type: .unknown, from: self, subtitle: "Чтобы зачекиниться, Вы должны находиться рядом.\nВключите Bluetooth/Wi-Fi/GPS, если Вы находитесь неподалеку.", title: "Ошибка!")
        }
    }
    
    
    @IBAction func info_tapped(_ sender: Any) {
        self.showAccessory(with: .placeInfo)
    }
    
    
    
    var place: Place!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.makeClear()
        
        tableItems.addPullToRefresh { [weak self] (refreshControl) in
            self?.requestFullProfile {
                refreshControl?.endRefreshing()
            }
        }
        
        tableItems.delegate = self
        tableItems.dataSource = self
        let clear = UIView()
        clear.backgroundColor = .clear
        tableItems.tableFooterView = clear
        tableItems.estimatedRowHeight = 100
        tableItems.rowHeight = UITableViewAutomaticDimension
        
        fill()
        async {
            self.tableItems.tableHeaderView?.frame.size.height = self.tableItems.bounds.height + self.profileBoard.bounds.height - self.place_name_label.bounds.height - 10
            self.tableItems.reloadData()
        }
        
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.setLeftBarButton(nil, animated: false)
        
        favorites_button = PlaceFavoriteNavigationButton(type: .custom)
        favorites_button.isFavorite = Server.makeRequest.isLocallyFavorite(place: place)
        favorites_button.addTarget(self, action: #selector(self.addToFavorites(_:)), for: .touchUpInside)
        favorites_button.imageView?.contentMode =  .scaleAspectFit
        favorites_button.contentMode = .scaleAspectFit
        let barButton = UIBarButtonItem(customView: favorites_button)
        
        let moreButton = UIBarButtonItem(image: UIImage(named: "nav_more_button"), style: .plain, target: self, action: #selector(self.moreTapped))
        self.navigationItem.setRightBarButtonItems([moreButton, barButton], animated: false)
        
        
        arrowTapped(arrowButton)
    }
    
    @objc private func moreTapped() {
        let alert = StatusControllerAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.statusbarStyle = .lightContent
        alert.addAction(UIAlertAction(title: place.blocked ? "Разблокировать место": "Заблокировать место", style:  .destructive, handler: { (_) in
            Server.makeRequest.changeBlockState(for: self.place, completion: { [weak self] (blocked) in
                self?.place.blocked = blocked
            })
        }))
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fill()
        requestFullProfile()
        
        async {
            self.tableItems.setContentOffset(CGPoint(x: 0, y: self.profileBoard.bounds.height), animated: false)
        }
        
    }
    
    var curentCheckinsPageToLoad = 0
    var hasNext = true
    
    func requestFullProfile(_ completion: (()->Void)? = nil) {
        Server.makeRequest.getPlace(uuid: place.uuid) { [weak self] (place_full) in
            if place_full != nil {
                self?.place = place_full!

                self?.loadCheckins(page: 0, completion: {
                    async {
                        self?.fill()
                        completion?()
                        self?.tableItems?.reloadData()
                    }

                })
              
       
            }
        }
    }
    
    func loadCheckins(page: Int = -1, completion: @escaping ()->Void) {
        
        if page >= 0 {
            curentCheckinsPageToLoad = page
        }
        
        Server.makeRequest.getCheckins(page: curentCheckinsPageToLoad, place: place, completion: { (checkins, hasNext) in
            if self.curentCheckinsPageToLoad == 0 {
                self.posts = checkins.compactMap({Post(with: Searchable(checkin: $0))})
            } else {
                self.posts += checkins.compactMap({Post(with: Searchable(checkin: $0))})
            }
            self.curentCheckinsPageToLoad += 1
            self.hasNext = hasNext
            completion()
        })
    }
    
    
    func fill() {
        self.placeholder_image.isHidden = false
        if place.types.count > 3 {
            self.typeLabel?.text = [place.types[0], place.types[1], place.types[2]].map({$0.title}).joined(separator: " / ")
        } else {
            self.typeLabel?.text = place.types.map({$0.title}).joined(separator: " / ")
        }
        self.typeLabel?.isHidden = self.typeLabel.text?.isEmpty ?? true
        
        if let url = URL(string: place.image_large) {
            self.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_gray")
            self.view.backgroundColor = UIColor.init(hex: "F1F1F1")
            
            self.user_icon.sd_setImageAnimated(with: url, placeholderImage: nil, options: [], completed: { image,_,_,_ in
                self.view.backgroundColor = UIColor.init(hex: "43A3F1")
                self.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_huge")
                self.placeholder_image.isHidden = image != nil
            })
        } else if let url = URL(string: place.image_small) {
            self.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_gray")
            self.view.backgroundColor = UIColor.init(hex: "F1F1F1")
            
            self.user_icon.sd_setImageAnimated(with: url, placeholderImage: nil, options: [], completed: { image,_,_,_ in
                self.view.backgroundColor = UIColor.init(hex: "43A3F1")
                self.placeholder_image.image = #imageLiteral(resourceName: "place_placeholder_huge")
                self.placeholder_image.isHidden = image != nil
            })
        } else {
            self.user_icon.image = nil
            self.placeholder_image.isHidden = false
        }
        self.address_label?.text = place.short_address_string
        var name = place.name//.replacingOccurrences(of: "«", with: "").replacingOccurrences(of: "»", with: "")
        if name.isEmpty {
            name = "Название не указано"
        }
        let mutable = NSMutableAttributedString(attributedString: self.place_name_label.attributedText!)
        mutable.mutableString.setString(name + " ")
        self.place_name_label.attributedText = mutable
        
        self.offers_count.text = String(place.offers_count)
        self.events_count.text = String(place.events_count)
        self.checkins_count.text = String(place.checkins_count)
        self.checkings_count_dubbling?.text = String(place.checkins_count)
        
        self.favorites_button?.isFavorite = place.isFavorite
        self.fillActionLabels()
        
        
    }
    
    
    private func fillActionLabels() {
        for label in action_labels {
            switch label.tag {
            case 1:
                var defaultText = "Акци"
                switch (place.offers_count % 100) {
                case 10...20: defaultText += "й"
                default:
                    switch place.offers_count % 10 {
                    case 1: defaultText += "я"
                    case 2,3,4: defaultText += "и"
                    default: defaultText += "й"
                    }
                }
                label.text = defaultText
                
            case 2:
                var defaultText = "Событи"
                switch (place.events_count % 100) {
                case 10...20: defaultText += "й"
                default:
                    switch place.events_count % 10 {
                    case 1: defaultText += "е"
                    case 2,3,4: defaultText += "я"
                    default: defaultText += "й"
                    }
                }
                label.text = defaultText
                
            default:
                var defaultText = "Чекин"
                switch (place.checkins_count % 100) {
                case 10...20: defaultText += "ов"
                default:
                    switch place.checkins_count % 10 {
                    case 1: break;
                    case 2,3,4: defaultText += "а"
                    default: defaultText += "ов"
                    }
                }
                label.text = defaultText
            }
        }
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }

    
    func back() {
        self.navigationController?.popToViewController(self.navigationController!.viewControllers.dropLast().last!, animated: true)
    }

        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if scrollView.contentOffset.y > self.profileBoard.bounds.height*4/5 {
            UIView.animate(withDuration: 0.2) {
                self.arrowButton.transform = CGAffineTransform(rotationAngle: -.pi)
            }
        } else if scrollView.contentOffset.y < self.profileBoard.bounds.height*1/5 {
            UIView.animate(withDuration: 0.2) {
                self.arrowButton.transform = .identity
            }
        }
        
        var currentOffset = scrollView.contentOffset.y
        if currentOffset >= 0 && currentOffset <= self.profileBoard.bounds.height {
            currentOffset = 0
        } else if currentOffset > self.profileBoard.bounds.height {
            currentOffset -= self.profileBoard.bounds.height
        } else {
            currentOffset *= 2
        }
        
        self.blue_blur.alpha = abs(currentOffset/(self.tableItems.bounds.height - self.profileBoard.bounds.height))
    }
    
    @IBAction func show_accessory(_ sender: UIButton) {
        self.showAccessory(with: sender.tag == 1 ? .actions : sender.tag == 2 ? .events : .checkins)
    }
    
    fileprivate func showAccessory(with mode: PlaceEventsViewController.modes) {
       
        guard let nav = mode == .placeInfo ? self.storyboard?.instantiateViewController(withIdentifier: "PlaceInfoViewController_Nav") as? UINavigationController : UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "NavPlaceEventsViewController") as? UINavigationController else { return  }
        
        let placeEvents = nav.viewControllers.first as! PlaceEventsViewController
        placeEvents.mode = mode
        placeEvents.place = self.place
        nav.modalTransitionStyle = .crossDissolve
        nav.modalPresentationStyle = .overCurrentContext
        placeEvents.previousStatusBarStyle = UIApplication.shared.statusBarStyle
        self.present(nav, animated: true, completion: nil)
    }

    
}


extension PlaceProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(1, posts.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if posts.count == 0 && !hasNext || posts.count <= indexPath.row {
            let cell = tableItems.dequeueReusableCell(withIdentifier: post_types.placeholder.stringValue, for: indexPath)
             (cell.viewWithTag(1) as? UILabel)?.text = "Пока что чекинов нет"
            return cell
        }
        let currentPost = posts[indexPath.row]
        let cell = tableItems.dequeueReusableCell(withIdentifier: currentPost.type.stringValue, for: indexPath) as! PostCell
        configure(postCell: cell, with: currentPost, commentsCountChanged: { [weak self] (newCount) in
            self?.posts[indexPath.row].comments_count = newCount
        })
        
        cell.setCanDelete(currentPost.canDelete) { [weak self] in
            Server.makeRequest.deletePost(currentPost) { success in
                if success {
                    async {
                        self?.posts.remove(at: indexPath.row)
                        self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                    }
                } else {
                    (self?.tableItems.cellForRow(at: indexPath) as? PostCell)?.hideMenu()
                }
            }
        }
        cell.setCanComplain(currentPost.canComplain) { [weak self] in
            guard let checkin = currentPost.searchable_item?.checkin else { return }
            Server.makeRequest.complain(to: checkin, completion: {
                async {
                    self?.posts.remove(at: indexPath.row)
                    self?.tableItems?.deleteRows(at: [indexPath], with: .automatic)
                }
            })
            
        }
        
        cell.likes_button?.addTarget(self, action: #selector(self.animate_like(_:)), for: .touchDown)
        cell.likes_button?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        cell.likes_button?.tag = indexPath.row
        return cell
    }
    
    
    func animate_like(_ sender: UIButton) {
        let anim = (sender.superview as? CSAnimationView)
        anim?.duration = 0.3
        anim?.delay = 0
        anim?.type = CSAnimationTypePop
        anim?.startCanvasAnimation()
    }
    
    func like(_ sender: UIButton) {
        Server.makeRequest.like(post: posts[sender.tag]) { (success, liked, count) in
            self.posts[sender.tag].isLiked = liked
            sender.setImage(liked ? #imageLiteral(resourceName: "liked_button") : #imageLiteral(resourceName: "myprofile_likes"), for: .normal)
            guard var likesCount = Int(sender.title(for: .normal) ?? "") else { return }
            likesCount = count
            self.posts[sender.tag].likes_count = count
            sender.setTitle(String(likesCount), for: .normal)
        }
    }
    
    
}




extension UIImageView {
    public func sd_setImageAnimated(with url: URL, placeholderImage placeholder: UIImage? = nil, options: SDWebImageOptions = [], animationDuration: Double = 0.3, completed: SDExternalCompletionBlock?) {
        self.sd_setImage(with: url, placeholderImage: placeholder, options: options, progress: nil) { (fetchedImage, error, cacheType, url) in
            completed?(fetchedImage, error, cacheType, url)
            
            self.alpha = 0
            self.image = fetchedImage
            UIView.transition(with: self, duration: (cacheType == .none ? animationDuration : 0), options: .transitionCrossDissolve, animations: { () -> Void in
                self.alpha = 1
            })
        }
    }
}


class StatusControllerAlertController: UIAlertController {
    var statusbarStyle: UIStatusBarStyle = .default
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return statusbarStyle
        }
    }
}
