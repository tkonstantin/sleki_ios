//
//  Server+Constants.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


extension Server {
    
    
    enum url_fields {
        
        enum auth {
            static let username = "username"
            static let password = "password"
        }
        
        enum reg {
            static let email = "email"
            static let password = "password1"
            static let passwordConfirm = "password2"
        }
    }
    
    //--------- Paths
    
    enum url_paths {
        
        static let base = "https://api.sleki.com/v1"
        static let base_auth = base + "/auth"
        static let base_reg = base + "/registration"
        static let vk_auth = "/vk/auth"
        
        enum password {
            static let request = "/password/request-recovery"
            static let complete = "/password/recovery"
        }
        
        enum profile {
            static let main = "/profile"
            static let interests = "/profile/interests"
            static let image = "/profile/image"
            static let imageUpload = "/profile/image/upload"
            static let imageSmallUpload = "/profile/avatar_image/upload"
        }
        enum places {
            static let main = "/places"
            static let types = "/places/types"
            static let checkins = "/places/checkins"
            static let offers = "/places/offers"
            static let searchByBeacons = "/places/beacons/search"
            static let searchByWAPS = "/places/wifi-access-points/search"
            static let searchByGPS = "/places/geo-position/search"
            ///POST
            static let block = "/places/%@/block"
            ///PUT
            static let unblock = "/places/%@/unblock"

        }
        
        static let subways = "/subway_stations"
        
        enum interests {
            static let dictionary = "/persons/interests"
        }
        
        enum wall {
            static let main = "/wall"
        }
        
        enum beacons {
            static let main = "/beacons"
            static let search = "/beacons/search"
        }
        
        enum waps {
            static let main = "/wifi-access-points"
            static let search = "/wifi-access-points/search"
        }
        
        enum persons {
            static let main = "/persons"
            static let subscribers = "/subscribers"
            static let subscriptions = "/subscriptions"
            static let searchByBeacons = "/persons/beacons/search"
            static let searchByWAPS = "/persons/wifi-access-points/search"
            static let searchByGPS = "/persons/geo-position/search"


        }
        
        enum notifications {
            static let main = "/notifications"
            static let token = "/notifications/token"
        }
        
        enum eventsOffsers {
            static let main = "/events_offers/"
            static let events = "/events"
            static let offers = "/offers"
        }
        
        enum favorites {
            static let main = "/favorites"
            static let offers = "/favorites/offers"
            static let places = "/favorites/places"
            static let events = "/events/accepted"
        }
        
        enum subscriptions {
            static let main = "/subscriptions"
        }
        enum subscribers {
            static let main = "/subscribers"
        }
        
        enum offers {
            static let main = "/offers"
        }
        
        enum feed {
            static let main = "/feed"
        }
        
        enum chats {
            static let main = "/chats"
            static let initializeWithPerson = "/chats/person/%@"
            static let messages = "/chats/%@/messages"
        }
        
        
        enum checkin {
            static let main = "/checkins/"
            static let imageUpload = "/checkins/%@/image/upload"
            ///POST
            static let block = "/checkins/%@/block"
            ///PUT
            static let unblock = "/checkins/%@/unblock"
        }
        
        enum stuff {
            static let cities = "/cities"
        }
    }
    

    
}

