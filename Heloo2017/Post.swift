//
//  Post.swift
//  Heloo2017
//
//  Created by Константин on 08.05.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage


class Post: NSObject {
    var type: post_types = .event_created
    
    var imageHeight: CGFloat {
        get {
            var height: CGFloat = (UIScreen.main.bounds.width - 20)*9/16
            if let image = self.post_image {
                let currentWidth = UIScreen.main.bounds.width - 20
                let postAspect = image.height/(image.width == 0 ? 1 : image.width)
                height = postAspect*currentWidth
            }
            return max(min(height, UIScreen.main.bounds.height*2/3), 64)
        }
    }
    
    private var _requiredHeight: CGFloat = 0
    var requiredHeight: CGFloat {
        get {
            if _requiredHeight == 0 {
                let insetsSize: CGFloat = 6 + 6
                
                let imageTop: CGFloat = 88
                
                let smallTopLabelHeight = 26 + small_top_label.requiredHeight(18, font: UIFont.boldSystemFont(ofSize: 18), width: UIScreen.main.bounds.width - 48, lineSpacing: nil)
                
                let noImgDateLabelHeight: CGFloat = 20 + (self.post_image == nil ? 0 : 20)
                
                let postTextHeight: CGFloat = 20 + self.post_text.requiredHeight(14, font: UIFont.systemFont(ofSize: 14), width: UIScreen.main.bounds.width - 48, lineSpacing: nil)
                
                let bottomViewHeight: CGFloat = 88
                
                _requiredHeight = insetsSize + imageHeight + imageTop + smallTopLabelHeight + noImgDateLabelHeight + postTextHeight + bottomViewHeight
                
            }
            return _requiredHeight
        }
    }
    
    var post_text = ""
    var post_title = ""
    var post_image: ImageObject?
    var isExpanded = false
    
    var real_date: Date?
    
    var date = ""
    var duration = ""
    var small_top_icon = ""
    var big_top_icon = ""
    var small_top_label = ""
    var big_top_label = ""
    
    var likes_count = 0
    var comments_count = 0
    
    var isLiked = false
    var isFavorite = false
    var isAccepted = false
    
    var action_title: NSMutableAttributedString = NSMutableAttributedString(string: "")
    
    
    var canEdit = false
    var canDelete = false
    var canComplain = false
        
    
    var searchable_item: Searchable?
    
    var isMine: Bool {
        get {
            return searchable_item?.isMine ?? false
        }
    }
    
    func didSelect(from controller: UIViewController) {
        
        if let place = self.searchable_item?.event?.place ?? self.searchable_item?.offer?.place ?? self.searchable_item?.accept?.event?.place ?? self.searchable_item?.checkin?.place, let profile = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "PlaceProfileViewController") as? PlaceProfileViewController {
            UIApplication.shared.statusBarStyle = .lightContent
            profile.place = place
            controller.navigationController?.setNavigationBarHidden(false, animated: true)
            controller.navigationController?.pushViewController(profile, animated: true)
            
        }
    }
    
    
    func didSelectUser(from controller: UIViewController) {
        if let user = self.searchable_item?.checkin?.user ?? self.searchable_item?.accept?.user {
            if let current = controller as? MyProfileViewController, current.user.uuid == user.uuid {
                return;
            }
            let profile = MyProfileViewController.makeOne(with: user, mode: UserDataManager.instance.isMe(user) ? .my : .other)
            controller.navigationController?.setNavigationBarHidden(false, animated: true)
            controller.navigationController?.pushViewController(profile, animated: true)
        } else {
            self.didSelect(from: controller)
        }
    }
    
    
    static func dateString(_ date: Date!, secondDate: Date? = nil) -> (String) {
        guard date != nil else { return "" }
        if secondDate == nil {
            return date.feedDate()
        } else {
            let second = secondDate!
            return date.feedDateRange(to: second)

        }
    }
    
    
    init?(with item: Searchable!) {
        guard item != nil else { return nil }
        self.searchable_item = item
        if let offer = item.offer {
            self.post_text = offer.descript_text
            self.post_title = offer.title
            self.post_image = offer.artwork
            
            self.real_date = offer.start
            
            self.date = Post.dateString(offer.start, secondDate: offer.end)
            if self.date.isEmpty {
                self.date = [offer.startTimeString, offer.endTimeString].compactMap({$0}).joined(separator: " - ")
            }
            self.big_top_icon = offer.place?.image_small ?? ""
            self.big_top_label = offer.place?.name ?? ""
//            self.small_top_label = offer.title
            self.likes_count = offer.likes_count
            self.isFavorite = offer.isFavorite
            self.isLiked = offer.isLiked
            self.type = .action_created
            self.canEdit = false
            self.canDelete = false
            self.canComplain = false
            
        } else if let checkin = item.checkin {
            self.post_text = checkin.descript
            self.post_image = checkin.artwork
            
            self.real_date = checkin.date
            
            self.date = Post.dateString(checkin.date)
            
            
            self.big_top_icon = checkin.place?.image_small ?? ""
            self.small_top_icon = checkin.user?.image_small ?? ""
            self.big_top_label = (checkin.user?.firstName ?? "") + " " + (checkin.user?.lastName ?? "")
            self.small_top_label = checkin.place?.name ?? ""
            self.likes_count = checkin.likes_count
            self.comments_count = checkin.comments_count
            self.isLiked = checkin.isLiked
            self.type = .checkin_created

            let mutable = NSMutableAttributedString(string: "")
            mutable.append(NSAttributedString(string: (checkin.user?.gender == .female ? "Сделала " : "Сделал ")))
            mutable.append(NSAttributedString(string: "чекин", attributes: [NSForegroundColorAttributeName: blueColor]))
            self.action_title = mutable
            
            self.canEdit = false//checkin.isMine
            self.canDelete = checkin.isMine
            self.canComplain = !checkin.isMine
            
        } else if let event = item.event {
            self.post_text = event.description_text
            self.post_title = event.description_title
            self.post_image = event.artwork
            self.comments_count = event.comments_count
            
            self.real_date = event.start
            
            self.date = Post.dateString(event.start, secondDate: event.end)
            if self.date.isEmpty {
                self.date = [event.startTimeString, event.endTimeString].compactMap({$0}).joined(separator: " - ")
            }
            self.big_top_icon = event.place?.image_small ?? ""
            self.big_top_label = event.place?.name ?? ""
            self.small_top_icon = event.place?.image_small ?? ""
            self.small_top_label = event.title
            self.likes_count = event.likes_count
            self.isLiked = event.isLiked
            self.isAccepted = event.isAccepted
            self.type = .event_created
            self.action_title = NSMutableAttributedString(string: "Запустил событие")
            
            self.canEdit = false
            self.canDelete = false
            self.canComplain = false

        } else if let accept = item.accept {
            self.post_text = accept.event?.description_text ?? ""
            self.post_title = accept.event?.description_title ?? ""
            self.post_image = accept.event?.artwork
            self.comments_count = accept.comments_count
            
            self.real_date = accept.date
            
            self.date = Post.dateString(accept.event?.start, secondDate: accept.event?.end)
            self.big_top_icon = accept.user?.image_small ?? ""
            self.big_top_label = (accept.user?.firstName ?? "") + " " + (accept.user?.lastName ?? "")
            self.small_top_label = accept.event?.title ?? ""
            self.small_top_icon = accept.user?.image_small ?? ""
            self.likes_count = accept.likes_count
            self.isLiked = accept.isLiked
            self.isAccepted = accept.event?.isAccepted ?? false
            self.type = .user_will_go
            self.action_title = NSMutableAttributedString(string: "Пойдет на")

            
            self.canEdit = false//accept.isMine
            self.canDelete = accept.isMine
            self.canComplain = false
            
        } else if let place = item.place {
            self.post_image = ImageObject(with: ["url": place.image_large.isEmpty ? place.image_small : place.image_large])
            self.big_top_label = place.name
            self.small_top_label = place.short_address_string
            self.isFavorite = place.isFavorite
            self.type = .place_Favorite
            
            self.canEdit = false
            self.canDelete = false
            self.canComplain = false
            
        } else {
            return nil
        }
        
    }
}

