//
//  PlaceFilter.swift
//  Heloo2017
//
//  Created by Константин on 09.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


typealias placeFilterObject = (subways: [PickableThing], categories: [TreeItem], city: PickableThing?, fromPrice: Int?, toPrice: Int?, text: String?)

func ==(lhs: placeFilterObject?, rhs: placeFilterObject?) -> Bool {
    guard lhs?.text == rhs?.text else { return false }
    guard lhs?.subways.map({$0.tag}).sorted() == rhs?.subways.map({$0.tag}).sorted() else { return false }
    guard lhs?.categories.map({$0.key}).sorted() == rhs?.categories.map({$0.key}).sorted() else { return false }
    guard lhs?.city?.tag == rhs?.city?.tag else { return false }
    return lhs?.fromPrice == rhs?.fromPrice && lhs?.toPrice == rhs?.toPrice
}

func !=(lhs: placeFilterObject?, rhs: placeFilterObject?) -> Bool {
    return !(lhs == rhs)
}


protocol PlaceFilterDelegate: class {
    func filtersChanged(filter: placeFilterObject)
}


class PlaceFilter: UIViewController {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var subwayLabel: UILabel!
    @IBOutlet weak var searchField: UITextField!
    
    @IBOutlet weak var subwayButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var cityButton: UIButton!

    @IBOutlet weak var slider: RangeSlider! {
        didSet {
            slider.minValue = 0
            slider.maxValue = 5500
            slider.leftThumbValue = self.fromPrice == nil ? slider.minValue : CGFloat(self.fromPrice!)
            slider.rightThumbValue = self.toPrice == nil ? slider.maxValue : CGFloat(self.toPrice!)
        }
    }
    
    weak var delegate: PlaceFilterDelegate!
    
    
    func restore(with filter: placeFilterObject?) {
        fromPrice = filter?.fromPrice
        toPrice = filter?.toPrice
        selectedCity = filter?.city ?? PlaceFilter.myCityItem
        selectedSubways = filter?.subways ?? []
        selectedCategories = filter?.categories ?? []
        searchText = filter?.text
    }

    
    var searchText: String?

    
    var fromPrice: Int? = nil {
        didSet {
            applyPrices()
        }
    }
    var toPrice: Int? = nil {
        didSet {
            applyPrices()
        }
    }
    
    func applyPrices() {
        guard priceLabel != nil else { return }
        
        priceLabel.textColor = .black
        if fromPrice == nil && toPrice == nil {
            priceLabel.text = "Укажите средний чек"
            priceLabel.textColor = UIColor.black.withAlphaComponent(0.3)
        } else if fromPrice != nil && toPrice != nil {
            priceLabel.text = "от \(fromPrice!) до \(toPrice!)"
        } else if fromPrice != nil {
            priceLabel.text = "от \(fromPrice!)"
        } else if toPrice != nil {
            priceLabel.text = "до \(toPrice!)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyPrices()
        updateCity()
        updateSubways()
        updateCategories()
        searchField?.text = searchText
    }
 
    
    func updateCity() {
        cityLabel?.text = selectedCity?.title ?? "Выберите город из списка"
        cityLabel?.textColor = selectedCity == nil ? UIColor.black.withAlphaComponent(0.3) : .black
    }
    
    func updateSubways() {
        subwayLabel?.text = selectedSubways.count == 0 ? "Выберите станции метро" : selectedSubways.map({$0.title}).joined(separator: ", ")
        subwayLabel?.textColor = selectedSubways.count == 0 ? UIColor.black.withAlphaComponent(0.3) : .black
    }
    
    func updateCategories() {
        categoriesLabel?.text = selectedCategories.count == 0 ? "Выберите категорию" : selectedCategories.map({$0.title}).joined(separator: ", ")
        categoriesLabel?.textColor = selectedCategories.count == 0 ? UIColor.black.withAlphaComponent(0.3) : .black
    }
    
    private class var myCityItem: PickableThing? {
        get {
            if let city = UserDataManager.instance.currentUser?.city, let city_tag = UserDataManager.instance.currentUser?.city_tag {
                if !city.isEmpty && !city_tag.isEmpty {
                    return PickableThing(title: city, tag: city_tag)
                }
            }
            return nil
        }
    }
    
    var selectedCity: PickableThing? = myCityItem {
        didSet {
           updateCity()
           selectedSubways = []
        }
    }
    
    var selectedSubways: [PickableThing] = [] {
        didSet {
           updateSubways()
        }
    }
    
    var selectedCategories: [TreeItem] = [] {
        didSet {
           updateCategories()
        }
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        searchText = searchField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        delegate?.filtersChanged(filter: (subways: self.selectedSubways, categories: self.selectedCategories, city: self.selectedCity, fromPrice: self.fromPrice, toPrice: self.toPrice, text: searchText))
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearTapped(_ sender: UIButton) {
        slider.leftThumbValue = slider.minValue
        slider.rightThumbValue = slider.maxValue
        searchField.text = nil
        searchText = ""
        fromPrice = nil
        toPrice = nil
        selectedCity = nil
        selectedCategories = []
        selectedSubways = []
    }
    
    
    @IBAction func subwayTapped(_ sender: UIButton) {
        guard UserDataManager.isAuthorized || selectedCity != nil else { return }
        self.performSegue(withIdentifier: "subway", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dest = segue.destination as? FilterPickerViewController {
            
            switch sender as? UIButton {
            case cityButton:
                dest.selectedItemTags = [self.selectedCity?.tag].compactMap({$0})
                dest.placeholderText = "Города не найдены"
                dest.maxSelectionCount = 1

                Server.makeRequest.getCitiesList { (result) in
                    dest.setItems(result.map({PickableThing(title: $0.title, tag: $0.tag)}))
                }
            case nil:
                dest.selectedItemTags = self.selectedSubways.compactMap({$0.tag})
                dest.maxSelectionCount = 100500
                dest.placeholderText = "Станции метро недоступны для выбранного города"

                Server.makeRequest.getAvailableSubways(by: self.selectedCity?.tag) { dict in
                    dest.setItems(dict.keys.map({PickableThing(title: dict[$0]!, tag: $0)}))
                }
     
            default:
                return;
                
            }
            
            dest.onSelected = { items in
                switch sender as? UIButton {
                case self.cityButton:
                    self.selectedCity = items.first
                case nil:
                    self.selectedSubways = items
                default: return;
                }
            }
        } else if let dest = segue.destination as? FilterRangeViewController {
            dest.leftValue = self.fromPrice ?? dest.leftValue
            dest.rightValue = self.toPrice ?? dest.rightValue
            dest.minValue = 0
            dest.maxValue = 5500
            dest.leftValue = self.fromPrice ?? 0
            dest.rightValue = self.toPrice ?? 5500
            dest.update(updateSlider: true)
            dest.onSelected = { prices in
                self.fromPrice = prices.0
                self.toPrice = prices.1
            }
        } else if let dest = segue.destination as? FilterTreeViewController {
            dest.setItems(self.selectedCategories, mainSource: UserDataManager.instance.availablePlaceTypes)
            dest.onPicked = { pickedInterests in
                self.selectedCategories = pickedInterests
            }
        }
    }
    
    @IBAction func sliderChanged(_ sender: RangeSlider) {
        sender.leftThumbValue = CGFloat(Int(sender.leftThumbValue)/100)*100
        sender.rightThumbValue = CGFloat(Int(sender.rightThumbValue)/100)*100

        fromPrice = Int(sender.leftThumbValue)
        toPrice = Int(sender.rightThumbValue)
        
        if toPrice == 5500 {
            toPrice = nil
        }
    }
    
}


extension PlaceFilter: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchText = textField.text
        return true
    }
}
