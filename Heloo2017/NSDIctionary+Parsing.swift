//
//  NSDIctionary+Parsing.swift
//  Heloo2017
//
//  Created by Константин on 07/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import UIKit


extension NSDictionary {
    func intValue(for key: String) -> Int {
        let value = self.findValue(for: key)
        
        return value as? Int ?? Int(value as? String ?? "0") ?? 0
    }
    
    
    func intArrValue(for key: String) -> [Int] {
        let value = self.findValue(for: key)
        var result: [Int?] = []
        for item in (value as? [Any] ?? []) {
            result.append(item as? Int ?? Int(item as? String ?? ""))
        }
        return result.compactMap({$0})
    }
    
    func doubleValue(for key: String) -> Double? {
        let value = self.findValue(for: key)
        
        return value as? Double ?? Double(value as? String ?? "0")
    }
    
    func cgFloatValue(for key: String) -> CGFloat? {
        let value = self.findValue(for: key)
        
        if let cgFloat = value as? CGFloat {
            return cgFloat
        } else if let string = value as? String {
            if let double = Double(string) {
                return CGFloat(double)
            } else {
                return nil
            }
        } else {
            return nil
        }
        return value as? CGFloat ?? CGFloat(Double(value as? String ?? "0") ?? 0)
    }
    
    func stringValue(for key: String) -> String {
        let value = self.findValue(for: key)
        if let string = value as? String {
            return string
        } else if let int = value as? Int {
            return String(int)
        } else if let double = value as? Double {
            return String(double)
        } else {
            return ""
        }
    }
    
    func boolValue(for key: String) -> Bool {
        let value = self.findValue(for: key)
        
        return value as? Bool ?? false
    }
    
    private func findValue(for key: String) -> Any? {
        if self.allKeys.contains(where: {$0 as? String == key}) {
            return self[key]
//        } else if let customs = self["custom_attributes"] as? [NSDictionary] {
//            var attributes: [String: Any?] = [:]
//            for custom in customs {
//                if let code = custom["attribute_code"] as? String {
//                    attributes[code] = custom["value"]
//                }
//            }
//            return attributes[key]
        } else {
            return nil
        }
    }
}
