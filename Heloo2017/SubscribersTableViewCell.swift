//
//  SubscribersTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 09.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SubscribersTableViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var unfollow_button: UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
