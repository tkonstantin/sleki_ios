//
//  Server+Local.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire

extension Server {
    
    func isLocallyFavorite(place: Place) -> Bool {
        if place.isFavorite {
            if removedFromFavorites.contains(place.uuid) {
                return false
            } else {
                return true
            }
        } else {
            if addedToFavorites.contains(place.uuid) {
                return true
            } else {
                return false
            }
        }
    }
}
