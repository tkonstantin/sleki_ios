//
//  AppDelegate+RemoteNotifications.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Firebase
import UserNotifications
import UserNotificationsUI

extension AppDelegate {
    
    class func configureNotifications() {
        let application = UIApplication.shared 
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = UIApplication.shared.delegate as? AppDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //
    //    }
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //
    //    }
    
}


extension AppDelegate: MessagingDelegate {
    
    
    //TODO: - handle push notification tapped somehow ???
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        handleRemoteNotificationInfo(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handleRemoteNotificationInfo(userInfo)
        completionHandler(.newData)
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("NOTIFICATION:  application received remoteMessage \(remoteMessage)")
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        Server.makeRequest.registerNotificationsToken(fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("NOTIFICATION:  didReceive remoteMessage = \(remoteMessage)")
    }
    
    private func handleRemoteNotificationInfo(_ userInfo: [AnyHashable: Any]) {
        if let rawType = userInfo["type"] as? String, let type = notificationTypes(rawValue: rawType) {
            if UIApplication.shared.applicationState == .active {
                type.action(with: userInfo)?()
            } else if UIApplication.shared.applicationState == .inactive {
                NotificationsManager.shared.onTapped(with: userInfo, type: type)
            }
        }
    }
    
    enum notificationTypes {
        case checkinComment
        case acceptComment
        case newSubscriber
        case newOffer
        case chatMessage
        
        var rawValue: String {
            get {
                switch self {
                case .checkinComment: return "checkIn_comment"
                case .acceptComment: return "accepted_event_comment"
                case .newSubscriber: return "new_subscriber"
                case .newOffer: return "newOffer"
                case .chatMessage: return "chatMessage"
                }
            }
        }
        
        init?(rawValue: String) {
            switch rawValue {
            case notificationTypes.checkinComment.rawValue: self = .checkinComment
            case notificationTypes.newOffer.rawValue: self = .newOffer
            case notificationTypes.newSubscriber.rawValue: self = .newSubscriber
            case notificationTypes.acceptComment.rawValue: self = .acceptComment
            case notificationTypes.chatMessage.rawValue: self = .chatMessage
            default: return nil
            }
        }
        
        func action(with userInfo: [AnyHashable: Any]) -> (()->Void)? {
            var action: (()->Void)? = nil
            switch self {
            case .checkinComment:
                action = {
                    NotificationsManager.shared.showNotification(with: NotificationCheckinComment(userInfo: userInfo))
                }
            case .acceptComment:
                action = {
                    NotificationsManager.shared.showNotification(with: NotificationAcceptComment(userInfo: userInfo))
                }
            case .newOffer: 
                action = {
                    NotificationsManager.shared.showNotification(with: NotificationOffer(userInfo: userInfo))
                }
            case .newSubscriber:
                action = {
                    NotificationsManager.shared.showNotification(with: User(userInfo: userInfo))
                }
            case .chatMessage:
                action = {
                    if let message = Message.makeMessage(userInfo: userInfo) {
                        if let _ = CoreDataManager.shared.chat(with: message.chat_uuid) {
                            NotificationsManager.shared.showNotification(with: message)
                        } else {
                            Server.makeRequest.loadAllChats {
                                if let _ = CoreDataManager.shared.chat(with: message.chat_uuid) {
                                    NotificationsManager.shared.showNotification(with: message)
                                }
                            }
                            
                        }
                    }
                }
            default:
                break;
            }
            return action
        }
    }
    
}

