//
//  FilterCheckCell.swift
//  Heloo2017
//
//  Created by Константин on 09.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class FilterCheckCell: UITableViewCell {

    @IBOutlet weak var checkmarkIcon: UIImageView!
    @IBOutlet weak var title: UILabel!

}
