//
//  Server+GPS.swift
//  Heloo2017
//
//  Created by Константин on 17.07.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire


extension Server {

    
    func getPlacesByGPS(location _location: CLLocationCoordinate2D?, completion: @escaping ([Place])->Void) {
        
        guard let location = _location else { completion([]); return }

        Alamofire.request(url_paths.base + url_paths.places.searchByGPS, method: .get, parameters: ["lat": location.latitude, "lon": location.longitude], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var places: [Place] = []
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    if let result = dict["place"] as? NSDictionary {
                        let place = Place(with: result)
                        place.geo_distance = dict["distance"] as? Double ?? -1
                        places.append(place)
                    }
                }
    
            }
            
            var currentFoundedPlaces = self.foundedPlaces
            
            for place in places {
                if let existingPlace = self.foundedPlaces.keys.first(where: {$0.uuid == place.uuid}) {
                    currentFoundedPlaces[existingPlace] = nil
                }
                currentFoundedPlaces[place] = Date()
            }
            
            self.foundedPlaces = currentFoundedPlaces
            
            completion(places)
        }
    }
    
    func getPersonsByGPS(location _location: CLLocationCoordinate2D?, completion: @escaping ([User])->Void) {
        
        guard let location = _location else { completion([]); return }
        
        Alamofire.request(url_paths.base + url_paths.persons.searchByGPS, method: .get, parameters: ["lat": location.latitude, "lon": location.longitude], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var persons: [User] = []
            if let json = response.result.value as? [NSDictionary] {
                for result in json.compactMap({$0["person"] as? NSDictionary}) {
                    persons.append(User(with: result))
                }
            }
            completion(persons)
        }
    }
}
