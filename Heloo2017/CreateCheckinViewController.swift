//
//  CreateCheckinViewController.swift
//  Heloo2017
//
//  Created by Константин on 28.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SVProgressHUD
import CropViewController

class CreateCheckinViewController: UIViewController {
    
    
    class func createOne(with place: Place) -> CreateCheckinViewController {
        let new = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "CreateCheckinViewController") as! CreateCheckinViewController
        new.place = place
        new.modalTransitionStyle = .crossDissolve
        new.modalPresentationStyle = .overCurrentContext
        return new
    }
    
    var place: Place!
    
    var selectedImage: UIImage? {
        didSet {
            self.picked_image.image = selectedImage
            self.picked_image_mask.isHidden = selectedImage == nil
            self.upload_photo_button.setImage(selectedImage == nil ? #imageLiteral(resourceName: "upload_photo") : #imageLiteral(resourceName: "reupload_photo"), for: .normal)
            self.upload_photo_label.text = selectedImage == nil ? "Загрузить фото" : "Изменить фото"
            self.upload_photo_label.textColor = selectedImage == nil ? blueColor : .white
            self.updateSaveButton()
        }
    }
    
    //вызвать этот блок после успешного создания чекина
    var checkinCompletionBlock: ((Int)->Void)?
    
    @IBOutlet weak var bg_card: UIView!
    @IBOutlet weak var close_button: UIButton!
    @IBOutlet weak var bg_image: UIImageView!
    @IBOutlet weak var top_card: UIView!
    @IBOutlet weak var top_card_top_space: NSLayoutConstraint!
    
    @IBOutlet weak var picked_image: UIImageView!
    @IBOutlet weak var picked_image_mask: UIView!
    @IBOutlet weak var upload_photo_button: UIButton!
    @IBOutlet weak var upload_photo_label: UILabel!
    
    
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var main_view_top_space: NSLayoutConstraint!
    @IBOutlet weak var place_name: UILabel!
    @IBOutlet weak var timeAndAddress: UILabel!
    @IBOutlet weak var placeholder_label: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var save_button: UIButton!
    @IBOutlet weak var save_active_button: UIButton!
    @IBOutlet weak var save_keyboard_button: UIButton!
    @IBOutlet weak var save_button_bottom_space: NSLayoutConstraint!
    
    let top_card_top_space_constant: CGFloat = 49
    let main_view_expanded_top_space: CGFloat = 201
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fill()
        animateAppearance()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textView.delegate = self
        self.addTapOutsideGestureRecognizer()
        self.subscribeToKeyboard()
        self.updateSaveButton()
        
    }
    
    override func keyboardWillBeShown(_ notification: Notification) {
        if let height = notification.keyboardHeight {
            self.save_button_bottom_space.constant = 24 + height
            self.animateChanges()
        }
    }
    
    override func keyboardWillBeHidden() {
        self.save_button_bottom_space.constant = 24
        self.animateChanges()
    }
    
    func fill() {
        self.place_name.text = place.name
        self.timeAndAddress.text = Date().checkinDate() + ", " + place.address_string
    }
    
    
    func animateChanges() {
        UIView.animate(withDuration: 0.6, delay: 0.1, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.2, options: [], animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func animateTextViewEditing() {
        self.updateSaveButton()
        self.placeholder_label.isHidden = true
        self.main_view_top_space.constant = 0
        animateChanges()
    }
    
    func animateTextViewNonEditing() {
        self.updateSaveButton()
        self.placeholder_label.isHidden = !textView.text.isEmpty
        self.main_view_top_space.constant = main_view_expanded_top_space
        animateChanges()
    }
    
    func animateAppearance() {
        if let url = URL(string: place.image_large) {
            bg_image.sd_setImage(with: url, placeholderImage: UIImage(named: "blue_blur_rect"))
        } else {
            bg_image.image = UIImage(named: "blue_blur_rect")
        }
        
        async {
            UIView.animate(withDuration: 0.2, delay: 0.3, options: [], animations: {
                self.bg_card.alpha = 0.5
            })
            self.top_card_top_space.constant = self.top_card_top_space_constant
            self.animateChanges()
        }
    }
    
    
    @IBAction func close_tapped(_ sender: UIButton) {
        self.makeCoverVerticalDismiss()
    }
    
    @IBAction func upload_tapped(_ sender: UIButton) {
        self.showPickerAlert()
    }
    
    func updateSaveButton() {
        if textView.isFirstResponder {
            save_button.isHidden = true//!textView.text.isEmpty
            save_keyboard_button.isHidden = false
            save_active_button.isHidden = true
        } else {
            save_button.isHidden = true//!(textView.text.isEmpty && selectedImage == nil)
            save_keyboard_button.isHidden = true
            save_active_button.isHidden = false
        }
    }
    
    @IBAction func save_tapped(_ sender: UIButton) {
        sender.isHidden = true
        SVProgressHUD.show()
        self.view.isUserInteractionEnabled = false
        Server.makeRequest.createCheckin(place: place, text: self.textView.text.trimmingCharacters(in: .whitespacesAndNewlines), image: self.selectedImage) { [weak self] (success, errorMessage) in
            SVProgressHUD.dismiss()
            self?.view.isUserInteractionEnabled = true
            sender.isHidden = false
            if success {
               self?.checkinCompletionBlock?(self!.place.checkins_count + 1)
                self?.checkinCompletionBlock = nil
                self?.dismiss(animated: true, completion: { _ in
                    DoneViewController.presentOne(with: "Чекин выполнен")
                })
            } else {
                ErrorPopupManager.showError(type: .unknown, from: self, subtitle: errorMessage ?? "Неизвестная ошибка")
            }
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
}

extension CreateCheckinViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.animateTextViewEditing()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.animateTextViewNonEditing()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.updateSaveButton()
    }
}

extension CreateCheckinViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate { //uploading images
    func showPickerAlert() {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (_) in
            self.presentPicker(with: .camera)
        }))
        sheet.addAction(UIAlertAction(title: "Моменты", style: .default, handler: { (_) in
            self.presentPicker(with: .savedPhotosAlbum)
        }))
        sheet.addAction(UIAlertAction(title: "Альбомы", style: .default, handler: { (_) in
            self.presentPicker(with: .photoLibrary)
        }))
        if self.selectedImage != nil {
            sheet.addAction(UIAlertAction(title: "Удалить фото", style: .destructive, handler: { (_) in
                self.selectedImage = nil
            }))
        }
        sheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.present(sheet, animated: true, completion: nil)
    }
    
    
    func presentPicker(with type: UIImagePickerControllerSourceType) {
        let picker = UIImagePickerController()
        picker.navigationBar.tintColor = .white
        picker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        picker.navigationBar.isTranslucent = false
        picker.modalPresentationCapturesStatusBarAppearance = false
        picker.navigationBar.barTintColor = UIColor(red: 57/255, green: 152/255, blue: 226/255, alpha: 1.0)
        picker.sourceType = type
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
            picker.dismiss(animated: true, completion: {_ in
                self.showImageCropper(with: img)
            })
        } else {
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func showImageCropper(with image: UIImage) {
        let crop = CropViewController.init(croppingStyle: .default, image: image)
        crop.aspectRatioPreset = .presetOriginal
//        crop.aspectRatioLockEnabled = true
        crop.aspectRatioPickerButtonHidden = true
        crop.resetAspectRatioEnabled = true
        crop.delegate = self
        self.present(crop, animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.selectedImage = image
        cropViewController.delegate = nil
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
}
