//
//  PlaceCardFavoriteButton.swift
//  Heloo2017
//
//  Created by Константин on 08.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class PlaceCardFavoriteButton: ResizableButton {

    var isFavorite: Bool = false {
        didSet {
            self.backgroundColor = isFavorite ? UIColor(hex: "FFB500") : UIColor(hex: "D2D2D2")
        }
    }

}
