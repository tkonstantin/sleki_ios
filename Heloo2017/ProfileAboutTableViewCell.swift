//
//  ProfileAboutTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 21.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ProfileAboutTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    func upate(with place: Place) {
        descriptionLabel.text = place.descript
    }
}
