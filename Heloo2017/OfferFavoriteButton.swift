//
//  OfferFavoriteButton.swift
//  Heloo2017
//
//  Created by Константин on 25.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class OfferFavoriteButton: ResizableButton {
    var uuid: String!

    @IBInspectable var isFavorite: Bool = false {
        didSet {
                self.setImage(self.isFavorite ? #imageLiteral(resourceName: "offerpost_favorite_enabled") : #imageLiteral(resourceName: "offerpost_favorite_disabled"), for: .normal)
            }
    }

}


class PlaceFavoriteButton: OfferFavoriteButton {
    
    @IBInspectable var isInCard: Bool = false
}


class PlaceFavoriteNavigationButton: UIButton {
    
     var isFavorite: Bool = false {
        didSet {
            self.setImage(self.isFavorite ? #imageLiteral(resourceName: "PlaceFavoriteNavigationButton_Active") : #imageLiteral(resourceName: "PlaceFavoriteNavigationButton_Inactive"), for: .normal)
        }
    }
    
}
