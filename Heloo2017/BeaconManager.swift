//
//  BeaconManager.swift
//  Heloo2017
//
//  Created by Константин on 19.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit



var beaconsUpdatedNotification: Notification.Name {
    get {
        return Notification.Name("beaconsUpdatedNotification")
    }
}

var beaconUpdateBlock: (()->Void)?

extension UIViewController {
    
    
    func subscribeForBeaconsUpdate(_ block: @escaping ()->Void) {
        addBeaconsObserver()
        beaconUpdateBlock = block
    }
    
    @objc private func deliverBeaconNotification() {
        beaconUpdateBlock?()
    }
    
    private func addBeaconsObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.deliverBeaconNotification), name: beaconsUpdatedNotification, object: nil)
    }
}


extension Set where Element: Beacon {
    
    func contains(_ beacon: CLBeacon) -> Bool {
        return self.filter({$0.major == Int(beacon.major) && $0.minor == Int(beacon.minor)}).count != 0
    }
    
    func updateActualBeacons(with located: Set<Beacon>) {
        for beacon in located {
            let updated = beacon
            updated.lastSeenDate = Date()
            BeaconManager.instance.all_beacons.insertSafely(updated)
        }
    }
}

extension Array where Element: CLBeacon {
    func contains(_ beacon: Beacon) -> Bool {
        return self.filter({Int($0.major) == beacon.major && Int($0.minor) == beacon.minor}).count != 0
    }
}

class BeaconManager: NSObject, CLLocationManagerDelegate {
    
    
    static let instance = BeaconManager()
    static let beacons_timeout: Double = 15.0
    
    var cachedBeacons: Set<Beacon> {
        get {
            return Set(CoreDataService.instance.allBeacons())
        }
    }
    var _all_beacons: Set<Beacon> = []
    var all_beacons: Set<Beacon> {
        get {
            if _all_beacons.count == 0 {
                _all_beacons = cachedBeacons
            }
            return _all_beacons
        } set {
            _all_beacons = newValue
        }
    }
    
    
    var activeBeacons: [Beacon] {
        get {
            return all_beacons.filter({$0.isActual})
        }
    }
    
    var isShowingDebugMenu = true
    
    var locationManager: CLLocationManager {
        get {
            return (UIApplication.shared.delegate as! AppDelegate).locationManager
        }
    }
    
    var lastRSSI: Int = 0
    
    var isSearchingStarted = false
    
    func startSearchingBeacons() {
        guard !isSearchingStarted else { return }
        isSearchingStarted = true
        
        if Date().timeIntervalSince1970 - lastBeaconFetch > 60*60*2 {
            CoreDataService.instance.clearBeacons()
            loadBeacons()
        } else {
            self.config_location_manager()
            self.startRangingCachedBeacons()
        }
        
    }
    
    var activatedProximityUUIDs: [UUID] = []
    
    private func startRangingCachedBeacons() {

        for beacon in all_beacons {
            if let uuid = UUID(uuidString: beacon.proximityUUID), !self.activatedProximityUUIDs.contains(uuid) {
                self.activatedProximityUUIDs.append(uuid)
                let reg = CLBeaconRegion(proximityUUID: uuid, identifier: beacon.uuid)
                self.locationManager.startMonitoring(for: reg)
                self.locationManager.startRangingBeacons(in: reg)
            }
        }
    }
    private var lastBeaconFetch: Double {
        get {
            return UserDefaults.standard.double(forKey: "lastBeaconFetch")
        } set {
            UserDefaults.standard.set(newValue, forKey: "lastBeaconFetch")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    private func loadBeacons(page: Int = 0) {

        Server.makeRequest.getAvailableBeacons(page: page) { (beacons, hasNext) in
            var startedUUIDs: [UUID] = []
            for beacon in (beacons) {
                if let beaconUUID: UUID = UUID(uuidString: beacon.proximityUUID), !startedUUIDs.contains(beaconUUID), !self.activatedProximityUUIDs.contains(beaconUUID) {
                    self.activatedProximityUUIDs.append(beaconUUID)
                    startedUUIDs.append(beaconUUID)
                    let beaconRegion: CLBeaconRegion = CLBeaconRegion(proximityUUID: beaconUUID, identifier: beacon.uuid)
                    self.locationManager.startMonitoring(for: beaconRegion)
                    self.locationManager.startRangingBeacons(in: beaconRegion)
                }
                self.all_beacons.insertSafely(beacon)
            }
            if hasNext {
                self.loadBeacons(page: page + 1)
            } else {
                self.lastBeaconFetch = Date().timeIntervalSince1970
                self.config_location_manager()
                self.startRangingCachedBeacons()
            }
        }
    }
    
    func config_location_manager() {
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = false
        if #available(iOS 11.0, *) {
            locationManager.showsBackgroundLocationIndicator = false
        }
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
    }
    
    func sendLocalNotificationWithMessage(_ message: String!) {
        let notification = UILocalNotification()
        notification.alertBody = message
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    var lastUpdateDate: Date = Date(timeIntervalSince1970: 1)
    
    var task: UIBackgroundTaskIdentifier!
    
    func showLocalUINotification(_ text: String, needsSound: Bool = false) {
//        let n = UILocalNotification()
//        n.alertTitle = "Тестируем беконы"
//        n.alertBody = text
//        if needsSound {
//            n.soundName = UILocalNotificationDefaultSoundName
//        }
//
//        UIApplication.shared.presentLocalNotificationNow(n)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if UserDataManager.instance.isTutorialCompletedOnPlaces {
            AppDelegate.config_local_notifications()
        }
    }

    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        if self.task == nil {
            self.task = UIApplication.shared.beginBackgroundTask() {
                UIApplication.shared.endBackgroundTask(self.task!)
                self.task = nil
            }
        }

        if Date().timeIntervalSince1970 - lastUpdateDate.timeIntervalSince1970 > 15 {
            lastUpdateDate = Date()
            for beacon in beacons {
                if let index = self.all_beacons.index(where: {$0.major == beacon.major.intValue && $0.minor == beacon.minor.intValue}) {
                    self.showInAppNotificationIfNeeded(beacon: self.all_beacons[index])
                    self.all_beacons[index].lastSeenDate = Date()
                    self.sendSystemNotification()
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        guard let beaconReg = region as? CLBeaconRegion else { return }
        manager.startRangingBeacons(in: beaconReg)
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        guard let beaconReg = region as? CLBeaconRegion else { return }
        manager.stopRangingBeacons(in: beaconReg)
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        if state == .inside, let reg = region as? CLBeaconRegion {
            locationManager.startRangingBeacons(in: reg)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        pendingLocationRequest?(manager.location?.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(#function, "error: \(error.localizedDescription)")
    }
    
    var shownedNotifications: [String: Date] = [:]
    
    func sendSystemNotification() {
        NotificationCenter.default.post(name: beaconsUpdatedNotification, object: nil)
    }
    
    
    func showInAppNotificationIfNeeded(beacon: Beacon?) {
        guard let uuid = beacon?.uuid else { return }
        
        if Date().timeIntervalSince1970 - (shownedNotifications[uuid]?.timeIntervalSince1970 ?? 0) > beacon_timeout_seconds {
            //проверили шо бекон устарел (либо он новый совсем)
            getPlaceToShowNotification(with: beacon!) { places in
                for place in places {
                    self.showNotification(with: place, beaconUUID: uuid)
                }
            }
        }
    }
    
    
    
    private func getPlaceToShowNotification(with beacon: Beacon, completion: @escaping ([Place])->Void) {
        Server.makeRequest.searchPlaces(by: [beacon], completion: { (places) in
            self.showLocalUINotification("Отправили пассивный чекин для бекона \(places.first?.name ?? "без места")", needsSound: true)
            if self.task != nil {
                UIApplication.shared.endBackgroundTask(self.task!)
            }
            
            completion(places)
        })
    }
    
    private func showNotification(with p: Place?, beaconUUID uuid: String) {
        guard let place = p else { return }
        
        guard !Server.makeRequest.notificatedPlacesUUIDsToday.contains(place.uuid) else { return }
        
//        guard shownedNotifications[uuid] == nil else { return }
        
        if Date().timeIntervalSince1970 - (Array(shownedNotifications.values).max()?.timeIntervalSince1970 ?? 0) >= 7 { //проверили шо  с последнего уведомления прошло 10с
            self.shownedNotifications[uuid] = Date()

            async {
                Server.makeRequest.notificatedPlacesUUIDsToday.insert(place.uuid)
                print("notification: place sent to NotificationsManager \(place.name)")
                NotificationsManager.shared.showNotification(with: place)
                NotificationsManager.shared.postPlaceUpdatedNotifications()
            }
        } else {
            print("notification: delayed to 5 seconds \(place.name)")

            //если с последнего увеедомления не прошло 10с - пробуем заново через 5с
            async(after: 1000, {

                self.showNotification(with: p, beaconUUID: uuid)
            })
        }
    }
    
    func nearPlacesHadBeenShown() {
        for beacon in self.activeBeacons {
            self.shownedNotifications[beacon.uuid] = Date()
        }
        let activeUUIDs: Set<String> = Set(Server.makeRequest.activePlaces.map({$0.uuid}))
        Server.makeRequest.viewedPlacesUUIDsToday = Server.makeRequest.viewedPlacesUUIDsToday.union(activeUUIDs)
    }
    
    
    func show_debug_menu_if_needed(_ message: String="",_ major: NSNumber?=0, _ minor: NSNumber?=0) {
        if isShowingDebugMenu {
            self.showDebug(message, CGFloat(major ?? 0), CGFloat(minor ?? 0))
        } else {
            self.removeDebug()
        }
    }
    
    private var pendingLocationRequest: ((CLLocationCoordinate2D?)->Void)? = nil
    
    func startTrackingLocation(completion: @escaping (CLLocationCoordinate2D?)->Void) {
        if locationManager.delegate == nil {
            locationManager.delegate = self
            locationManager.activityType = .otherNavigation
        }
        pendingLocationRequest = completion
        pendingLocationRequest?(locationManager.location?.coordinate)
        locationManager.startUpdatingLocation()
    }
    
    func stopLocationRequests() {
        locationManager.stopUpdatingLocation()
        pendingLocationRequest = nil
    }
    
    private func showDebug(_ message: String, _ major: CGFloat, _ minor: CGFloat) {
        self.removeDebug()
        if let root = UIApplication.shared.keyWindow?.rootViewController {
            let label = UILabel(frame: CGRect(x: 0, y: 20, width: root.view.frame.size.width , height: 44))
            label.font = UIFont.systemFont(ofSize: 14)
            label.textAlignment = .center
            label.numberOfLines = 0
            label.backgroundColor = UIColor(red: major/65535, green: minor/65535, blue: (major+minor)/131070, alpha: 0.75)
            label.text = message
            label.tag = 404
            root.view.addSubview(label)
        }
    }
    
    private func removeDebug() {
        UIApplication.shared.keyWindow?.rootViewController?.view.viewWithTag(404)?.removeFromSuperview()
    }
    
    
}


class NotificationButton: ResizableButton {
    var place: Place?
}


extension Set where Element: Beacon {
    mutating func insertSafely(_ newElement: Element) {
        if let index = self.index(where: {$0.uuid == newElement.uuid}) {
            self[index].updateWithOther(newElement)
        } else {
            self.insert(newElement)
        }
    }
}
