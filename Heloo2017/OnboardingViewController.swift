//
//  OnboardingViewController.swift
//  Heloo2017
//
//  Created by Константин on 19.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    typealias item = (title: String, body: String, image: UIImage)
    
    var items: [item] = [
        (title: "Люди", body: "Sleki найдет людей по интересам\rв вашем городе или совсем рядом\rв радиусе 1 км., достаточно включить\rGPS, Wifi или Bluetooth", image: #imageLiteral(resourceName: "onboarding_0.png")),
        (title: "Места", body: "Подборка мест исключительно\rпо вашим интересами", image: #imageLiteral(resourceName: "onboarding_1.png")),
        (title: "Чекины", body: "Делайте чекины в местах и получайте\rскидки, пусть друзья будут в курсе,\rгде вы и как проводите время", image: #imageLiteral(resourceName: "onboarding_2.png")),
        (title: "События и акции", body: "Подборка акций и событий\rв вашем городе", image: #imageLiteral(resourceName: "onboarding_3.png")),
        (title: "Лента", body: "Будьте в курсе новых чекинов\rи событий от ваших друзей", image: #imageLiteral(resourceName: "onboarding_4.png"))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollection()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    @IBAction func start(_ sender: UIButton) {
        if pageControl.currentPage >= pageControl.numberOfPages - 1 {
            UserDataManager.instance.isOnboardingCompleted = true
            let main = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController()!
            UIView.transition(from: self.view, to: main.view, duration: 0.5, options: .transitionFlipFromRight, completion: { _ in
                UIApplication.shared.keyWindow?.rootViewController = main
            })
        } else {
            self.view.isUserInteractionEnabled = false
            collectionView.scrollToItem(at: IndexPath(item: pageControl.currentPage + 1, section: 0), at: .centeredHorizontally, animated: true)
            async(after: 300) {
                self.view.isUserInteractionEnabled = true
            }
        }
    }
}

extension OnboardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func configureCollection() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OnboardingCell
        cell.update(with: items[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.view.bounds.size
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
    }
    
}
