//
//  WeekdaysStack.swift
//  Heloo2017
//
//  Created by Константин on 09/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class WeekdaysStack: UIStackView {
    
    @IBOutlet weak var mondayImage: UIImageView!
    @IBOutlet weak var tuesdayImage: UIImageView!
    @IBOutlet weak var wensdayImage: UIImageView!
    @IBOutlet weak var thursdayImage: UIImageView!
    @IBOutlet weak var fridaydayImage: UIImageView!
    @IBOutlet weak var saturdayImage: UIImageView!
    @IBOutlet weak var sundayImage: UIImageView!

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    func update(item: WeekActivityItems?) {
        guard let item = item else { heightConstraint.constant = 0; self.alpha = 0; return }
        
        let activeCount = item.items.filter({$0.active}).count
        heightConstraint.constant = activeCount % 7 == 0 ? 0 : 36
        self.alpha = activeCount % 7 == 0 ? 0 : 1
        self.subviews.forEach({ $0.alpha = activeCount % 7 == 0 ? 0 : 1 })
        
        for (index, image) in [mondayImage, tuesdayImage, wensdayImage, thursdayImage, fridaydayImage, saturdayImage, sundayImage].enumerated() {
            image?.isHidden = item.items.first(where: {$0.day_of_week == index})?.active != true
        }
    }
}
