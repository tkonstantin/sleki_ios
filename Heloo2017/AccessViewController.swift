//
//  AccessViewController.swift
//  Heloo2017
//
//  Created by Константин on 30.06.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class AccessViewController: UIViewController {

    @IBAction func login(_ sender: ResizableButton) {
        self.dismiss(animated: true, completion: {
            Server.makeRequest.logout {
                UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "SplashScreen", bundle: nil).instantiateInitialViewController()
            }
        })
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
}
