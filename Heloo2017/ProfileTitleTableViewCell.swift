//
//  ProfileTitleTableViewCell.swift
//  Heloo2017
//
//  Created by Константин on 21.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ProfileTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var isOpenedLabel: UILabel!
    
    
    func update(with place: Place) {
        titleLabel.text = place.name
        if place.isOpen == nil {
            isOpenedLabel.text = nil
        } else {
            isOpenedLabel.text = place.isOpen! ? "СЕЙЧАС ОТКРЫТО" : "СЕЙЧАС ЗАКРЫТО"
        }
    }

}
