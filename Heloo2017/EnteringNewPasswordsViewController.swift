//
//  EnteringNewPasswordsViewController.swift
//  Heloo2017
//
//  Created by Константин on 02.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class EnteringNewPasswordsViewController: UIViewController {
    
    
    typealias resettingData = (pass1: String, pass2: String, token: String)
    static func present(token: String, from: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) {
        let new = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "EnteringNewPasswordsViewController") as! EnteringNewPasswordsViewController
        new.data.token = token
        from?.present(new, animated: true, completion: nil)
    }
    
    var data: resettingData = (pass1: "", pass2: "", token: "")
    
    @IBOutlet var defaultConstraints: [NSLayoutConstraint]!
    @IBOutlet var typingConstraings: [NSLayoutConstraint]!
    @IBOutlet weak var pass_confirm_field: UITextField!
    @IBOutlet weak var pass_field: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [pass_field, pass_confirm_field].forEach({$0?.delegate = self})
        self.addTapOutsideGestureRecognizer()
        self.subscribeToKeyboard()
        
    }
    override func keyboardWillBeShown(_ notification: Notification) {
        self.activateTypingConstraints()
    }
    
    override func keyboardWillBeHidden() {
        self.activateDefaultConstrains()
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func next(_ sender: UIButton) {
        if checkData() {
            
            Server.makeRequest.passwordRecovery_complete(pass: data.pass1, token: data.token, completion: { (success) in
                if success {
                    self.dismiss(animated: true, completion: {
                        async {
                            UIApplication.goMain()
                            BeaconManager.instance.startSearchingBeacons()
                            WAPManager.instance.startScanning()
                            ChattingService.shared.getChats { }


                        }
                    })
                } else {
                    self.dismiss(animated: true, completion: {
                        async {
                            ErrorPopupManager.showError(type: .unknown, from: self)
                        }
                    })

                }
            })
        }
    }
    
    private func checkData() -> Bool {
        
        if data.pass1.isEmpty || data.pass2.isEmpty {
            ErrorPopupManager.showError(type: .fields, from: self)
            return false
        } else if data.pass1 != data.pass2 {
            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: "Пароли не совпадают")
            return false
        } else if data.pass1.count < 8 {
            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: "Пароль короче 8 символов")
        }
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    let keyboardAnimationDuration: Double = 0.25
    
    func activateDefaultConstrains() {
        defaultConstraints.forEach({$0.isActive = true})
        typingConstraings.forEach({$0.isActive = false})
        layoutAnimate()
        
    }
    
    func activateTypingConstraints() {
        defaultConstraints.forEach({$0.isActive = false})
        typingConstraings.forEach({$0.isActive = true})
        
        layoutAnimate()
    }
    
    func layoutAnimate() {
        UIView.animate(withDuration: keyboardAnimationDuration*2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
}

extension EnteringNewPasswordsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let next = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            next.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        switch textField.tag {
        case 2: data.pass1 = newString
        default: data.pass2 = newString
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 2: data.pass1 = textField.text ?? ""
        default: data.pass2 = textField.text ?? ""
        }
    }
}

