//
//  ErrorHandler.swift
//  Heloo2017
//
//  Created by Константин on 16.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire



class ErrorHandler {
    static let instance = ErrorHandler()
    
    func handleError(with response: DataResponse<Any>, completion: ((Bool, Bool)->Void)?=nil) {
        guard response.response?.statusCode != 404 else { completion?(true, false); return }
        if let status = response.response?.statusCode, status == 401 {
            Server.makeRequest.refreshToken(completion: { (success) in
                if !success {
                    Presenter.instance.showErrorAlert("Авторизация нарушена", message: "Необходимо войти заново", _completion: {
                        Server.makeRequest.logout {
                            UIApplication.shared.setStatusBarHidden(true, with: .fade)
                            UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "SplashScreen", bundle: nil).instantiateInitialViewController()
                            completion?(false, false)
                        }
                    })
                } else {
                    completion?(false, true)
                }
            })
            
            //       } else  if let error = response.result.error {
            //        Presenter.instance.showErrorAlert(nil, message: error.localizedDescription)
            //        completion?(false)
        } else {
            completion?(true, false)
        }
    }
}

