//
//  Searchable.swift
//  Heloo2017
//
//  Created by Константин on 09.05.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class Searchable: NSObject {
    var sortDate: Date?
    
    var user: User?
    var place: Place?
    var offer: Offer?
    var checkin: Checkin?
    var event: Event?
    var accept: Accept?
    
    
    var uuid: String? {
        get {
            if user != nil {
                return user?.uuid
            } else if place != nil {
                return place?.uuid
            } else if offer != nil {
                return offer?.id
            } else if checkin !=  nil {
                return checkin?.id
            }  else if event != nil {
                return event?.id
            } else if accept != nil {
                return accept?.id
            }
            return nil
        }
    }
    
    var isMine: Bool {
        get {
            if let _user = user {
                return UserDataManager.instance.isMe(_user)
            } else if let _user = checkin?.user {
                return UserDataManager.instance.isMe(_user)
            } else if let _user = accept?.user {
                return UserDataManager.instance.isMe(_user)
            } else {
                return false
            }
        }
    }
    
    required init?(user _user: User?=nil, place _place: Place?=nil, offer _offer: Offer?=nil, checkin _checkin: Checkin?=nil, event _event: Event?=nil, accept _accept: Accept?=nil) {
        if _user != nil {
            self.user = _user
        } else if _place != nil {
            self.place = _place
        } else if _offer != nil {
            self.offer = _offer
            self.sortDate = _offer!.start
        } else if _checkin != nil {
            self.checkin = _checkin
            self.sortDate = _checkin!.date
        } else if _event != nil {
            self.event = _event
        } else if _accept != nil {
            self.accept = _accept
            self.sortDate = _accept!.date
        } else {
            return nil
        }
    }
}
