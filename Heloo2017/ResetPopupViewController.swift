
//
//  ResetPopupViewController.swift
//  Heloo2017
//
//  Created by Константин on 09.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ResetPopupViewController: UIViewController {

    @IBAction func ok_tapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    

}
