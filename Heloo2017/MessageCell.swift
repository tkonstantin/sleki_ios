//
//  MessageCell.swift
//  Heloo2017
//
//  Created by Константин on 18.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import Foundation
import SafariServices
import MessageUI

class MessageCell: UITableViewCell {
    static let text_incoming_id = "Message_Text_Incoming"
    static let text_id = "Message_Text"
    
    @IBOutlet weak var text_label: UILabel!
    @IBOutlet weak var date_label: UILabel!
    @IBOutlet weak var bubbleView: UIView!
    
    var shape: CAShapeLayer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.transform = CGAffineTransform(rotationAngle: .pi)
    }
    
    lazy var longPressGR: UILongPressGestureRecognizer = {
        let gr = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed))
        self.bubbleView.addGestureRecognizer(gr)
        return gr
    }()
    
    var longPressStarted = false
    
    @objc private func longPressed(_ sender: UILongPressGestureRecognizer) {
        if longPressStarted {
            longPressStarted = sender.state.rawValue <= 2
            return;
        } else  {
            longPressStarted = sender.state.rawValue <= 2
        }
        
        vibrateFeedback()

        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let text = text_label.attributedText?.string ?? ""
        
        if let range = text.emailRanges().first(where: {self.text_label.boundingRectForCharacterRange($0).extended.contains(sender.location(in: self.text_label))}) {
            menu.title = self.text_label.string(range)
            menu.addAction(UIAlertAction(title: "Скопировать Email", style: .default, handler: { (_) in
                UIPasteboard.general.string = self.text_label.string(range)
            }))
            menu.addAction(UIAlertAction(title: "Открыть Email", style: .default, handler: { (_) in
                _ = self.process(email: range)
            }))
        } else if let range = text.linksRanges().first(where: { self.text_label.boundingRectForCharacterRange($0).extended.contains(sender.location(in: self.text_label))}) {
            menu.title = self.text_label.string(range)
            menu.addAction(UIAlertAction(title: "Скопировать ссылку", style: .default, handler: { (_) in
                UIPasteboard.general.string = self.text_label.string(range)
            }))
            menu.addAction(UIAlertAction(title: "Открыть ссылку", style: .default, handler: { (_) in
                _ = self.process(link: range)
            }))
        } else  if let range = text.phoneRanges().first(where: {self.text_label.boundingRectForCharacterRange($0).extended.contains(sender.location(in: self.text_label))}) {
            menu.title = self.text_label.string(range)
            menu.addAction(UIAlertAction(title: "Скопировать телефон", style: .default, handler: { (_) in
                UIPasteboard.general.string = self.text_label.string(range)
            }))
            menu.addAction(UIAlertAction(title: "Позвонить", style: .default, handler: { (_) in
                _ = self.process(phone: range)
            }))
        } else {
            menu.addAction(UIAlertAction(title: "Скопировать", style: .default, handler: { (_) in
                UIPasteboard.general.string = self.text_label.text
            }))
        }
        menu.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        rootNav?.topViewController?.present(menu, animated: true, completion: nil)
    }
    
    func update(with message: Message, needsTail: Bool = false) {
        _ = longPressGR
        
        let mutable = NSMutableAttributedString(string: message.text)
        (message.text.linksRanges() + message.text.emailRanges() + message.text.phoneRanges()).forEach({mutable.addAttributes([NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue], range: $0)})
        self.text_label?.attributedText = mutable
        
        date_label.text = message.date.parseTime()
        
        if !needsTail {
            self.shape?.removeFromSuperlayer()
            return;
        }
                
        async(after: 50) { [weak self] in
            guard let `self` = self else { return }
            
            if self.shape == nil {
                self.shape = CAShapeLayer()
                self.shape.fillColor = self.bubbleView.backgroundColor!.cgColor

            }
            let path = UIBezierPath()
            
            let miltiplierX: CGFloat = message.isIncoming ? -1 : 1
            
            let x = message.isIncoming ? self.bubbleView.frame.minX : self.bubbleView.frame.maxX
            let maxY = self.bubbleView.frame.maxY
            
            let startPoint = CGPoint(x: x - 3*miltiplierX, y: maxY - 4)
            
            path.move(to: startPoint)
            path.addQuadCurve(to: CGPoint(x: x + 8*miltiplierX, y: maxY), controlPoint: CGPoint(x: x, y: maxY - 1))
            path.addQuadCurve(to: CGPoint(x: x, y: maxY - 12), controlPoint: CGPoint(x: x + 2*miltiplierX, y: maxY - 4))
            path.addLine(to: startPoint)
            path.close()
            
            self.shape.path = path.cgPath
            
            if self.shape.superlayer == nil {
                self.layer.addSublayer(self.shape)
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        guard let touch = touches.first else { return }
        let location = touch.location(in: self.text_label)
        
        if MFMailComposeViewController.canSendMail() {
            for range in (text_label.attributedText?.string.emailRanges() ?? []) {
                if self.text_label.boundingRectForCharacterRange(range).extended.contains(location) {
                    if process(email: range) {
                        return
                    }
                }
            }
        }
        
        for range in (text_label.attributedText?.string.linksRanges() ?? []) {
            if self.text_label.boundingRectForCharacterRange(range).extended.contains(location) {
                if process(link: range) {
                    return
                }
            }
        }
        
        for range in (text_label.attributedText?.string.phoneRanges() ?? []) {
            if self.text_label.boundingRectForCharacterRange(range).extended.contains(location) {
                if process(phone: range) {
                    return
                }
            }
        }
        
        
    }
    
    
    func process(link range: NSRange) -> Bool {
        let linkString = text_label.string(range).prepareLink()
        if let url = URL(string: linkString) {
            let sf = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            rootNav?.topController()?.present(sf, animated: true, completion: nil)
            return true
        }
        return false
    }
    
    func process(email range: NSRange) -> Bool {
        if !text_label.string(range).isValidEmail  {
            return false
        }
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients([text_label.string(range)])
        rootNav?.topController()?.present(mail, animated: true, completion: nil)
        return true
    }
    
    func process(phone range: NSRange) -> Bool {
        if let url = URL(string: "tel://" + text_label.string(range).filter({ "0123456789".contains($0) })) {
            UIApplication.shared.openURL(url)
            return true
        }
        return false
    }
}

extension MessageCell: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


extension UILabel {
    func boundingRectForCharacterRange(_ range: NSRange?=nil) -> CGRect {
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = self.textAlignment
        let attributed = NSAttributedString(string: self.attributedText?.string ?? self.text ?? "", attributes: [
            NSFontAttributeName: self.font!,
            NSParagraphStyleAttributeName: paragraph
            ])
        
        let textStorage = NSTextStorage.init(attributedString: attributed)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer.init(size: self.bounds.size)
        textContainer.lineFragmentPadding = 0
        
        layoutManager.addTextContainer(textContainer)
        return layoutManager.boundingRect(forGlyphRange: range ?? NSMakeRange((self.text ?? "").count-1, 1), in: textContainer)
    }
    
    func string(_ range: NSRange) -> String {
        return ((self.attributedText?.string ?? "") as NSString).substring(with: range)
    }
}

extension String {
    func linksRanges() -> [NSRange] {
        guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return [] }
        let link_matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count))
        return link_matches.map({$0.range})
    }
    
    func phoneRanges() -> [NSRange] {
        guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue) else { return [] }
        let link_matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count))
        return link_matches.map({$0.range})
    }
    
    func emailRanges() -> [NSRange] {
        guard let expression = try? NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: [.caseInsensitive]) else { return [] }
        let matches = expression.matches(in: self, options: [], range: NSMakeRange(0, self.count))
        return matches.map({$0.range})
    }

    
    func prepareLink() -> String {
        if !self.lowercased().hasPrefix("http") && !self.isValidEmail {
            return "http://" + self
        } else {
            return self
        }
    }
}

extension CGRect {
    fileprivate var extended: CGRect {
        get {
            return self.insetBy(dx: -4, dy: -2)
        }
    }
}

