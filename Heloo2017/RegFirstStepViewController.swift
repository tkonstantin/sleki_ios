//
//  RegFirstStepViewController.swift
//  Heloo2017
//
//  Created by Константин on 09.08.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

typealias regFirstStep = (email: String, pass: String)



class RegFirstStepViewController: UIViewController {
    

    
    
    fileprivate var pass_confirm: String = ""
    
    
    var data: regFirstStep = (email: "", pass: "")

    @IBOutlet var defaultConstraints: [NSLayoutConstraint]!
    @IBOutlet var typingConstraings: [NSLayoutConstraint]!
    @IBOutlet weak var pass_confirm_field: UITextField!
    @IBOutlet weak var pass_field: UITextField!
    @IBOutlet weak var email_field: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        [email_field, pass_field, pass_confirm_field].forEach({$0?.delegate = self})
        self.addTapOutsideGestureRecognizer()
        self.subscribeToKeyboard()
    }
    
    
    override func keyboardWillBeShown(_ notification: Notification) {
        self.activateTypingConstraints()
    }
    
    override func keyboardWillBeHidden() {
        self.activateDefaultConstrains()
    }

    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func next(_ sender: UIButton) {
        self.view.endEditing(true)
        if checkData() {
        Server.makeRequest.reg(email: data.email, pass: data.pass, completion: { (success, errorMessage) in
                if success {
                    sender.isHidden = true
                    self.performSegue(withIdentifier: "nextStep", sender: nil)
                } else {
                    async {
                        if let message = errorMessage {
                            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: message)
                        } else {
                            ErrorPopupManager.showError(type: .unknown, from: self)
                        }
                    }
                }
            })
        }
    }
    
    private func checkData() -> Bool {
        
        if data.email.isEmpty || data.pass.isEmpty || pass_confirm.isEmpty {
            ErrorPopupManager.showError(type: .fields, from: self)
            return false
        } else if data.pass != pass_confirm {
            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: "Пароли не совпадают")
            return false
        } else if !data.email.isValidEmail {
            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: "Некорректный Email")
            return false
        } else if data.pass.count < 8 {
            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: "Пароль короче 8 символов")
            return false
//        } else if data.pass.filter({"ABCDEFGHIJKLMNOPQRSTUVWXYZ".contains($0)}).count == 0 {
//            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: "Пароль должен содержать заглавные буквы")
//            return false
        } else if data.pass.filter({"0123456789".contains($0)}).count == 0 {
            ErrorPopupManager.showError(type: .fields_custom, from: self, subtitle: "Пароль должен содержать цифры")
            return false
        }
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    let keyboardAnimationDuration: Double = 0.25
    
    func activateDefaultConstrains() {
        defaultConstraints.forEach({$0.isActive = true})
        typingConstraings.forEach({$0.isActive = false})
        layoutAnimate()

    }
    
    func activateTypingConstraints() {
        defaultConstraints.forEach({$0.isActive = false})
        typingConstraings.forEach({$0.isActive = true})
        
        layoutAnimate()
    }
    
    func layoutAnimate() {
        UIView.animate(withDuration: keyboardAnimationDuration*2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: .curveLinear, animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
}

extension RegFirstStepViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let next = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            next.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        switch textField.tag {
        case 1: data.email = newString
        case 2: data.pass = newString
        default: pass_confirm = newString
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1: data.email = textField.text ?? ""
        case 2: data.pass = textField.text ?? ""
        default: pass_confirm = textField.text ?? ""
        }
    }
}
