//
//  OnboardingCell.swift
//  Heloo2017
//
//  Created by Константин on 19.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class OnboardingCell: UICollectionViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!


    func update(with item: OnboardingViewController.item) {
        icon.image = item.image
        title.text = item.title
        body.text = item.body
    }
}
