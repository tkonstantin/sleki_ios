//
//  Server+Files.swift
//  Heloo2017
//
//  Created by Константин on 14.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire


extension Server {
    
    enum fileTypes {
        case jpg
        case data
    }
    
    enum targetType {
        case profile
        case profile_small
        case checkin
    }
    
    
    
    func compress(_ file: Any, for type: targetType) -> Data? {
        var data: Data?
        if type == .profile {
            let fullsizeData = NSData(data: UIImageJPEGRepresentation(file as! UIImage, 1)!)
            if fullsizeData.length < 1900000 {
                data = fullsizeData as Data
            } else {
                let scale = 1900000/CGFloat(fullsizeData.length)
                data = UIImageJPEGRepresentation(file as! UIImage, scale)
            }
            return data
        } else {
            guard let image = file as? UIImage else { return nil }
            let width = image.size.width
            if width <= 1000 {
                return UIImageJPEGRepresentation(file as! UIImage, 1)
            } else {
                return UIImageJPEGRepresentation(file as! UIImage, 1000/width)
            }
            
        }
    }
    
    func fileUpload(file _file: Any?, type: fileTypes = .jpg, target: targetType, checkin_id: String?=nil, completion: @escaping (String?, String?)->Void) {
        guard let file = _file else { print("ERROR: uploading file is nil!"); completion(nil, "Ошибка загрузки файла: файл отсутствует"); return }
        guard target != .checkin || checkin_id != nil else { print("ERROR: fileUpload type == checkin but chekin_id is nil"); completion(nil, "Неизвестная ошибка (checkinid is empty)"); return }
        
        let url: String = {
            switch target {
            case .profile: return url_paths.base + url_paths.profile.imageUpload
            case .profile_small: return url_paths.base + url_paths.profile.imageSmallUpload
            case .checkin: return url_paths.base + String(format: url_paths.checkin.imageUpload, checkin_id!)
            }
        }()
        
        
        var data: Data?
        switch type {
        case .jpg: data = compress(file, for: target)
        case .data: data = file as? Data
        }
        
        guard let uploadingData = data else { print("ERROR: file cannot be converted to jpg or png!"); completion(nil, "Ошибка конвертации файла"); return }
        
        
        Alamofire.upload(multipartFormData: { (formData) in
            formData.append(uploadingData, withName: "file", fileName: "file", mimeType: type == .jpg ? "image/jpeg" : "multipart/form-data")
        }, to: url, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let request, _,  _) :
                request.responseJSON(completionHandler: { (response) in
                    if let new_url = response.response?.allHeaderFields["Location"] as? String {
                        completion(new_url, nil)
                    } else {
                        completion(nil, response.json?["errorMessage"] as? String)
                    }
                })
            case .failure(let error) :
                print("ERROR: Alamofire.upload failed with eror \(error.localizedDescription)")
            }
        })
        
    }
    
}
