//
//  Server+Chat.swift
//  Heloo2017
//
//  Created by Константин on 19.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire


extension Server {
    
    func loadChats(page: Int, completion: @escaping ([Chat], Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.chats.main, method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            DispatchQueue.main.async {
                var chats: [Chat] = []
                
                if let json = response.json, let items = json["items"] as? [NSDictionary] {
                    for item in items {
                        if let chat = Chat(with: item) {
                            chats.append(chat)
                        }
                    }
                }
                
                    CoreDataService.instance.save()
                    completion(chats, response.json?["hasNext"] as? Bool ?? false)

            }
            
        }
    }
    
    func loadAllChats(completion: @escaping ()->Void) {
        
        func loadPages(_ page: Int = 0, completion: @escaping ()->Void) {
            Server.makeRequest.loadChats(page: page) { (_, hasNext) in
                if hasNext {
                    loadPages(page + 1, completion: completion)
                } else {
                    completion()
                }
            }
        }
        
        loadPages(completion: completion)
    }
    
    func initializeChat(to user: User, completion: @escaping (Chat?)->Void) {
        Alamofire.request(url_paths.base + String(format: url_paths.chats.initializeWithPerson, user.uuid), method: .post, parameters: [:], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print("initializeChat response = \(response)")
            if let json = response.result.value as? NSDictionary {
                completion(Chat(with: json))
            } else {
                completion(nil)
            }
        }
    }
    
    func sendMessage(_ message: Message, to chat: Chat, completion: @escaping (Message?)->Void) {
        var message_params = [String: Any]()
        message_params["text"] = message.text
        Alamofire.request(url_paths.base + String(format: url_paths.chats.messages, chat.uuid), method: .post, parameters: message_params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            DispatchQueue.main.async {
                var message: Message?
                if let json = response.result.value as? NSDictionary {
                    message = Message.makeMessage(with: json, in: chat)
                }
                CoreDataService.instance.save()
                completion(message)
            }
        }
    }
    
    func getMessages(from chat: Chat, after afterMessage: Message?=nil, before beforeMessage: Message?=nil, page: Int = 0, completion: @escaping ([Message], Bool)->Void) {
        var params: [String: Any] = [:]
        if afterMessage != nil {
            params["after_message"] = afterMessage!.uuid
        } else if beforeMessage != nil {
            params["before_message"] = beforeMessage!.uuid
            params["page"] = page
            params["size"] = 50
        } else {
            params["page"] = page
            params["size"] = 50
        }
        
        Alamofire.request(url_paths.base + String(format: url_paths.chats.messages, chat.uuid), method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            DispatchQueue.main.async {
                var messages: [Message] = []
                if let json = response.result.value as? NSDictionary {
                    
                    for item in (json["items"] as? [NSDictionary] ?? []) {
                        if let message = Message.makeMessage(with: item, in: chat) {
                            messages.append(message)
                        }
                    }
                }
                if messages.count != 0 {
                    CoreDataService.instance.save()
                }
                completion(messages,  response.json?["hasNext"] as? Bool ?? false)
            }

        }
    }
    
    func clearChat(_ chat: Chat, completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base + String(format: url_paths.chats.messages, chat.uuid), method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in

            let success = (response.response?.statusCode ?? 0) / 100 == 2
            if !success {
                completion(success)
                return
            } else {
                DispatchQueue.main.async {
                    let msgCount = chat.messages.count
                    for index in 0..<chat.messages.count {
                        CoreDataService.instance.privateContext.delete(chat.messages.first!)
                        chat.messages.removeFirst()
                        if index == msgCount - 1 {
                            CoreDataService.instance.privateContext.delete(chat)
                            CoreDataService.instance.save()
                            completion(success)
                        }
                    }

                }
            }
        }
    }
    
    func deleteMessage(_ message: Message, from chat: Chat, completion: @escaping (Bool)->Void) {
        
    }
    
    
}
