//
//  DoneViewController.swift
//  Heloo2017
//
//  Created by Константин on 22.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class DoneViewController: UIViewController {

    class func presentOne(with comment: String, completion: (()->Void)?=nil) {
        async {
            let new = UIStoryboard(name: "DoneViewController", bundle: nil).instantiateInitialViewController() as! DoneViewController
            new.modalPresentationStyle = .overCurrentContext
            new.modalTransitionStyle = .crossDissolve
            new.comment = comment
            new.completionBlock = completion
            UIApplication.shared.keyWindow?.rootViewController?.present(new, animated: true, completion: nil)
        }
    }
    
    var comment: String = ""
    
    var completionBlock:  (()->Void)?
    
    @IBOutlet weak var commentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentLabel.text = comment
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.perform(#selector(self.dismissMe), with: nil, afterDelay: 1)
    }
    
    
    func dismissMe() {
        self.dismiss(animated: true, completion: {_ in
            self.completionBlock?()
        })
    }
    
    @IBAction func tapped(_ sender: UITapGestureRecognizer) {
        dismissMe()
    }
}
