//
//  TabBarTransitioning.swift
//  SnowQueen
//
//  Created by Константин on 28.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

protocol TabBarTransitioningDelegate: class {
    func directionIsRight(from: UIViewController, to: UIViewController) -> Bool
}

class TabBarTransitioningAnimated: NSObject, UIViewControllerAnimatedTransitioning {
    

    weak var delegate: TabBarTransitioningDelegate?
    
    
    init(delegate d: TabBarTransitioningDelegate) {
        delegate = d
    }
    
    let duration: Double = 0.4
    
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard let from = transitionContext.viewController(forKey: .from) else { return }
        guard let to = transitionContext.viewController(forKey: .to) else { return }
        let animateR2L = delegate?.directionIsRight(from: from, to: to) ?? true
        
        to.view.transform = CGAffineTransform(translationX: (animateR2L ? 1 : -1)*UIScreen.main.bounds.width, y: 0)
        transitionContext.containerView.backgroundColor = .white
        
        transitionContext.containerView.addSubview(to.view)
        
//        UIView.transition(from: from.view, to: to.view, duration: duration, options: .transitionCrossDissolve)
        
        defaultSpringAnimation(duration: duration, usingSpringWithDamping: 1, animations: {
            to.view.transform = .identity
            from.view.transform = CGAffineTransform(translationX: (animateR2L ? -1 : 1)*UIScreen.main.bounds.width, y: 0)
        }, completion: {
            from.view.transform = .identity
            transitionContext.completeTransition(true)
        })
        
    }
}
