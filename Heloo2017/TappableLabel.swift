//
//  TappableLabel.swift
//  Heloo2017
//
//  Created by Константин on 05.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices

extension String {
    
    var url: URL? {
        get {
            var linkString = self
            if !linkString.hasPrefix("http") {
                linkString = "http://" + linkString
            }
            return URL(string: linkString)
        }
    }
}

class TappableLabel: UILabel {
    
    enum type {
        case phone
        case email
        case link
    }
    
    var currentType: type  {
        get {
            guard let txt = self.text?.trimmingCharacters(in: .whitespacesAndNewlines), !txt.isEmpty else { return .link }
            
            if txt.isValidEmail {
                return .email
            } else if isPhone(txt) {
                return .phone
            } else {
                return .link
            }
        }
    }
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        self.isUserInteractionEnabled = true
        
        let gr = UITapGestureRecognizer(target: self, action: #selector(self.tapped))
        gr.numberOfTapsRequired = 1
        gr.numberOfTouchesRequired = 1
        gr.cancelsTouchesInView = true
        self.addGestureRecognizer(gr)
    }
    
    override func removeFromSuperview() {
        self.gestureRecognizers?.forEach({self.removeGestureRecognizer($0)})
        super.removeFromSuperview()
    }

    
    func makeAction() {
        guard let txt = self.text?.trimmingCharacters(in: .whitespacesAndNewlines), !txt.isEmpty else { return }
        switch currentType {
        case .email:
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
            mailVC.setToRecipients([txt])
            rootNav?.present(mailVC, animated: true, completion: nil)
            
        case .phone:
            let rawPhone = String(txt.filter({"0123456789".contains($0)}))
            let phoneLink = "tel://\(rawPhone)"

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: phoneLink)!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(URL(string: phoneLink)!)
            }
        case .link:
            guard let link = txt.url else { return }
            let sf = SFSafariViewController(url: link)
            rootNav?.present(sf, animated: true, completion: nil)
        }
    }
    
    func tapped() {
        self.makeAction()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.tapped()
    }

    
    func isPhone(_ txt: String) -> Bool {
        guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue) else { return false }
        let matches = detector.matches(in: txt, options: [], range: NSRange(location: 0, length: txt.utf16.count))
        return matches.count != 0
    }
    
}

extension TappableLabel: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
