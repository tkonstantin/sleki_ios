//
//  Server+Subscriptions.swift
//  Heloo2017
//
//  Created by Константин on 16.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire


extension Server {
    func subscribeToPerson(uuid: String, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        Alamofire.request(url_paths.base + url_paths.subscriptions.main, method: .post, parameters: ["uuid": uuid], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(true)
        }
    }
    
    
    func unsibscribeFromPerson(uuid: String, completion: @escaping (Bool)->Void) {
        guard UserDataManager.isAuthorized else {
            ErrorPopupManager.showError(type: .access)
            completion(false)
            return
        }
        guard !uuid.isEmpty else { completion(false); return }
        Alamofire.request(url_paths.base + url_paths.subscriptions.main + "/\(uuid)", method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion(true)
        }
    }
    
    func getSubscriptions(page: Int, completion: @escaping ([User], Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.subscriptions.main, method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var result: [User] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    result.append(User(with: item))
                }
            }
            completion(result, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    func getSubscribers(page: Int, completion: @escaping ([User], Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.subscribers.main, method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var result: [User] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    result.append(User(with: item))
                }
            }
            completion(result, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    
    func getSubscriptions(uuid: String, page: Int, completion: @escaping ([User], Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.persons.main + "/" + uuid + url_paths.persons.subscriptions, method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var result: [User] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    result.append(User(with: item))
                }
            }
            completion(result, response.json?["hasNext"] as? Bool ?? false)
        }
    }
    
    func getSubscribers(uuid: String, page: Int, completion: @escaping ([User], Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.persons.main + "/" + uuid + url_paths.persons.subscribers, method: .get, parameters: ["page": page, "size": Server.defaultPageSize], encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var result: [User] = []
            if let json = response.json, let items = json["items"] as? [NSDictionary] {
                for item in items {
                    result.append(User(with: item))
                }
            }
            completion(result, response.json?["hasNext"] as? Bool ?? false)
        }
    }
}

