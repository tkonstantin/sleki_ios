//
//  UsernameLabel.swift
//  Heloo2017
//
//  Created by Константин on 29/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit



@IBDesignable class UsernameLabel: UILabel {
    
    
    private var dotRadius: CGFloat = 3 {
        didSet {
            updateDot()
        }
    }
    
    @IBInspectable var dotColor: UIColor = blueColor
    @IBInspectable var dotAlpha: CGFloat = 0.4

    
    var user: User? {
        didSet {
            if user == nil {
                NotificationCenter.default.removeObserver(self, name: .checkOnlineLabels, object: nil)
            } else {
                NotificationCenter.default.addObserver(self, selector: #selector(self.updateDot), name: .checkOnlineLabels, object: nil)
            }
            
        }
    }
    
    private var isOnline: Bool {
        get {
            return user?.isOnline ?? false
        }
    }
    
    override var frame: CGRect {
        didSet {
            updateDot()
        }
    }
    
    override var text: String? {
        didSet {
            updateDot()
        }
    }
    
    override var attributedText: NSAttributedString? {
        didSet {
            updateDot()
        }
    }
    
    func updateDot() {
        async { [weak self] in
            if let textFrame = self?.textRect(forBounds: self!.bounds, limitedToNumberOfLines: self!.numberOfLines) {
                self?.onlineView.backgroundColor = self!.dotColor
                self?.onlineView.alpha = self!.isOnline ? self!.dotAlpha : 0
                self?.onlineView.frame.origin = CGPoint(x: -2*self!.dotRadius - 4, y: textFrame.midY - self!.dotRadius)
                self?.onlineView.cornerRadius = self!.dotRadius

            }
        }
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        updateDot()
    }
    
    lazy var onlineView: UIView  = {
        let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 3*2, height: 3*2)))
        view.cornerRadius = 3
        view.backgroundColor = blueColor
        view.clipsToBounds = true
        view.alpha = 0
        self.addSubview(view)
        return view
    }()
}


class OnlineTimerManager {
    
    private static let instance = OnlineTimerManager()
    private var timer: Timer!

    
    class func start() {
        instance.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
    }
    
    @objc private class func tick() {
        NotificationCenter.default.post(name: .checkOnlineLabels, object: nil)
    }
    
}


extension NSNotification.Name {
    static let checkOnlineLabels = NSNotification.Name(rawValue: "checkOnlineLabels")
}
