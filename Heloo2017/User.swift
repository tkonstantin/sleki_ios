//
//  User.swift
//  Heloo2017
//
//  Created by Константин on 08.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding {
    
    enum genders {
        case male
        case female
    }
    
    enum fields {
        static let date_of_birth = "date_of_birth"
        static let city = "city"
        static let city_tag = "city_tag"
        static let country = "country"
        static let country_tag = "country_tag"
        static let value = "value"
        static let displayName = "display_name"
        static let firstName = "first_name"
        static let gender = "gender"
        static let image = "image_url"
        static let image_small = "avatar_url"
        static let interests = "interests"
        static let lastName = "last_name"
        static let uuid = "uuid"
        static let subscribers_count = "subscribers_count"
        static let subscriptions_count = "subscriptions_count"
        static let is_subscribed = "is_subscribed"
        static let notificate_on_comments = "notify_for_new_comments"
        static let notificate_on_subscribers = "notify_for_new_subscribers"
        static let last_seen = "last_seen"
    }
    
    var age: Int {
        get {
            guard let birth = self.date_of_birth else { return 0}
            return Int((Date().timeIntervalSince1970 - birth.timeIntervalSince1970)/Double(365*86400))
        }
    }
    
    var age_string: String {
        get {
            switch (age % 100) {
            case 10...20: return "\(age) лет"
            default:
                switch (age % 10) {
                case 1: return "\(age) год"
                case 2,3,4: return "\(age) года"
                default: return "\(age) лет"
                }
            }
        }
    }
    var city: String?
    var city_tag: String?
    var country: String?
    var country_tag: String?
    var displayName = ""
    var firstName = ""
    var gender: genders? = nil
    var image = ""
    var image_small = ""
    var interests: [TreeItem] = []
    var lastName = ""
    var uuid = ""
    var date_of_birth: Date?
    var subscribers_count = 0
    var subscriptions_count = 0
    var is_subscribed = false
    var notificate_on_comments = false
    var notificate_on_subscribers = false
    var last_seen: Date?
    
    var isOnline: Bool {
        get {
            guard let last = last_seen else { return false }
            return Date().timeIntervalSince(last) < 900
        }
    }
    
    init(with json: NSDictionary) {
        super.init()
        
        self.notificate_on_comments = json[fields.notificate_on_comments] as? Bool ?? false
        self.notificate_on_subscribers = json[fields.notificate_on_subscribers] as? Bool ?? false
        
        if let person = json["person"] as? NSDictionary {
            self.parsePerson(person)
        } else {
            self.parsePerson(json)
        }
        
        
    }
    
    init(userInfo _userInfo: [AnyHashable: Any]) {
        
        super.init()
        
        if let subscriberDict = try? JSONSerialization.jsonObject(with: ((_userInfo as NSDictionary)["subscriber"] as? String ?? "").data(using: .utf8) ?? Data(), options: []) as? NSDictionary {
            self.image_small = subscriberDict?["avatar_url"] as? String ?? ""
            self.uuid = subscriberDict?["uuid"] as? String ?? ""
            self.gender = genders.init(subscriberDict?["gender"] as? String)
            self.firstName = subscriberDict?["first_name"] as? String ?? ""
            self.lastName = subscriberDict?["last_name"] as? String ?? ""
            
        } else if let subscriberDict = try? JSONSerialization.jsonObject(with: ((_userInfo as NSDictionary)["person"] as? String ?? "").data(using: .utf8) ?? Data(), options: []) as? NSDictionary {
            self.image_small = subscriberDict?["avatar_url"] as? String ?? ""
            self.uuid = subscriberDict?["uuid"] as? String ?? ""
            self.gender = genders.init(subscriberDict?["gender"] as? String)
            self.firstName = subscriberDict?["first_name"] as? String ?? ""
            self.lastName = subscriberDict?["last_name"] as? String ?? ""
            
        } else if let subscriberDict = _userInfo["person"] as? NSDictionary {
            self.image_small = subscriberDict["avatar_url"] as? String ?? ""
            self.uuid = subscriberDict["uuid"] as? String ?? ""
            self.gender = genders.init(subscriberDict["gender"] as? String)
            self.firstName = subscriberDict["first_name"] as? String ?? ""
            self.lastName = subscriberDict["last_name"] as? String ?? ""
            
        } else if let subscriberDict = _userInfo["person"] as? Array<Any> {
            print("this shit is any array")
            
        } else if let str = _userInfo["person"] as? Array<Any> {
            print("this shit is string")
            
        } else {
            print("_userInfo = \(_userInfo)")
            print("_userInfo[person] = \(_userInfo["person"])")
            
        }
        
    }
    
    
    private func parsePerson(_ json: NSDictionary) {
        if let cityDict = json[fields.city] as? NSDictionary {
            self.city_tag = cityDict[fields.city_tag] as? String
            self.city = cityDict[fields.value] as? String
            if let countryDict = cityDict[fields.country] as? NSDictionary {
                self.country_tag = countryDict[fields.country_tag] as? String
                self.country = countryDict[fields.value] as? String
            }
        }
        if let someValue = json[fields.displayName] as? String {
            self.displayName = someValue
        }
        if let someValue = json[fields.firstName] as? String {
            self.firstName = someValue.uppercasedByFirstLetter()
        }
        if let someValue = json[fields.gender] as? String {
            self.gender = genders(someValue)
        }
        if let someValue = json[fields.image] as? String {
            self.image =  someValue
        }
        if let someValue = json[fields.image_small] as? String {
            self.image_small = someValue
        }
        if let someValue = json[fields.interests] as? [NSDictionary] {
            for value in someValue {
                self.interests.append(TreeItem(with: value))
            }
        }
        interests = interests.makeFlat()
        if let someValue = json[fields.lastName] as? String {
            self.lastName = someValue.uppercasedByFirstLetter()
        }
        if let someValue = json[fields.uuid] as? String {
            self.uuid = someValue
        }
        if let someValue = json[fields.date_of_birth] as? String {
            self.date_of_birth = someValue.stringToDate(format: "dd-MM-yyyy")
        }
        if let someValue = json[fields.subscriptions_count] as? Int {
            self.subscriptions_count = someValue
        }
        if let someValue = json[fields.subscribers_count] as? Int {
            self.subscribers_count = someValue
        }
        self.is_subscribed = json[fields.is_subscribed] as? Bool ?? false
        
        if let milliiseconds = json[fields.last_seen] as? Double ?? Double(json[fields.last_seen] as? String ?? "0") {
            self.last_seen = Date(timeIntervalSince1970: milliiseconds/1000)
        }
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.date_of_birth, forKey: fields.date_of_birth)
        aCoder.encode(self.city, forKey: fields.city)
        aCoder.encode(self.city_tag, forKey: fields.city_tag)
        aCoder.encode(self.country, forKey: fields.country)
        aCoder.encode(self.country_tag, forKey: fields.country_tag)
        aCoder.encode(self.displayName, forKey: fields.displayName)
        aCoder.encode(self.firstName, forKey: fields.firstName)
        aCoder.encode(self.gender?.rawValue ?? "", forKey: fields.gender)
        aCoder.encode(self.image, forKey: fields.image)
        aCoder.encode(self.image_small, forKey: fields.image_small)
        aCoder.encode(self.interests, forKey: "interests_treeItem")
        aCoder.encode(self.lastName, forKey: fields.lastName)
        aCoder.encode(self.uuid, forKey: fields.uuid)
        aCoder.encode(self.subscriptions_count, forKey: fields.subscriptions_count)
        aCoder.encode(self.subscribers_count, forKey: fields.subscribers_count)
        aCoder.encode(self.is_subscribed, forKey: fields.is_subscribed)
        aCoder.encode(self.notificate_on_comments, forKey: fields.notificate_on_comments)
        aCoder.encode(self.notificate_on_subscribers, forKey: fields.notificate_on_subscribers)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.date_of_birth = aDecoder.decodeObject(forKey: fields.date_of_birth) as? Date
        self.city = aDecoder.decodeObject(forKey: fields.city) as? String
        self.city_tag = aDecoder.decodeObject(forKey: fields.city_tag) as? String
        self.country = aDecoder.decodeObject(forKey: fields.country) as? String
        self.country_tag = aDecoder.decodeObject(forKey: fields.country_tag) as? String
        self.displayName = aDecoder.decodeObject(forKey: fields.displayName) as? String ?? ""
        self.firstName = aDecoder.decodeObject(forKey: fields.firstName) as? String ?? ""
        self.gender = genders(aDecoder.decodeObject(forKey: fields.gender) as? String)
        self.image = aDecoder.decodeObject(forKey: fields.image) as? String ?? ""
        self.image_small = aDecoder.decodeObject(forKey: fields.image_small) as? String ?? ""
        self.interests = aDecoder.decodeObject(forKey: "interests_treeItem") as? [TreeItem] ?? []
        self.lastName = aDecoder.decodeObject(forKey: fields.lastName) as? String ?? ""
        self.uuid = aDecoder.decodeObject(forKey: fields.uuid) as? String ?? ""
        self.subscriptions_count = aDecoder.decodeInteger(forKey: fields.subscriptions_count)
        self.subscribers_count = aDecoder.decodeInteger(forKey: fields.subscribers_count)
        self.is_subscribed = aDecoder.decodeBool(forKey: fields.is_subscribed)
        self.notificate_on_comments = aDecoder.decodeBool(forKey: fields.notificate_on_comments)
        self.notificate_on_subscribers = aDecoder.decodeBool(forKey: fields.notificate_on_subscribers)
        
        
    }
    
    
    var mpcDict: [String: String] {
        get {
            var lastNameString = ""
            if let first = self.lastName.first {
                lastNameString = "\(first).".uppercased()
            }
            return [
                "0": self.firstName,
                "1": lastNameString,
                "2": self.uuid,
                "3": self.image_small,
                "5": self.gender?.rawValue ?? "",
                ].mapValues({CryptoInterface.encode($0)})
        }
    }
    
    init?(mpcDict _dict: [String: String]) {
        let dict = _dict.mapValues({ CryptoInterface.decode($0) })
        
        guard let _uuid = dict["2"], !_uuid.isEmpty else {  return nil }
        self.uuid = _uuid
        self.firstName = dict["0"] ?? ""
        self.lastName = dict["1"] ?? ""
        self.image_small = dict["3"] ?? ""
        self.gender = genders(dict["5"] ?? "")
        self.last_seen = Date()
    }
}

extension User.genders {
    init?(_ rawValue: String!) {
        guard rawValue != nil, !rawValue.isEmpty else { return nil }
        switch rawValue {
        case "M": self = .male
        default: self = .female
        }
    }
    
    var rawValue: String {
        get {
            return self == .male ? "M" : "F"
        }
    }
    
    var title: String {
        return self == .male ? "Мужской" : "Женский"
    }
}
