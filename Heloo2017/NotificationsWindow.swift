//
//  NotificationsWindow.swift
//  Heloo2017
//
//  Created by Константин on 19.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


class NotificationsWindow: UIWindow {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if let notificationView = NotificationsUICoordinator.currentNotificationController?.notificationsView {
            return notificationView.frame.contains(point)
        }
        
        return false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


