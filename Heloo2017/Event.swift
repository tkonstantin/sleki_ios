//
//  Event.swift
//  Heloo2017
//
//  Created by Константин on 29.04.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class Event: NSObject {
    
    
    enum eventKeys {
        static let id = "uuid"
        static let title = "title"
        static let startDate = "start_date"
        static let startTime = "start_time"
        static let endDate = "end_date"
        static let endTime = "end_time"
        static let artwork = "image"
        static let description_title = "description_title"
        static let description_text = "description_body"
        static let likes_count = "likes_count"
        static let comments_count = "comments_count"
        static let isLiked = "is_liked"
        static let isAccepted = "is_accepted"
        static let place = "place"
        static let weekActivityItems = "weekActivityItems"
    }

    var id = ""
    var title = ""
    var start: Date?
    var end: Date?
    var artwork: ImageObject?
    var description_title = ""
    var description_text = ""
    var likes_count = 0
    var comments_count = 0
    var isLiked = false
    var isAccepted = false
    var place: Place?
    var weekActivityItems = WeekActivityItems(json: [:])
    
    var startTimeString: String?
    var endTimeString: String?

    private var dateFormatter = DateFormatter()

    

    init(with json: NSDictionary, place _place: Place? = nil) {
        if let someValue = json[eventKeys.id] as? String {
            self.id = someValue
        }
        if let someValue = json[eventKeys.title] as? String {
            self.title = someValue.uppercasedByFirstLetter().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let someValue = json[eventKeys.startDate] as? String {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.start = dateFormatter.date(from: someValue)
        }
        if let someValue = json[eventKeys.startTime] as? String {
            dateFormatter.dateFormat = "HH:mm:ss"
            if let timeDate = dateFormatter.date(from: someValue) {
                self.start = self.start?.setTimeFromDate(timeDate)
                self.startTimeString = timeDate.parseTime()
            } else {
                dateFormatter.dateFormat = "HH:mm"
                if let timeDate = dateFormatter.date(from: someValue) {
                    self.start = self.start?.setTimeFromDate(timeDate)
                    self.startTimeString = timeDate.parseTime()
                }
            }
        }
        if let someValue = json[eventKeys.endDate] as? String {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.end = dateFormatter.date(from: someValue)
        }
        if let someValue = json[eventKeys.endTime] as? String {
            dateFormatter.dateFormat = "HH:mm:ss"
            if let timeDate = dateFormatter.date(from: someValue) {
                self.end = self.end?.setTimeFromDate(timeDate)
                self.endTimeString = timeDate.parseTime()

            } else {
                dateFormatter.dateFormat = "HH:mm"
                if let timeDate = dateFormatter.date(from: someValue) {
                    self.end = self.end?.setTimeFromDate(timeDate)
                    self.endTimeString = timeDate.parseTime()
                }
            }
        }
        if let someValue = json[eventKeys.artwork] as? NSDictionary {
            self.artwork = ImageObject(with: someValue)
        }
        if let someValue = json[eventKeys.description_title] as? String {
            self.description_title = someValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let someValue = json[eventKeys.description_text] as? String {
            self.description_text = someValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if let someValue = json[eventKeys.likes_count] as? Int {
            self.likes_count = someValue
        }
        if let someValue = json[eventKeys.comments_count] as? Int {
            self.comments_count = someValue
        }
        if let someValue = json[eventKeys.isLiked] as? Bool {
            self.isLiked = someValue
        }
        if let someValue = json[eventKeys.isAccepted] as? Bool {
            self.isAccepted = someValue
        }
        if let someValue = json[eventKeys.place] as? NSDictionary {
            self.place = Place(with: someValue)
        } else {
            self.place = _place
        }
        weekActivityItems = WeekActivityItems(json: json)

    }
    
}

