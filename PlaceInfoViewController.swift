//
//  PlaceInfoViewController.swift
//  Heloo2017
//
//  Created by Константин on 24.03.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SDWebImage

class PlaceInfoViewController: PlaceEventsViewController {
  

    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
    func back() {
        self.navigationController?.popToViewController(self.navigationController!.viewControllers.dropLast().last!, animated: true)
    }
    
    
    
    var photosSectionNumber: Int? {
        get {
            return self.place.photos.count == 0 ? nil : 0
        }
    }
    
    var aboutSectionNumber: Int {
        get {
            return (photosSectionNumber ?? -1) + 1
        }
    }
    
    var addressSectionNumber: Int? {
        get {
            return place.address_string == "Адрес не указан" ? nil : (aboutSectionNumber + 1)
        }
    }
    
    var subwaySectionNumber: Int? {
        get {
            return place.subwayString.isEmpty ? nil : (addressSectionNumber ?? aboutSectionNumber) + 1
        }
    }
    
    var hoursSectionNumber: Int? {
        get {
            return place.openingHoursString.count == 0 ? nil : ((subwaySectionNumber ?? addressSectionNumber ?? aboutSectionNumber) + 1)
        }
    }
    
    var contactsSectionNumber: Int {
        get {
            return (hoursSectionNumber ?? subwaySectionNumber ?? addressSectionNumber ?? aboutSectionNumber) + 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 + (place.photos.count == 0 ? 0 : 1) + (place.address_string == "Адрес не указан" ? 0 : 1) + (place.subwayString.isEmpty ? 0 : 1) + (place.openingHoursString.count == 0 ? 0 : 1) + min(place.contacts.count, 1)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == hoursSectionNumber {
            return place.openingHoursString.count + 1
        } else if section == contactsSectionNumber {
            return 1 + place.contacts.count
        }
        
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
            
        case photosSectionNumber:
            let cell = tableView.dequeueReusableCell(withIdentifier: "photosCell") as! ProfilePhotosTableViewCell
            cell.update(with: place.photos, delegate: self)
            return cell
            
        case aboutSectionNumber:
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell") as! ProfileAboutTableViewCell
            cell.upate(with: place)
            return cell
            
        case addressSectionNumber:
            let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell") as! ProfileAddressTableViewCell
            cell.update(with: place)
            return cell
            
        case subwaySectionNumber:
            let cell = tableView.dequeueReusableCell(withIdentifier: "subwayCell") as! ProfileSubwayTableViewCell
            cell.update(with: place)
            return cell
            
        case hoursSectionNumber:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! ProfileHeaderTableViewCell
                cell.update(with: "Часы работы")
                return cell
            } else {
                let shouldCorner = indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "rowCell") as! ProfileRowTableViewCell
                let key = Array(place.openingHoursString.keys).sorted()[indexPath.row - 1]
                let currentLeft = place.openingHoursString[key]?.keys.first ?? ""
                let currentRight = place.openingHoursString[key]?.values.first ?? ""
                cell.update(left: currentLeft, right: currentRight, cornersBottom: shouldCorner)
                return cell
            }
            
        case contactsSectionNumber:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! ProfileHeaderTableViewCell
                cell.update(with: "Контакты")
                return cell
            } else {
                let shouldCorner = indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "rowCell") as! ProfileRowTableViewCell
                let currentContactKey = Array(place.contacts.keys)[indexPath.row - 1]
                let currentContactValue = place.contacts[currentContactKey]
                cell.update(left: currentContactKey, right: currentContactValue, cornersBottom: shouldCorner)
                return cell
            }
            
        default:
            return UITableViewCell()
        }
    }
    
    
    
   
}


extension PlaceInfoViewController: PhotosCellDelegate {
    
    func shouldOpenPhoto(_ link: String?) {
        guard link != nil else { return }
        PhotoViewerViewController.present(with: [link!], from: self, shouldPushIfPossible: false)
    }
}




